//
//  Model.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
// Update Model to provide for uniquely identifying Notes and Groups by UUIDs. //leg20220429 - TopXNotes_Catalyst_2
//
// Added support for HTMl versions of the note data. New notepad version = 6,   //leg20211011 - TopXNotes_Catalyst_2
//  new Note field = noteHTML.
//
// Copied from TopXNotes_Catalyst_3.                                            //leg20210917 - TopXNotes_Catalyst_2
//
//leg20200310 - Catalyst - 1.0.0 - Increased notePadVersion to 5 (kNotePadVersionCurrent).
//leg20200309 - Catalyst - 1.0.0 - Added notePadFileName to support SYNC process.
//leg20200219 - Catalyst - 1.0.0 - Added nextNoteID to support SYNC process.
//leg20200219 - Catalyst - 1.0.0 - Implement getNoteForNoteID to support SYNC process.
//leg20200219 - Catalyst - 1.0.0 - Implement getIndexOfNoteWithNoteID to support SYNC process.
//leg20200324 - Catalyst - 1.0.0 - Implement getNextNoteID to support SYNC process.
//
//leg20120911 - 1.7.5 - Added syncNextNewNoteID to Model.
//leg20120207 - 1.7.5 - Added notePadSyncDate to Model.
//leg20150318 - 1.5.0 - iOS - Fix for sort by title and date no longer working
//                      with iOS 8.2.  Changed sortUsingFunction comparison
//                      function proto type from static int sortByDate(Note… to
//                      NSComparisonResult sortByDate(Note…
//leg20130205 - 1.3.0 - Added: - (BOOL)hasEncryptedNotes;
//
//leg20121223 - 1.3.0 - Increased notepad version (kNotePadVersionCurrent) to 4.
//
//leg20121022 - 1.2.2 - Added - (int)sortNotesByTitle:(BOOL)ascending;
//
//leg20120911 - 1.7.5 - #ifdef NOTEPAD_MODEL_PATH_IS_MAC added to control the path
//                      to the Model whether on Mac or iOS device because Apple
//                      requires us to use the Application Support directory when
//                      running on MacOS and the Documents directory is still used
//                      on iOS.
//
//*leg - 2010-05-01 - Re-implemented Model so that other data besides noteList can be part of it.
//						NotePad object now contains instances of noteList and additional data items.
//						Model object now contains one instance of NotePad object.  Changed loadData
//						and saveData so that individual objects are key archived/unarchived.
//
//

// Constants
#define kNotePadVersion0		-1                                              //leg20120207 - 1.7.5
#define kNotePadVersion1		1                                               //leg20120207 - 1.7.5
#define kNotePadVersion2		2                                               //leg20120207 - 1.7.5
#define kNotePadVersion3        3                                               //leg20120207 - 1.7.5
#define kNotePadVersion4        4                                               //leg20121223 - 1.3.0
#define kNotePadVersion5        5                                               //leg20200310 - Catalyst
#define kNotePadVersion6        6                                               //leg20211011 - TopXNotes_Catalyst_2
#define kNotePadVersion7        7                                               //leg20220427 - TopXNotes_Catalyst_2
#define kNotePadVersionCurrent	kNotePadVersion7                                //leg20220427 - TopXNotes_Catalyst_2

// Keys to note pad archive data
#define KEY_NotePadFileName     @"KEY_NotePadFileName"                          //leg20200309 - Catalyst
#define KEY_NotePadVersion      @"KEY_NotePadVersion"                           //leg20120809 - 1.7.5
#define KEY_SyncNextNewNoteID   @"KEY_SyncNextNewNoteID"                        //leg20120911 - 1.7.5
#define KEY_NextNoteID	        @"KEY_NextNoteID"                               //leg20200219 - Catalyst
#define KEY_NotePadSyncDate		@"KEY_NotePadSyncDate"                          //leg20120207 - 1.7.5
#define KEY_DeviceUDID			@"KEY_DeviceUDID"
#define KEY_RealDeviceUDID		@"KEY_RealDeviceUDID"                           //leg20120403 - 1.7.3
#define KEY_ProductCode			@"KEY_ProductCode"
#define KEY_NoteList            @"KEY_NoteList"
#define KEY_GroupList           @"KEY_GroupList"                                //leg20220427 - TopXNotes_Catalyst_2
#define KEY_DisplayList         @"KEY_DisplayList"                              //leg20220629 - TopXNotes_Catalyst_2
#define KEY_CloudKitSync	    @"KEY_CloudKitSync"                             //leg20220907 - TopXNotes_Catalyst_2

@class Model;
@class Note;
@class Group;                                                                   //leg20220505 - TopXNotes_Catalyst_2

// Structure to contain Group tree traversal data.                              //leg20220517 - TopXNotes_Catalyst_2
typedef struct {
    NSInteger       depthOfCell;
    NSObject        *itemForCell;
} NotepadCellData;

@interface NotePad : NSObject {
    NSMutableString*    notePadFileName;    // NotePadVersion5                  //leg20200309 - Catalyst
    NSNumber*           notePadVersion;                                         //leg20120809 - 1.7.5
    NSNumber*           notePadVersionAtOpen;                                   //leg20220817 - TopXNotes_Catalyst_2
    NSNumber*           syncNextNewNoteID;                                      //leg20120911 - 1.7.5
    NSNumber*           nextNoteID;         // NotePadVersion5                  //leg20200219 - Catalyst
    NSNumber*           cloudKitSync;                                           //leg20220907 - TopXNotes_Catalyst_2
    NSDate*             notePadSyncDate;                                        //leg20120207 - 1.7.5
	NSMutableString*	deviceUDID;
	NSMutableString*	realDeviceUDID;                                         //leg20120403 - 1.7.3
	NSMutableString*	productCode;
    NSMutableArray*     noteList;
	NSMutableArray*		groupList;                                              //leg20220427 - TopXNotes_Catalyst_2
    NSMutableArray*     displayList;                                            //leg20220517 - TopXNotes_Catalyst_2
}

@property (nonatomic, strong) NSMutableString* notePadFileName;                 //leg20200309 - Catalyst
@property (nonatomic, strong) NSNumber* notePadVersion;                         //leg20120911 - 1.7.5
@property (nonatomic, strong) NSNumber* notePadVersionAtOpen;                   //leg20220817 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSNumber* syncNextNewNoteID;                      //leg20120809 - 1.7.5
@property (nonatomic, strong) NSNumber* nextNoteID;                             //leg20200219 - Catalyst
@property (nonatomic, strong) NSNumber* cloudKitSync;                           //leg20220907 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSDate* notePadSyncDate;                          //leg20120207 - 1.7.5
@property (nonatomic, strong) NSMutableString* deviceUDID;
@property (nonatomic, strong) NSMutableString* realDeviceUDID;                  //leg20120403 - 1.7.3
@property (nonatomic, strong) NSMutableString* productCode;
@property (nonatomic, strong) NSMutableArray* noteList;
@property (nonatomic, strong) NSMutableArray* groupList;                        //leg20220427 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSMutableArray* displayList;                      //leg20220517 - TopXNotes_Catalyst_2

@end

@interface Model : NSObject {
	NotePad* notePad;
    
    bool  NotePad_Version_Changed;

    NSURL *appGroupSubDirectoryURL;                                             //leg20211202 - TopXNotes_Catalyst_2
}

- (void)setFileName:(NSString*)fileName;                                        //leg20200309 - Catalyst
- (NSString*)fileName;                                                          //leg20200309 - Catalyst

- (void)setDeviceUDID:(NSString*)deviceUDID;
- (NSString*)deviceUDID;

- (void)setRealDeviceUDID:(NSString*)deviceUDID;                                //leg20120403 - 1.7.3
- (NSString*)realDeviceUDID;                                                    //leg20120403 - 1.7.3

- (void)setDeviceLastSyncDate:(NSDate*)deviceSyncDate;                          //leg20120207 - 1.7.5
- (NSDate*)deviceLastSyncDate;                                                  //leg20120207 - 1.7.5

- (void)setSyncNextNewNoteID:(NSNumber*)deviceSyncDate;                         //leg20120911 - 1.7.5
- (NSNumber*)syncNextNewNoteID;                                                 //leg20120911 - 1.7.5

- (void)setNextNoteID:(NSNumber*)noteID;                                        //leg20200219 - Catalyst
- (NSNumber*)nextNoteID;                                                        //leg20200219 - Catalyst

- (void)setCloudKitSync:(NSNumber*)state;                                       //leg20220907 - TopXNotes_Catalyst_2
- (NSNumber*)cloudKitSync;                                                      //leg20220907 - TopXNotes_Catalyst_2

- (NSMutableArray*)noteList;                                                    //leg20220523 - TopXNotes_Catalyst_2
- (NSMutableArray*)groupList;                                                   //leg20220523 - TopXNotes_Catalyst_2
- (NSMutableArray*)displayList;                                                 //leg20220519 - TopXNotes_Catalyst_2

- (NSNumber*)notePadVersion;                                                    //leg20120207 - 1.7.5
- (NSNumber*)notePadVersionAtOpen;                                              //leg20220817 - TopXNotes_Catalyst_2

// MARK: Note methods.

- (NSInteger)numberOfNotes;                                                     //leg20150318 - 1.5.0
- (void)addNote:(Note*)note;
- (void)replaceNoteAtIndex:(NSUInteger)index withNote:(Note*)note;              //leg20121127 - 1.2.2
- (void)updateNoteAtIndex:(NSUInteger)index withNote:(Note*)note;               //leg20121204 - 1.3.0
//- (NSUInteger)sortNotesByDate:(BOOL)ascending;                                  //leg20150318 - 1.5.0
//- (NSUInteger)sortNotesByTitle:(BOOL)ascending;                                 //leg20150318 - 1.5.0
- (NSUInteger)sortNodeContentsByDate:(BOOL)ascending;                           //leg20220601 - TopXNotes_Catalyst_2
- (NSUInteger)sortNodeContentsByLineage:(BOOL)ascending;                        //leg20220531 - TopXNotes_Catalyst_2
- (NSUInteger)sortNodesByLineage:(BOOL)ascending;                               //leg20220705 - TopXNotes_Catalyst_2
- (NSNumber*)getNextNoteID;                                                     //leg20200324 - Catalyst
- (Note*)getNoteForIndex:(NSInteger)index;
- (Note*)getNoteForNoteID:(NSNumber*)noteID;                                    //leg20200219 - Catalyst
- (NSUInteger)getIndexOfNoteWithNoteID:(NSNumber*)noteID;                       //leg20200219 - Catalyst
- (void)removeNoteAtIndex:(NSInteger)index;
- (void)removeAllNotes;                                                         //leg20120303 - 1.7.5
- (void)removeAllUnprotectedNotes;                                              //leg20130918 - 1.8.0
- (BOOL)hasEncryptedNotes;                                                      //leg20200219 - Catalyst
- (NSUUID *)getUUIDOfNoteAtIndex:(NSInteger)index;                              //leg20220513 - TopXNotes_Catalyst_2
- (Note *)getNoteForUUID:(NSUUID*)noteUUID;                                     //leg20220514 - TopXNotes_Catalyst_2
- (NSInteger)getIndexOfNoteForUUID:(NSUUID*)noteUUID;                           //leg20220516 - TopXNotes_Catalyst_2

- (id)init:(NSString*)fileName; // Initializer for non-default file name.       //leg20200305 - Catalyst

// MARK: Group methods.

- (NSInteger) numberOfGroups;                                                   //leg20220517 - TopXNotes_Catalyst_2
- (void)addGroup:(Group*) group;                                                //leg20220509 - TopXNotes_Catalyst_2
- (void)addNote:(Note*)note toGroup:(NSUUID *)groupUUID;                        //leg20220527 - TopXNotes_Catalyst_2
- (void)removeNote:(Note *)note fromGroup:(NSUUID *)groupUUID;                  //leg20220602 - TopXNotes_Catalyst_2
- (void)removeGroup:(Group *)group fromGroup:(NSUUID *)groupUUID;               //leg20220609 - TopXNotes_Catalyst_2
- (void)replaceGroupAtIndex:(NSUInteger)index withGroup:(Group*)group;          //leg20220525 - TopXNotes_Catalyst_2
- (NSUUID *)getRootGroupUUID;                                                   //leg20220526 - TopXNotes_Catalyst_2
- (Group *)getGroupForIndex:(NSInteger)index;                                   //leg20220509 - TopXNotes_Catalyst_2
- (Group *)getGroupForUUID:(NSUUID*)groupID;                                    //leg20220509 - TopXNotes_Catalyst_2
- (NSUUID *)getUUIDOfGroupAtIndex:(NSInteger)index;                             //leg20220509 - TopXNotes_Catalyst_2
- (Group *)getGroupNamed:(NSString*)name withinGroup:(NSUUID*)groupUUID;        //leg20220618 - TopXNotes_Catalyst_2
- (NSInteger)getIndexOfGroupForUUID:(NSUUID*)groupUUID;                         //leg20220516 - TopXNotes_Catalyst_2
- (BOOL)isMember:(NSUUID*)memberUUID ofGroup:(NSUUID*)groupUUID;                //leg20220606 - TopXNotes_Catalyst_2
- (void)removeGroupAtIndex:(NSInteger)index;                                    //leg20220606 - TopXNotes_Catalyst_2
                    
// MARK: Tree manipulation methods.

- (BOOL)isNodeNote:(NSUUID*)node;                                               //leg20220531 - TopXNotes_Catalyst_2
- (BOOL)isNodeGroup:(NSUUID*)node;                                              //leg20220617 - TopXNotes_Catalyst_2
- (BOOL)moveNoteOrGroup:(NSUUID *)itemUUID                                      //leg20220513 - TopXNotes_Catalyst_2
              fromGroup:(NSUUID *)from
                toGroup:(NSUUID *)to;

- (void) getChildrenOfGroup:(NSUUID *)groupUUID                                 //leg20220515 - TopXNotes_Catalyst_2
                     parent:(NSUUID *)parentUUID
                  fromDepth:(NSInteger)depth
                       base:(BOOL)isFirstGroup
                   intoList:(NSMutableArray *)list;

- (NSArray*)getLineageOfNode:(NSUUID *)nodeUUID;                                //leg20220523 - TopXNotes_Catalyst_2
- (NSString *)getLineageOfNodeAsString:(NSUUID *)nodeUUID;                      //leg20220530 - TopXNotes_Catalyst_2
- (void)collectChildrenOf:(NSUUID *)node into:(NSMutableArray *)nodes;          //leg20220528 - TopXNotes_Catalyst_2
- (BOOL)isNodeWithinCollapsedAncestor:(NSUUID *)nodeUUID;                       //leg20220523 - TopXNotes_Catalyst_2
- (Group*)getDisplayGroupForIndex:(NSInteger)index;                             //leg20220524 - TopXNotes_Catalyst_2
- (id)getDisplayItemForIndex:(NSInteger)index;                                  //leg20220610 - TopXNotes_Catalyst_2
- (void)addDisplayNote:(Note*) note;                                            //leg20220526 - TopXNotes_Catalyst_2
- (void)addDisplayGroup:(Group*)group;                                          //leg20220527 - TopXNotes_Catalyst_2
- (void)replaceDisplayGroupAtIndex:(NSUInteger)index withGroup:(Group*)group;   //leg20220525 - TopXNotes_Catalyst_2
- (void)replaceDisplayNoteAtIndex:(NSUInteger)index withNote:(Note*)note;       //leg20220525 - TopXNotes_Catalyst_2
- (void)removeDisplayItemAtIndex:(NSInteger)index;                              //leg20220602 - TopXNotes_Catalyst_2
- (void)moveDisplayItemAtIndex:(NSInteger)from toIndex:(NSInteger)to;           //leg20220606 - TopXNotes_Catalyst_2
- (NSInteger)getIndexOfDisplayItem:(NSUUID*)itemUUID;                           //leg20220606 - TopXNotes_Catalyst_2
- (void)deleteGroupContents:(Group*)group                                       //leg20220614 - TopXNotes_Catalyst_2
               andSaveIndex:(NSMutableArray *)indexArray;
- (Note *)duplicateNote:(Note*)note;                                            //leg20220615 - TopXNotes_Catalyst_2

    // MARK: Notepad methods

- (NSString*)pathToData;
+ (NSString*)pathToData:(NSString*)fileName;                                    //leg20200309 - Catalyst

- (BOOL)saveData;

- (BOOL)loadData;

- (BOOL)syncCloudData:(BOOL)canUpload;                                          //leg20220912 - TopXNotes_Catalyst_2

+ (NSUUID *) genGroupUUID;                                                      //leg20220519 - TopXNotes_Catalyst_2

+ (NSUUID *) genNoteUUID;                                                       //leg20220519 - TopXNotes_Catalyst_2

@property (nonatomic, strong) NSURL* appGroupSubDirectoryURL;                   //leg20211202 - TopXNotes_Catalyst_2

@end
