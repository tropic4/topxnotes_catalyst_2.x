//
//  Note.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
// Add noteUUID and groupUUID properities.                                      //leg20220429 - TopXNotes_Catalyst_2

// Added support for HTMl versions of the note data. New notepad version = 6,   //leg20211011 - TopXNotes_Catalyst_2
//  new Note field = noteHTML.
//
// Copied from TopXNotes_Catalyst_3.                                            //leg20210917 - TopXNotes_Catalyst_2
//
//leg20120911 - 1.7.5 - Added createDate to Note.
//leg20121120 - 1.2.2 - Added encryption/decryption fields.
//
// Added to emulate Mac NotePad sync control fields:                            //leg20200214 - Catalyst
//  @synthesize npSync;                                                         //leg20200214 - Catalyst
//  @synthesize nfChangedSync;                                                  //leg20200214 - Catalyst
//

@interface Note : NSObject <NSCoding> {
	NSString*	title;
	NSDate*		createDate;                                                     //leg20120911 - 1.7.5
	NSDate*		date;
    NSString*   noteText;
	NSString*	noteHTML;                                                       //leg20211011 - TopXNotes_Catalyst_2
    NSData*     encryptedNoteText;                                              //leg20121120 - 1.2.2
	NSData*     encryptedNoteHTML;                                              //leg20211129 - TopXNotes_Catalyst_2
	NSData*     encryptedPassword;                                              //leg20121120 - 1.2.2
	NSString*   passwordHint;                                                   //leg20121204 - 1.3.0
	NSString*   decryptedPassword;                                              //leg20121204 - 1.3.0
	NSNumber*	noteID;
    NSUUID*     noteUUID;                                                       //leg20220429 - TopXNotes_Catalyst_2
    NSNumber*   groupID;
	NSUUID*	    groupUUID;                                                      //leg20220429 - TopXNotes_Catalyst_2
    NSInteger   depth;                                                          //leg20220518 - TopXNotes_Catalyst_2
	NSNumber*	syncFlag;
	NSNumber*	needsSyncFlag;
	NSNumber*	protectedNoteFlag;                                              //leg20121120 - 1.2.2
	NSNumber*	decryptedNoteFlag;                                              //leg20121204 - 1.3.0
    
    // Added to emulate Mac NotePad sync control fields.                        //leg20200214 - Catalyst
    NSMutableArray* npSync;                                                     //leg20200326 - Catalyst
    NSNumber*       nfChangedSync;                                              //leg20200214 - Catalyst
}

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSDate* createDate;                               //leg20120911 - 1.7.5
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSString* noteText;
@property (nonatomic, strong) NSString* noteHTML;                               //leg20211011 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSData* encryptedNoteText;                        //leg20121120 - 1.2.2
@property (nonatomic, strong) NSData* encryptedNoteHTML;                        //leg20211129 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSData* encryptedPassword;                        //leg20121120 - 1.2.2
@property (nonatomic, strong) NSString* passwordHint;                           //leg20121204 - 1.3.0
@property (nonatomic, strong) NSString* decryptedPassword;                      //leg20121204 - 1.3.0
@property (nonatomic, strong) NSNumber* noteID;
@property (nonatomic, strong) NSUUID* noteUUID;                                 //leg20220429 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSNumber* groupID;
@property (nonatomic) NSInteger depth;                                          //leg20220518 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSUUID* groupUUID;                                //leg20220429 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSNumber* syncFlag;
@property (nonatomic, strong) NSNumber* needsSyncFlag;
@property (nonatomic, strong) NSNumber* protectedNoteFlag;                      //leg20121120 - 1.2.2
@property (nonatomic, strong) NSNumber* decryptedNoteFlag;                      //leg20121204 - 1.3.0

@property (nonatomic, strong) NSMutableArray* npSync;                           //leg20200326 - Catalyst
@property (nonatomic, strong) NSNumber* nfChangedSync;                          //leg20200214 - Catalyst

- (id) initWithNoteTitle:(NSString*) noteTitle                                  //leg20200310 - Catalyst
				text:(NSString*) textField 
				  withID:(NSNumber*) noteID
				  onDate:(NSDate*) entryDate;


- (id) initWithNoteTitle:(NSString*) noteTitle                                  //leg20200310 - Catalyst
                text:(NSString*) textField
              withID:(NSNumber*)noteIDField
              onDate:(NSDate*) entryDate
          createDate:(NSDate*) creationDate;

// New initializer to support Groups.                                           //leg20220519 - TopXNotes_Catalyst_2
- (id) initWithNoteTitle:(NSString*) title
                text:(NSString*) text
                  withID:(NSNumber*) noteID
             andWithUUID:(NSUUID*) noteUUID
             withinGroup:(NSUUID*) noteGroupUUID
                  onDate:(NSDate*) entryDate;
    
// New initializer to support Groups.                                           //leg20220519 - TopXNotes_Catalyst_2
- (id) initWithNoteTitle:(NSString*) noteTitle                                  
                text:(NSString*) title
              withID:(NSNumber*) noteID
         andWithUUID:(NSUUID*) noteUUID
         withinGroup:(NSUUID*) noteGroupUUID
              onDate:(NSDate*) entryDate
          createDate:(NSDate*) creationDate;

@end
