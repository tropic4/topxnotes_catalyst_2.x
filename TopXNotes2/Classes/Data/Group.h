//
//  Group.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/27/2022.
//  Copyright 2022 Tropical Software. All rights reserved.
//
// Changes:
//
// Group model.
//
//

@interface Group : NSObject <NSCoding> {
	NSString*	    title;
	NSDate*		    createDate;
	NSDate*		    date;
    NSUUID*         groupUUID;
    NSUUID*         parentGroupUUID;
	NSInteger	    depth;                                                      //leg20220518 - TopXNotes_Catalyst_2
    NSNumber*       expanded;
    NSMutableArray* contents;
}

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSDate* createDate;
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSUUID* groupUUID;
@property (nonatomic, strong) NSUUID* parentGroupUUID;
@property (nonatomic) NSInteger depth;                                          //leg20220518 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSNumber* expanded;
@property (nonatomic, strong) NSMutableArray* contents;

- (id) initWithGroupTitle:(NSString*) groupTitle
				parent:(NSUUID*) parentUUID;
- (id) initRootGroup;
@end
