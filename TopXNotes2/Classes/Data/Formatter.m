//
//  Formatter.m
//  GasTracker
//
//  Created by Rich Warren on 11/10/08.
//  Copyright 2008 Freelance Mad Science. All rights reserved.
//

#import "Formatter.h"


@implementation NSString (Formatter) 

+ (NSString*)shortDate:(NSDate*)date {
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];	// yields May 8, 2009
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	NSString *dateString = [dateFormatter stringFromDate:date];
	
	return dateString;
}

+ (NSString*)longDate:(NSDate*)date{
	
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *dateString;

    //	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//	//[dateFormatter setDateStyle:NSDateFormatterLongStyle];	// yields March 8, 2009
//	//[dateFormatter setDateStyle:NSDateFormatterShortStyle];	// yields 3/8/09
//	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];	// yields Mar 8, 2009
//	//[dateFormatter setDateStyle:kCFDateFormatterFullStyle];		// yields Friday, May 8, 2009
//	//[dateFormatter setTimeStyle:NSDateFormatterNoStyle];			// yields no time
//	[dateFormatter setTimeStyle:kCFDateFormatterShortStyle];		// yields May 8, 2009 2:05 PM
// NSDateFormatterMediumStyle and kCFDateFormatterShortStyle yields: May 27, 2021 at 11:41 AM
//
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];     // yields May 27, 2021 at 12:30 PM
//
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];        // yields May 27, 2021
//
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];    // yields May 27, 2021 at 12:30:06 PM
//

// Defer relative dates until a future release notepad setting.                 //leg20210728 - TopXNotes2
    // On iOS 13 or greater display date in relative format.                    //leg20210528 - TopXNotes2
//    if (@available(iOS 13.0, *)) {
    BOOL showRelativeDate = NO;
    if (showRelativeDate) {
        [dateFormatter setDateStyle:NSDateFormatterNoStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle]; // yields 12:30 PM
        dateString = [dateFormatter stringFromDate:date];
        
        NSRelativeDateTimeFormatter *relativeDateFormatter = [[NSRelativeDateTimeFormatter alloc] init];
        relativeDateFormatter.dateTimeStyle = NSRelativeDateTimeFormatterStyleNamed;
        relativeDateFormatter.unitsStyle = NSRelativeDateTimeFormatterUnitsStyleShort;
        NSString *relativeDate = [relativeDateFormatter localizedStringForDate:date
                              relativeToDate:[NSDate date]];
        dateString = [NSString stringWithFormat:@"%@ %@", relativeDate, dateString];
    } else {
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        
        // yields: May 27, 2021
        dateString = [dateFormatter stringFromDate:date];
        
        [dateFormatter setDateStyle:NSDateFormatterNoStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        
        // yields: 12:30 PM
        NSString *timeString = [dateFormatter stringFromDate:date];
        
        // yields: May 27, 2021 at 12:30 PM
        dateString = [NSString stringWithFormat:@"%@ %@", dateString, timeString];
    }
    
    return dateString;
}

+ (NSString*)decimal:(double)value{
	
	NSNumber *number = [NSNumber numberWithDouble:value];
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSString *decimalString = [formatter stringFromNumber:number];
	
	return decimalString;
}

+ (NSString*)currency:(double)value {
	
	NSNumber *number = [NSNumber numberWithDouble:value];
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	
	NSString *currencyString = [formatter stringFromNumber:number];
	
	return currencyString;
}

+ (double) parseDecimal:(NSString*)string {
	NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	NSNumber* value;
	NSString* error;
	
	[formatter getObjectValue: &value forString:string errorDescription:&error];
	
	return [value doubleValue];
}

+ (double) parseCurrency:(NSString*)string {
	NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	
	NSNumber* value;
	NSString* error;
	
	[formatter getObjectValue: &value forString:string errorDescription:&error];
	
	return [value doubleValue];
}

@end
