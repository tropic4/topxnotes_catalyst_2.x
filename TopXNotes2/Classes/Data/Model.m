//
//  Model.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes: see Model.h
//
// Update Model to add notePadVersionAtOpen.                                    //leg20220817 - TopXNotes_Catalyst_2
//
// Update Model to provide for uniquely identifying Notes and Groups by UUIDs.  //leg20220429 - TopXNotes_Catalyst_2
//
// Added support for HTMl versions of the note data. New notepad version = 6,   //leg20211011 - TopXNotes_Catalyst_2
//  new Note field = noteHTML.
//
// Copied from TopXNotes_Catalyst_3.                                            //leg20210917 - TopXNotes_Catalyst_2
//

#import "Model.h"
#import "Note.h"
#import "Group.h"                                                               //leg20220427 - TopXNotes_Catalyst_2
#import "Constants.h"
#import "SyncConstants.h"                                                       //leg20200219 - Catalyst
#import <CloudKit/CloudKit.h>                                                   //leg20220906 - TopXNotes_Catalyst_2
#import "CloudKitManager.h"                                                     //leg20220906 - TopXNotes_Catalyst_2

NSComparisonResult sortByDate(Note *note1, Note *note2, void *context);         //leg20150318 - 1.5.0
NSComparisonResult sortByTitle(Note *note1, Note *note2, void *context);        //leg20150318 - 1.5.0

@implementation NotePad

@synthesize notePadVersion, deviceUDID, realDeviceUDID, productCode, noteList, notePadSyncDate, syncNextNewNoteID, nextNoteID, notePadFileName;  //leg20200309 - Catalyst
@synthesize notePadVersionAtOpen;                                               //leg20220817 - TopXNotes_Catalyst_2
@synthesize groupList, displayList;                                             //leg20220427 - TopXNotes_Catalyst_2
@synthesize cloudKitSync;                                                       //leg20220907 - TopXNotes_Catalyst_2

- (id)init {
	
	if (self = [super init]) {
        notePadFileName = [[NSMutableString alloc] init];                       //leg20200309 - Catalyst
        notePadVersion = [[NSNumber alloc] init];
        notePadVersionAtOpen = [[NSNumber alloc] init];                         //leg20220817 - TopXNotes_Catalyst_2
		deviceUDID = [[NSMutableString alloc] init];
		realDeviceUDID = [[NSMutableString alloc] init];                        //leg20120403 - 1.7.3
		productCode = [[NSMutableString alloc] init];
        noteList = [[NSMutableArray alloc] init];
		groupList = [[NSMutableArray alloc] init];                              //leg20220509 - TopXNotes_Catalyst_2
        displayList = [[NSMutableArray alloc] init];                            //leg20220509 - TopXNotes_Catalyst_2
        Group *rootGroup = [[Group alloc] initRootGroup];
        [groupList insertObject: rootGroup atIndex:0];  //add to beginning of array
		notePadSyncDate = [[NSDate alloc] init];                                //leg20120207 - 1.7.5
        syncNextNewNoteID = [[NSNumber alloc] init];                            //leg20120911 - 1.7.5
        nextNoteID = [[NSNumber alloc] init];                                   //leg20200219 - Catalyst
        nextNoteID = [NSNumber numberWithUnsignedLong: noteID_None+1];          //leg20200310 - Catalyst
        cloudKitSync = [[NSNumber alloc] init];                                 //leg20220907 - TopXNotes_Catalyst_2
    }
	
    return self;
}


@end


@implementation Model

@synthesize appGroupSubDirectoryURL;                                            //leg20211202 - TopXNotes_Catalyst_2

- (void)setFileName:(NSString*)fileName {                                       //leg20200309 - Catalyst
    notePad.notePadFileName = (NSMutableString*)fileName;
}

- (NSString*)fileName {                                                         //leg20200309 - Catalyst
    return notePad.notePadFileName;
}

- (void)setDeviceUDID:(NSString*)deviceUDID {
	notePad.deviceUDID = (NSMutableString*)deviceUDID;
}

- (NSString*)deviceUDID {
	return notePad.deviceUDID;
}

- (void)setRealDeviceUDID:(NSString*)deviceUDID {                               //leg20120403 - 1.7.3
	notePad.realDeviceUDID = (NSMutableString*)deviceUDID;
}


- (NSString*)realDeviceUDID {                                                   //leg20120403 - 1.7.3
	return notePad.realDeviceUDID;
}

- (void)setDeviceLastSyncDate:(NSDate*)deviceSyncDate {                         //leg20120207 - 1.7.5
    notePad.notePadSyncDate = (NSDate*)deviceSyncDate;
}

- (NSDate*)deviceLastSyncDate {                                                 //leg20120207 - 1.7.5
	return notePad.notePadSyncDate;
}

- (NSNumber*)notePadVersion {                                                   //leg20120207 - 1.7.5
    return notePad.notePadVersion;
}

- (NSNumber*)notePadVersionAtOpen {                                             //leg20220817 - TopXNotes_Catalyst_2
	return notePad.notePadVersionAtOpen;
}

- (void)setSyncNextNewNoteID:(NSNumber*)syncNextNewNoteID {                     //leg20120911 - 1.7.5
    notePad.syncNextNewNoteID = (NSNumber*)syncNextNewNoteID;
}

- (NSNumber*)syncNextNewNoteID {                                                //leg20120911 - 1.7.5
    return notePad.syncNextNewNoteID;
}

- (NSNumber*)nextNoteID {                                                       //leg20200219 - Catalyst
    NSNumber *nextNoteID = notePad.nextNoteID;                                  //leg20200310 - Catalyst
    [self setNextNoteID:[NSNumber numberWithUnsignedLong: [notePad.nextNoteID unsignedLongValue] + 1]];   //leg20200310 - Catalyst
    return nextNoteID;                                                          //leg20200310 - Catalyst
}

- (NSNumber*)cloudKitSync {                                                     //leg20220907 - TopXNotes_Catalyst_2
    return notePad.cloudKitSync;
}

- (void)setCloudKitSync:(NSNumber*)state {                                      //leg20220907 - TopXNotes_Catalyst_2
    notePad.cloudKitSync = (NSNumber*)state;
}

- (void)setNextNoteID:(NSNumber*)noteID {                                       //leg20200219 - Catalyst
    notePad.nextNoteID = (NSNumber*)noteID;
}

- (NSMutableArray*)displayList  {                                               //leg20220519 - TopXNotes_Catalyst_2
    return notePad.displayList;
}

- (NSMutableArray*)noteList  {                                                 //leg20220523 - TopXNotes_Catalyst_2
    return notePad.noteList;
}

- (NSMutableArray*)groupList  {                                                 //leg20220523 - TopXNotes_Catalyst_2
    return notePad.groupList;
}

// Fetch objects from our bundle based on keys in our Info.plist.               //leg20211202 - TopXNotes_Catalyst_2
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

- (id)init {
    NotePad_Version_Changed = NO;                                               //leg20120223 - 1.7.5
    
    if (self = [super init]) {
        
        // Make URL to App Group sub directory + file name.                     //leg20211202 - TopXNotes_Catalyst_2
        NSURL *appGroupURL = [[NSFileManager defaultManager]
            containerURLForSecurityApplicationGroupIdentifier:
                @"group.com.tropic4.topxnotes"];
        self.appGroupSubDirectoryURL = [appGroupURL URLByAppendingPathComponent:[self infoValueForKey:(NSString *)kCFBundleNameKey]];
        self.appGroupSubDirectoryURL = [self.appGroupSubDirectoryURL URLByAppendingPathComponent:kModelFileName];

        notePad = [[NotePad alloc] init];
        
        // Set default file name.                                               //leg20200309 - Catalyst
        [self setFileName: (NSMutableString*)kModelFileName];
        
        // Establish pre note pad version 1 (for conversion purposes)
        notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersion0];    //leg20120207 - 1.7.5
        BOOL loaded = [self loadData];
        
        // If not loaded then this is first time, create notepad.
        if (!loaded) {
            
            // Establish current note pad version
            notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];    //leg20120207 - 1.7.5
            
            // Save the model if the notepad version changed.                   //leg20120223 - 1.7.5
            if (NotePad_Version_Changed)
                [self saveData];
            
            if (![self saveData]) {
                NSLog(@"Unable to save user data");
            }
        }
    }
    
    return self;
}

// Initializer for non-default file name.                                       //leg20200305 - Catalyst
- (id)init:(NSString*)fileName {
	NotePad_Version_Changed = NO;
    
	if (self = [super init]) {
		
		notePad = [[NotePad alloc] init];
        
        // Set file name.                                                       //leg20200309 - Catalyst
        notePad.notePadFileName = (NSMutableString*)fileName;

        // Establish pre note pad version 1 (for conversion purposes)
		notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersion0];
		BOOL loaded = [self loadData];                                          //leg20200309 - Catalyst
		
        // If not loaded then this is first time, create notepad.
		if (!loaded) {
            
            // Establish current note pad version
			notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];
            
            // Save the model if the notepad version changed.
            if (NotePad_Version_Changed)
                [self saveData];                                                //leg20200309 - Catalyst
            
            if (![self saveData]) {                                             //leg20200309 - Catalyst
				NSLog(@"Unable to save user data");
			}
		}
    }
	
    return self;
}

// MARK: Notes methods

- (void) addNote:(Note*) note {
	[notePad.noteList insertObject: note atIndex:0]; //add to beginning of array
    
    if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}

- (void)replaceNoteAtIndex:(NSUInteger)index withNote:(Note*)note              //leg20121127 - 1.2.2
{
    [notePad.noteList replaceObjectAtIndex:index withObject:note];    
	if (![self saveData]) {
		NSLog(@"Unable to save user data - replaceNoteAtIndex");
	}
}

- (void)updateNoteAtIndex:(NSUInteger)index withNote:(Note*)note                //leg20121204 - 1.3.0
{
	[notePad.noteList removeObjectAtIndex: index];		// remove note from old location
	[notePad.noteList insertObject: note atIndex: 0];	//	and place it at top of list
	if (![self saveData]) {
		NSLog(@"Unable to save user data - updateNoteAtIndex");
	}
}


- (NSInteger) numberOfNotes {      
    return [notePad.noteList count];
}

//- (NSUInteger) sortNotesByDate:(BOOL)ascending {                                //leg20150318 - 1.5.0
//    // ascending = YES, descending = NO
//    int sortAscending = ascending ? 1 : 0;
//    [notePad.noteList sortUsingFunction:sortByDate context:&sortAscending];
//
//    return [notePad.noteList count];
//}
//
//- (NSUInteger) sortNotesByTitle:(BOOL)ascending {                               //leg20150318 - 1.5.0
//    // ascending = YES, descending = NO
//    int sortAscending = ascending ? 1 : 0;
//    [notePad.noteList sortUsingFunction:sortByTitle context:&sortAscending];
//
//    return [notePad.noteList count];
//}

// OBSOLETE - superceded by -sortNodeContentsByLineage                          //leg20220531 - TopXNotes_Catalyst_2
- (NSUInteger) sortNodesByLineage:(BOOL)ascending {                             //leg20220531 - TopXNotes_Catalyst_2
    // ascending = YES, descending = NO
    int sortAscending = ascending ? 1 : 0;
    
    NSArray *sortedArray;

    sortedArray = [notePad.displayList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        Note *noteLeft;
        Note *noteRight;
        Group *groupLeft;
        Group *groupRight;
        NSString *leftOperand;
        NSString *rightOperand;
        
        if ([a isKindOfClass:[Note class]]) {
            noteLeft = (Note*)a;
            leftOperand = [self getLineageOfNodeAsString:noteLeft.noteUUID];
        } else {
            groupLeft = (Group*)a;
            leftOperand = [self getLineageOfNodeAsString:groupLeft.groupUUID];
        }
        
        if ([b isKindOfClass:[Note class]]) {
            noteRight = (Note*)b;
            rightOperand = [self getLineageOfNodeAsString:noteRight.noteUUID];
        } else {
            groupRight = (Group*)b;
            rightOperand = [self getLineageOfNodeAsString:groupRight.groupUUID];
        }

        if (sortAscending)
            return [leftOperand localizedCompare:rightOperand];
        else
            return [rightOperand localizedCompare:leftOperand];
    }];

    notePad.displayList = [NSMutableArray arrayWithArray:sortedArray];
    
    return [notePad.displayList count];
}

// Sort each group's children by node lineage.                                  //leg20220531 - TopXNotes_Catalyst_2
//  Effectively sorts displayList by title.
- (NSUInteger) sortNodeContentsByLineage:(BOOL)ascending {
    // ascending = YES, descending = NO
    int sortAscending = ascending ? 1 : 0;
    
    NSEnumerator *enumerator = [notePad.displayList objectEnumerator];
    id anObject;
    NSMutableArray *rebuiltDisplayList = [NSMutableArray arrayWithCapacity:500];;

    while (anObject = [enumerator nextObject]) {
//        Group *group = anObject;
        NSArray *sortedContents;

        if ([anObject isKindOfClass:[Group class]]) {
            Group *group = anObject;                                            //leg20220622 - TopXNotes_Catalyst_2

            sortedContents = [group.contents sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                Note *noteLeft;
                Note *noteRight;
                Group *groupLeft;
                Group *groupRight;
                NSString *leftOperand;
                NSString *rightOperand;
                
                if ([self isNodeNote:a]) {
                    noteLeft = [self getNoteForUUID:(NSUUID *)a];
                    leftOperand = [self getLineageOfNodeAsString:noteLeft.noteUUID];
                } else {
                    groupLeft = [self getGroupForUUID:(NSUUID *)a];
                    leftOperand = [self getLineageOfNodeAsString:groupLeft.groupUUID];
                }
                
                if ([self isNodeNote:b]) {
                    noteRight = [self getNoteForUUID:(NSUUID *)b];
                    rightOperand = [self getLineageOfNodeAsString:noteRight.noteUUID];
                } else {
                    groupRight = [self getGroupForUUID:(NSUUID *)b];
                    rightOperand = [self getLineageOfNodeAsString:groupRight.groupUUID];
                }

                // Return result of comparison                                  //leg20220622 - TopXNotes_Catalyst_2
                //    If:
                //       a < b   then return NSOrderedAscending. The left operand is smaller than the right operand.
                //       a > b   then return NSOrderedDescending. The left operand is greater than the right operand.
                //       a == b  then return NSOrderedSame. The operands are equal.
                //    NSOrderedAscending = -1L,
                //    NSOrderedSame=0,
                //    NSOrderedDescending=1

                if (sortAscending)
                    return [leftOperand localizedCompare:rightOperand];
                else
                    return [rightOperand localizedCompare:leftOperand];
//                if (sortAscending)
//                    return [rightOperand localizedCompare:leftOperand];
//                else
//                    return [leftOperand localizedCompare:rightOperand];
            }];
            
            group.contents = [NSMutableArray arrayWithArray:sortedContents];
            [rebuiltDisplayList addObject:group];
       } else {
           [rebuiltDisplayList addObject:anObject];
       }
    }
    
    notePad.displayList = [NSMutableArray arrayWithArray:rebuiltDisplayList];
    
    return [notePad.displayList count];
}

// Sort each group's children by Date.                                          //leg20220601 - TopXNotes_Catalyst_2
//  Effectively sorts displayList by Date.
- (NSUInteger) sortNodeContentsByDate:(BOOL)ascending {
    // ascending = YES, descending = NO
    int sortAscending = ascending ? 1 : 0;
    
    NSEnumerator *enumerator = [notePad.displayList objectEnumerator];
    id anObject;
    NSMutableArray *rebuiltDisplayList = [NSMutableArray arrayWithCapacity:500];;

    while (anObject = [enumerator nextObject]) {
//        Group *group = anObject;
        NSArray *sortedContents;

        if ([anObject isKindOfClass:[Group class]]) {
            Group *group = anObject;                                            //leg20220622 - TopXNotes_Catalyst_2

            sortedContents = [group.contents sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                Note *noteLeft;
                Note *noteRight;
                Group *groupLeft;
                Group *groupRight;
                NSDate *leftOperand;
                NSDate *rightOperand;
                
                if ([self isNodeNote:a]) {
                    noteLeft = [self getNoteForUUID:(NSUUID *)a];
                    leftOperand = noteLeft.date;
                } else {
                    groupLeft = [self getGroupForUUID:(NSUUID *)a];
                    
                    // Default group to oldest date.
                    leftOperand = [NSDate dateWithTimeIntervalSince1970:0];
                }
                
                if ([self isNodeNote:b]) {
                    noteRight = [self getNoteForUUID:(NSUUID *)b];
                    rightOperand = noteRight.date;
                } else {
                    groupRight = [self getGroupForUUID:(NSUUID *)b];

                    // Default group to oldest date.
                    rightOperand = [NSDate dateWithTimeIntervalSince1970:0];
                }

                // Return result of comparison                                  //leg20220622 - TopXNotes_Catalyst_2
                //    If:
                //       a < b   then return NSOrderedAscending. The left operand is smaller than the right operand.
                //       a > b   then return NSOrderedDescending. The left operand is greater than the right operand.
                //       a == b  then return NSOrderedSame. The operands are equal.
                //    NSOrderedAscending = -1L,
                //    NSOrderedSame=0,
                //    NSOrderedDescending=1
                
                if (sortAscending)
                    return [leftOperand compare:rightOperand];
                else
                    return [rightOperand compare:leftOperand];
                
//                if (sortAscending)
//                    return [rightOperand compare:leftOperand];
//                else
//                    return [leftOperand compare:rightOperand];
            }];
            
            group.contents = [NSMutableArray arrayWithArray:sortedContents];
            [rebuiltDisplayList addObject:group];
       } else {
           [rebuiltDisplayList addObject:anObject];
       }
    }
    
    notePad.displayList = [NSMutableArray arrayWithArray:rebuiltDisplayList];
    
	return [notePad.displayList count];
}

- (NSNumber*)getNextNoteID {                                                    //leg20200324 - Catalyst
    return notePad.nextNoteID;
}

- (Note*)getNoteForIndex:(NSInteger)index {
	return [notePad.noteList objectAtIndex:index];
}

- (Note*)getNoteForNoteID:(NSNumber*)noteID {                                   //leg20200219 - Catalyst
    NSEnumerator *enumerator = [notePad.noteList objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        Note *note = anObject;
        if ((NoteIDT)[note.noteID unsignedLongValue] == (NoteIDT)[noteID unsignedLongValue])  //leg20200316 - Catalyst
            return note;
    }
    
    return nil;
}

- (NSUInteger)getIndexOfNoteWithNoteID:(NSNumber*)noteID {                      //leg20200219 - Catalyst
    NSUInteger index = NSNotFound;
    Note *note = nil;
    
    note = [self getNoteForNoteID:noteID];
    
    if (note) {
        index = [notePad.noteList indexOfObject: note];
    }
    
    return index;
}

- (void)removeNoteAtIndex:(NSInteger)index {
	[notePad.noteList removeObjectAtIndex:index];
	if (![self saveData]) {
		NSLog(@"Unable to save user data");
	}
}

- (void)removeAllNotes {                                                        //leg20120303 - 1.7.5
    [notePad.noteList removeAllObjects];
    [notePad.groupList removeAllObjects];                                       //leg20220513 - TopXNotes_Catalyst_2
    
    // Recreate root group.
    Group *rootGroup = [[Group alloc] initRootGroup];
    [notePad.groupList insertObject: rootGroup atIndex:0];  //add to beginning of array
}

- (void)removeAllUnprotectedNotes {                                             //leg20200219 - Catalyst
	NSEnumerator *enumerator = [notePad.noteList reverseObjectEnumerator];
	id anObject;
	
	while (anObject = [enumerator nextObject]) {
		Note *note = anObject;
		if (![note.protectedNoteFlag boolValue])
			[notePad.noteList removeObject:anObject];
	}
}

- (NSUUID *)getUUIDOfNoteAtIndex:(NSInteger)index {                             //leg20220513 - TopXNotes_Catalyst_2
    if (index <= [notePad.noteList count]-1) {
        Note *note = [notePad.noteList objectAtIndex:index];
        return note.noteUUID;
    } else {
        // Index not valid shouldn't happen.
        NSLog(@"Bad index: %ld passed to getUUIDOfNoteAtIndex!", (long)index);
        return nil;
    }
}

- (Note *)getNoteForUUID:(NSUUID*)noteUUID {                                    //leg20220514 - TopXNotes_Catalyst_2
    NSEnumerator *enumerator = [notePad.noteList objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        Note *note = anObject;
        if ([note.noteUUID.UUIDString isEqualToString:noteUUID.UUIDString])
            return note;
    }
    
    return nil;
}

- (NSInteger)getIndexOfNoteForUUID:(NSUUID*)noteUUID {                          //leg20220516 - TopXNotes_Catalyst_2
    return [notePad.noteList indexOfObject:[self getNoteForUUID:noteUUID]];
 }

// MARK: Groups methods

- (void) addGroup:(Group*) group {                                              //leg20220427 - TopXNotes_Catalyst_2
    [notePad.groupList addObject: group];   //add to end of array
    
    if (![self saveData]) {
        NSLog(@"Unable to save user data after AddGroup");
    }
}

// Update the group children with a new note.                                   //leg20220427 - TopXNotes_Catalyst_2
- (void)addNote:(Note*)note toGroup:(NSUUID *)groupUUID {
    Group *group = [self getGroupForUUID:groupUUID];
    
    [group.contents addObject:note.noteUUID];
    
    [self replaceGroupAtIndex:[self getIndexOfGroupForUUID:groupUUID] withGroup:group];

    if (![self saveData]) {
        NSLog(@"Unable to save user data after - addNote:toGroup");
    }
}

// Remove Note from Group.                                                      //leg20220602 - TopXNotes_Catalyst_2
- (void)removeNote:(Note *)note fromGroup:(NSUUID *)groupUUID {
    Group *fromGroup = [self getGroupForUUID:groupUUID];
    NSInteger index = 0;
    
    while (index <  [fromGroup.contents count]) {
        NSUUID* uuid = fromGroup.contents[index];
        if ([uuid.UUIDString isEqualToString:note.noteUUID.UUIDString])
            break;
        
        index++;
    }
    
    if (index < [fromGroup.contents count]) {
        [fromGroup.contents removeObjectAtIndex:index];
        [self replaceGroupAtIndex:[self getIndexOfGroupForUUID:groupUUID] withGroup:fromGroup];
        
        if (![self saveData]) {
            NSLog(@"Unable to save user data - removeNote:fromGroup");
        }
    } else {
        NSLog(@"Note \"%@\" not found in Group \"%@ - removeNote:fromGroup", note.title, fromGroup.title);
    }
}

// Remove Group from Group.                                                     //leg20220609 - TopXNotes_Catalyst_2
- (void)removeGroup:(Group *)group fromGroup:(NSUUID *)groupUUID {
    Group *fromGroup = [self getGroupForUUID:groupUUID];
    NSInteger index = 0;
    
    while (index <  [fromGroup.contents count]) {
        NSUUID* uuid = fromGroup.contents[index];
        if ([uuid.UUIDString isEqualToString:group.groupUUID.UUIDString])
            break;
        
        index++;
    }
    
    if (index < [fromGroup.contents count]) {
        [fromGroup.contents removeObjectAtIndex:index];
        NSInteger idx = [self getIndexOfGroupForUUID:groupUUID];
        [self replaceGroupAtIndex:[self getIndexOfGroupForUUID:groupUUID] withGroup:fromGroup];
                
        if (![self saveData]) {
            NSLog(@"Unable to save user data - removeGroup:fromGroup");
        }
    } else {
        NSLog(@"Group \"%@\" not found in Group \"%@ - removeGroup:fromGroup", group.title, fromGroup.title);
    }
}

- (void)replaceGroupAtIndex:(NSUInteger)index withGroup:(Group*)group {         //leg20220525 - TopXNotes_Catalyst_2
    [notePad.groupList replaceObjectAtIndex:index withObject:group];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - replaceGroupAtIndex");
    }
}


- (NSInteger) numberOfGroups {                                                  //leg20220517 - TopXNotes_Catalyst_2
    return [notePad.groupList count];
}

- (NSUUID *)getRootGroupUUID {                                                  //leg20220526 - TopXNotes_Catalyst_2
    NSEnumerator *enumerator = [notePad.groupList objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        Group *group = anObject;
        if ([group.groupUUID.UUIDString isEqualToString:kRootGroup])
            return group.groupUUID;
    }
    
    return nil;
}

- (Group*)getGroupForIndex:(NSInteger)index {                                   //leg20220509 - TopXNotes_Catalyst_2
    return [notePad.groupList objectAtIndex:index];
}

- (Group *)getGroupForUUID:(NSUUID*)groupUUID {
    NSEnumerator *enumerator = [notePad.groupList objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        Group *group = anObject;
        if ([group.groupUUID.UUIDString isEqualToString:groupUUID.UUIDString])
            return group;
    }
    
    return nil;
}

- (NSUUID *)getUUIDOfGroupAtIndex:(NSInteger)index {                            //leg20220509 - TopXNotes_Catalyst_2
    if (index <= [notePad.groupList count]-1) {
        Group *group = [notePad.groupList objectAtIndex:index];
        return group.groupUUID;
    } else {
        // Return root group's.
        Group *group = [notePad.groupList objectAtIndex:0];
        return group.groupUUID;
    }
}

- (Group *)getGroupNamed:(NSString*)name withinGroup:(NSUUID*)groupUUID {       //leg20220618 - TopXNotes_Catalyst_2
    Group *group = [self getGroupForUUID:groupUUID];
    NSEnumerator *enumerator = [group.contents objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        NSUUID *childNode = anObject;
        
        if ([self isNodeGroup:childNode]) {
            if ([[self getGroupForUUID:childNode].title isEqualToString:name]) {
                return [self getGroupForUUID:childNode];
            }
        }
    }
    
    return nil;
}

- (NSInteger)getIndexOfGroupForUUID:(NSUUID*)groupUUID {                        //leg20220516 - TopXNotes_Catalyst_2
    return [notePad.groupList indexOfObject:[self getGroupForUUID:groupUUID]];
}

- (BOOL)isMember:(NSUUID*)memberUUID ofGroup:(NSUUID*)groupUUID {               //leg20220606 - TopXNotes_Catalyst_2
    Group *group = [self getGroupForUUID:groupUUID];
    
    NSEnumerator *enumerator = [group.contents objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        NSUUID *uuid = anObject;
        
        if ([memberUUID.UUIDString isEqualToString:uuid.UUIDString])
            return true;
    }
    
    return false;
}

- (void)removeGroupAtIndex:(NSInteger)index {                                   //leg20220606 - TopXNotes_Catalyst_2
    [notePad.groupList removeObjectAtIndex:index];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - removeGroupAtIndex");
    }
}



// MARK: Tree manipulation methods

- (BOOL)isNodeNote:(NSUUID*)node {                                              //leg20220531 - TopXNotes_Catalyst_2
    return [node.UUIDString hasPrefix:kNoteUUID_Prefix];
}

- (BOOL)isNodeGroup:(NSUUID*)node {                                             //leg20220617 - TopXNotes_Catalyst_2
    if ([node.UUIDString hasPrefix:kGroupUUID_Prefix] ||
        [node.UUIDString isEqualToString:kRootGroup]) {
        return true;
    }
    return false;
}

// Move Note or Group from one Group to another.                                         //leg20220513 - TopXNotes_Catalyst_2
//  If "from" and "to" are valid then Note or Group is removed from one Group's
//   children and added to another Group's children.
//  If from==nil Note or Group is not moved, it is just added to "to" Group's
//   children.
//  if "from" is invalid then nothing happens.
- (BOOL)moveNoteOrGroup:(NSUUID *)itemUUID
                   fromGroup:(NSUUID *)from
                     toGroup:(NSUUID *)to {
    BOOL result = true;
    NSUInteger index = NSNotFound;

    if ([from.UUIDString isEqualToString:to.UUIDString]) {
        result = false; // "from" and "to" is same group.
        return result;
    }
        
    // If "from" group is valid remove the item from its children.
    Group *fromGroup = [self getGroupForUUID:from];
    if (fromGroup != nil) {
        [fromGroup.contents removeObject:itemUUID];
        index = [notePad.groupList indexOfObject: fromGroup];
        if (index != NSNotFound) {
            [notePad.groupList replaceObjectAtIndex:index withObject:fromGroup];
        }
        else
            result = false;
    }
    
    // If "to" group is valid add the item to its children.
    if (result) {
        index = NSNotFound;
        Group *toGroup = [self getGroupForUUID:to];
        if (toGroup != nil) {
            [toGroup.contents addObject:itemUUID];
            index = [notePad.groupList indexOfObject: toGroup];
            if (index != NSNotFound) {
                [notePad.groupList replaceObjectAtIndex:index withObject:toGroup];

                // If item is a Note then update its group.
                Note *note = [self getNoteForUUID:itemUUID];
                if (note != nil) {
                    note.groupUUID = to;
                    note.depth = toGroup.depth+1;
                    index = NSNotFound;
                    index = [notePad.noteList indexOfObject: note];
                    if (index != NSNotFound)
                        [notePad.noteList replaceObjectAtIndex:index withObject:note];
                } else {
                    Group *group = [self getGroupForUUID:itemUUID];
                    if (group != nil) {
                        group.parentGroupUUID = to;                             //leg20220523 - TopXNotes_Catalyst_2
                        group.depth = toGroup.depth+1;
                        index = NSNotFound;
                        index = [notePad.groupList indexOfObject: group];
                        if (index != NSNotFound)
                            [notePad.groupList replaceObjectAtIndex:index withObject:group];
                    }
                }
            }
            else
                result = false;
        }
        
        if (![self saveData]) {
                NSLog(@"Unable to save user data after moveNoteOrGroup:fromGroup:toGroup:");
        }
    }

    return result;
}

// Determine ancestral lineage of node.                                         //leg20220523 - TopXNotes_Catalyst_2
- (NSArray *) getLineageOfNode:(NSUUID *)nodeUUID {
    NSMutableArray *parents = [NSMutableArray arrayWithCapacity:100];;
    NSUUID *childNode = nodeUUID;
    NSUUID *parentNode;
    
    while (![childNode.UUIDString isEqualToString:kRootGroup] ) {
        // Determine if leaf node.
        if ([childNode.UUIDString hasPrefix:kNoteUUID_Prefix]) {
            // Leaf node should only occur for beginning node: nodeUUID.
            parentNode = [self getNoteForUUID:childNode].groupUUID;
        } else {
            // Group node has a parent.
            parentNode = [self getGroupForUUID:childNode].parentGroupUUID;
        }
        
        // Accumulate ancestors oldest [0] to descendant [n];.
        //  Oldest ancestor will always be root group.
        //  Add it to list of parents if node was found.                        //leg20220811 - TopXNotes_Catalyst_2
        if (parentNode != nil) {
            [parents insertObject:parentNode atIndex:0];
            childNode = parentNode;
        }
    }

    // Add the descendant.
    [parents addObject:nodeUUID];

    return parents;
}

// Make a string of oldest ancestors to descendant.                             //leg20220530 - TopXNotes_Catalyst_2
- (NSString *) getLineageOfNodeAsString:(NSUUID *)nodeUUID {
    NSString *lineageString = @"";
    NSString *descendantName = @"";
    
    // Lineage is ordered oldest ancestor [0] to descendant [n];.
    //  Oldest ancestor will always be root group.
    NSArray *lineage = [self getLineageOfNode:nodeUUID];

    if ([lineage count] > 0l) {
        NSEnumerator *enumeratorLineage = [lineage objectEnumerator];
        id anObject;
        NSUUID *descendantNode;
        
        while (anObject = [enumeratorLineage nextObject]) {
            descendantNode = anObject;
            if ([descendantNode.UUIDString hasPrefix:kNoteUUID_Prefix])
                descendantName = [self getNoteForUUID:descendantNode].title;
            else
                descendantName = [self getGroupForUUID:descendantNode].title;

            lineageString = [NSString stringWithFormat:@"%@/%@", lineageString, descendantName];
        }
        return lineageString;
    }
    return nil;
}

// Breadth First Search of Groups Tree.                                         //leg20220528 - TopXNotes_Catalyst_2
//  This was supposed to be an implementation of a Breadth First (or Level
//  Order) search, but it appears to me it is actually a Depth-First Search.
- (void)collectChildrenOf:(NSUUID *)node into:(NSMutableArray *)nodes {
    NSUUID *childNode = nil;
    Group *groupNode;
        
    if ([node.UUIDString hasPrefix:kNoteUUID_Prefix]) {
        Note *note = [self getNoteForUUID:node];
        childNode = nil;
        [nodes addObject:childNode];
        NSLog(@"1-Child Note: %@, of Group: %@", note.title, [self getGroupForUUID:note.groupUUID].title);
    } else {
        groupNode = [self getGroupForUUID:node];

        NSEnumerator *enumerator = [groupNode.contents objectEnumerator];
        id anObject;

        while (anObject = [enumerator nextObject]) {
            childNode = anObject;
            [nodes addObject:childNode];
 
            if ([childNode.UUIDString hasPrefix:kNoteUUID_Prefix]) {
                Note *note = [self getNoteForUUID:childNode];
                childNode = nil;
                NSLog(@"2-Child Note: %@, of Group: %@", note.title, groupNode.title);
            } else {
                Group *childGroup = [self getGroupForUUID:childNode];
                NSLog(@"Child Group: %@, of Group: %@", childGroup.title, groupNode.title);
                
                [self collectChildrenOf:childNode into:nodes];
           }
        }
    }
}

// Determine if node should be visible.                                         //leg20220523 - TopXNotes_Catalyst_2
- (BOOL) isNodeWithinCollapsedAncestor:(NSUUID *)nodeUUID {
    NSUUID *childNode = nodeUUID;
    NSUUID *parentNode;
    
    while (![childNode.UUIDString isEqualToString:kRootGroup] ) {
        // Determine if leaf node.
        if ([childNode.UUIDString hasPrefix:kNoteUUID_Prefix]) {
            // Leaf node should only occur for beginning node: nodeUUID.
            parentNode = [self getNoteForUUID:childNode].groupUUID;
        } else {
            // Group node has a parent.
            parentNode = [self getGroupForUUID:childNode].parentGroupUUID;
        }
        
        // If any ancestor is collapsed then the node is currently hidden.
        if (![[self getGroupForUUID:parentNode].expanded boolValue])
            return true;
        
        childNode = parentNode;
    }

    return false;
}

- (Group*)getDisplayGroupForIndex:(NSInteger)index {                            //leg20220524 - TopXNotes_Catalyst_2
    return [notePad.displayList objectAtIndex:index];
}

- (id)getDisplayItemForIndex:(NSInteger)index {                                 //leg20220524 - TopXNotes_Catalyst_2
    return [notePad.displayList objectAtIndex:index];
}

// Traverse the Groups Tree visiting every group and note.                      //leg20220515 - TopXNotes_Catalyst_2
// Used this website for pseudo code examples of n "Ary-Tree Traversals":
//  https://www.interviewkickstart.com/learn/dfs-traversal-of-a-tree-using-recursion
//  Depth-First Search Traversal of a Tree Using Recursion.
//  n Ary-Tree Traversals Through DFS Using Recursion.
//
// Modified to list any Notes contained in a Group before listing any
//  contained Groups.
//
- (void) getChildrenOfGroup:(NSUUID *)groupUUID                                 //leg20220515 - TopXNotes_Catalyst_2
                     parent:(NSUUID *)parentUUID
                  fromDepth:(NSInteger)depth
                       base:(BOOL)isFirstGroup
                   intoList:(NSMutableArray *)list {

    Group *groupNode = [self getGroupForUUID:groupUUID];
    Note *noteLeaf;

    // List any Notes found in base Group so that they are shown in order as
    //  contained in base Group.
    if (isFirstGroup) {
        groupNode.depth = depth;
//        // Debugging
//        NSLog(@"Level:%ld, Group Index:%ld, Group Node, UUID:%@, named:\'%@\'",
//              groupNode.depth, [self getIndexOfGroupForUUID:groupNode.groupUUID], groupNode.groupUUID, groupNode.title);

        [list addObject: groupNode];

        NSEnumerator *enumerator0 = [groupNode.contents objectEnumerator];
        id anObject0;

        while (anObject0 = [enumerator0 nextObject]) {
            NSUUID *childNode = anObject0;
            if ([childNode.UUIDString hasPrefix:kNoteUUID_Prefix]) {
                noteLeaf = [self getNoteForUUID:childNode];
                noteLeaf.depth = depth+1;
//                // Debugging
//                NSLog(@"Level:%ld, Note Index:%ld, Note Leaf of Group UUID:%@, Note UUID:%@, named:\'%@\'",
//                      noteLeaf.depth, [self getIndexOfNoteForUUID:noteLeaf.noteUUID], noteLeaf.groupUUID, noteLeaf.noteUUID, noteLeaf.title);
                
                // Add it to list if leaf was found.                            //leg20220811 - TopXNotes_Catalyst_2
                if (noteLeaf != nil)
                    [list addObject: noteLeaf];
            }
        }
    }
    
    NSEnumerator *enumerator = [groupNode.contents objectEnumerator];
    id anObject;

    while (anObject = [enumerator nextObject]) {
        NSUUID *child = anObject;
        Group *group;
        if (![child.UUIDString isEqualToString:parentUUID.UUIDString]) {
            if ([child.UUIDString hasPrefix:kGroupUUID_Prefix]) {
                group = [self getGroupForUUID:child];
                
                NSEnumerator *enumerator2 = [group.contents objectEnumerator];
                id anObject2;
                NSInteger childDepth = depth+1;
                group.depth = childDepth;
//                // Debugging
//                NSLog(@"Level:%ld, Group Index:%ld, Group Node of Group UUID:%@, Group UUID:%@, named:\'%@\'",
//                      group.depth, [self getIndexOfGroupForUUID:group.groupUUID], group.parentGroupUUID, group.groupUUID, group.title);
                
                [list addObject: group];

                while (anObject2 = [enumerator2 nextObject]) {
                    NSUUID *childNode = anObject2;
                    
                    // List any Notes found in Group so that they are shown in
                    //  order as contained in Group.
                    if ([childNode.UUIDString hasPrefix:kNoteUUID_Prefix]) {
                        noteLeaf = [self getNoteForUUID:childNode];
                        noteLeaf.depth = childDepth+1;
//                        // Debugging
//                        NSLog(@"Level:%ld, Note Index:%ld, Note Leaf of Group UUID:%@, Note UUID:%@, named:\'%@\'",
//                              noteLeaf.depth, [self getIndexOfNoteForUUID:noteLeaf.noteUUID], noteLeaf.groupUUID, noteLeaf.noteUUID,
//                                noteLeaf.title);
                        
                        [list addObject: noteLeaf];
                    }
                }
            }

            // Recurse to next level if it is a Grouo.                          //leg20220811 - TopXNotes_Catalyst_2
            if ([self isNodeGroup:child])
                [self getChildrenOfGroup:child parent:parentUUID fromDepth:depth+1 base:false intoList:list];
        }
    }
}

- (void) addDisplayNote:(Note*) note {                                          //leg20220526 - TopXNotes_Catalyst_2
    [notePad.displayList addObject:note];
    
//    // Update parent group.
//    [self moveNoteOrGroup:note.noteUUID
//                   fromGroup:nil
//                     toGroup:note.groupUUID];

    if (![self saveData]) {
        NSLog(@"Unable to save user data - addDisplayNote");
    }
}

- (void) addDisplayGroup:(Group*)group {                                        //leg20220527 - TopXNotes_Catalyst_2
    [notePad.displayList addObject:group];
    
    if (![self saveData]) {
        NSLog(@"Unable to save user data - addDisplayGroup");
    }
}

- (void)replaceDisplayGroupAtIndex:(NSUInteger)index withGroup:(Group*)group {  //leg20220525 - TopXNotes_Catalyst_2
    [notePad.displayList replaceObjectAtIndex:index withObject:group];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - replaceDisplayGroupAtIndex");
    }
}

- (void)replaceDisplayNoteAtIndex:(NSUInteger)index withNote:(Note*)note        //leg20220525 - TopXNotes_Catalyst_2
{
    [notePad.displayList replaceObjectAtIndex:index withObject:note];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - replaceDisplayNoteAtIndex");
    }
}

- (void)removeDisplayItemAtIndex:(NSInteger)index {                             //leg20220602 - TopXNotes_Catalyst_2
    [notePad.displayList removeObjectAtIndex:index];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - removeDisplayItemAtIndex");
    }
}

- (void)moveDisplayItemAtIndex:(NSInteger)from toIndex:(NSInteger)to {          //leg20220606 - TopXNotes_Catalyst_2
    id anObject = [notePad.displayList objectAtIndex:from];
    
    [notePad.displayList removeObjectAtIndex:from];
    [notePad.displayList insertObject:anObject atIndex:to];
    if (![self saveData]) {
        NSLog(@"Unable to save user data - moveDisplayItemAtIndex:toIndex");
    }
}

- (NSInteger)getIndexOfDisplayItem:(NSUUID*)itemUUID {                          //leg20220606 - TopXNotes_Catalyst_2
    NSInteger itemIndex = NSNotFound;
  
    if ([self isNodeNote:itemUUID] ) {
        itemIndex = [notePad.displayList indexOfObject:[self getNoteForUUID:itemUUID]];
    } else {
        itemIndex = [notePad.displayList indexOfObject:[self getGroupForUUID:itemUUID]];
    }
    
    return itemIndex;
}

- (void)deleteGroupContents:(Group*)group                                       //leg20220613 - TopXNotes_Catalyst_2
               andSaveIndex:(NSMutableArray *)indexArray {
    NSArray *groupContents = [NSArray arrayWithArray:group.contents];
    NSEnumerator *enumerator = [groupContents objectEnumerator];
    id anObject;

    while (anObject = [enumerator nextObject]) {
        NSUUID *uuid = anObject;
        NSInteger displayIndex;

        if ([uuid.UUIDString hasPrefix:kNoteUUID_Prefix]) {
            Note *note = [self getNoteForUUID:uuid];
            if (note == nil) return;
            
            // Remove the note from its parent group.
            [self removeNote:note fromGroup:note.groupUUID];

            // Get index of note display item.
            displayIndex = [self getIndexOfDisplayItem:note.noteUUID];
        } else {
            Group *group = [self getGroupForUUID:uuid];
            if (group == nil) return;

            // Drill down to next Group level.
            [self deleteGroupContents:group andSaveIndex:indexArray];

            // Remove the group from its parent group.
            [self removeGroup:group fromGroup:group.parentGroupUUID];

            // Get index of group display item.
            displayIndex = [self getIndexOfDisplayItem:group.groupUUID];
        }
        
        // Keep track of tableview cells needing to be deleted at commit.
        [indexArray addObject:[NSIndexPath indexPathForRow:displayIndex inSection:0]];
    }

    return;
}

// Duplicate note, add it to same parent group, and add to display tree.        //leg20220615 - TopXNotes_Catalyst_2
- (Note *)duplicateNote:(Note*)note {
    // Change init to support Groups.
    Note *dupNote = [[Note alloc] initWithNoteTitle:note.title
                                            text:note.title
                                          withID:note.noteID
                                    andWithUUID:(NSUUID*) [Model genNoteUUID]
                                    withinGroup:(NSUUID*) [self getRootGroupUUID]
                                         onDate:(NSDate*) note.date
                                     createDate:(NSDate*) note.createDate];

    dupNote.title = note.title;
    dupNote.createDate = note.createDate;
    dupNote.date = note.date;

// macOS is server, and as such controls noteIDs assignment.  On
//  iOS noteID is 0 at note creation.
#if TARGET_OS_UIKITFORMAC
    dupNote.noteID = [self nextNoteID];
#else
    dupNote.noteID = [NSNumber numberWithUnsignedLong: 0];
#endif
    
    dupNote.noteText = note.noteText;
    dupNote.noteHTML = note.noteHTML;
    dupNote.encryptedNoteText = note.encryptedNoteText;
    dupNote.encryptedNoteHTML = note.encryptedNoteHTML;
    dupNote.encryptedPassword = note.encryptedPassword;
    dupNote.passwordHint = note.passwordHint;
    dupNote.decryptedPassword = note.decryptedPassword;
    dupNote.decryptedNoteFlag = note.decryptedNoteFlag;
    dupNote.groupID = note.groupID;
    dupNote.depth = note.depth;
    dupNote.syncFlag = [NSNumber numberWithBool:YES];
    dupNote.needsSyncFlag = [NSNumber numberWithBool:YES];
    dupNote.protectedNoteFlag = note.protectedNoteFlag;
    
    dupNote.npSync = note.npSync;
    dupNote.nfChangedSync = [NSNumber numberWithBool:NO];    // Mark note as eligible to be synced
        
    // Add note to front of list, so it will have an index of 0.
    [self addNote:dupNote];

    // Put note into its parent group.
    [self moveNoteOrGroup:dupNote.noteUUID
                 fromGroup:nil
                   toGroup:note.groupUUID];
    
    // Get updated note to add to display tree.
//    dupNote = [self getNoteForUUID:note.noteUUID];
    dupNote = [self getNoteForUUID:dupNote.noteUUID];                           //leg20220630 - TopXNotes_Catalyst_2

    // Add note to display list.
    [self addDisplayNote:dupNote];

    return dupNote;
}

// MARK: Notepad methods

// Return whether or note notepad has any notes currently encrypted.            //leg20130205 - 1.3.0
- (BOOL)hasEncryptedNotes
{
	NSEnumerator *enumerator = [notePad.noteList objectEnumerator];
	id anObject;
	
	while (anObject = [enumerator nextObject]) {
		Note *note = anObject;
		if ([note.protectedNoteFlag boolValue])
			return YES;
	}
	
	return NO;
}

- (NSString*)pathToData {
#ifdef NOTEPAD_MODEL_PATH_IS_MAC
    // Mac path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#else
    // iOS path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#endif
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";
    }
    
//    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kModelFileName];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent: self.fileName];  //leg20200309 - Catalyst
    
    return appFile;
}

// Path to non-default notepad file name.                                       //leg20200305 - Catalyst
+ (NSString*)pathToData:(NSString*)fileName {
#ifdef NOTEPAD_MODEL_PATH_IS_MAC
    // Mac path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES); //leg20120111 - 1.7.2
#else
    // iOS path to model.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
#endif

    NSString *documentsDirectory = [paths objectAtIndex:0];

    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";
    }

    NSString *appFile = [documentsDirectory stringByAppendingPathComponent: fileName];

	return appFile;
}

- (BOOL)saveData {
    NSMutableData *data;
    NSKeyedArchiver *archiver;
    BOOL result;
    NSError *error = nil;                                                       //leg20210415 - TopXNotes2

    data = [NSMutableData data];
    archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    // Archive objects
    [archiver encodeObject:notePad.notePadFileName forKey:KEY_NotePadFileName]; //leg20200309 - Catalyst
    [archiver encodeObject:notePad.notePadVersion forKey:KEY_NotePadVersion];
    [archiver encodeObject:notePad.notePadSyncDate forKey:KEY_NotePadSyncDate]; //leg20120207 - 1.7.5
    [archiver encodeObject:notePad.syncNextNewNoteID forKey:KEY_SyncNextNewNoteID];    //leg20120809 - 1.7.5
    [archiver encodeObject:notePad.nextNoteID forKey:KEY_NextNoteID];           //leg20200219 - Catalyst
    [archiver encodeObject:notePad.cloudKitSync forKey:KEY_CloudKitSync];       //leg20220907 - TopXNotes_Catalyst_2
    [archiver encodeObject:notePad.deviceUDID forKey:KEY_DeviceUDID];
    [archiver encodeObject:notePad.realDeviceUDID forKey:KEY_RealDeviceUDID];   //leg20120403 - 1.7.3
    [archiver encodeObject:notePad.productCode forKey:KEY_ProductCode];
    [archiver encodeObject:notePad.noteList forKey:KEY_NoteList];
    [archiver encodeObject:notePad.groupList forKey:KEY_GroupList];             //leg20220427 - TopXNotes_Catalyst_2
    [archiver encodeObject:notePad.displayList forKey:KEY_DisplayList];         //leg20220629 - TopXNotes_Catalyst_2
    
    [archiver finishEncoding];
    
    // Save the notepad to the App Group sub directory. Ignore any error.       //leg20211202 - TopXNotes_Catalyst_2
    result = [data writeToURL:appGroupSubDirectoryURL atomically:YES];

    //    result = [data writeToFile:appFile atomically:YES];
//    result = [data writeToFile:[self pathToData] atomically:YES];               //leg20200309 - Catalyst
    
    // Capture error information in case writeToFile fails.                     //leg20210415 - TopXNotes2
    result = [data writeToFile:[self pathToData] options:NSDataWritingAtomic error:&error];
    if ([error code] != 0) {
        NSLog(@"Error saving notepad - error=\"%@\"", [error localizedDescription]);
    }
    
    return result;
}

+ (NSUUID *) genGroupUUID {                                                     //leg20220519 - TopXNotes_Catalyst_2
    return [[NSUUID alloc] initWithUUIDString:[kGroupUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
}

+ (NSUUID *) genNoteUUID {                                                      //leg20220519 - TopXNotes_Catalyst_2
    return [[NSUUID alloc] initWithUUIDString:[kNoteUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
}

- (BOOL)loadData {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self pathToData]]) { //leg20200309 - Catalyst
        return NO;
    }
    
    @try {
        // Unarchive notepad data
        NSData *data;
        NSKeyedUnarchiver *unarchiver;
        notePad.notePadVersion = [NSNumber numberWithInt:0];    // Set default note pad version
        
        data = [NSData dataWithContentsOfFile:[self pathToData]];               //leg20200309 - Catalyst
        unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
        // Unarchive objects
        if ([unarchiver decodeObjectForKey:KEY_NotePadVersion] != nil) {
            // This notepad is greater than version zero, so unarchive by decoding each object.
            notePad.notePadVersion = [unarchiver decodeObjectForKey:KEY_NotePadVersion];
            notePad.notePadVersionAtOpen = notePad.notePadVersion;              //leg20220817 - TopXNotes_Catalyst_2
            notePad.deviceUDID = [unarchiver decodeObjectForKey:KEY_DeviceUDID];
            notePad.realDeviceUDID = [unarchiver decodeObjectForKey:KEY_RealDeviceUDID];    //leg20120403 - 1.7.3
            notePad.productCode = [unarchiver decodeObjectForKey:KEY_ProductCode];
            notePad.noteList = [unarchiver decodeObjectForKey:KEY_NoteList];
            
            if ([notePad.notePadVersion intValue] >= kNotePadVersion2) {
                if (!(notePad.notePadSyncDate = [unarchiver decodeObjectForKey:KEY_NotePadSyncDate]))      //leg20120207 - 1.7.5
                    notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date. //leg20120207 - 1.7.5
            } else {
                notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date. //leg20120207 - 1.7.5
            }
            
            // Initialize group list.                                           //leg20220427 - TopXNotes_Catalyst_2
            if ([notePad.notePadVersion intValue] >= kNotePadVersion7) {
                if (!(notePad.groupList = [unarchiver decodeObjectForKey:KEY_GroupList]))
                    notePad.groupList = [NSMutableArray arrayWithCapacity:100]; // Default to empty array.
            } else {
                notePad.groupList = [NSMutableArray arrayWithCapacity:100]; // Default to empty array.
            }
            
            // Initialize display list.                                         //leg20220629 - TopXNotes_Catalyst_2
            if ([notePad.notePadVersion intValue] >= kNotePadVersion7) {
                if (!(notePad.displayList = [unarchiver decodeObjectForKey:KEY_DisplayList]))
                    notePad.displayList = [NSMutableArray arrayWithCapacity:100]; // Default to empty array.
            } else {
                notePad.displayList = [NSMutableArray arrayWithCapacity:100]; // Default to empty array.
            }
            
            // Make sure notepad has a root group.                              //leg20220509 - TopXNotes_Catalyst_2
            if ([notePad.groupList count] <= 0) {
                Group *rootGroup = [[Group alloc] initRootGroup];
                [notePad.groupList insertObject: rootGroup atIndex:0];  //add to beginning of array
            }
            
            // Convert version 6 or less notepad so that all notes are in       //leg20220527 - TopXNotes_Catalyst_2
            //  root group.  Change this to a fast version.
            if ([notePad.notePadVersion intValue] < kNotePadVersion7) {
                NSEnumerator *enumerator = [self.noteList objectEnumerator];
                id anObject;
                Group *group = [self getGroupForUUID:[self getRootGroupUUID]];

                while (anObject = [enumerator nextObject]) {
                    Note *note = anObject;
                    
                    [group.contents addObject:note.noteUUID];
                  }
                
                [self replaceGroupAtIndex:[self getIndexOfGroupForUUID:group.groupUUID] withGroup:group];
            }
            
            // TODO: syncNextNewNoteID is obsolete and no longer used -- REMOVE //leg20211206 - TopXNotes_Catalyst_2
            if ([notePad.notePadVersion intValue] >= kNotePadVersion3) {                                    //leg20120911 - 1.7.5
                if (!(notePad.syncNextNewNoteID = [unarchiver decodeObjectForKey:KEY_SyncNextNewNoteID]))
                    notePad.syncNextNewNoteID = [NSNumber numberWithUnsignedLong:0]; // Default to zero.
            } else {
                notePad.syncNextNewNoteID = [NSNumber numberWithUnsignedLong:0]; // Default to zero.
            }

             if ([notePad.notePadVersion intValue] >= kNotePadVersion7) {        //leg20220907 - TopXNotes_Catalyst_2
                if (!(notePad.cloudKitSync = [unarchiver decodeObjectForKey:KEY_CloudKitSync]))
                    notePad.cloudKitSync = [NSNumber numberWithBool:0]; // Default to false.
            } else {
                notePad.syncNextNewNoteID = [NSNumber numberWithBool:0]; // Default to false.
            }

            if (!(notePad.nextNoteID = [unarchiver decodeObjectForKey:KEY_NextNoteID]))
                notePad.nextNoteID = [NSNumber numberWithUnsignedLong: noteID_None+1]; // Default to first NoteID.  //leg20200310 - Catalyst
            
            // Do not replace notePad.notePadFileName with a default since it   //leg20200309 - Catalyst
            //  could potentially cause a default model to be overlayed with a
            //  non-default notePad.notePadFileName file.
        }
        
        [unarchiver finishDecoding];
        
        // Indicate if the notepad version changed.                             //leg20120223 - 1.7.5
        if ([notePad.notePadVersion intValue] != kNotePadVersionCurrent)
            NotePad_Version_Changed = YES;
        
        if ([notePad.notePadVersion intValue] == 0) {
            // This notepad is version zero, so only the notelist was archived.
            notePad.noteList = [NSKeyedUnarchiver unarchiveObjectWithFile:[self pathToData]];   //leg20200309 - Catalyst
            notePad.groupList = [NSMutableArray arrayWithCapacity:100];        //leg20220427 - TopXNotes_Catalyst_2
            notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];    // Establish note pad version        //leg20120207 - 1.7.5
            notePad.deviceUDID = (NSMutableString *)@" ";
            notePad.productCode = (NSMutableString *)@" ";
            notePad.notePadSyncDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0]; // Default to my reference date.    //leg20120207 - 1.7.5
            notePad.syncNextNewNoteID = [NSNumber numberWithUnsignedLong:0];  // Default to zero.                 //leg20120911 - 1.7.5
            notePad.nextNoteID = [NSNumber numberWithUnsignedLong: noteID_None+1]; // Default to first NoteID. //leg20200310 - Catalyst
            notePad.notePadFileName = (NSMutableString*)kModelFileName;         // Default fileName.  //leg20200309 - Catalyst
        }
        
        // Regardless of the notepad version at -loadData time, it is           //leg20120207 - 1.7.5
        //  now converted to the current version.
        notePad.notePadVersion = [NSNumber numberWithInt: kNotePadVersionCurrent];
        
        // Save notepad in case any fields were changed by -loadData
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error = nil;
        NSDictionary *fileAttributesDictionary;
        NSDate *currentModelModificationDate;
        BOOL result = NO;
        
        // Get the modification date of the current notepad.
        fileAttributesDictionary = [fileManager attributesOfItemAtPath:[self pathToData] error:&error]; //leg20200309 - Catalyst
        currentModelModificationDate = [fileAttributesDictionary fileModificationDate];

        if (![self saveData]) {
            NSLog(@"Unable to save user data");
        }

        // Retain the modification date of the notepad loaded by -loadData
        fileAttributesDictionary = [NSDictionary dictionaryWithObject:currentModelModificationDate forKey:NSFileModificationDate];
        result = [fileManager setAttributes:fileAttributesDictionary ofItemAtPath:[self pathToData] error:&error];    //leg20200309 - Catalyst
        if ([error code] != 0 || !result) {
            NSLog(@"Error setting modification date of backup notepad file! Error code=%ld", (long)[error code]);
            return NO;
        }
    }
        
    @catch (NSException* exception) {
        NSLog(@"Model.m: Caught %@: %@", [exception name], [exception  reason]);
        return NO;
    }
    
    return YES;
}

#pragma mark CloudKit Sync

// Send Sync data to CloudKit container.                                        //leg20220907 - TopXNotes_Catalyst_2
- (BOOL)syncCloudData:(BOOL)canUpload {                                         //leg20220912 - TopXNotes_Catalyst_2

    __block BOOL result = true;
    BOOL iCloudAvailable = false;
    BOOL iCloudSync = false;
    
    if ([[NSFileManager defaultManager] ubiquityIdentityToken] != nil) {
        iCloudAvailable = true;
    }
 
    iCloudSync = [[self cloudKitSync] boolValue];

    // Use iCloud to Sync notepad if available and if iCloud Sync enabled.      //leg20220831 - TopXNotes_Catalyst_2
    if (iCloudAvailable && iCloudSync) {
        
        // Establish the Update Subscripton.                                    //leg20220905 - TopXNotes_Catalyst_2
        [CloudKitManager updateCloudSubscriptions];
        
        // Fetch the Notepad records and use the most recent one.
        [CloudKitManager fetchNotepadWithCompletionHandler:^(NSArray *results, NSError *error) {
            if (results.count > 0) {
                if (error) {
                    if (error.code == 6) {
                        NSLog(@"Error 6 fetching iCloud notepad records - error=\"%@\"", [error localizedDescription]);
                    } else {
                        NSLog(@"Error fetching iCloud notepad records - error=\"%@\"", [error localizedDescription]);
                    }
                    result = false;
               } else {
                    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:results.count];
                   for(CKRecord *record in results) {
                       [arr addObject:record];
                       NSLog(@"Notepad recordID: %@ creationDate: %@ modifiedDate: %@",record.recordID, record.creationDate, record.modificationDate);
                   }
                    
                    // Sort by created date
                    NSArray *sortedRecords = [arr sortedArrayUsingComparator:^NSComparisonResult(CKRecord *record1, CKRecord *record2){
                          return [record2.modificationDate compare:record1.modificationDate]; // sort most recent to top.
                    }];
                    NSLog(@"Sorted %lu Notepad records", (unsigned long)sortedRecords.count);

                    for(CKRecord *record in sortedRecords) {
                        NSLog(@"Sorted Notepad recordID: %@ creationDate: %@ modifiedDate: %@",record.recordID, record.creationDate, record.modificationDate);
                    }

                    // Update the CloudKit Notepad record fields.
                    CKRecord *updateRecord = sortedRecords[0];
                    [CloudKitManager updateRecordFileWithId:updateRecord.recordID.recordName
                                                       path:[self pathToData]
                                          completionHandler:^(NSArray *results, NSError *error) {

                                              if (error) {
                                                  NSLog(@"Error updating Notepad record - error=\"%@\"", [error localizedDescription]);
                                                  result = false;
                                              } else {
                                                  NSLog(@"Notepad Record Id\"%@\" updated!", updateRecord.recordID.recordName);
                                              }
                    }];
                }
            } else if (canUpload) {                                             //leg20220912 - TopXNotes_Catalyst_2
                NSLog(@"No CloudKit Notepad records found!");
                
                // Set this device's notepad as the base to begin syncing with.
                [CloudKitManager createRecord:[self pathToData]
                            completionHandler:^(NSArray *results, NSError *error) {
            
                    if ([error code] != 0) {
                        NSLog(@"Error saving iCloud notepad record - error=\"%@\"", [error localizedDescription]);
                        result = false;
                     } else {
                         NSLog(@"Save iCloud notepad record!");
                     }
                }];
            }
        }];

    }

    return result;
}
            
@end

#pragma mark ** SORT Notes Array By Date Compare Function

NSComparisonResult sortByDate(Note *note1, Note *note2, void *context)          //leg20150318 - 1.5.0
{
    BOOL sortAscending = (*((int*) context) == 1);
    if (sortAscending)
        return [note1.date compare:note2.date];
    else
        return [note2.date compare:note1.date];
}

#pragma mark ** SORT Notes Array By Title Compare Function

NSComparisonResult sortByTitle(Note *note1, Note *note2, void *context)         //leg20150318 - 1.5.0
{
	BOOL sortAscending = (*((int*) context) == 1);
	if (sortAscending)
		return [note1.title localizedCompare:note2.title];
	else
		return [note2.title localizedCompare:note1.title];
}
