//
//  Note.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Changes:
//
// Create groupUUID initialized to the Root Group UUIDString. groupUUID will    //leg20220429 - TopXNotes_Catalyst_2
//  be populated with the parent groupUUID.
//
//  Constant kRootGroup identifies the root-level Group UUIDString.
//  Constant kGroupUUID represents the prefix of Group UUIDStrings.
//  Constant kNoteUUID represents the prefix of Note UUIDStrings.
//
// Added support for HTMl versions of the note data. New notepad version = 6,   //leg20211011 - TopXNotes_Catalyst_2
//  new Note field = noteHTML.
//
// Copied from TopXNotes_Catalyst_3.                                            //leg20210917 - TopXNotes_Catalyst_2
//
//leg20120911 - 1.7.5 - Added createDate to Note.
//leg20121120 - 1.2.2 - Added encryption/decryption fields.
//
// Added to emulate Mac NotePad sync control fields:                            //leg20200214 - Catalyst
//  @synthesize npSync;                                                         //leg20200214 - Catalyst
//  @synthesize nfChangedSync;                                                  //leg20200214 - Catalyst
//

#import "Model.h"                                                               //leg20220527 - TopXNotes_Catalyst_2
#import "Note.h"
#import "SyncConstants.h"                                                       //leg20200219 - Catalyst
#import "Constants.h"                                                           //leg20220427 - TopXNotes_Catalyst_2

@implementation Note
@synthesize title, createDate, date, noteText, noteHTML;                        //leg20211011 - TopXNotes_Catalyst_2
@synthesize noteID, groupID, syncFlag, needsSyncFlag;                           //leg20211011 - TopXNotes_Catalyst_2
@synthesize encryptedNoteText, encryptedNoteHTML;                               //leg20211129 - TopXNotes_Catalyst_2
@synthesize encryptedPassword, protectedNoteFlag;                               //leg20211129 - TopXNotes_Catalyst_2
@synthesize passwordHint;                                                       //leg20121204 - 1.3.0
@synthesize decryptedPassword;                                                  //leg20121204 - 1.3.0
@synthesize decryptedNoteFlag;                                                  //leg20121204 - 1.3.0
@synthesize noteUUID, groupUUID;                                                //leg20220429 - TopXNotes_Catalyst_2
@synthesize depth;                                                              //leg20220518 - TopXNotes_Catalyst_2

// Added to emulate Mac NotePad sync control fields.                            //leg20200214 - Catalyst
@synthesize npSync;                                                             //leg20200214 - Catalyst
@synthesize nfChangedSync;                                                      //leg20200214 - Catalyst


// Create default sync info array.                                              //leg20200214 - Catalyst
- (NSMutableArray *) defaultSyncArray {                                         //leg20200326 - Catalyst

    NSDictionary *syncDictionarys[kMaxSyncDevices];
    NSMutableDictionary *noteSyncInfoDict =
                          [NSMutableDictionary dictionaryWithObjectsAndKeys:    //leg20200317 - Catalyst
                          @"", @"nsDeviceUDID",
                          [NSDate dateWithTimeIntervalSinceReferenceDate:0], @"siSyncDate", nil];

    for (int i=0; i<kMaxSyncDevices; i++) {
        syncDictionarys[i] = noteSyncInfoDict;
    }
    return [NSMutableArray arrayWithObjects:syncDictionarys count:kMaxSyncDevices]; //leg20200326 - Catalyst
}

+ (NSUUID *) genGroupUUID {                                                     //leg20220519 - TopXNotes_Catalyst_2
    return [[NSUUID alloc] initWithUUIDString:[kGroupUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
}

+ (NSUUID *) genNoteUUID {                                                      //leg20220519 - TopXNotes_Catalyst_2
    return [[NSUUID alloc] initWithUUIDString:[kNoteUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
}

// New initializer to support Groups.                                           //leg20220519 - TopXNotes_Catalyst_2
- (id) initWithNoteTitle:(NSString*) title
                text:(NSString*) text
                  withID:(NSNumber*) noteID
             andWithUUID:(NSUUID*) noteUUID
             withinGroup:(NSUUID*) noteGroupUUID
                  onDate:(NSDate*) entryDate {
    
    if (self = [super init]) {
        self.title = title;
        self.createDate = [NSDate date];
        self.date = entryDate;
        self.noteID = noteID;
        self.noteUUID = noteUUID;
        self.noteText = text;
        self.noteHTML = @"";
        self.encryptedNoteText = [NSData data];
        self.encryptedNoteHTML = [NSData data];
        self.encryptedPassword = [NSData data];
        self.passwordHint = @"";
        self.decryptedPassword = @"";
        self.decryptedNoteFlag = [NSNumber numberWithBool:NO];
        self.groupID = [NSNumber numberWithUnsignedLong:0];
        self.groupUUID = noteGroupUUID;
        self.depth = 0;
        self.syncFlag = [NSNumber numberWithBool:NO];
        self.needsSyncFlag = [NSNumber numberWithBool:NO];
        self.protectedNoteFlag = [NSNumber numberWithBool:NO];
        
        self.npSync = [self defaultSyncArray];
        self.nfChangedSync = [NSNumber numberWithBool:NO];
    }
    
    return self;
}

// New initializer to support Groups.                                           //leg20220519 - TopXNotes_Catalyst_2
- (id) initWithNoteTitle:(NSString*) title
                text:(NSString*) text
              withID:(NSNumber*) noteID
         andWithUUID:(NSUUID*) noteUUID
         withinGroup:(NSUUID*) noteGroupUUID
              onDate:(NSDate*) entryDate
          createDate:(NSDate*) creationDate {
    
    if (self = [super init]) {
        self.title = title;
        self.createDate = creationDate;
        self.date = entryDate;
        self.noteID = noteID;
        self.noteUUID = noteUUID;
        self.noteText = text;
        self.noteHTML = @"";
        self.encryptedNoteText = [NSData data];
        self.encryptedNoteHTML = [NSData data];
        self.encryptedPassword = [NSData data];
        self.passwordHint = @"";
        self.decryptedPassword = @"";
        self.decryptedNoteFlag = [NSNumber numberWithBool:NO];
        self.groupID = [NSNumber numberWithUnsignedLong:0];
        self.groupUUID = noteGroupUUID;
        self.depth = 0;
        self.syncFlag = [NSNumber numberWithBool:NO];
        self.needsSyncFlag = [NSNumber numberWithBool:NO];
        self.protectedNoteFlag = [NSNumber numberWithBool:NO];
        
        self.npSync = [self defaultSyncArray];
        self.nfChangedSync = [NSNumber numberWithBool:NO];
    }
    
    return self;
}


- (id) initWithNoteTitle:(NSString*) noteTitle                                  //leg20200310 - Catalyst
					text:(NSString*) textField 
				  withID:(NSNumber*)noteIDField
				  onDate:(NSDate*) entryDate {
	
	if (self = [super init]) {
		self.title = noteTitle;
		self.createDate = [NSDate date];                                        //leg20120911 - 1.7.5
		self.date = entryDate;
		self.noteID = noteIDField;
        self.noteUUID = [[NSUUID alloc] initWithUUIDString:[kNoteUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]]; //leg20220429 - TopXNotes_Catalyst_2
        self.noteText = textField;
		self.noteHTML = @"";                                                    //leg20211011 - TopXNotes_Catalyst_2
        self.encryptedNoteText = [NSData data];                                 //leg20121120 - 1.2.2
		self.encryptedNoteHTML = [NSData data];                                 //leg20211129 - TopXNotes_Catalyst_2
		self.encryptedPassword = [NSData data];                                 //leg20121120 - 1.2.2
		self.passwordHint = @"";                                                //leg20121204 - 1.3.0
		self.decryptedPassword = @"";                                           //leg20121204 - 1.3.0
		self.decryptedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121204 - 1.3.0
        self.groupID = [NSNumber numberWithUnsignedLong:0];
		self.groupUUID = [[NSUUID alloc] initWithUUIDString:kRootGroup];        //leg20220429 - TopXNotes_Catalyst_2
        self.depth = 0;                                                         //leg20220518 - TopXNotes_Catalyst_2
		self.syncFlag = [NSNumber numberWithBool:NO];
		self.needsSyncFlag = [NSNumber numberWithBool:NO];
        self.protectedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121120 - 1.2.2
        
        self.npSync = [self defaultSyncArray];                                  //leg20200214 - Catalyst
		self.nfChangedSync = [NSNumber numberWithBool:NO];                      //leg20200214 - Catalyst
    }
    
    return self;	
}

- (id) initWithNoteTitle:(NSString*) noteTitle                                  //leg20200310 - Catalyst
                text:(NSString*) textField
              withID:(NSNumber*)noteIDField
              onDate:(NSDate*) entryDate
              createDate:(NSDate*) creationDate {
	
	if (self = [super init]) {
		self.title = noteTitle;
		self.createDate = creationDate;                                         //leg20120911 - 1.7.5
		self.date = entryDate;
		self.noteID = noteIDField;
        self.noteUUID = [[NSUUID alloc] initWithUUIDString:[kNoteUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]]; //leg20220429 - TopXNotes_Catalyst_2
		self.noteText = textField;
        self.noteHTML = @"";                                                    //leg20211011 - TopXNotes_Catalyst_2
        self.encryptedNoteText = [NSData data];                                 //leg20121120 - 1.2.2
		self.encryptedNoteHTML = [NSData data];                                 //leg20211129 - TopXNotes_Catalyst_2
		self.encryptedPassword = [NSData data];                                 //leg20121120 - 1.2.2
		self.passwordHint = @"";                                                //leg20121204 - 1.3.0
		self.decryptedPassword = @"";                                           //leg20121204 - 1.3.0
		self.decryptedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121204 - 1.3.0
		self.groupID = [NSNumber numberWithUnsignedLong:0];
        self.groupUUID = [[NSUUID alloc] initWithUUIDString:kRootGroup];        //leg20220429 - TopXNotes_Catalyst_2
        self.depth = 0;                                                         //leg20220518 - TopXNotes_Catalyst_2
		self.syncFlag = [NSNumber numberWithBool:NO];
		self.needsSyncFlag = [NSNumber numberWithBool:NO];
		self.protectedNoteFlag = [NSNumber numberWithBool:NO];                  //leg20121120 - 1.2.2
        
        self.npSync = [self defaultSyncArray];                                  //leg20200214 - Catalyst
        self.nfChangedSync = [NSNumber numberWithBool:NO];                      //leg20200214 - Catalyst
    }
    
    return self;	
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject: self.title forKey: @"title"];
	[encoder encodeObject: self.createDate forKey: @"createDate"];              //leg20120911 - 1.7.5
	[encoder encodeObject: self.date forKey: @"date"];
    [encoder encodeObject: self.noteID forKey: @"noteID"];
	[encoder encodeObject: self.noteUUID forKey: @"noteUUID"];                  //leg20220429 - TopXNotes_Catalyst_2
    [encoder encodeObject: self.noteText forKey: @"noteText"];
	[encoder encodeObject: self.noteHTML forKey: @"noteHTML"];                  //leg20211011 - TopXNotes_Catalyst_2
    [encoder encodeObject: self.encryptedNoteText forKey: @"encryptedNoteText"];    //leg20121120 - 1.2.2
	[encoder encodeObject: self.encryptedNoteHTML forKey: @"encryptedNoteHTML"];    //leg20211129 - TopXNotes_Catalyst_2
	[encoder encodeObject: self.encryptedPassword forKey: @"encryptedPassword"];    //leg20121120 - 1.2.2
	[encoder encodeObject: self.passwordHint forKey: @"passwordHint"];          //leg20121204 - 1.3.0
	[encoder encodeObject: self.decryptedPassword forKey: @"decryptedPassword"];    //leg20121204 - 1.3.0
	[encoder encodeObject: self.decryptedNoteFlag forKey: @"decryptedNoteFlag"];    //leg20121204 - 1.3.0
	[encoder encodeObject: self.groupID forKey: @"groupID"];
    [encoder encodeObject: self.groupUUID forKey: @"groupUUID"];                //leg20220429 - TopXNotes_Catalyst_2
    [encoder encodeInteger: self.depth forKey: @"depth"];                       //leg20220518 - TopXNotes_Catalyst_2
	[encoder encodeObject: self.syncFlag forKey: @"syncFlag"];
	[encoder encodeObject: self.needsSyncFlag forKey: @"needsSyncFlag"];
	[encoder encodeObject: self.protectedNoteFlag forKey: @"protectedNoteFlag"];    //leg20121120 - 1.2.2

    [encoder encodeObject: self.npSync forKey: @"npSync"];                      //leg20200214 - Catalyst
    [encoder encodeObject: self.nfChangedSync forKey: @"nfChangedSync"];        //leg20200214 - Catalyst
}

- (id)initWithCoder:(NSCoder *)decoder {

	if (!(self.title = [decoder decodeObjectForKey: @"title"]))                 //leg20130222 - 1.3.0
        self.title = @"";                                                       //leg20130222 - 1.3.0
	if (!(self.createDate = [decoder decodeObjectForKey: @"createDate"]))       //leg20120911 - 1.7.5
        self.createDate = [NSDate date]; 
	if (!(self.date = [decoder decodeObjectForKey: @"date"]))                   //leg20130222 - 1.3.0
        self.date = [NSDate date];                                              //leg20130222 - 1.3.0
    if (!(self.noteID = [decoder decodeObjectForKey: @"noteID"]))               //leg20130222 - 1.3.0
        self.noteID = [NSNumber numberWithUnsignedLong:0];                      //leg20200310 - Catalyst
	if (!(self.noteUUID = [decoder decodeObjectForKey: @"noteUUID"]))           //leg20220503 - TopXNotes_Catalyst_2
//        self.noteUUID = [NSUUID UUID];                                          //leg20220429 - TopXNotes_Catalyst_2
        self.noteUUID = [Model genNoteUUID];                                    //leg20220527 - TopXNotes_Catalyst_2
    if (!(self.noteText = [decoder decodeObjectForKey: @"noteText"]))           //leg20130222 - 1.3.0
        self.noteText = @"";                                                    //leg20130222 - 1.3.0
    if (!(self.noteHTML = [decoder decodeObjectForKey: @"noteHTML"])) {         //leg20211011 - TopXNotes_Catalyst_2
        self.noteHTML = @"";                                                    //leg20211015 - TopXNotes_Catalyst_2
    }
    if (!(self.encryptedNoteText = [decoder decodeObjectForKey: @"encryptedNoteText"]))  //leg20121120 - 1.2.2
        self.encryptedNoteText = [NSData data];
	if (!(self.encryptedNoteHTML = [decoder decodeObjectForKey: @"encryptedNoteHTML"]))  //leg20211129 - TopXNotes_Catalyst_2
        self.encryptedNoteHTML = [NSData data];
	if (!(self.encryptedPassword = [decoder decodeObjectForKey: @"encryptedPassword"]))  //leg20121120 - 1.2.2
        self.encryptedPassword = [NSData data];
	if (!(self.passwordHint = [decoder decodeObjectForKey: @"passwordHint"]))   //leg20121204 - 1.3.0
        self.passwordHint = @"";
	if (!(self.decryptedPassword = [decoder decodeObjectForKey: @"decryptedPassword"]))  //leg20121204 - 1.3.0
        self.decryptedPassword = @"";
	if (!(self.decryptedNoteFlag = [decoder decodeObjectForKey: @"decryptedNoteFlag"]))  //leg20121120 - 1.2.2
        self.decryptedNoteFlag = [NSNumber numberWithBool:NO];
	if (!(self.groupID = [decoder decodeObjectForKey: @"groupID"]))             //leg20130222 - 1.3.0
        self.groupID = [NSNumber numberWithLong:0];                             //leg20130222 - 1.3.0
    if (!(self.groupUUID = [decoder decodeObjectForKey: @"groupUUID"]))         //leg20220429 - TopXNotes_Catalyst_2
        self.groupUUID = [[NSUUID alloc] initWithUUIDString:kRootGroup];        //leg20220429 - TopXNotes_Catalyst_2
    if (!(self.depth = [decoder decodeIntegerForKey: @"depth"]))                //leg20220518 - TopXNotes_Catalyst_2
        self.depth = 0;
	if (!(self.syncFlag = [decoder decodeObjectForKey: @"syncFlag"]))           //leg20130222 - 1.3.0
        self.syncFlag = [NSNumber numberWithBool:NO];                           //leg20130222 - 1.3.0
	if (!(self.needsSyncFlag = [decoder decodeObjectForKey: @"needsSyncFlag"])) //leg20130222 - 1.3.0
        self.needsSyncFlag = [NSNumber numberWithBool:NO];                      //leg20130222 - 1.3.0
	if (!(self.protectedNoteFlag = [decoder decodeObjectForKey: @"protectedNoteFlag"]))  //leg20121120 - 1.2.2
        self.protectedNoteFlag = [NSNumber numberWithBool:NO];
    
    if (!(self.npSync = [decoder decodeObjectForKey: @"npSync"]))               //leg20200214 - Catalyst
        self.npSync = [self defaultSyncArray];                                  //leg20200214 - Catalyst
    if (!(self.nfChangedSync = [decoder decodeObjectForKey: @"nfChangedSync"])) //leg20200214 - Catalyst
        self.nfChangedSync = [NSNumber numberWithBool:NO];                      //leg20200214 - Catalyst
	
	return self;
}


@end
