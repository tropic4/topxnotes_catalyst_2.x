//
//  Group.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/27/2022.
//  Copyright 2022 Tropical Software. All rights reserved.
//
// Group model.
//
// Changes:
//
// Prefix NSUUID with group identifier.                                         //leg20220429 - TopXNotes_Catalyst_2
// Add additional properties.                                                   //leg20220429 - TopXNotes_Catalyst_2
//

#import "Constants.h"
#import "Group.h"

@implementation Group
@synthesize title, createDate, date, groupUUID, parentGroupUUID, expanded, contents;
@synthesize depth;                                                              //leg20220518 - TopXNotes_Catalyst_2

- (id) initWithGroupTitle:(NSString*) groupTitle
                    parent:(NSUUID*) parentUUID {
    
    if (self = [super init]) {
        self.title = groupTitle;
        self.createDate = [NSDate date];
        self.date = self.createDate;
        self.groupUUID = [[NSUUID alloc] initWithUUIDString:[kGroupUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
        self.parentGroupUUID = parentUUID;
        self.depth = 0;                                                         //leg20220518 - TopXNotes_Catalyst_2
        self.expanded = [NSNumber numberWithBool:NO];
        self.contents = [NSMutableArray arrayWithCapacity:10];
   }
    
    return self;
}

- (id) initRootGroup {
    if (self = [super init]) {
//        self.title = @"Root-Group";
		self.title = @"Groups";                                                 //leg20220613 - TopXNotes_Catalyst_2
		self.createDate = [NSDate date];
		self.date = self.createDate;
        self.groupUUID = [[NSUUID alloc] initWithUUIDString:kRootGroup];
        self.parentGroupUUID = nil;
        self.depth = 0;                                                         //leg20220518 - TopXNotes_Catalyst_2
        self.expanded = [NSNumber numberWithBool:YES];
        self.contents = [NSMutableArray arrayWithCapacity:1000];
   }
    
    return self;	
}

- (void)encodeWithCoder:(NSCoder *)encoder {
	
	[encoder encodeObject: self.title forKey: @"title"];
	[encoder encodeObject: self.createDate forKey: @"createDate"];
	[encoder encodeObject: self.date forKey: @"date"];
	[encoder encodeObject: self.groupUUID forKey: @"groupUUID"];
    [encoder encodeObject: self.parentGroupUUID forKey: @"parentGroupUUID"];
    [encoder encodeInteger: self.depth forKey: @"depth"];                       //leg20220518 - TopXNotes_Catalyst_2
    [encoder encodeObject: self.expanded forKey: @"expanded"];
    [encoder encodeObject: self.contents forKey: @"contents"];
}

- (id)initWithCoder:(NSCoder *)decoder {

	if (!(self.title = [decoder decodeObjectForKey: @"title"]))
        self.title = @"";
	if (!(self.createDate = [decoder decodeObjectForKey: @"createDate"]))
        self.createDate = [NSDate date]; 
	if (!(self.date = [decoder decodeObjectForKey: @"date"]))
        self.date = [NSDate date];
	if (!(self.groupUUID = [decoder decodeObjectForKey: @"groupUUID"]))
        self.groupUUID = [[NSUUID alloc] initWithUUIDString:[kGroupUUID_Prefix stringByAppendingString:[[NSUUID UUID].UUIDString substringFromIndex:8]]];
    if (!(self.parentGroupUUID = [decoder decodeObjectForKey: @"parentGroupUUID"]))
        self.parentGroupUUID = [[NSUUID alloc] initWithUUIDString:kRootGroup];
    if (!(self.depth = [decoder decodeIntegerForKey: @"depth"]))                //leg20220518 - TopXNotes_Catalyst_2
        self.depth = 0;
    if (!(self.expanded = [decoder decodeObjectForKey: @"expanded"]))
        self.expanded = [NSNumber numberWithBool:NO];
    if (!(self.contents = [decoder decodeObjectForKey: @"contents"]))
        self.contents = [NSMutableArray arrayWithCapacity:10];
	
	return self;
}

@end
