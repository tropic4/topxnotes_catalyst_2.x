//
//  Notepad.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 8/30/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
//  Derived from Yalantis CloudKit-demo Created by Maksim Usenko on 3/16/15.    //leg20220830 - TopXNotes_Catalyst_2
//

#import "Notepad.h"
#import <CloudKit/CloudKit.h>

static NSString * const kCitiesPlistName = @"Cities";

const struct CloudKitNotepadFields CloudKitNotepadFields = {
    .identifier = @"id",
    .version = @"version",
    .file = @"file"
};

@implementation Notepad

#pragma mark - Class methods

//+ (NSDictionary *)defaultContent {
//
//    NSString *path = [[NSBundle mainBundle] pathForResource:kCitiesPlistName ofType:@"plist"];
//    NSData *plistData = [NSData dataWithContentsOfFile:path];
//
//    NSAssert(plistData, @"Source doesn't exist");
//
//    NSPropertyListFormat format;
//    NSError *error = nil;
//    NSDictionary *plistDic = [NSPropertyListSerialization propertyListWithData:plistData
//                                                                       options:NSPropertyListMutableContainersAndLeaves
//                                                                        format:&format
//                                                                         error:&error];
//
//    NSAssert(!error, @"Can not read data from the plist");
//
//    return plistDic;
//}

#pragma mark - Lifecycle

- (instancetype)initWithInputData:(id)inputData {
    self = [super init];
    if (self) {
        [self mapObject:inputData];
    }
    
    return self;
}

#pragma mark - Private

- (void)mapObject:(CKRecord *)object {
    _version = [object valueForKeyPath:CloudKitNotepadFields.version];
    _asset = (CKAsset *)[object valueForKeyPath:CloudKitNotepadFields.file];
    _identifier = object.recordID.recordName;
}

@end

