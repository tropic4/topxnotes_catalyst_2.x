//
//  Notepad.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 8/30/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
//  Derived from Yalantis CloudKit-demo Created by Maksim Usenko on 3/16/15.    //leg20220830 - TopXNotes_Catalyst_2
//

#ifndef Notepad_h
#define Notepad_h


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CloudKit/CloudKit.h>

extern const struct CloudKitNotepadFields {
    __unsafe_unretained NSString *identifier;
    __unsafe_unretained NSString *version;
    __unsafe_unretained NSString *file;
} CloudKitNotepadFields;

@interface Notepad : NSObject

@property (nonatomic, copy, readonly) NSString *identifier;
@property (nonatomic, copy, readonly) NSString *version;
@property (nonatomic, copy, readonly) CKAsset *asset;

//+ (NSDictionary *)defaultContent;

- (instancetype)initWithInputData:(id)inputData;

@end

#endif /* Notepad_h */
