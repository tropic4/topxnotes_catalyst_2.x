//
//  PasswordViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 11/15/12.
//
//

#import <UIKit/UIKit.h>

enum PasswordDialogType
{
    GetPassword = 0,
    SetPassword = 1,
    RemovePassword = 2,
};

// Communicate back to instantiator.
@protocol PasswordViewControllerDelegate <NSObject>
- (BOOL)didGetUnprotect: (NSString *)password;
- (BOOL)didGetDecrypt: (NSString *)password;
- (BOOL)didSetProtect: (NSString *)password withHint:(NSString *)hint;
@end

//
// Task of keeping view out from under keyboard is now handled by               //leg20210611 - TopXNotes2
//  IQKeyboardManager framework.
//
//@interface PasswordViewController : UIViewController <UITextFieldDelegate> {
@interface PasswordViewController : UIViewController {
    
//    NSInteger passwordDialogType;
}

@property (nonatomic, strong) id<PasswordViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString *noteTitle;
@property (strong, nonatomic) NSString *passwordHint;
@property (strong, nonatomic) IBOutlet UIView *gettingPasswordView;
@property (strong, nonatomic) IBOutlet UIView *settingPasswordView;
@property (strong, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *createPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *hintPasswordTextField;
@property (strong, nonatomic) IBOutlet UILabel *getPasswordPromptLabel;
@property (strong, nonatomic) IBOutlet UILabel *setPasswordPromptLabel;
@property (strong, nonatomic) IBOutlet UILabel *hintPasswordLabel;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic) int passwordDialogType;                                   //leg20210422 - TopXNotes2

// This is now handled by segue @"Password_Dialog_Segue".                       //leg20210422 - TopXNotes2
//- (id)initWithNibName:(NSString *)nibNameOrNil
//                                  bundle:(NSBundle *)nibBundleOrNil
//                                  withPasswordDialogType:(NSInteger)type
//                                  andNoteTitle:(NSString*)title;

- (IBAction)dismissCancelAction:(id)sender;
- (IBAction)dismissDoneAction:(id)sender;

@end
