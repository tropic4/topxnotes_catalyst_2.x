//
//  BackupSettingsViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackupInfoViewController.h"

@class BackupPickerController;

@class Model;

@interface BackupSettingsViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate> {

	NSMutableDictionary *savedSettingsDictionary;			

	IBOutlet Model			*model;

    UIView *pickerViewContainer;
	
    UIView *backupPickerViewContainer;
    BackupPickerController *backupPickerController;
    
    UIBarButtonItem *modalButton;                                               //leg20220310 - TopXNotes_Catalyst_2
        
	NSUInteger selectedUnit;

	BackupInfoViewController	*modalViewController;
	
    // Change button that is moved.                                             //leg20210518 - TopXNotes2
    CGRect originalLocRemoveButton;
    CGRect landscapeLocRemoveButton;
}

@property (nonatomic, strong) Model *model;

@property (nonatomic, strong) IBOutlet UIView *pickerViewContainer;

@property (nonatomic, strong) IBOutlet BackupPickerController *backupPickerController;
@property (nonatomic, strong) IBOutlet UIView *backupPickerViewContainer;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *modalButton;            //leg20220310 - TopXNotes_Catalyst_2

- (void)positionRemoveButton:(UIInterfaceOrientation)toInterfaceOrientation;   //leg20210518 - TopXNotes2

- (NSString*)pathToAutoBackup:(NSString*)withFileName;

@end
