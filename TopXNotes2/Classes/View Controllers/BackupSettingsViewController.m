//
//  BackupSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "BackupSettingsViewController.h"
#import "BackupPickerController.h"
#import "Formatter.h"
#import "Constants.h"

@interface BackupSettingsViewController () <UIPopoverPresentationControllerDelegate>  //leg20220310 - TopXNotes_Catalyst_2
@end

@implementation BackupSettingsViewController

@synthesize pickerViewContainer;
@synthesize backupPickerController;
@synthesize backupPickerViewContainer;
@synthesize modalButton;                                                        //leg20220310 - TopXNotes_Catalyst_2

@synthesize model;

// Obsolete.                                                                    //leg20210518 - TopXNotes2
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"Backup/Restore";
	}

	return self;

//•leg - 06/09/09 -  Note that model is not yet valid here -- reference it from -viewDidAppear
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    // this will appear as the title in the navigation bar                      //leg20210517 - TopXNotes2
    self.title = @"Restore Backup";

    // Reading Defaults 
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)  
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    // Provide view controller for presenting alert.                            //leg20210420 - TopXNotes2
    backupPickerController.backupSettingsViewController = self;


	// Get auto backup notepad data
	backupPickerController.backupDescriptionsArray = [NSMutableArray array];    //leg20140216 - 1.2.7

	NSArray *array = [dict objectForKey: kAutoBackupsArray_Key];
	if (array != nil) {

		// Prepare to retreive backup file descriptions
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;

		NSEnumerator *enumerator = [array objectEnumerator];
		id anObject;
		NSMutableString *pathToBackupModelDataFile;
		NSMutableString *backupDescription = nil;
		NSDictionary *fileAttributesDictionary;
		NSDate *modificationDate;
		while (anObject = [enumerator nextObject]) {
			pathToBackupModelDataFile = (NSMutableString*)[self pathToAutoBackup:anObject];

			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToBackupModelDataFile error:&error];
			modificationDate = [fileAttributesDictionary fileModificationDate];
			NSRange range = [anObject rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
			if (range.location != NSNotFound)
				backupDescription = (NSMutableString*)[anObject substringToIndex:range.location];

			// Add the backup description to the picker list
			backupDescription = (NSMutableString*)[NSString                     //leg20140216 - 1.2.7
                                    stringWithFormat:@"%@ - %@",
                                    backupDescription,
                                    [NSString longDate:modificationDate]];
			[backupPickerController.backupDescriptionsArray addObject:backupDescription];
		}
	}
	
	// Assemble the picker view
	[pickerViewContainer addSubview:backupPickerViewContainer];
	
//    // Select the oldest backup
//    [backupPickerController.pickerView selectRow:0 inComponent:0 animated:NO];
	// Change initial selection of backup picker to most recent backup.         //leg20220514 - TopXNotes_Catalyst_2
	[backupPickerController.pickerView selectRow:[array count]-1 inComponent:0 animated:NO];

//    // Update the restore button with the oldest backup description.            //leg20210518 - TopXNotes2
//    backupPickerController.backup2Restore = [backupPickerController pickerView:backupPickerController.pickerView titleForRow:0 forComponent:0];
    // Update the restore button with the most recent backup.                   //leg20220514 - TopXNotes_Catalyst_2
    backupPickerController.backup2Restore = [backupPickerController pickerView:backupPickerController.pickerView titleForRow:[array count]-1 forComponent:0];
    NSString *restoreButtonTitle = [NSString stringWithFormat:@"Restore: %@", backupPickerController.backup2Restore];
    [backupPickerController.restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];
    [backupPickerController.restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];

	// Make sure the picker has access to the notepad
	backupPickerController.model = model;

	// make sure Restore button is positioned appropriate to device orientation
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];    //leg20210518 - TopXNotes2
	[self positionRemoveButton:deviceOrientation];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
#pragma unused (duration)
    
	[self positionRemoveButton:toInterfaceOrientation];
}

- (void)positionRemoveButton:(UIInterfaceOrientation)toInterfaceOrientation {
    // Position "Remove All Notes" button depending on device orientation.      //leg20210518 - TopXNotes2
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {            //leg20210518 - TopXNotes2
        backupPickerController.removeButton.frame = landscapeLocRemoveButton;
    } else {
        backupPickerController.removeButton.frame = originalLocRemoveButton;
    }
}

- (void)viewDidLoad {
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;

    // Position "Remove All Notes" button depending on device orientation.      //leg20210518 - TopXNotes2
    [super viewDidLoad];
    
    // Save original portrait device orientation location of Remove button
    //  before positioning and calculate the landscape device orientation
    //  location of Restore button
    originalLocRemoveButton = backupPickerController.removeButton.frame;
    CGFloat midViewX = CGRectGetMidX(backupPickerController.pickerView.frame);
    CGFloat midButtonX = backupPickerController.removeButton.frame.size.width/2.0;
    landscapeLocRemoveButton = CGRectMake(backupPickerController.pickerView.frame.origin.x+midViewX-midButtonX,
                                        backupPickerController.pickerView.frame.origin.y,
                                        backupPickerController.removeButton.frame.size.width,
                                        backupPickerController.removeButton.frame.size.height);

    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];    //leg20210518 - TopXNotes2
    [self positionRemoveButton:deviceOrientation];

	// add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
	modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton]; //leg20220310 - TopXNotes_Catalyst_2
	self.navigationItem.rightBarButtonItem = modalButton;
}

// user clicked the "i" button, present the info view.
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)
    // Show the Info view in a popover.                                         //leg20220310 - TopXNotes_Catalyst_2
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"BackupInfoViewController"];

    // Present the controller.
    //  on iPad, this will be a Popover.
    //  on iPhone, this will be an action sheet.
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];

    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = modalButton;
    popController.delegate = self;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	self.pickerViewContainer = nil;
	
	self.backupPickerViewContainer = nil;
	
	[super viewDidUnload];
}


- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

# pragma mark - Popover Presentation Controller Delegate

// Popover delegate methods for Font settings popover.                          //leg20220310 - TopXNotes_Catalyst_2

// TODO: Determine what to replace deprecated methods with.

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // called when a Popover is dismissed
    NSLog(@"Popover was dismissed with external tap. Have a nice day!");
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // return YES if the Popover should be dismissed
    // return NO if the Popover should not be dismissed
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
    // called when the Popover changes positon
}

@end
