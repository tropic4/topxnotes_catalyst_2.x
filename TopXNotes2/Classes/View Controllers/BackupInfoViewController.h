
#import <UIKit/UIKit.h>
#import "Model.h"

@class Model;

@interface BackupInfoViewController : UIViewController
{
    IBOutlet UILabel *heading;                                                  //leg20220310 - TopXNotes_Catalyst_2
	IBOutlet UILabel *appName;
	IBOutlet UILabel *copyright;
	IBOutlet Model	 *model;
}

@property (nonatomic, strong) Model *model;
@property (strong, nonatomic) IBOutlet UITextView *instructionsText;            //leg20140205 - 1.2.7
@property (strong, nonatomic) IBOutlet UILabel *heading;                        //leg20220310 - TopXNotes_Catalyst_2

- (IBAction)dismissAction:(id)sender;

@end
