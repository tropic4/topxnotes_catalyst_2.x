//
//  NoteFontPickerController.h
//  TopXNotes
//
// Abstract: Controller to manage a picker view displaying Fonts and Font Sizes.
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

@class Model;

// Protocol for obtaining selected font.                                        //leg20211230 - TopXNotes_Catalyst_2
@protocol NoteFontPickerControllerDelegate
- (void)setSelectedFontFamilyAndSize:(NSString *)fontFamily size:(CGFloat)fontSize;
- (void)dismissPopOverViewController:(UIViewController *)viewController;
@end


@interface NoteFontPickerController : NSObject <UIPickerViewDataSource, UIPickerViewDelegate,
												UIAlertViewDelegate, UIActionSheetDelegate> {

    id<NoteFontPickerControllerDelegate> __weak delegate;                       //leg20211230 - TopXNotes_Catalyst_2

    IBOutlet UIPickerView	*pickerView;
    IBOutlet UILabel		*label;
    IBOutlet UIButton       *saveFontButton;
    IBOutlet UIButton		*closeButton;                                       //leg20211230 - TopXNotes_Catalyst_2
	IBOutlet Model			*model;
    IBOutlet UISearchBar    *quickFind;                                         //leg20220218 - TopXNotes_Catalyst_2
                                                    
	NSMutableArray	*fontNamesArray;
	NSArray			*fontSizesArray;
    BOOL            isSetDefaultFontMode;                                       //leg20211230 - TopXNotes_Catalyst_2
    NSMutableArray  *listContent;            // the master content              //leg20220218 - TopXNotes_Catalyst_2
}

@property (nonatomic, strong) NSMutableArray *fontNamesArray;
@property (nonatomic, strong) NSArray *fontSizesArray;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UIButton *saveFontButton;
@property (nonatomic, strong) IBOutlet UIButton	*closeButton;                   //leg20211230 - TopXNotes_Catalyst_2
@property (nonatomic, strong) IBOutlet UISearchBar *quickFind;                  //leg20220218 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSMutableArray *listContent;                      //leg20220218 - TopXNotes_Catalyst_2
@property (nonatomic, strong) Model *model;
@property (nonatomic) BOOL isSetDefaultFontMode;                                //leg20211230 - TopXNotes_Catalyst_2
@property (weak) id <NoteFontPickerControllerDelegate> delegate;

- (void)saveFontSelection;
- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (NSString*)pathToBackupData;
- (NSString*)fontSizeDescription:(NSInteger)size;                               //leg20220928 - TopXNotes_Catalyst_2

@end
