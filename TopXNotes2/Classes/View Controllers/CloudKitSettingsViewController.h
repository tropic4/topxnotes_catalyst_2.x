//
//  CloudKitSettingsViewController.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 9/6/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
// Derived from EncryptionSettingsViewController.h/m.                           //leg20220906 - TopXNotes_Catalyst_2

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@class Model;

@interface CloudKitSettingsViewController : UIViewController {
        NSMutableDictionary *savedSettingsDictionary;
        
        IBOutlet Model            *model;
}

@property (nonatomic, strong) Model *model;
@property (strong, nonatomic) IBOutlet UILabel *cloudKitStatusLabel;
@property (strong, nonatomic) IBOutlet UISwitch *cloudKitStatusSwitch;

- (void)updateCloudKitStatus:(BOOL)status;
- (IBAction)cloudKitEnableOrDisable:(id)sender;

@end

NS_ASSUME_NONNULL_END
