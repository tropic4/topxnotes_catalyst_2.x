
#import "BackupInfoViewController.h"
#import "Constants.h"                                                           //leg20220310 - TopXNotes_Catalyst_2

@implementation BackupInfoViewController
@synthesize model;
@synthesize heading;                                                            //leg20220310 - TopXNotes_Catalyst_2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		//self.title = NSLocalizedString(@"PageSixTitle", @"");
		self.title = @"About TopXNotes Touch";
	}
	
	return self;
}


// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Automatically invoked after -loadView
// This is the preferred override point for doing additional setup after -initWithNibName:bundle:
//
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [Constants lightBlueColor];                     //leg20220310 - TopXNotes_Catalyst_2
}

- (void)viewWillAppear:(BOOL)animated {                                         //leg20140205 - 1.2.7
    [super viewWillAppear:animated];

    // Fill in the info view text.                                              //leg20220310 - TopXNotes_Catalyst_2
    heading.text = NSLocalizedString(@"Backup/Restore_Info_Heading", @"");
    NSString * const formatStr = NSLocalizedString(@"Backup/Restore_Info_Message", @"");
    self.instructionsText.text = [NSString stringWithFormat: formatStr, kNumberOfAutoBackups];
}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)
    
    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)

	// do something here as our view re-appears

    [super viewDidAppear:animated];
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
