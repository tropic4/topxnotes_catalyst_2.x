//
//  NoteRichTextViewController.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 10/14/21.
//  Copyright © 2021 Tropical Software, Inc. All rights reserved.
//
//  Presents a Rich Text Editor View which outputs HTML.
//   Derived from: https://github.com/nnhubbard/ZSSRichTextEditor
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ZSSRichTextEditor.h"
#import "NotePaperView.h"

@class Note;
@class Model;
@class HoverView;

extern NSString *Show_HoverView;

@interface NoteRichTextViewController : ZSSRichTextEditor <NotePaperViewDelegate,
                                                            UIGestureRecognizerDelegate,
                                                            UIAlertViewDelegate,
                                                            UIActionSheetDelegate,
                                                            MFMailComposeViewControllerDelegate> {
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *dateLabel;
    NSString *noteHTML;
    NSString *notePlainText;
    IBOutlet UIBarButtonItem *doneButton;
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *toolsHUDButton;
    IBOutlet UISlider *slider;
    IBOutlet HoverView *hoverView;
    IBOutlet UISearchBar *searchFieldOutlet;                                    //leg20220413 - TopXNotes_Catalyst_2

    NSTimer* myTimer;
    UITapGestureRecognizer *tapRecognizer;
    UISwipeGestureRecognizer *swipeLeftRecognizer;
    UILongPressGestureRecognizer *longPressRecognizer;
    NSString    *dateString;
    NSString    *newPasswordString;
    NSString    *oldPasswordString;
    UIAlertView *passwordAlertView;
    NSNumber    *syncFlag;
    NSNumber    *noteID;
    NSNumber    *needsSyncFlag;
    NSDate      *noteCreationDate;
    BOOL        noteIsDirty;
    BOOL        keyboardShown;
    BOOL        emailInProgress;
    BOOL        showNotepaper;
    int         noteIndex;
	NSInteger   displayTreeIndex;                                               //leg20220527 - TopXNotes_Catalyst_2
    NSUUID      *selectedGroupUUID;                                             //leg20220526 - TopXNotes_Catalyst_2
    Model*    __weak model;
    int fontSliderValue; // 0 to 100
    NotePaperView *notePaperView;
    NSMutableDictionary *savedSettingsDictionary;
    NSMutableString *__weak noteFontName;
    NSMutableString *__weak noteFontSize;
    UIBarButtonItem *barButtonItem;
}

@property (nonatomic) int fontSliderValue;
@property (strong) IBOutlet UISlider *slider;
@property (strong) IBOutlet NotePaperView *notePaperView;
@property (strong) IBOutlet UISearchBar *searchFieldOutlet;                     //leg20220413 - TopXNotes_Catalyst_2
@property (nonatomic, weak) Model* model;
@property (nonatomic, weak) NSMutableString *noteFontName;
@property (nonatomic, weak) NSMutableString *noteFontSize;

@property (nonatomic, strong) UIButton *emailButton;
@property (nonatomic, strong) UIButton *toolsHUDButton;
@property (nonatomic, strong) HoverView *hoverView;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeLeftRecognizer;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressRecognizer;
@property (nonatomic, strong) NSDate *noteCreationDate;
@property (nonatomic, strong) UIBarButtonItem *barButtonItem;
@property (nonatomic) int noteIndex;
@property (nonatomic) NSInteger displayTreeIndex;                               //leg20220527 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSUUID *selectedGroupUUID;                        //leg20220526 - TopXNotes_Catalyst_2

-(IBAction)fontSliderValueChanged:(UISlider *)sender;
-(IBAction)done;
-(IBAction)emailNote;
-(IBAction)toggleToolsHUD:(id)sender;

- (void)showHoverView:(BOOL)show;
- (void)showViewNotif:(NSNotification *)aNotification;

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;

// Action Sheet
- (void)dialogOKCancelAction;

// Email
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end


