//
//  NoteSettingsViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteSettingsInfoViewController.h"

@class NoteFontPickerController;

@class Model;

@interface NoteSettingsViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate> {

	NSMutableDictionary *savedSettingsDictionary;			

	IBOutlet Model			*model;

    UIView *pickerViewContainer;
	
    UIBarButtonItem *modalButton;                                               //leg20220310 - TopXNotes_Catalyst_2

    UIView *noteFontPickerViewContainer;
    NoteFontPickerController *noteFontPickerController;
        
	NSUInteger selectedUnit;

	NoteSettingsInfoViewController	*modalViewController;
	
    BOOL            isSetDefaultFontMode;                                       //leg20211230 - TopXNotes_Catalyst_2
}

@property (nonatomic, strong) Model *model;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *modalButton;            //leg20220310 - TopXNotes_Catalyst_2
@property (nonatomic, strong) IBOutlet UIView *pickerViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *iOS6PointLabel;                 //leg20140205 - 1.2.7
@property (strong, nonatomic) IBOutlet UILabel *iOS7PointLabel;                 //leg20140205 - 1.2.7
@property (nonatomic, strong) IBOutlet NoteFontPickerController *noteFontPickerController;
@property (nonatomic, strong) IBOutlet UIView *noteFontPickerViewContainer;
@property (nonatomic) BOOL isSetDefaultFontMode;                                //leg20211230 - TopXNotes_Catalyst_2

- (NSString*)pathToAutoBackup:(NSString*)withFileName;

@end
