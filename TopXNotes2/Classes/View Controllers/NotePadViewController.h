//
//  NotePadViewController.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NoteListCell.h"
#import "GroupListCell.h"                                                       //leg20220509 - TopXNotes_Catalyst_2
#import "PasswordViewController.h"                                              //leg20121204 - 1.3.0

@class Model;

@interface NotePadViewController : UITableViewController <
// Removed UITableViewDelegate and UITableViewDataSource after remake           //leg20210602 - TopXNotes2
//  of NotePadViewController split view prototype cell.
//                                                          UITableViewDelegate,
//															UITableViewDataSource,
                                                            UITextFieldDelegate,    //leg20220509 - TopXNotes_Catalyst_2
															UIAlertViewDelegate,
															UIActionSheetDelegate,
                                                            NoteListCellDelegate,
                                                            GroupListCellDelegate,  //leg20220509 - TopXNotes_Catalyst_2
                                                            PasswordViewControllerDelegate,     //leg20121204 - 1.3.0
                                                            UIDocumentPickerDelegate,           //leg20220104 - TopXNotes_Catalyst_2
															MFMailComposeViewControllerDelegate> {
                                                                
    UITextField *alertTextField;                                                //leg20220509 - TopXNotes_Catalyst_2

    NSString *ascendingIndicator;                                               //leg20121121 - 1.2.2
    NSString *descendingIndicator;                                              //leg20121121 - 1.2.2
	NSMutableDictionary *savedSettingsDictionary;
	NSMutableArray		*autoBackupsArray;
    NSNumber			*nextAutoBackupNumber;
    NSInteger			sortType;                                               //leg20121121 - 1.2.2
    UIButton            *lockUnlockCellButton;                                  //leg20121204 - 1.3.0
    UIActionSheet       *confirmEmailActionSheet;                               //leg20121212 - 1.3.0
    UIActionSheet       *noEmailLockedActionSheet;                              //leg20121212 - 1.3.0
    NSNumber			*encryptionStatus;                                      //leg20130205 - 1.3.0
    NSString            *viewTitle;                                             //leg20210429 - TopXNotes2

    NSInteger           lockUnlockNoteIndex;                                    //leg20210422 - TopXNotes2
                                                                
    NSIndexPath *editingGroupTitleIndexPath                                     ;//leg20220923 - TopXNotes_Catalyst_2
                                                                
	IBOutlet Model* model;

    int                 noteIndex;
	NSInteger           displayTreeIndex;                                       //leg20220527 - TopXNotes_Catalyst_2
    NSUUID              *selectedGroupUUID;                                     //leg20220526 - TopXNotes_Catalyst_2

    NSString*       noteTitle;                                                  //leg20210422 - TopXNotes2
    NSString*       passwordHint;                                               //leg20210422 - TopXNotes2
    int             passwordDialogType;                                         //leg20210422 - TopXNotes2
}

@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) UIToolbar * toolBar;                              //leg20121017 - 1.2.2
@property (strong, nonatomic) UIImage *lockedPadLockImage;                      //leg20121204 - 1.3.0
@property (strong, nonatomic) UIImage *unLockedPadLockImage;                    //leg20121204 - 1.3.0
@property (strong, nonatomic) UIImage *disclosureOpenedImage;                   //leg20220524 - TopXNotes_Catalyst_2
@property (strong, nonatomic) UIImage *disclosureClosedImage;                   //leg20220524 - TopXNotes_Catalyst_2

- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (void)sortControlHit:(id)sender;                                              //leg20121017 - 1.2.2
- (void)updateBadgeValue;
- (void)reloadDisplay;                                                          //leg20220629 - TopXNotes_Catalyst_2

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
-(void)makeAutoBackup;                                                          //leg20220222 - TopXNotes_Catalyst_2
-(IBAction)newNote;                                                             //leg20220413 - TopXNotes_Catalyst_2
-(IBAction)newGroup;                                                            //leg20220615 - TopXNotes_Catalyst_2
-(IBAction)deleteNoteOrGroup:(id)sender;                                        //leg20220614 - TopXNotes_Catalyst_2
-(IBAction)printNote:(id)sender;                                                //leg20220615 - TopXNotes_Catalyst_2
-(NSUUID*)makeGroupPathFrom:(NSArray *)components within:(NSUUID*)groupUUID;    //leg20220617 - TopXNotes_Catalyst_2
-(void)saveLastSelectedNote:(NSInteger)index;                                   //leg20220908 - TopXNotes_Catalyst_2
-(void)selectDisplayItem:(NSInteger)index;                                      //leg20220908 - TopXNotes_Catalyst_2
-(void)resetDisplayAfterCloudKitUpdate;                                         //leg20220908 - TopXNotes_Catalyst_2
@end

