//
//  EmailTableViewController.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 9/22/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
//
// Derived from NoxtePadViewController in order to eliminate dual use of.        //leg20220922 - TopXNotes_Catalyst_2
//

#import "EmailTableViewController.h"
#import "Formatter.h"
#import "Encrypt.h"                                                             //leg20121127 - 1.2.2
#import "Constants.h"
#import "Model.h"
#import "Note.h"
#import "Group.h"                                                               //leg20220509 - TopXNotes_Catalyst_2
#import "NoteListCell.h"
#import "GroupListCell.h"                                                       //leg20220509 - TopXNotes_Catalyst_2
#import "NSMutableString+EmailEncodingExtensions.h"
#import "TopXNotesAppDelegate.h"                                                //leg20210413 - TopXNotes2
#import "TopXNotes2-Swift.h"                                                    //leg20220103 - TopXNotes_Catalyst_2
#import "NSAttributedString+MMRTFWithImages.h"                                  //leg20220103 - TopXNotes_Catalyst_2
#import "UUID.h"
#import "NoteRichTextViewController.h"                                          //leg20220124 - TopXNotes_Catalyst_2
@import MobileCoreServices;                                                     //leg20220321 - TopXNotes_Catalyst_2
#import "PRHTask.h"                                                             //leg20220329 - TopXNotes_Catalyst_2
#import "JGProgressHUD.h"                                                       //leg20220331 - TopXNotes_Catalyst_2

@implementation EmailTableViewController
@synthesize model;
@synthesize toolBar;                                                            //leg20121017 - 1.2.2
@synthesize unLockedPadLockImage, lockedPadLockImage;                           //leg20121204 - 1.3.0
@synthesize disclosureOpenedImage, disclosureClosedImage;                       //leg20220524 - TopXNotes_Catalyst_2

// Reload the note list table, properly sorted          //leg20121022 - 1.2.2
- (void)reloadDisplay                                                           //leg20220629 - TopXNotes_Catalyst_2
{
    // Get Encryption setting from defaults.                                    //leg20210609 - TopXNotes2
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];
    encryptionStatus = [savedSettingsDictionary objectForKey: kEncryptionStatus_Key];
    
    if (encryptionStatus == nil) {
        encryptionStatus = [NSNumber numberWithBool:NO];
    }

//    // Build group tree to display.                                             //leg20220526 - TopXNotes_Catalyst_2
//    [model.displayList removeAllObjects];
//    [model getChildrenOfGroup:[model getRootGroupUUID] parent:nil fromDepth:0 base:true intoList:model.displayList];
//
//    switch (sortType) {
//        case NotesSortByDateAscending:                                          //leg20220601 - TopXNotes_Catalyst_2
//            [model sortNodeContentsByDate:YES];    // sort date ascending
//            break;
//
//        case NotesSortByDateDescending:                                         //leg20220601 - TopXNotes_Catalyst_2
//            [model sortNodeContentsByDate:NO];  // sort date descending
//            break;
//
//        case NotesSortByTitleAscending:                                         //leg20220531 - TopXNotes_Catalyst_2
//            [model sortNodeContentsByLineage:YES];    // sort title ascending
//            break;
//
//        case NotesSortByTitleDescending:                                        //leg20220531 - TopXNotes_Catalyst_2
//            [model sortNodeContentsByLineage:NO];    // sort title descending
//            break;
//
//        default:
//            break;
//    }
    NSInteger numSorted = [self sortDisplayList];
    NSLog(@"# of items in displayList sorted: %ld\r\r", numSorted);
    
    [self.tableView reloadData];

//•••• removed - not needed for Email ••••    [self updateBadgeValue];    // restore badge value for TopXNotesPro         //leg20150924 - 1.5.0

    // Refresh note count in notepad's title.                                   //leg20211217 - TopXNotes_Catalyst_2
    self.navigationItem.title = [NSString stringWithFormat:@"%@ (%ld notes)",
                                    viewTitle,
                                    (long)[self.model numberOfNotes]];

    // Save data as it is sorted.                                               //leg20220629 - TopXNotes_Catalyst_2
    [model saveData];
}

- (NSInteger)sortDisplayList {
    NSInteger sortCount = 0;
    
    // Build group tree to sort.                                                //leg20220526 - TopXNotes_Catalyst_2
    [model.displayList removeAllObjects];
    [model getChildrenOfGroup:[model getRootGroupUUID] parent:nil fromDepth:0 base:true intoList:model.displayList];
    
    switch (sortType) {
        case NotesSortByDateAscending:                                          //leg20220601 - TopXNotes_Catalyst_2
            sortCount = [model sortNodeContentsByDate:YES];    // sort date ascending
            break;

        case NotesSortByDateDescending:                                         //leg20220601 - TopXNotes_Catalyst_2
            sortCount = [model sortNodeContentsByDate:NO];  // sort date descending
            break;

        case NotesSortByTitleAscending:                                         //leg20220531 - TopXNotes_Catalyst_2
            sortCount = [model sortNodeContentsByLineage:YES];    // sort title ascending
            break;

        case NotesSortByTitleDescending:                                        //leg20220531 - TopXNotes_Catalyst_2
            sortCount = [model sortNodeContentsByLineage:NO];    // sort title descending
            break;

        default:
            break;
    }

    // Rebuild group tree to display so that sorted group.contents are used     //leg20220701 - TopXNotes_Catalyst_2
    //  to determine sort order.
    [model.displayList removeAllObjects];
    [model getChildrenOfGroup:[model getRootGroupUUID] parent:nil fromDepth:0 base:true intoList:model.displayList];

    return sortCount;
}

// Reload the note list table without sorting.                                  //leg20220629 - TopXNotes_Catalyst_2
- (void)reloadDisplayNoSort
{
    [self.tableView reloadData];

//•••• removed - not needed for Email ••••    [self updateBadgeValue];    // restore badge value for TopXNotesPro

    // Refresh note count in notepad's title.
    self.navigationItem.title = [NSString stringWithFormat:@"%@ (%ld notes)",
                                    viewTitle,
                                    (long)[self.model numberOfNotes]];
}

// fetch objects from our bundle based on keys in our Info.plist                //leg20150924 - 1.5.0
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

#pragma mark - Helper Methods

// Traverse the Group tree mock data.                                           //leg20220515 - TopXNotes_Catalyst_2
-(void)traverseTree {

    [model.displayList removeAllObjects];

    // Build group tree from root.
    [model getChildrenOfGroup:[model getRootGroupUUID] parent:nil fromDepth:0 base:true intoList:model.displayList];

    // List all nodes of tree.
    id cellData;
    
    NSEnumerator *enumerator = [model.displayList objectEnumerator];
    id anObject;
    NSInteger index;

    while (anObject = [enumerator nextObject]) {
        cellData = anObject;
        if ([cellData isKindOfClass:[Note class]]) {
            Note *note = (Note*)cellData;
            Group *parentGroup = [model getGroupForUUID:note.groupUUID];
            index = [model getIndexOfNoteForUUID:note.noteUUID];
            
            NSLog(@"Note Depth: %ld, Index=%ld, Title=%@, Parent title:%@",
                  (long)note.depth,
                  (long)index,
                  note.title,
                  parentGroup.title);
        } else {
            Group *group = (Group*)cellData;
            Group *parentGroup = [model getGroupForUUID:group.parentGroupUUID];
            index = [model getIndexOfGroupForUUID:group.groupUUID];
            
            NSLog(@"Group Depth: %ld, Index=%ld, Title=%@, Parent title:%@",
                  (long)group.depth,
                  (long)index,
                  group.title,
                  parentGroup.title);
        }
    }
    
//    // List lineages of all display items.                                      //leg20220529 - TopXNotes_Catalyst_2
//    NSEnumerator *enumeratorDisplayList = [model.displayList objectEnumerator];
//
//    while (anObject = [enumeratorDisplayList nextObject]) {
//        cellData = anObject;
//        NSUUID *targetNode;
//        NSString *targetNodeTitle;
//
//        if ([cellData isKindOfClass:[Note class]]) {
//            Note *note = (Note*)cellData;
//            targetNode = note.noteUUID;
//            targetNodeTitle = note.title;
//        } else {
//            Group *group = (Group*)cellData;
//            targetNode = group.groupUUID;
//            targetNodeTitle = group.title;
//        }
//
//        NSArray *lineage = [model getLineageOfNode:targetNode];
//
//        NSString * lineagePathString = @"";
//        NSString * nodeTitle = @"";
//
//        NSEnumerator *enumeratorLineage = [lineage objectEnumerator];
//
//        while (anObject = [enumeratorLineage nextObject]) {
//            NSUUID *node = anObject;
//            if ([node.UUIDString hasPrefix:kNoteUUID_Prefix]) {
//                Note *note = [model getNoteForUUID:node];
//                nodeTitle = note.title;
//                NSLog(@"Ancestor Index: %ld, Depth: %ld, item:%@", (long)[model getIndexOfNoteForUUID:node], (long)note.depth, nodeTitle);
//            } else {
//                Group *group = [model getGroupForUUID:node];
//                nodeTitle = group.title;
//               NSLog(@"Ancestor Index: %ld, Depth: %ld, item:%@", (long)[model getIndexOfGroupForUUID:node], (long)group.depth, nodeTitle);
//            }
//            lineagePathString = [NSString stringWithFormat:@"%@/%@",lineagePathString, nodeTitle];
//        }
//
//        lineagePathString = [NSString stringWithFormat:@"%@/%@",lineagePathString, targetNodeTitle];
//
//        NSLog(@"Complete Lineage of Node: %@ is:%@", targetNode, lineagePathString);
//    }

    
    // List lineages of all display items.                                      //leg20220529 - TopXNotes_Catalyst_2
    NSEnumerator *enumeratorDisplayList = [model.displayList objectEnumerator];

    while (anObject = [enumeratorDisplayList nextObject]) {
        cellData = anObject;
        NSUUID *descendantNode;

        if ([cellData isKindOfClass:[Note class]]) {
            Note *note = (Note*)cellData;
            descendantNode = note.noteUUID;
        } else {
            Group *group = (Group*)cellData;
            descendantNode = group.groupUUID;
        }

        NSString *lineagePathString = [model getLineageOfNodeAsString:descendantNode];

        if (lineagePathString != nil)
            NSLog(@"Complete Lineage of Node: %@ is:%@", descendantNode, lineagePathString);
        else
            NSLog(@"Root Node: %@ is terminal.", descendantNode);
    }
        

//    int x=47;

//    Breadth-first
//
//    Recursion
//
//    //add the children first (i.e. the entire level) and then go deeper
//    void collectChildren(Node node, Collection<Node> nodes) {
//      for(Node child : node.getChildren()) {
//        nodes.add(child);
//        collectChildren(child, nodes);
//      }
//    }
//
//    //special case: root node
//    void collect(Collection<Node> nodes) {
//       nodes.add(root);
//       collectChildren(root, nodes);
//    }

//    [self bfsTraverseTree];
    
//    int z=45;
}

// Traverse the Group tree mock data BFS - breadth first Search.
-(void)bfsTraverseTree {
    NSMutableArray *children = [NSMutableArray arrayWithCapacity:100];
    NSUUID *root = [model getRootGroupUUID];
    
    [children addObject:root];
    NSLog(@"Root Group: %@", [model getGroupForUUID:root].title);

    [model collectChildrenOf:root into:children];
    
//    int x=55;
}

// Build Group tree mock data.                                             //leg20220523 - TopXNotes_Catalyst_2
//
//  Root Group + 5 Groups + 8 Notes.
//  Build Group tree according to 5/13/2022-7. diagram.
//
-(void)buildGroupMockData {
    
    // Clear out notes from notepad.
    [model removeAllNotes];
    
    // Clear out groups from notepad.
    [model.groupList removeAllObjects];
    
    // Clear out display list.
    [model.displayList removeAllObjects];
    
    // Add back root group.
    Group *rootGroup = [[Group alloc] initRootGroup];
    [model.groupList insertObject: rootGroup atIndex:0];  // add to beginning of list


    // Add 5 Groups to Root Group.
    NSInteger count = 1;
    while (count < 6) {
        Group *group = [[Group alloc] initWithGroupTitle:(NSString*) [NSString stringWithFormat:@"Group %ld", count]
                                                  parent:[model getRootGroupUUID]];
        group.depth = 1;
        
        // Show Group 4 expanded.
        if (count != 4)
            group.expanded = [NSNumber numberWithBool:YES];
        
        [model addGroup:group];                    // add to end of list.
        [model moveNoteOrGroup:group.groupUUID     // move Group to root-group.
                       fromGroup:nil
                         toGroup:[model getRootGroupUUID]];
        count++;
    }

    // Add 8 Notes to Root Group.
    count = 0;
    while (count < 8) {
        Note *note = [[Note alloc] initWithNoteTitle:[NSString stringWithFormat:@"Note %ld", count]
                                                text:[NSString stringWithFormat:@"Note %ld\r\rSome data for Note %ld", count, count]
                                              withID:[model nextNoteID]
                                         andWithUUID:[Model genNoteUUID]
                                         withinGroup:[model getRootGroupUUID]
                                              onDate:[NSDate dateWithTimeIntervalSince1970:((count-8+1)*(500000*60))]];

        note.depth = 1;
        [model.noteList addObject:note];         // add to end of list.
        [model moveNoteOrGroup:note.noteUUID     // move Note to root-group.
                       fromGroup:nil
                         toGroup:note.groupUUID];

        count++;
    }

    if (![model saveData]) {
        NSLog(@"Unable to save user data");
    }

    // Build Group tree according to 5/13/2022-7. diagram.
    [model moveNoteOrGroup:[model getUUIDOfGroupAtIndex:3]      // move Group 3 from root-group to Group 1
                   fromGroup:[model getUUIDOfGroupAtIndex:0]
                     toGroup:[model getUUIDOfGroupAtIndex:1]];

    [model moveNoteOrGroup:[model getUUIDOfGroupAtIndex:4]      // move Group 4 root-group to Group 1
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:1]];

    [model moveNoteOrGroup:[model getUUIDOfGroupAtIndex:5]      // move Group 5 root-group to Group 2
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:2]];

    [model moveNoteOrGroup:[model getUUIDOfNoteAtIndex:4]       // move Note 4 root-group to Group 3
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:3]];

    [model moveNoteOrGroup:[model getUUIDOfNoteAtIndex:5]       // move Note 5 root-group to Group 3
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:3]];

    [model moveNoteOrGroup:[model getUUIDOfNoteAtIndex:7]       // move Note 7 root-group to Group 4
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:4]];

    [model moveNoteOrGroup:[model getUUIDOfNoteAtIndex:3]       // move Note 3 root-group to Group 4
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:4]];

    [model moveNoteOrGroup:[model getUUIDOfNoteAtIndex:6]       // move Note 6 root-group to Group 2
                   fromGroup:[model getRootGroupUUID]
                     toGroup:[model getUUIDOfGroupAtIndex:2]];
    
    // List the Groups.
    NSEnumerator *enumerator = [model.groupList objectEnumerator];
    id anObject;
    NSInteger index = 0;
    
    while (anObject = [enumerator nextObject]) {
        Group *group = anObject;
        
        NSLog(@"Group Name: %@, Depth: %ld, Group Index:%ld, UUID:%@, Parent UUID:%@, Contents:%@",
            group.title,
            (long)group.depth,
            index,
            group.groupUUID,
            group.parentGroupUUID,
            group.contents);
        
              
        index++;
    }
}

// Create a new group within a group.
-(Group *)createGroupNamed:(NSString *)name                                     //leg20220513 - TopXNotes_Catalyst_2
                   withinGroup:(NSUUID *)groupUUID {
    Group *group = [[Group alloc] initWithGroupTitle: name parent: groupUUID];
    [model addGroup: group];
    
    // Update parent group.
    [model moveNoteOrGroup:group.groupUUID
                   fromGroup:nil
                     toGroup:group.parentGroupUUID];
    
    // Get updated group to add to display tree.                                //leg20220527 - TopXNotes_Catalyst_2
    group = [model getGroupForUUID:group.groupUUID];

    [model addDisplayGroup:group];

    // Notify notepad to make an auto backup.
    [[NSNotificationCenter defaultCenter] postNotificationName:kAutoBackups_Notification object:nil];

    return group;
}

// Return the UUID of current selection. Default to root group.                 //leg20220526 - TopXNotes_Catalyst_2
-(NSUUID *)getSelectedGroup {
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    if (selectedIndexPath == nil)
        return [model getRootGroupUUID];
    else {
        id cellData = [model.displayList objectAtIndex:selectedIndexPath.row];
        
        // Determine if Group or Note cell.
        if ([cellData isKindOfClass:[Note class]]) {
            return ((Note *)cellData).groupUUID;
        } else {
            return ((Group *)cellData).groupUUID;
        }
    }
}

// Process the note file pointed to by the URL.                                 //leg20220321 - TopXNotes_Catalyst_2
- (void) processNoteURL:(NSURL*)url
{
    __block NSError *error = nil;
//    __block NSStringEncoding fileEncoding = 0;
    __block NSStringEncoding fileEncoding = NSUTF8StringEncoding;               //leg20220401 - TopXNotes_Catalyst_2
    BOOL accessing = [url startAccessingSecurityScopedResource];
    if (accessing) {
        NSFileCoordinator *fileCoordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
        [fileCoordinator coordinateReadingItemAtURL:url
                                            options:NSFileCoordinatorReadingWithoutChanges
                                              error:nil
                                         byAccessor:^(NSURL *newURL) {
            
        // Assemble note data fields.
        NSString *fileName = newURL.lastPathComponent;
        NSString *fileType = newURL.pathExtension;
        NSString *noteFromFile = [NSString stringWithContentsOfURL:newURL encoding:fileEncoding error:&error];
        __block NSString *notePlainText;
        NSFileManager *fileManager = [NSFileManager defaultManager];

        // Check for RTF file, else default to TXT.                             //leg20220330 - TopXNotes_Catalyst_2
        if ([fileType.uppercaseString isEqualToString:@"RTF"]) {
            // Imported file is RTF - convert it to HTML.                       //leg20220330 - TopXNotes_Catalyst_2
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            if (!documentsDirectory) {
                NSLog(@"Documents directory not found!");
            } else {
                NSString *workFilePath = [documentsDirectory stringByAppendingPathComponent: fileName];
                NSLog(@"Workfile path=%@", workFilePath);

                NSURL *workFileURL = [NSURL fileURLWithPath:workFilePath];
                NSLog(@"Workfile URL=%@", workFileURL);
                BOOL result = [fileManager removeItemAtPath:workFilePath error:&error];     //leg20220620 - TopXNotes_Catalyst_2
                result = [fileManager copyItemAtURL:newURL toURL:workFileURL error:&error];

                if (result) {
                    // Make an HTML file from the RTF file.
                    __weak PRHTask *task = [PRHTask taskWithProgramName:@"textutil" arguments:@"-convert", [NSArray arrayWithObjects:@"html", workFileURL.path, nil], nil];
                    task.accumulatesStandardOutput = YES;

                    task.successfulTerminationBlock = ^(PRHTask *completedTask) {
                        NSLog(@"Completed task: %@ with exit status: %i", completedTask, completedTask.terminationStatus);
                        NSLog(@"Accumulated output: %@", [task outputStringFromStandardOutputUTF8]);
                        
                        // Get the modification and creation dates of the note file.
                        NSDictionary *fileAttributesDictionary;
                        fileAttributesDictionary = [fileManager attributesOfItemAtPath:newURL.path error:&error];
                        if ([error code] != 0) {                                //leg20220620 - TopXNotes_Catalyst_2
                            NSLog(@"Error getting attributed string from HTML - error=\"%@\", code=%ld", [error localizedDescription], (long)[error code] );
                        }
                        NSDate *modificationDate = [fileAttributesDictionary fileModificationDate];
                        NSDate *creationDate = [fileAttributesDictionary fileCreationDate];
                        NSURL *htmlURL = [workFileURL.URLByDeletingPathExtension URLByAppendingPathExtension:@"html"];
                        error = nil;                                            //leg20220620 - TopXNotes_Catalyst_2
                        NSString *htmlFromFile = [NSString stringWithContentsOfURL:htmlURL encoding:fileEncoding error:&error];

                        if ([error code] != 0) {
                            NSLog(@"Error getting HTML from file - error=\"%@\", code=%ld", [error localizedDescription], (long)[error code] );
                            htmlFromFile = @"";
                            notePlainText = noteFromFile;
                        } else {
                            // Convert HTML string with UTF-8 encoding to NSAttributedString
                            NSError *error2 = nil;
                            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                                   initWithData: [htmlFromFile dataUsingEncoding:NSUnicodeStringEncoding]
                                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                                   documentAttributes: nil
                                                                   error: &error2 ];
                            
                            // If an error occurred creating the attributed string containing
                            //  images we must use Ashton to decode the HTML into an attributed
                            //  string.
                            if ([error2 code] != 0) {
                                NSLog(@"Error creating attributed string from HTML - error=\"%@\", code=%ld", [error2 localizedDescription], (long)[error2 code] );
                                attributedString = [Ashton decode:htmlFromFile defaultAttributes:[NSDictionary dictionary]];
                            }

                            // Fill in the Text extracted from the HTML.
                            NSString *notePlainText = attributedString.string;
                            
                            // Remove any leading control characters from note text.
                            NSRange range = [notePlainText rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]];
                            if (range.location != NSNotFound)
                                notePlainText = [notePlainText substringFromIndex:range.location];
                            
                            // Remove any leading whitespace from note text.
                            range = [notePlainText rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
                            notePlainText = [notePlainText stringByReplacingCharactersInRange:range withString:@""];


                        }

                        // Remove bad characters introduced by textutil, I think.
                        // MARK: No longer necessary since NSUTF8StringEncoding fix.
//                        htmlFromFile = [htmlFromFile stringByReplacingOccurrencesOfString:@"Â" withString:@""];

                        // Erase copy of RTF file and textutil generated html file.
                        [fileManager removeItemAtURL:workFileURL error:&error];
                        [fileManager removeItemAtURL:htmlURL error:&error];
                        
                        NSArray *components = [fileName componentsSeparatedByString:@"^"];
                        NSString *fName = [components lastObject];
                        NSUUID * selectedGroupUUID = [self getSelectedGroup];

                        // Walk the Group hierarchy creating groups as needed.
                        if ([components count]>1) {
                            selectedGroupUUID = [self makeGroupPathFrom:components
                                                                 within:selectedGroupUUID];   //leg20220617 - TopXNotes_Catalyst_2
                        }

                        // Prepend note title to the note text
                        [self makeNoteFromType:fileType
                                      wthTitle:fName
                                       andText:[NSString stringWithFormat: @"%@%@%@", fName, @"\r", notePlainText]
                                       andHTML:[NSString stringWithFormat: @"%@%@%@", fName, @"<div><br></div>", htmlFromFile]
                                   withinGroup:selectedGroupUUID                //leg20220617 - TopXNotes_Catalyst_2
                                     createdOn:creationDate
                                 andModifiedOn:modificationDate];
                    };
                    
                    task.abnormalTerminationBlock = ^(PRHTask *completedTask) {
                        NSLog(@"Task exited abnormally: %@ with exit status: %i", completedTask, completedTask.terminationStatus);
                    };

                    [task launch];
                }
            }
        } else {
            // Imported file is TXT - use as-is.                                //leg20220330 - TopXNotes_Catalyst_2
            // Get the modification and creation dates of the note file.
            NSDictionary *fileAttributesDictionary;
            fileAttributesDictionary = [fileManager attributesOfItemAtPath:newURL.path error:&error];
            NSDate *modificationDate = [fileAttributesDictionary fileModificationDate];
            NSDate *creationDate = [fileAttributesDictionary fileCreationDate];
            
            // Create a note with the data.
            [self makeNoteFromType:fileType
                          wthTitle:fileName
                           andText:noteFromFile
                           andHTML:@""
                       withinGroup:selectedGroupUUID                            //leg20220617 - TopXNotes_Catalyst_2
                        createdOn:creationDate
                     andModifiedOn:modificationDate];
        }
    }];
  }
  [url stopAccessingSecurityScopedResource];
}

-(NSUUID*)makeGroupPathFrom:(NSArray *)components within:(NSUUID*)groupUUID {   //leg20220617 - TopXNotes_Catalyst_2
    // Walk the Group hierarchy creating groups as needed.
    NSUUID *selectedGroupUUID = groupUUID;
    NSMutableArray *groupComponents = [NSMutableArray arrayWithArray:components];
    [groupComponents removeLastObject]; // Remove the note component.
    NSEnumerator *enumerator = [groupComponents objectEnumerator];
    id anObject;

    while (anObject = [enumerator nextObject]) {
        NSString *groupName = anObject;

        Group *foundGroup = [model getGroupNamed:groupName withinGroup:selectedGroupUUID];
        if (foundGroup != nil) {
            selectedGroupUUID = foundGroup.groupUUID;
        } else {
            selectedGroupUUID = [self createGroupNamed:groupName withinGroup:selectedGroupUUID].groupUUID;
        }
    }

    return selectedGroupUUID;
}

// Make new note from imported text.                                            //leg20220321 - TopXNotes_Catalyst_2
-(void)makeNoteFromType:(NSString *)type
               wthTitle:(NSString *)title
                andText:(NSString *)text
                andHTML:(NSString *)html                                        //leg20220330 - TopXNotes_Catalyst_2
            withinGroup:(NSUUID *)groupUUID                                     //leg20220617 - TopXNotes_Catalyst_2
              createdOn:(NSDate *)createDate
          andModifiedOn:(NSDate *)modDate {

    NSString* titleString = title;
    // Fix crash after substring range error caused by note.title not           //leg20220328 - TopXNotes_Catalyst_2
    //  prepended to note text.
    NSString* notePlainText = [NSString stringWithFormat: @"%@%@%@", title, @"\r", text];
    
    // macOS is server, and as such controls noteIDs assignment.  On
    //  iOS noteID is 0 at note creation.
    #if TARGET_OS_UIKITFORMAC
        NSNumber *noteID = [self.model nextNoteID];
    #else
        NSNumber *noteID  = [NSNumber numberWithUnsignedLong: 0];
    #endif

    // Intit note and add it to displayList.                                    //leg20220527 - TopXNotes_Catalyst_2
    Note *note = [[Note alloc] initWithNoteTitle: titleString
                                            text: notePlainText
                                          withID: noteID
                                     andWithUUID:[Model genNoteUUID]
//                                     withinGroup:[model getRootGroupUUID]
//                                     withinGroup:[self getSelectedGroup]
                                     withinGroup:groupUUID                      //leg20220617 - TopXNotes_Catalyst_2
                                          onDate: modDate
                                      createDate: createDate];
    
    note.noteHTML = html;

    // Add note to front of list, so it will have an index of 0.
    [model addNote:note];

    // Move note to its parent group.
    [model moveNoteOrGroup:note.noteUUID
                   fromGroup:nil
                     toGroup:note.groupUUID];
    
    // Get updated note to add to display tree.                                 //leg20220527 - TopXNotes_Catalyst_2
    note = [model getNoteForUUID:note.noteUUID];

    // Add to note to group tree display list.
    [model addDisplayNote:note];
}

// Example of block definition and call
//NSArray *result = deleteGroupData(selectedIndexPath);
//
//    NSLog(@"The result is %@", result);
NSArray * (^emailDeleteGroupData)(NSIndexPath *);
NSArray * (^emailDeleteGroupData)(NSIndexPath *) =
                           ^(NSIndexPath * firstValue) {
                               
                               // return an array of indexpaths.
                               NSArray *indexPathArray = [NSArray arrayWithObject:firstValue];
                               return indexPathArray;
};

// Reset display list after a CloudKit Sync update.                             //leg20220908 - TopXNotes_Catalyst_2
//
// Triggered by: postNotificationName:kReset_Display_After_CloudKit_Update
//
-(void)resetDisplayAfterCloudKitUpdate
{
    // Adjust display list so that note view is valid.                          //leg20220908 - TopXNotes_Catalyst_2
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
//    UIInterfaceOrientation deviceOrientation = self.scene.interfaceOrientation;
#if TARGET_OS_UIKITFORMAC
    // Select the first item of display list.                                   //leg20220908 - TopXNotes_Catalyst_2
//    [self selectDisplayItem:1];
    [self selectDisplayItem:[self getLastSelectedNoteIndex]];                   //leg20220916 - TopXNotes_Catalyst_2
#else
    // Reset SplitView to master view.                                          //leg20220908 - TopXNotes_Catalyst_2
    if (UIInterfaceOrientationIsPortrait(deviceOrientation)) {
        [self.navigationController popViewControllerAnimated:true];
    }
    
    // Re-select last selected note.                                            //leg20220916 - TopXNotes_Catalyst_2
    [self selectDisplayItem:[self getLastSelectedNoteIndex]];
#endif
}

// Select last display list selection.                                          //leg20220908 - TopXNotes_Catalyst_2
-(void)selectDisplayItem:(NSInteger)index
{
    NSIndexPath *idx = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    [self tableView:self.tableView didSelectRowAtIndexPath:idx];
}

// Get last selected note index.                                                //leg20220916 - TopXNotes_Catalyst_2
-(NSInteger)getLastSelectedNoteIndex
{
    // Get the last selected note index so we can re-select it.
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Get last selected note index.
    NSNumber *lastSelectedIndex;
    if (!(lastSelectedIndex = [savedSettingsDictionary objectForKey:kLastNoteSelected_Key])) {
        // Set default note index.
        lastSelectedIndex = [NSNumber numberWithInt:0];
        [savedSettingsDictionary setObject:lastSelectedIndex forKey:kLastNoteSelected_Key];
    }
    
    // Reset to index 0 if notepad has fewer notes than last selected.
    if ([self.model numberOfNotes]-1 < [lastSelectedIndex intValue]) {
        lastSelectedIndex = [NSNumber numberWithInt:0];
        [savedSettingsDictionary setObject:lastSelectedIndex forKey:kLastNoteSelected_Key];
    }

    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return [lastSelectedIndex integerValue];;
}

// Save last selected note index.                                               //leg20220217 - TopXNotes_Catalyst_2
-(void)saveLastSelectedNote:(NSInteger)index                                    //leg20220908 - TopXNotes_Catalyst_2
{
//    // If we are in "TopXNotes" tab, Save the currently selected note index so
//    //  we can restore it the next time the Note List is shown.
//    NSString * tabTitle = self.navigationItem.title;
//    if ([self.navigationItem.title hasPrefix:@"TopXNotes"]) {
        // Reading Defaults
        NSUserDefaults *defaults;
        defaults = [NSUserDefaults standardUserDefaults];

        NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
        if (dict != nil)
            savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
        else
            savedSettingsDictionary = [NSMutableDictionary dictionary];

        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSNumber *lastSelectedIndex = [NSNumber numberWithInt:(int)selectedIndexPath.row];
        [savedSettingsDictionary setObject:lastSelectedIndex forKey:kLastNoteSelected_Key];

        // Save settings
        [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
}

//•••• removed - not needed for Email ••••
//#pragma mark - Menu Commands
//
//// "New Note" menu command.                                                     //leg20220105 - TopXNotes_Catalyst_2
//-(IBAction)newNoteMenuCMD:(id)sender {
//    [self newNote];
//}
//
//// Create a new group.
//-(IBAction)newGroup {
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Create New Group"
//                                                                   message:@""
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//
//    // The preferredStyle:UIAlertControllerStyleAlert required for
//    //  addTextFieldWithConfigurationHandler.
//    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//
//        textField.delegate = self;
//        textField.text = @"";
//
//        self->alertTextField = textField;
//
//        NSLog(@"The textField text is - %@",self->alertTextField.text);
//    }];
//
//    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"Save"
//                                                          style:UIAlertActionStyleDefault
//                                                        handler:^(UIAlertAction * action) {
//                                                            [self createGroupNamed:((UITextField *)[alert.textFields objectAtIndex:0]).text withinGroup:[self getSelectedGroup]];
//
//                                                            // Notify notepad to refresh.
//                                                            [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
//                                                        }];
//
//     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
//        handler:^(UIAlertAction * action) {
//         // Do nothing––action cancelled!
//     }];
//
//    [alert addAction:saveAction];
//    [alert addAction:cancelAction];
//    [self presentViewController:alert animated:YES completion:nil];
//}
//
//// Create a new note.
//-(IBAction)newNote {
//    // Set parent group of new note.                                            //leg20220526 - TopXNotes_Catalyst_2
//    selectedGroupUUID = [self getSelectedGroup];
//
//    // Segue to create a new note.                                              //leg20210626 - TopxNotes 1.6
//    [self performSegueWithIdentifier:@"New_Note_Segue" sender: self];
//}
//
//// Delete a single note.                                                        //leg20220105 - TopXNotes_Catalyst_2
//-(IBAction)deleteNoteOrGroup:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//    if (selectedIndexPath == nil) return;
//
//    // Proceed with delete only if a Note row is selected.                      //leg20220614 - TopXNotes_Catalyst_2
//    NSInteger row = selectedIndexPath.row;
//    id cellData;
//
//    // Retreive Note or Group data.
//    cellData = [model.displayList objectAtIndex:row];
//
//    // Determine whether Group or Note cell.
//    if ([cellData isKindOfClass:[Group class]]) {
//        Group *group = cellData;
//
//        if ([group.groupUUID.UUIDString isEqualToString:kRootGroup]) {
//            // Disallow deleting Root Group.                                    //leg20220613 - TopXNotes_Catalyst_2
//            return;
//        }
//    }
//
//    // In order to prevent crash ignore delete if only 1 note in notepad.       //leg20220124 - TopXNotes_Catalyst_2
//    if ([model.noteList count] == 1) return;
//
//    [self tableView:self.tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:selectedIndexPath];
//}
//
//// Duplicate a single note.                                                     //leg20220615 - TopXNotes_Catalyst_2
//-(IBAction)duplicateNote:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//    if (selectedIndexPath == nil) return;
//
//    // Proceed with note duplication only if a Note row is selected.
//    //  Retreive Note or Group data.
//    id cellData = [model.displayList objectAtIndex:selectedIndexPath.row];
//    Note *theNote;
//
//    // Determine whether Group or Note cell.
//    if ([cellData isKindOfClass:[Group class]]) {
//        // Exit since it is not a Note.
//        return;
//    } else {
//        theNote = (Note *)cellData;
//    }
//
//    Note *duplicateNote = [model duplicateNote:theNote];
//    NSLog(@"Note \"%@\" duplicated.", duplicateNote.title);
//
//    // Notify notepad to refresh.
//    [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
//
//    // Reselect the original note that was duplicated.
//    NSIndexPath *reselectedIndexPath = [NSIndexPath indexPathForRow:[model getIndexOfDisplayItem:theNote.noteUUID] inSection:0];
//    [self.tableView selectRowAtIndexPath:reselectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
//    [self tableView:self.tableView didSelectRowAtIndexPath:reselectedIndexPath];
//}
//
//// Export a note to an HTML file.                                               //leg20220103 - TopXNotes_Catalyst_2
//-(IBAction)exportNoteAsHTML:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//    if (selectedIndexPath == nil) return;
//
//    // Proceed with export only if a Note row is selected.                      //leg20220614 - TopXNotes_Catalyst_2
//    //  Retreive Note or Group data.
//    id cellData = [model.displayList objectAtIndex:selectedIndexPath.row];
//    Note *theNote;
//
//    // Determine whether Group or Note cell.
//    if ([cellData isKindOfClass:[Group class]]) {
//        // Exit since it is not a Note.
//        return;
//    } else {
//        theNote = (Note *)cellData;
//    }
//
//    NSError *error2;
//
//    // Save note as HTML in temporary file.
//    NSString *htmlStringToWrite = theNote.noteHTML;
//    NSString *noteFileName = [NSString stringWithFormat:@"%@.html", theNote.title];
//    NSString *htmlFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:noteFileName];
//    [htmlStringToWrite writeToFile:htmlFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error2];
//    if ([error2 code] != 0) {
//        NSLog(@"Error saving note as HTML file - error=\"%@\"", [error2 localizedDescription]);
//    }
//
//    // Export note as HTML file.
//    NSURL *urlHTML = [NSURL fileURLWithPath:htmlFilePath];
//    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:urlHTML
//                                                                                                  inMode:UIDocumentPickerModeExportToService];
//    documentPicker.delegate = self;
//    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
//    [self presentViewController:documentPicker animated:YES completion:nil];
//}
//
//// Export a note to an RTF file.                                                //leg20220103 - TopXNotes_Catalyst_2
//-(IBAction)exportNoteAsRTF:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//    if (selectedIndexPath == nil) return;
//
//    // Proceed with export only if a Note row is selected.                      //leg20220614 - TopXNotes_Catalyst_2
//    //  Retreive Note or Group data.
//    id cellData = [model.displayList objectAtIndex:selectedIndexPath.row];
//
//    Note *theNote;
//
//    // Determine whether Group or Note cell.
//    if ([cellData isKindOfClass:[Group class]]) {
//        // Exit since it is not a Note.
//        return;
//    } else {
//        theNote = (Note *)cellData;
//    }
//
//    NSError *error;
//
//    // Convert HTML string with UTF-8 encoding to NSAttributedString
//    NSAttributedString *attributedString = [Ashton decode:theNote.noteHTML defaultAttributes:[NSDictionary dictionary]];
//
//    // Save note as RTF in a temporary file.
//    NSString *rtfEncodedString = [attributedString encodeRTFWithImages];
//    NSString *noteFileNameRTF = [NSString stringWithFormat:@"%@.rtf", theNote.title];
//    NSString *rtfFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:noteFileNameRTF];
//    [rtfEncodedString writeToFile:rtfFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
//    if ([error code] != 0) {
//        NSLog(@"- (void)exportHTML - Error saving note as RTF file - error=\"%@\", code=%ld", [error localizedDescription], (long)[error code] );
//    }
//
//    // Export the note as RTF file.
//    NSURL *urlRTF = [NSURL fileURLWithPath:rtfFilePath];
//    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:urlRTF
//                                                                                                  inMode:UIDocumentPickerModeExportToService];
//    documentPicker.delegate = self;
//    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
//    [self presentViewController:documentPicker animated:YES completion:nil];
//}
//
//// Present document picker to import notes from TEXT or RTF files.              //leg20220321 - TopXNotes_Catalyst_2
//-(IBAction)importNotes:(id)sender {
//    NSArray* fileTypes = @[(id)kUTTypeText, (id)kUTTypePlainText, (id)kUTTypeRTF]; //(id)kUTTypeItem];
//    UIDocumentPickerViewController *vc = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:fileTypes inMode:UIDocumentPickerModeOpen];
//    vc.allowsMultipleSelection = YES;
//    if (@available(iOS 13.0, *)) {
//        vc.shouldShowFileExtensions = YES;
//    }
//    vc.delegate = self;
//    [self presentViewController:vc animated:YES completion:^{}];
//}
//
//// Print a single note.                                                         //leg20220124 - TopXNotes_Catalyst_2
//-(IBAction)printNote:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//    if (selectedIndexPath == nil) return;
//
//    // Proceed with print only if a Note row is selected.                       //leg20220615 - TopXNotes_Catalyst_2
//    //  Retreive Note or Group data.
//    id cellData = [model.displayList objectAtIndex:selectedIndexPath.row];
//    Note *theNote;
//
//    // Determine whether Group or Note cell.
//    if ([cellData isKindOfClass:[Group class]]) {
//        // Exit since it is not a Note.
//        return;
//    } else {
//        theNote = (Note *)cellData;
//    }
//
//    NSLog(@"Printing Note: \"%@\"", theNote.title);
//
//    // Find the note's editorView.
//    NoteRichTextViewController *noteRichTextViewController =nil;
//    for(UINavigationController *navController in self.splitViewController.viewControllers){
//        if ([navController.topViewController isKindOfClass:[NoteRichTextViewController class]]) {
//            noteRichTextViewController = (NoteRichTextViewController *)navController.topViewController;
//            break;
//        }
//    }
//
//    // Have the editor print the found note's WKWebView.
//    if (noteRichTextViewController != nil)
//        [noteRichTextViewController printEditorView];
//}
//
//- (IBAction)openExportDocumentPicker:(id)sender {
//    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
//
////    NSLog(@"-exportHTML, after -getHTML result=%@", result);
//
//    // MARK: Experimental code to save notes to .html and .rtf files.       //leg20211027 - TopXNotes_Catalyst_2
//    Note *theNote = [self->model getNoteForIndex:selectedIndexPath.row];
//
////    theNote.noteHTML = result;
//    NSError *error2;
//
//    // Export note as HTML.
//    NSString *htmlStringToWrite = theNote.noteHTML;
//    NSString *htmlFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"myFile.html"];
//    [htmlStringToWrite writeToFile:htmlFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error2];
//    if ([error2 code] != 0) {
//        NSLog(@"Error saving note as HTML file - error=\"%@\"", [error2 localizedDescription]);
//    }
//
//    NSURL *url = [NSURL fileURLWithPath:htmlFilePath];
//
//    //    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"image" withExtension:@"jpg"]
////    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"README" withExtension:@"txt"]
//    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:url
//                                                                                                  inMode:UIDocumentPickerModeExportToService];
//    documentPicker.delegate = self;
//    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
//    [self presentViewController:documentPicker animated:YES completion:nil];
//}
//•••• removed - not needed for Email ••••

//// Split View - Respond to tap on cell's padlock button.                        //leg20210608 - TopXNotes2
//- (IBAction)disclosureButtonHit:(id)sender;
//{
//    // Based on current protection status of note, toggle between protect/unprotect.
//    lockUnlockCellButton = sender;
//    CGPoint buttonPosition = [lockUnlockCellButton convertPoint:CGPointMake(5, 5) toCoordinateSpace:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    lockUnlockNoteIndex = indexPath.row;
//    Note* note = [model getNoteForIndex: (int)lockUnlockNoteIndex];
//
//    if ([note.protectedNoteFlag boolValue]) {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue".
//        passwordHint = note.passwordHint;
//        passwordDialogType = RemovePassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    } else {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue".
//        passwordDialogType = SetPassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    }
//}

// Split View - Respond to tap on cell's padlock button.                        //leg20210608 - TopXNotes2
- (IBAction)padlockButtonHit:(id)sender;
{
//•••• removed - not needed for Email ••••
//    // Based on current protection status of note, toggle between protect/unprotect.
//    lockUnlockCellButton = sender;
//    CGPoint buttonPosition = [lockUnlockCellButton convertPoint:CGPointMake(5, 5) toCoordinateSpace:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    lockUnlockNoteIndex = indexPath.row;
//    Note* note = [model getNoteForIndex: (int)lockUnlockNoteIndex];
//
//    if ([note.protectedNoteFlag boolValue]) {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue".
//        passwordHint = note.passwordHint;
//        passwordDialogType = RemovePassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    } else {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue".
//        passwordDialogType = SetPassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    }
//•••• removed - not needed for Email ••••
}

//•••• removed - not needed for Email ••••
//#pragma mark - UIDocumentPickerDelegate
//
//- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls {
//
//    [self dismissViewControllerAnimated:YES completion:nil];
//
//    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
//        // currently not using ModeImport
//    } else if (controller.documentPickerMode == UIDocumentPickerModeOpen) {
//
//        // Begin displaying indeterminate activity indicator.                   //leg20220331 - TopXNotes_Catalyst_2
//        JGProgressHUD *HUD = [[JGProgressHUD alloc] init];
//        HUD.textLabel.text = @"Importing…";
//        [HUD showInView:self.view];
//
//// TODO: experiment showing progress incrementing as each note imported.
////        // Begin displaying determinate activity indicator.                   //leg20220331 - TopXNotes_Catalyst_2
////        JGProgressHUD *HUD = [[JGProgressHUD alloc] init];
//////        HUD.indicatorView = [[JGProgressHUDPieIndicatorView alloc] init]; //Or JGProgressHUDRingIndicatorView
////        HUD.indicatorView = [[JGProgressHUDRingIndicatorView alloc] init];
////        HUD.progress = 0.5f;
////        [HUD showInView:self.view];
//
//        [NSThread sleepForTimeInterval:2.0f];
//
//        NSEnumerator *enumerator = [urls objectEnumerator];
//        id anObject;
//
//        while (anObject = [enumerator nextObject]) {
//            [self processNoteURL:(NSURL*)anObject];
//        }
//
//        // End displaying activity indicator.                                   //leg20220331 - TopXNotes_Catalyst_2
//        [HUD dismissAfterDelay:3.0];
//
//        // For some unknown reason I could not get the "refresh" notification   //leg20220405 - TopXNotes_Catalyst_2
//        //  to reliably update the notelist after the import operation
//        //  finished.  Delaying for 2 seconds seems to insure that the
//        //  "refresh" happens.
//        [NSTimer scheduledTimerWithTimeInterval:2
//                                        repeats:NO
//                                            block:^(NSTimer *timer) {
//            // Notify notepad to refresh.
//            [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
//
//            // Select the first note in list.
//            NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0] ;
//            [self.tableView selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
//            [self tableView:self.tableView didSelectRowAtIndexPath:selectedIndexPath];
//        }];
//
//    } else {
//        // currently not using ModeExport or ModeMove
//    }
//}
//
//- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
//    if (controller.documentPickerMode == UIDocumentPickerModeOpen || controller.documentPickerMode == UIDocumentPickerModeImport) {
//      // currently not using.
//    }
//}
//
//// Pass information to view controller segued to.                               //leg20210414 - TopXNotes2
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    NoteRichTextViewController *controller;
//    if ([segue.identifier isEqualToString:@"New_Note_Segue"] || [segue.identifier isEqualToString:@"Edit_Note_Segue"]) {
//        // Get embedded controller from navigation controller.
//        UINavigationController *navigationController = segue.destinationViewController;
//        controller = (NoteRichTextViewController *)navigationController.topViewController;
//
//         // Restore < Back navigaton button.                                     //leg20210526 - TopXNotes2
//         controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
//         controller.navigationItem.leftItemsSupplementBackButton = YES;
//
//         controller.model = self.model;
//     } else if ([segue.identifier isEqualToString:@"Password_Dialog_Segue"]) {
//         // Pass information for segue @"Password_Dialog_Segue".                 //leg20210422 - TopXNotes2
//         UINavigationController *navigationController = segue.destinationViewController;
//         PasswordViewController *pwcontroller = (PasswordViewController *)navigationController.topViewController;
//         pwcontroller.passwordDialogType = passwordDialogType;
//
//         // Trim whitespace from front and back of note title.
//         pwcontroller.noteTitle = [noteTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//         pwcontroller.delegate = self;
//         pwcontroller.passwordHint = passwordHint;
//     }
//
//    // Pass the index into table view.
//    controller.displayTreeIndex = displayTreeIndex;                             //leg20220527 - TopXNotes_Catalyst_2
//
//    // Set parent group of note.                                                //leg20220527 - TopXNotes_Catalyst_2
//    selectedGroupUUID = [self getSelectedGroup];
//    controller.selectedGroupUUID = selectedGroupUUID;
//
//    if ([segue.identifier isEqualToString:@"New_Note_Segue"]) {                //leg20211119 - TopXNotes_Catalyst_2
//         controller.noteIndex = -1;
//     } else {
//         controller.noteIndex = noteIndex;
//     }
//}
//•••• removed - not needed for Email ••••

- (void)viewDidLoad {
    [super viewDidLoad];

    // Fix crash caused by use of setPrimaryBackgroundStyle in < iOS 13.        //leg20220114 - TopXNotes_Catalyst_2
    if (@available(iOS 13.0, *)) {
        // Set sidebar background style for macOS.                              //leg20220104 - TopXNotes_Catalyst_2
        self.splitViewController.primaryBackgroundStyle = UISplitViewControllerBackgroundStyleSidebar;
        
        // Set Appearance Style for view.                                       //leg20210528 - TopXNotes2
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }

// Configure navigation and tab bar appearance.                                 //leg20220222 - TopXNotes_Catalyst_2
#if TARGET_OS_UIKITFORMAC
    // Set navigation and tab bar appearance for macOS.                         //leg20220118 - TopXNotes_Catalyst_2
    UINavigationBarAppearance *barAppearance = [[UINavigationBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    UITabBarAppearance *tabBarAppearance = [[UITabBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    barAppearance.backgroundColor = [UIColor lightGrayColor];
//    tabBarAppearance.backgroundColor = [UIColor lightGrayColor];
    
    // For macOS 10.15.7 - Catalina: Fix transluscent navigation bar that       //leg20220916 - TopXNotes_Catalyst_2
    //  showed note list scrolling under it. Create gray color that approximates
    //  navigation bar's default bacground color. I tried many other ways to
    //  set an opaque background color for the navigation bar, but this is the
    //  only way that worked.
    UIColor *barColor = [UIColor colorWithWhite:0.85 alpha:1.0];
    barAppearance.backgroundColor = barColor;
    
    self.navigationController.navigationBar.standardAppearance = barAppearance;
    self.navigationController.navigationBar.scrollEdgeAppearance = barAppearance;
    self.tabBarController.tabBar.standardAppearance = tabBarAppearance;
#else
    // Set navigation and tab bar appearance for iOS.                           //leg20220118 - TopXNotes_Catalyst_2
    if (@available(iOS 13.0, *)) {
        UINavigationBarAppearance *barAppearance = [[UINavigationBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
        UITabBarAppearance *tabBarAppearance = [[UITabBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//        barAppearance.backgroundColor = [UIColor lightGrayColor];
//        tabBarAppearance.backgroundColor = [UIColor lightGrayColor];
        self.navigationController.navigationBar.standardAppearance = barAppearance;
        self.navigationController.navigationBar.scrollEdgeAppearance = barAppearance;
        self.tabBarController.tabBar.standardAppearance = tabBarAppearance;
    } else {
        // Fallback on earlier versions
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//        self.tabBarController.tabBar.barTintColor = [UIColor lightGrayColor];
//        self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor]; // not effective >= 11.4.
    }
#endif


    // Get the view's title.                                                    //leg20210429 - TopXNotes2
    // Replace "TopXNotes" as the title of the Note List.                       //leg20220411 - TopXNotes_Catalyst_2
    viewTitle = @"Note Organizer";;

    // Watch for refresh notelist notifications.                                //leg20220629 - TopXNotes_Catalyst_2
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDisplay)
                                                 name:kNotification_Refresh_NoteList object:nil];
    
    // Called after notepad has been updated by CloudKit Sync.                  //leg20220908 - TopXNotes_Catalyst_2
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetDisplayAfterCloudKitUpdate)
                                                 name:kReset_Display_After_CloudKit_Update object:nil];
    
    // Hook-up to model which is now owned by AppDelegate.                      //leg20210429 - TopXNotes2
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    self.model = appDelegate.model;

    // Establish Edit button as left button of navigation bar
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    NSString *tabTitle = self.navigationItem.title;

    // Add newNote button only to TopXNotes… tab.                               //leg20210416 - TopXNotes2
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                  target: self action:@selector(newNote)];
    }

    // Create UIImages.
    self.unLockedPadLockImage = [UIImage imageNamed:@"unlock.png"];             //leg20121204 - 1.3.0
    self.lockedPadLockImage = [UIImage imageNamed:@"lock.png"];                 //leg20121204 - 1.3.0
    self.disclosureOpenedImage = [UIImage imageNamed:@"disclosure_opened.png"]; //leg20220524 - TopXNotes_Catalyst_2
    self.disclosureClosedImage = [UIImage imageNamed:@"disclosure_closed.png"]; //leg20220524 - TopXNotes_Catalyst_2

    // Reading Defaults                                                         //leg20121017 - 1.2.2
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    // Get current note sort type                                               //leg20121017 - 1.2.2
    NSNumber *sortTypeNumber;
    if (!(sortTypeNumber = [savedSettingsDictionary objectForKey:kSortType_Key])) {
        // Set default sort type.
        sortTypeNumber = [NSNumber numberWithInt:kDefaultSortType];
    }
    
    [savedSettingsDictionary setObject:sortTypeNumber forKey:kSortType_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Set sort type
    sortType = [[savedSettingsDictionary objectForKey:kSortType_Key] intValue];
    
        
// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.
    
//•••• removed - not needed for Email ••••
//    // Button to create new Group.                                              //leg20220509 - TopXNotes_Catalyst_2
////    UIBarButtonItem * newGroupBarButtonItem = [[UIBarButtonItem alloc]
////                                                initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newGroup)];
//    // Set copied SFSymbol plus_folder.png for Add Group tool in TopXNotes      //leg20220824 - TopXNotes_Catalyst_2
//    //  sort toolbar.
//    UIImage *addGroupImage = [UIImage imageNamed:@"plus_folder.png"];
//    UIBarButtonItem *newGroupBarButtonItem = [[UIBarButtonItem alloc]
//                                      initWithImage:addGroupImage
//                                              style:UIBarButtonItemStylePlain
//                                             target:self
//                                             action:@selector(newGroup)];
//    [newGroupBarButtonItem setTag:3];
//
//    // Button to build Group Tree test data.                                    //leg20220514 - TopXNotes_Catalyst_2
//    UIBarButtonItem * buildTreeBarButtonItem = [[UIBarButtonItem alloc]
//                                                initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(buildGroupMockData)];
//    [buildTreeBarButtonItem setTag:4];
//
//    // Button to auto backup.                                    //leg20220514 - TopXNotes_Catalyst_2
//    UIBarButtonItem * autoBackupBarButtonItem = [[UIBarButtonItem alloc]
//                                                initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(makeAutoBackup)];
//    [autoBackupBarButtonItem setTag:5];
//
//    // Button to traverse the Group Tree test data.                             //leg20220515 - TopXNotes_Catalyst_2
//    UIBarButtonItem * traverseTreeBarButtonItem = [[UIBarButtonItem alloc]
//                                                initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(traverseTree)];
//    [traverseTreeBarButtonItem setTag:7];
//•••• removed - not needed for Email ••••
    
    // Add ability to sort note list by Title or Date.                          //leg20121017 - 1.2.2
    
    // Button to control type of sort.
    UIBarButtonItem * sortFieldBarButtonItem = [[UIBarButtonItem alloc]
//                                                initWithTitle:@"Sort by Date" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
                                                initWithTitle:@"Sort Date" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortFieldBarButtonItem setTag:1];
    
    // Button to control direction of sort.
    UIBarButtonItem * sortDirectionBarButtonItem = [[UIBarButtonItem alloc]
//                                                initWithTitle:@"Ascending" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
                                                initWithTitle:@"Ascend" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortDirectionBarButtonItem setTag:2];
    
    // Spacer button
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    self.navigationController.toolbarHidden=NO;
    self.navigationController.toolbar.barStyle = UIBarStyleDefault;             //leg20211208 - TopXNotes_Catalyst_2
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.navigationController.toolbar.barTintColor = [UIColor lightGrayColor];  //leg20211208 - TopXNotes_Catalyst_2
    self.navigationController.toolbar.translucent = YES;

//    UIToolbarAppearance *toolBarAppearance = [[UIToolbarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
//    toolBarAppearance.backgroundColor = [UIColor whiteColor];
//    self.navigationController.toolbar.standardAppearance = toolBarAppearance;

//    if (IS_OS_7_OR_LATER)                                                       //leg20140205 - 1.2.7
//        self.navigationItem.title = @"";    // Blank title area for iOS 7.
    
//•••• removed - not needed for Email ••••
//    // Toolbar variations.
//    self.toolbarItems = [NSArray arrayWithObjects:newGroupBarButtonItem, flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, buildTreeBarButtonItem, nil];
//    self.toolbarItems = [NSArray arrayWithObjects:newGroupBarButtonItem, flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, buildTreeBarButtonItem, flexiableItem, autoBackupBarButtonItem, nil]; //leg20220514 - TopXNotes_Catalyst_2
//    self.toolbarItems = [NSArray arrayWithObjects:flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, nil];
//
//    // Allow alternate toolbar with .pch macro setting.                         //leg20220818 - TopXNotes_Catalyst_2
//#if SHOW_MOCK_DATA_TOOLS
//    // Experimental Toolbars.
//    self.toolbarItems = [NSArray arrayWithObjects:newGroupBarButtonItem, flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, buildTreeBarButtonItem, flexiableItem, autoBackupBarButtonItem, flexiableItem, traverseTreeBarButtonItem, nil]; //leg20220515 - TopXNotes_Catalyst_2
//#else
//    // Production Toolbar.
//    self.toolbarItems = [NSArray arrayWithObjects:newGroupBarButtonItem, flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, nil];
//#endif
//•••• removed - not needed for Email ••••
    
    self.toolbarItems = [NSArray arrayWithObjects:flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, nil];

    switch (sortType) {
        case NotesSortByDateAscending:
//            [sortFieldBarButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
//            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            [sortFieldBarButtonItem setTitle:@"Sort Date"];                //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Ascend"];
            break;
            
        case NotesSortByDateDescending:
//            [sortFieldBarButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
//            [sortDirectionBarButtonItem setTitle:@"Descending"];
            [sortFieldBarButtonItem setTitle:@"Sort Date"];                //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Descend"];
            break;
            
        case NotesSortByTitleAscending:
//            [sortFieldBarButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
//            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            [sortFieldBarButtonItem setTitle:@"Sort Title"];               //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Ascend"];
            break;
            
        case NotesSortByTitleDescending:
//            [sortFieldBarButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
//            [sortDirectionBarButtonItem setTitle:@"Descending"];
            [sortFieldBarButtonItem setTitle:@"Sort Title"];                    //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Descend"];
            break;
            
        default:
            break;
    }
        
    // If we are in "TopXNotes" tab, do the auto backup process
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2

        // Get iPhone device information
        UIDevice *device = [UIDevice currentDevice];

        // Use our generated unique identifier as a replacement for the deprecated  //leg20130514 - 1.2.6
        //  [device uniqueIdentifier].
        NSString *uniqueIdentifier = [model deviceUDID];                    //leg20140116 - 1.2.6
        
        NSString *name = [device name];
        NSString *systemName = [device systemName];
        NSString *localizedModel = [device localizedModel];
        NSString *deviceModel = [device model];
        NSString *systemVersion = [device systemVersion];
        NSLog(@"Device UDID: %@, Model: %@, Localized Model: %@, Name: %@", uniqueIdentifier, deviceModel, localizedModel, name);
        NSLog(@"System Name: %@, System Version: %@", systemName, systemVersion);
        
//        // Fix notepad by clearing nil objects                                  //leg20131204 - 1.2.6
//        if ([[model notePadVersion] integerValue] <= kNotePadVersion3) {
//            for (int i=0; i < [model numberOfNotes]; i++) {                     //leg20130314 - 1.2.3
//                Note *note = [model    getNoteForIndex:i];
////                BOOL noteChanged = NO;
//                if (note.noteID == nil) {
//                    note.noteID = [NSNumber numberWithUnsignedLong:0];
////                    noteChanged = YES;
//                }
//                if (note.groupID == nil) {
//                    note.groupID = [NSNumber numberWithUnsignedLong:0];
////                    noteChanged = YES;
//                }
//                if (note.syncFlag == nil) {
//                    note.syncFlag = [NSNumber numberWithBool:NO];
////                    noteChanged = YES;
//                }
//                if (note.needsSyncFlag == nil) {
//                    note.needsSyncFlag = [NSNumber numberWithBool:NO];
////                    noteChanged = YES;
//                }
//                [model replaceNoteAtIndex:i withNote:note];
//            }
//        }

        // Must save model here in case this was a new notepad with no notes in it yet, else there
        //  is no representation on disk which screws Sync up.
        [model saveData];                    // Insure deviceUDID is saved
    }
    
//    [self reloadDisplay];                                                          //leg20121022 - 1.2.2
    // Reload the tableview without sorting.                                    //leg20220629 - TopXNotes_Catalyst_2
    [self reloadDisplayNoSort];

    //#if TARGET_OS_UIKITFORMAC
//    // Navigation bar not needed with a toolbar.                                //leg20220413 - TopXNotes_Catalyst_2
//    [self.navigationController setNavigationBarHidden:YES];
//#endif

}

// MARK: For macOS target save and restore last selected note index.

#if TARGET_OS_UIKITFORMAC

// ???: Note that -viewDidAppear does not seem to happen for iOS,               //leg20220916 - TopXNotes_Catalyst_2
// ???:  but does for macOS.
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

//    // If we are in "TopXNotes" tab, restore selection to last note selected.   //leg20220217 - TopXNotes_Catalyst_2
//    if ([self.navigationItem.title hasPrefix:@"TopXNotes"]) {   // •• this no longer works
    
    // If we are in "Notes" tab, restore selection to last note selected.       //leg20220916 - TopXNotes_Catalyst_2
    NSString * tabTitle = self.tabBarController.tabBar.items[self.tabBarController.selectedIndex].title;
    if ([tabTitle hasPrefix:@"Notes"]) {
        
        // Re-select the last selected note.                                    //leg20220916 - TopXNotes_Catalyst_2
        NSInteger row = [self.tableView numberOfRowsInSection:0];
        
        // Sanity check to see if row still exists after CloudKit update.
        row = [self getLastSelectedNoteIndex] > row-1 ? 0 : [self getLastSelectedNoteIndex];
        NSIndexPath *idx = [NSIndexPath indexPathForRow:row inSection:0];

        // Fix Issue #040 - Encrypting Note Puts App in Unbreakable Loop        //leg20220314 - TopXNotes_Catalyst_2
        //  Protected notes can't be re-selected because the decrypt dialog
        //  would be presented recursively without end.
        Note* note = [model getNoteForIndex: idx.row];
        // Fix issue where last selected note was not being reselected after    //leg20220329 - TopXNotes_Catalyst_2
        //  Quit and Restart or when switching back from another TopXNotes
        //  Select Tab.
        if (![note.protectedNoteFlag boolValue]) {
            [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            [self tableView:self.tableView didSelectRowAtIndexPath:idx];
        }
    }
}

#endif

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];

    // Refresh note list
    [self.tableView reloadData];
    
    // Add note count to notepad's title.                                       //leg20210429 - TopXNotes2
    self.navigationItem.title = [NSString stringWithFormat:@"%@ (%ld notes)",
                                    viewTitle,
                                    (long)[self.model numberOfNotes]];

    // Set Notes tab badge value to the number of un-synced notes.              //leg20110421 - 1.0.4
//•••• removed - not needed for Email ••••    [self updateBadgeValue];    // restore badge value for TopXNotesPro         //leg20150924 - 1.5.0

    // Get Encryption setting.                                                  //leg20210607 - TopXNotes2
    encryptionStatus = [savedSettingsDictionary objectForKey: kEncryptionStatus_Key];
    if (encryptionStatus == nil) {
        encryptionStatus = [NSNumber numberWithBool:NO];
        [savedSettingsDictionary setObject:encryptionStatus forKey:kEncryptionStatus_Key];
    }

// Moved this code from -viewDidLoad.  The code would not be executed after the    //leg20110614 - 1.1.0
//    initial start-up of the App on a device that background/resumes Apps.  This
//    insures that backups are taken when the notepad changes.

    // Check to see if we are in the "TopXNotes" tab.
    //    This check is necessary to keep from doing the auto backup process a
    //    second time after startup.  This is because NotePadView is reused
    //    when the "Email" tab is selected and so viewDidLoad can be entered
    //    a second time.
//    NSString *tabTitle = self.navigationItem.title;
//
//    // If we are in "TopXNotes" tab, do the auto backup process
//    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2
    
    // If we are in "Notes" tab, get some preferences.                          //leg20220916 - TopXNotes_Catalyst_2
    NSString * tabTitle = self.tabBarController.tabBar.items[self.tabBarController.selectedIndex].title;
    // Note that when switching back from Email tab that tabTitle is still
    //  "Email" here.  Doesn't hurt anything since we only wanted to do this
    //  one time.
   if ([tabTitle hasPrefix:@"Notes"]) {
        //noteText.font = [UIFont fontWithName:kNoteTextFont size:kNoteTextFontSize];

        // Reading Defaults
        NSUserDefaults *defaults;
        defaults = [NSUserDefaults standardUserDefaults];
        
        NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
        if (dict != nil)
            savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
        else
            savedSettingsDictionary = [NSMutableDictionary dictionary];

        // Get Font defaults                                                                //leg20110416 - 1.0.4
        NSString * fontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
        if (fontName == nil) {
            fontName = kNoteTextFont;
            [savedSettingsDictionary setObject:fontName forKey:kNoteFontName_Key];
        }
        NSString * fontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
        if (fontSize == nil) {
            fontSize = [NSString stringWithFormat:@"%d", kNoteTextFontSize];
            [savedSettingsDictionary setObject:fontSize forKey:kNoteFontSize_Key];
        }

        // Get Sync settings.                                                    //leg20110502 - 1.0.4
        NSNumber * syncEnabledOrDisabled = [savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key];
        if (syncEnabledOrDisabled == nil) {
            syncEnabledOrDisabled = [NSNumber numberWithBool:NO];
            [savedSettingsDictionary setObject:syncEnabledOrDisabled forKey:kSyncEnabledOrDisabled_Key];
        }
    }
}

//•••• removed - not needed for Email ••••
//// Moved auto backup to separate function, called by kAutoBackups_Notification
////  Notificaton.
//- (void)makeAutoBackup {
//    // Reading Defaults
//    NSUserDefaults *defaults;
//    defaults = [NSUserDefaults standardUserDefaults];
//
//    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
//    if (dict != nil)
//        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
//    else
//        savedSettingsDictionary = [NSMutableDictionary dictionary];
//
//    // Get auto notepad backup data
//    NSMutableArray *array = [dict objectForKey: kAutoBackupsArray_Key];
//    if (array != nil)
//        autoBackupsArray = [NSMutableArray arrayWithArray:array];
//    else
//        autoBackupsArray = [NSMutableArray array];
//
//    // Initialize the next backup number
//    nextAutoBackupNumber = [dict objectForKey: kNextAutoBackupNumber_Key];
//    NSInteger nextBackupNumber = 0;
//    if (nextAutoBackupNumber == nil)  {
//        nextBackupNumber = 1;            // first run
//    } else {
//        nextBackupNumber = [nextAutoBackupNumber intValue];
//    }
//
//    // Prepare to copy notepad
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSError *error = nil;
//    NSString *pathToModelDataFile = [model pathToData];
//    NSString *nextBackupFileName = [NSString stringWithFormat:@"%ld_%@", (long)nextBackupNumber, kBackupModelFileName];
//    NSString *pathToBackupModelDataFile = [self pathToAutoBackup:nextBackupFileName];
//    NSInteger lastBackupIndex;
//    NSString *lastBackupFileName;
//    NSString *pathToLastBackupModelDataFile;
//    NSDictionary *fileAttributesDictionary;
//    NSDate *nextBackupModificationDate;
//    NSDate *lastBackupModificationDate;
//    BOOL result = NO;
//
//    if ([autoBackupsArray count] > 0) {
//        lastBackupIndex  = [autoBackupsArray count] < kNumberOfAutoBackups ? [autoBackupsArray count]-1 : kNumberOfAutoBackups-1;
//        lastBackupFileName = [autoBackupsArray objectAtIndex:lastBackupIndex];
//        pathToLastBackupModelDataFile = [self pathToAutoBackup:lastBackupFileName];
//
//        // Get the modification date of the current notepad (would be next backup)
//        fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];
//        nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];
//
//        // Get the modification date of the last backup
//        fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToLastBackupModelDataFile error:&error];
//        lastBackupModificationDate = [fileAttributesDictionary fileModificationDate];
//
//        // if the modification date of the last backup matches that of the current notepad, no need to back it up
//        if ([nextBackupModificationDate isEqualToDate:lastBackupModificationDate])
//            return;
//    }
//
//    // Make a backup copy of the notepad
//    [fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
//    [fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];
//    if ([error code] != 0) {
//        NSLog(@"Error making a backup of notepad file! Error code=%ld", (long)[error code]);
//        return;
//    }
//
//    // Set the modification date of the backed-up notepad to the same as the current notepad model.        //leg20110614 - 1.1.0
//    fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];        //leg20110629 - 1.1.0
//    nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];                            //leg20110629 - 1.1.0
//    fileAttributesDictionary = [NSDictionary dictionaryWithObject:nextBackupModificationDate forKey:NSFileModificationDate];
//    result = [fileManager setAttributes:fileAttributesDictionary ofItemAtPath:pathToBackupModelDataFile error:&error];
//    if ([error code] != 0 || !result) {
//        NSLog(@"Error setting modification date of backup notepad file! Error code=%ld", (long)[error code]);
//        return;
//    }
//
//    // Add the backup to the list of backups
//    [autoBackupsArray addObject:nextBackupFileName];
//
//    // If backups list is not full yet, just increment to next backup number
//    if ([autoBackupsArray count] < kNumberOfAutoBackups) {
//        nextAutoBackupNumber = [NSNumber numberWithInt:(int)++nextBackupNumber];
//    } else {
//
//        // Backups list is full, reset backup number and get rid of oldest backup
//        if (nextBackupNumber < kNumberOfAutoBackups) {
//            nextAutoBackupNumber = [NSNumber numberWithInt:(int)++nextBackupNumber];
//        } else {
//
//            // start back with 1st backup number
//            nextAutoBackupNumber = [NSNumber numberWithInt:1];
//        }
//
//        // remove oldest backup
//        if ([autoBackupsArray count] > kNumberOfAutoBackups)
//            [autoBackupsArray removeObjectAtIndex:(NSUInteger)0];
//    }
//
//    // Update the backup settings
//    [savedSettingsDictionary setObject:nextAutoBackupNumber forKey:kNextAutoBackupNumber_Key];
//    [savedSettingsDictionary setObject:autoBackupsArray forKey:kAutoBackupsArray_Key];
//
//    // Save settings
//    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//•••• removed - not needed for Email ••••

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//#pragma unused (interfaceOrientation)
//
//    // Return YES for supported orientations
//
//    return YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
                                     // Release anything that's not essential, such as cached data
    NSLog(@"EmailTableViewController didReceiveMemoryWarning");
}


#pragma mark - UITableViewDelegate - Table Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

    return [model.displayList count];                                           //leg20220520 - TopXNotes_Catalyst_2
}

// Added heightForRowAtIndexPath() to silence: "Warning once only: Detected a   //leg20210513 - TopXNotes2
//  case where constraints ambiguously suggest a height of zero for a table
//  view cell's content view." after constraints for the cell were added
//  programatically.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cellData;
    Note* note;
    Group* group;
    NSUUID *node;
    cellData = [model.displayList objectAtIndex:indexPath.row];

    // Determine whether Group or Note cell.
    if ([cellData isKindOfClass:[Note class]]) {
        note = (Note*)cellData;
        node = note.noteUUID;
    } else {
        group = (Group*)cellData;
        node = group.groupUUID;
    }

    // Node is hidden if an ancestor is collapsed.                              //leg20220524 - TopXNotes_Catalyst_2
    if ([model isNodeWithinCollapsedAncestor:node]) {
        return 0;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

// Reorder Note or Group's position in table view.                              //leg20220606 - TopXNotes_Catalyst_2
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
                                                  toIndexPath:(NSIndexPath *)destinationIndexPath {
    id cellDataSource, cellDataDestination;
    Note* note;
    Group* sourceGroup, *destinationGroup;

    [tableView beginUpdates];

    // Retreive destination Note and Group data from displayList.
    cellDataDestination = [model.displayList objectAtIndex:destinationIndexPath.row-1];

    // Determine whether target is Group or Note cell.
    if ([cellDataDestination isKindOfClass:[Group class]]) {
        destinationGroup = (Group*)cellDataDestination;

        // Retreive source Note and Group data from displayList.
        cellDataSource = [model.displayList objectAtIndex:sourceIndexPath.row];
        
        // Determine whether Group or Note cell being moved.
        if ([cellDataSource isKindOfClass:[Note class]]) {
            note = (Note*)cellDataSource;
            
            // Move Note to destination in Group tree.
            [model moveNoteOrGroup:note.noteUUID
                           fromGroup:note.groupUUID
                             toGroup:destinationGroup.groupUUID];
        } else {
            sourceGroup = (Group*)cellDataSource;
            
            // Move Group to destination in Group tree.
            [model moveNoteOrGroup:sourceGroup.groupUUID
                           fromGroup:sourceGroup.parentGroupUUID
                             toGroup:destinationGroup.groupUUID];
        }
        
        // Notify notepad to refresh.
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    }

    [tableView endUpdates];
}

// Adjust proposed destination of Note or Group to be moved so that is moved    //leg20220606 - TopXNotes_Catalyst_2
//  to a valid Group.
- (NSIndexPath *)tableView:(UITableView *)tableView
        targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
        toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    id cellDataDestination;
    Note *note;
    Group *group;
    NSInteger rowOfGroup = NSNotFound;
    
    // Retreive proposed destination item.
    cellDataDestination = [model.displayList objectAtIndex:proposedDestinationIndexPath.row];

    // Determine whether destination is Group or Note cell.
    if ([cellDataDestination isKindOfClass:[Note class]]) {
        note = (Note *)cellDataDestination;
        rowOfGroup = [model getIndexOfDisplayItem:note.groupUUID];
        if (rowOfGroup != NSNotFound) {
            // Move item to note's parent group.
            NSLog(@"Destination index is parent group of a note = %ld, title: %@", rowOfGroup, [model getGroupForUUID:note.groupUUID].title);
        }
    } else if ([cellDataDestination isKindOfClass:[Group class]]) {
        group = (Group *)cellDataDestination;
        rowOfGroup = [model getIndexOfDisplayItem:group.groupUUID];
        if (rowOfGroup != NSNotFound) {
            // Move item to chosen group.
            NSLog(@"Chosen destination index of group = %ld, title: %@", rowOfGroup, group.title);
        }
    }
    
    // !!!: Workaround to prevent crash if moving item into empty Group at end of tableView list.
    // TODO: Revisit issue to find a way to allow the move to happen.
    // Prevent crash on attempt to -moveRowAtIndexPath item into empty Group    //leg20220608 - TopXNotes_Catalyst_2
    //  which is the last physical item in the tableView list by
    //  disallowing move.
    if (rowOfGroup == [model.displayList count]-1) {
        NSLog(@"Proposed destination index is end of list = %ld",rowOfGroup);
        rowOfGroup = sourceIndexPath.row;
    } else
        rowOfGroup++;

    NSLog(@"Proposed destination index of group = %ld",rowOfGroup);
    return [NSIndexPath indexPathForRow:rowOfGroup
                              inSection:proposedDestinationIndexPath.section];
}

// Allow all notes or groups to be moved except root group.                     //leg20220606 - TopXNotes_Catalyst_2
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    id cellData;
    Group* group;

    // Retreive Note and Group data from displayList.
    cellData = [model.displayList objectAtIndex:indexPath.row];

    // Determine whether to make Group or Note cell.
    if ([cellData isKindOfClass:[Group class]]) {
        group = cellData;
        if ([group.groupUUID.UUIDString isEqualToString:kRootGroup])
            return NO;
    }
    
    return YES;
}

//leg20220601 - TopXNotes_Catalyst_2
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    return UITableViewCellEditingStyleInsert;
    return UITableViewCellEditingStyleDelete;
//    return UITableViewCellEditingStyleNone;
}

//leg20220601 - TopXNotes_Catalyst_2
- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

// Populate prototype cells for split view.                                     //leg20220517 - TopXNotes_Catalyst_2
//  Refactored to generate Group cells as well as Note cells.
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
//    NSInteger depth;
    id cellDataFromArray;
    Note* note;
    Group* group;

    // Retreive Note and Group data from list built at -viewDidLoad with
    //  -treeTraverse.
    cellDataFromArray = [model.displayList objectAtIndex:row];

    // Determine whether to make Group or Note cell.
    if ([cellDataFromArray isKindOfClass:[Note class]]) {
        note = (Note*)cellDataFromArray;
        
        // Use custom cell layout to populate data fields so that cell
        //  is indentable via setFrame in NoteListCell and GroupListCell.
        NoteListCell *cell = nil;
        if (cell == nil)
        {
            if ([encryptionStatus boolValue])
                cell = [[NoteListCell alloc] initWithReuseIdentifier:@"Note_Lock_Cell_ID"];
            else
                cell = [[NoteListCell alloc] initWithReuseIdentifier:@"Note_Cell_ID"];
        }
            
        cell.delegate = self;
            
        // Check to see if it is a synced note
        if ([note.needsSyncFlag unsignedLongValue] != 0 &&
                ![note.protectedNoteFlag boolValue])
            cell.dateLabel.textColor = [UIColor redColor];  // Show it is a synced note
        else
            cell.dateLabel.textColor = [UIColor blueColor]; // Show it is not a synced note

        // Display title and modification date.
        NSString* dateString = [NSString longDate:note.date];
        cell.noteTitleLabel.text = note.title;
                
        if (![note.protectedNoteFlag boolValue]) {
            // Preview note text after title, if any.
            if ([note.noteText length] > 0) {
                NSString *preview = [note.noteText substringFromIndex:[note.title length]];
                preview = [preview stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                cell.infoLabel.text = preview;
            } else {
                cell.infoLabel.text = @"";
            }
        } else {
            cell.infoLabel.text = kEncrypted_Placeholder_Notice;
        }
        
        cell.dateLabel.text = dateString;
        
        if (IS_OS_7_OR_LATER)
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;

        // Indicate protection status of note.
        cell.encryptionStatus = encryptionStatus;
        if ([encryptionStatus boolValue]) {
            if ([note.protectedNoteFlag boolValue])
                [cell.padLockButton setBackgroundImage:self.lockedPadLockImage forState:UIControlStateNormal];
            else
                [cell.padLockButton setBackgroundImage:self.unLockedPadLockImage forState:UIControlStateNormal];
        }
        
        // Indent the note based on its depth in the Group tree.
        cell.indentationLevel = note.depth;
        cell.indentationWidth = 20;
        

        // Hide note if an ancestor is collapsed.                               //leg20220524 - TopXNotes_Catalyst_2
        if ([model isNodeWithinCollapsedAncestor:note.noteUUID]) {
            cell.hidden = YES;
        } else
            cell.hidden = NO;

        return cell;
        
    } else {
        group = (Group*)cellDataFromArray;

        // Use custom cell layout to populate data fields so that
        //  cell is indentable via setFrame in.
        GroupListCell *cell = nil;
        if (cell == nil)
            cell = [[GroupListCell alloc] initWithReuseIdentifier:@"Group_Cell_ID"];
                    
        cell.delegate = self;
            
        cell.groupTitleLabel.text = group.title;
        
        if (IS_OS_7_OR_LATER)
            cell.selectionStyle = UITableViewCellSelectionStyleDefault;

        // Indicate disclosure status of group.
        cell.disclosureStatus = group.expanded;
        if ([cell.disclosureStatus boolValue])
            [cell.disclosureButton setBackgroundImage:self.disclosureOpenedImage forState:UIControlStateNormal];
        else
            [cell.disclosureButton setBackgroundImage:self.disclosureClosedImage forState:UIControlStateNormal];

        // Indent the group based on its depth in the Group tree.
        cell.indentationLevel = group.depth;
        cell.indentationWidth = 20;

        // Hide group if an ancestor is collapsed.                              //leg20220524 - TopXNotes_Catalyst_2
        if ([model isNodeWithinCollapsedAncestor:group.groupUUID]) {
            cell.hidden = YES;
        } else
            cell.hidden = NO;

        return cell;
    }
}

// Delete items that were deferred so that they can be deleted from bottom-up.  //leg20220610 - TopXNotes_Catalyst_2
- (void)deferredDeletes:(NSMutableArray *)deletesArray {
    NSEnumerator *enumerator = [deletesArray objectEnumerator];
    id anObject;
    
    while (anObject = [enumerator nextObject]) {
        NSIndexPath *indexPath = anObject;
        NSInteger index;
        
        id item = [model getDisplayItemForIndex:indexPath.row];
        if ([item isKindOfClass:[Group class]]) {
            Group *group = item;

            // Remove the group from the Group tree.
            [model removeDisplayItemAtIndex:indexPath.row];

            // Remove the group from groupList.
            index = [model getIndexOfGroupForUUID:group.groupUUID];
            [model removeGroupAtIndex:index];
        } else {
            Note *note =item;

            // Remove the note from the Group tree.
            [model removeDisplayItemAtIndex:indexPath.row];
            
            // Remove the note from the noteList.
            index = [model getIndexOfNoteForUUID:note.noteUUID];
            [model removeNoteAtIndex:index];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath { //leg20220615 - TopXNotes_Catalyst_2
    return YES;
}


#pragma mark ** SORT Array By IndexPaths Compare Function

NSComparisonResult emailSortByIndexPath(NSIndexPath *path1, NSIndexPath *path2, void *context)
{
    BOOL sortAscending = (*((int*) context) == 1);
    if (sortAscending)
        return [path1 compare:path2];
    else
        return [path2 compare:path1];
}

// Commit editing: note deletion.
//  Modified to handle Note deletion from Group.                                //leg20220602 - TopXNotes_Catalyst_2
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
    forRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    // Proceed with delete only if a Note row is selected.
    NSInteger row = indexPath.row;
    id cellData;
    NSInteger displayIndex;
    NSMutableArray *cellsToDeleteArray = [NSMutableArray arrayWithCapacity:100];

    // Retreive Note or Group data.
    cellData = [model.displayList objectAtIndex:row];

//    [tableView beginUpdates];
    
    // Determine whether Group or Note cell.
    if ([cellData isKindOfClass:[Group class]]) {
        Group *group = (Group *)cellData;

        if ([group.groupUUID.UUIDString isEqualToString:kRootGroup]) {
            // Disallow deleting Root Group.                                    //leg20220613 - TopXNotes_Catalyst_2
            return;
        }

        [model deleteGroupContents:group andSaveIndex:cellsToDeleteArray];

        // Remove the group from its parent group.
        [model removeGroup:group fromGroup:group.parentGroupUUID];
        
        // Remove the group from the Group tree.
        displayIndex = [model getIndexOfDisplayItem:group.groupUUID];

        // Keep track of tableview cells needing to be deleted at commit.
        [cellsToDeleteArray addObject:[NSIndexPath indexPathForRow:displayIndex inSection:0]];

//        int sortAscending = false ? 1 : 0;
        int sortAscending = false;
        [cellsToDeleteArray sortUsingFunction:emailSortByIndexPath context:&sortAscending];

        [self deferredDeletes:cellsToDeleteArray];
        
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithArray:cellsToDeleteArray]
                              withRowAnimation:UITableViewRowAnimationFade];

        // Refresh note count in notepad's title.
        self.navigationItem.title = [NSString stringWithFormat:@"%@ (%ld notes)",
                                        viewTitle,
                                        (long)[self.model numberOfNotes]];
    } else {
        Note *note = (Note *)cellData;
        NSInteger noteIndex = [model getIndexOfNoteForUUID:note.noteUUID];
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            // In order to prevent crash ignore delete if only 1 note in notepad.
            if ([model.noteList count] == 1) return;

            // Remove the note from its parent group.
            [model removeNote:note fromGroup:note.groupUUID];
            
            // Remove the note from the noteList.
            [model removeNoteAtIndex:noteIndex];
            
            // Remove the note from the Group tree.
            [model removeDisplayItemAtIndex:indexPath.row];

            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];

            // Refresh note count in notepad's title.
            self.navigationItem.title = [NSString stringWithFormat:@"%@ (%ld notes)",
                                            viewTitle,
                                            (long)[self.model numberOfNotes]];
        }

// FIXME: didSelectRowAtIndexPath cause exceptions after modification for Groups - Revisit!
//         // Select row previous to deleted note as long there is at least
//         //  1 note left in notepad and not in tableView.editing mode.
//#if TARGET_OS_UIKITFORMAC
//         if ([self tableView:self.tableView numberOfRowsInSection:indexPath.section] > 0 && !tableView.editing) {
//
//            // Prevent crash if deleting row 0 note.                            //leg20220124 - TopXNotes_Catalyst_2
//            row = row-1 < 0 ? row : row-1;
//            NSIndexPath *idx = [NSIndexPath indexPathForRow:row inSection:indexPath.section];
//
//            [self.tableView selectRowAtIndexPath:idx animated:YES scrollPosition:UITableViewScrollPositionNone];
//            [self tableView:self.tableView didSelectRowAtIndexPath:idx];
//            [self tableView:self.tableView didHighlightRowAtIndexPath:idx];
//        }
//#endif
            
//        [tableView endUpdates];
        
            // Notify notepad to refresh.
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    }
}

// Perform an action for split view table view cell.                            //leg20220517 - TopXNotes_Catalyst_2
//  Refactored to generate Group cells as well as Note cells.
//
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)
    displayTreeIndex = indexPath.row;                                           //leg20220527 - TopXNotes_Catalyst_2
    NSInteger depth;
    id cellData;
    Note* note;
    Group* group;

    // Retreive Note and Group data from displayList.
    cellData = [model.displayList objectAtIndex:displayTreeIndex];              //leg20220527 - TopXNotes_Catalyst_2
    depth = 0;
    
    // Determine whether to make Group or Note cell.
    if ([cellData isKindOfClass:[Note class]]) {
        note = (Note*)cellData;
        NSInteger row = [model getIndexOfNoteForUUID:note.noteUUID];

        NSString *tabTitle = self.navigationItem.title;
        
        // Unknown why tabTitle no longer is set after previous assignment.     //leg20220525 - TopXNotes_Catalyst_2
        //  Make sure it is.
        if (self.tabBarController.selectedIndex == 0) {
            tabTitle = @"TopXNotes";
        } else if (self.tabBarController.selectedIndex == 2) {
            tabTitle = @"Email";
        }

// Save last selected note index regardless of target os.                       //leg20220916 - TopXNotes_Catalyst_2
//#if TARGET_OS_UIKITFORMAC
        // Save the index of the last note selected.
        if ([tabTitle hasPrefix:@"TopXNotes"]) {
//            [self saveLastSelectedNote:(int)row];
//            [self saveLastSelectedNote:(int)treeRow];
            [self saveLastSelectedNote:(int)displayTreeIndex];                  //leg20220527 - TopXNotes_Catalyst_2
        }
//#endif
    
        // If in the Email tab then send the note to Mail,
        //    else, put the note in a NoteView
        if ([tabTitle hasPrefix:@"Email"]) {
            noteIndex = (int)row;
            
            // Set parent group of note.                                        //leg20220526 - TopXNotes_Catalyst_2
            selectedGroupUUID = note.groupUUID;

            // Trim whitespace from front and back of note title.
            NSString *trimmedNoteTitle = [note.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            // Can't Email protected notes from here because it would require
            //  the user to supply the password.  Email protected notes while
            //  in view/edit mode.
            if ([note.protectedNoteFlag boolValue]) {
                // The response to this dialog is processed in UIActionSheetDelegate (sendEmail)
                noEmailLockedActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat: @"Note \"%@\" is a Protected note. Protected notes can only be Emailed while being viewed.", trimmedNoteTitle]
                        delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Cancel" otherButtonTitles:nil];
                noEmailLockedActionSheet.actionSheetStyle = UIActionSheetStyleDefault;

                // Show the dialog coming out of the tab bar
                UITabBar *tabBar = self.tabBarController.tabBar;
                [noEmailLockedActionSheet showFromTabBar:tabBar];
            } else {
                // Remove deprecated UIActionSheet usage and recode dialog asking   //leg20210416 - TopXNotes2
                //  permission to proceed with Email action.
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: @"Email the note \"%@\"?", trimmedNoteTitle]
                                               message:@""
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                   handler:^(UIAlertAction * action) {
                    [self sendEmail];
                    [tableView deselectRowAtIndexPath:indexPath animated:YES];
                }];
                
                 UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                    handler:^(UIAlertAction * action) {
                     [tableView deselectRowAtIndexPath:indexPath animated:YES];
                 }];

                [alert addAction:defaultAction];
                [alert addAction:cancelAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        } else {
            // If note is protected, get password and decrypt it.
            if ([note.protectedNoteFlag boolValue]) {
//•••• removed - not needed for Email ••••
//                lockUnlockNoteIndex = row;
//                noteIndex = (int)row;
//                passwordHint = note.passwordHint;
//                passwordDialogType = GetPassword;
//                noteTitle = note.title;
//
//                [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//•••• removed - not needed for Email ••••
                
            } else {
                // Make the note edit view the current view.
                noteIndex = (int)row;

                // Give user oppurtunity to prevent loading large note.
                if ([note.noteHTML length] > 500000 || [note.noteText length] > 500000) {
                    [self askBeforeLoading:note.title];
                } else {
                    [self performSegueWithIdentifier:@"Edit_Note_Segue" sender: self];
                }
            }
        }
    } else {
        // No action required for a group.
        group = (Group*)cellData;
    }
}

// Alert to ask user whether or not to proceed with loading large note.         //leg20220408 - TopXNotes_Catalyst_2
-(void)askBeforeLoading:(NSString *)noteName
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Large Note (over 500K) Warning!"
                                                                   message:[NSString stringWithFormat:@"Note \"%@\" may take a long time to load and loading cannot be stopped once begun!", noteName]
#if TARGET_OS_UIKITFORMAC
                                                            preferredStyle:UIAlertControllerStyleAlert];
#else
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
#endif

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        [self performSegueWithIdentifier:@"Edit_Note_Segue" sender: self];
    }];
    
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {
         // Do nothing––action cancelled!
     }];

    [alert addAction:defaultAction];
    [alert addAction:cancelAction];

    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - GroupListCellDelegate

// Toggle Hide/Show contents of Group based on disclosure button.               //leg20220525 - TopXNotes_Catalyst_2
- (void)disclosureTouchedAt: (GroupListCell*)cell withButton:(UIButton*)button
{
    // Based on current disclosure status, toggle between opened/closed.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];

    Group* group = [model getDisplayGroupForIndex: indexPath.row];

    // Indicate disclosure status of group.
    cell.disclosureStatus = group.expanded;
    BOOL isExpanded = [cell.disclosureStatus boolValue];
    if (isExpanded)
        [button setBackgroundImage:self.disclosureClosedImage forState:UIControlStateNormal];
    else
        [button setBackgroundImage:self.disclosureOpenedImage forState:UIControlStateNormal];

    // Toggle open/closed state of group.
    isExpanded ^= YES;
    cell.disclosureStatus = [NSNumber numberWithBool:isExpanded];
    group.expanded = [NSNumber numberWithBool:isExpanded];

    // Update Group in noteList and displayList.
    [model replaceGroupAtIndex:[model getIndexOfGroupForUUID:group.groupUUID] withGroup:group];
    [model replaceDisplayGroupAtIndex:indexPath.row withGroup:group];

    // Refresh tableview.
//    [self reloadDisplay];
    // Reload the tableview without sorting.                                    //leg20220629 - TopXNotes_Catalyst_2
    [self reloadDisplayNoSort];
}

#pragma mark - NoteListCellDelegate

- (void)padlockTouchedAt: (NoteListCell*)cell withButton:(UIButton*)button
{
//•••• removed - not needed for Email ••••
//    // Based on current protection status of note, toggle between protect/unprotect. //leg20121204 - 1.3.0
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
////    lockUnlockNoteIndex = indexPath.row;
////    Note* note = [model getNoteForIndex: lockUnlockNoteIndex];
//    // Fixed crash after tap on padlock - find true index of note from display. //leg20220620 - TopXNotes_Catalyst_2
//    //  Get note from display list.
//    Note *note = (Note *)[model.displayList objectAtIndex:indexPath.row];
//    lockUnlockNoteIndex = [model getIndexOfNoteForUUID:note.noteUUID];
//    note = [model getNoteForIndex: lockUnlockNoteIndex];
//
//
//    lockUnlockCellButton = button;
//    if ([note.protectedNoteFlag boolValue]) {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue". //leg20210422 - TopXNotes2
//        passwordHint = note.passwordHint;
//        passwordDialogType = RemovePassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    } else {
//        // Pass information to prepareSegue for segue @"Password_Dialog_Segue". //leg20210422 - TopXNotes2
//        passwordDialogType = SetPassword;
//        noteTitle = note.title;
//
//        [self performSegueWithIdentifier:@"Password_Dialog_Segue" sender: self];
//    }
//•••• removed - not needed for Email ••••
}

//•••• removed - not needed for Email ••••
//#pragma mark - Encryption Methods
//
//- (BOOL)verifyPassword:(Note *)note withPasword:(NSString*)password
//{
//    // Validate provided password with encrypted password in note.              //leg20121204 - 1.3.0
//    OSErr result =  noErr;
//    const char *    utf8Password;
//    char *          passwordData;
//    Str31           pPassword = "\p";
//    UInt32          key;
//
//    // Make a buffer to decrypt in and copy encrypted password into it.
//    passwordData = (char*) calloc ([note.encryptedPassword length],sizeof(char));
//    [note.encryptedPassword getBytes:passwordData length:[note.encryptedPassword length]];
//
//    // Convert the password to a p-string and make a key of it.
//    utf8Password = [password UTF8String];
//    memcpy((void *)&pPassword[1], (const void *)&utf8Password[0], (unsigned long)strlen(utf8Password));
//    pPassword[0] = strlen(utf8Password);
//    key = MakeKeyFromPassword(pPassword);
//
//    // Use key to decrypt encrypted password.
//    result = Decrypt(key, (void *)passwordData, (UInt32)[note.encryptedPassword length]);
//
//    // If decrypted password matches password passed in, then password is verified
//    //  and will decrypt associated note text.
//    if (memcmp (utf8Password, passwordData, [password length]) == 0 && result == noErr) {
//        // need to free calloc
//        free(passwordData);
//
//        return YES;
//    } else {
//        // need to free calloc
//        free(passwordData);
//
//        return NO;
//    }
//}
//
//- (BOOL)encryptPassword:(Note *)note withPassword:(NSString*)password
//{
//    // Encrypt provided password to be used to validate password at decryption. //leg20121204 - 1.3.0
//    OSErr result =  noErr;
//    const char *    utf8Password;
//    const char *    textBuffer;
//    Str31           pPassword = "\p";
//    UInt32          key;
//
//    utf8Password = [password UTF8String];
//    textBuffer = [password UTF8String];
//
//    memcpy((void *)&pPassword[1], (const void *)&utf8Password[0], (unsigned long)[password length]);
//
//    pPassword[0] = strlen(utf8Password);
//    key = MakeKeyFromPassword(pPassword);
//
//    result = Encrypt(key, (void *)textBuffer, (int)[password length]);
//    if (result == noErr) {
//        note.encryptedPassword = [NSData dataWithBytes:textBuffer length:[password length]];
//
//        if (DEBUG) {
//            if ([self verifyPassword:note withPasword:password])
//                NSLog(@"correct password");
//            else
//                NSLog(@"wrong password");
//        }
//
//        return YES;
//    } else {
//        return NO;
//    }
//
//}
//
//- (BOOL)encryptNote:(Note *)note withPassword:(NSString*)password
//{
//    // Encrypt note text with provided password.                                //leg20121204 - 1.3.0
//    OSErr result =  noErr;
//    const char *    utf8Password;
//    const char *    textBuffer;
//    const char *    htmlBuffer;
//    Str31           pPassword = "\p";
//
//    // Rich Text Modification:  Encrypt HTML and plain text.                    //leg20211129 - TopXNotes_Catalyst_2
//
//    // The first step is to encrypt the password so that the decrypted data can be
//    //  validated on decryption.
//    if ([self encryptPassword:note withPassword:password]) {
//        utf8Password = [password UTF8String];
//        textBuffer = [note.noteText UTF8String];
//
//        memcpy((void *)&pPassword[1], (const void *)&utf8Password[0], (unsigned long)strlen(utf8Password));
//
//        pPassword[0] = strlen(utf8Password);
//        UInt32 key = MakeKeyFromPassword(pPassword);
//
//        // Encrypt the plain text.
//        result = Encrypt(key, (void *)textBuffer, (UInt32)[note.noteText length]);
//        if (result == noErr) {
//            note.encryptedNoteText = [NSData dataWithBytes:textBuffer length:[note.noteText length]];
//            note.noteText = kEncrypted_Placeholder_Notice;
//
//            htmlBuffer = [note.noteHTML UTF8String];
//
//            // Encrypt the HTML.
//            result = Encrypt(key, (void *)htmlBuffer, (UInt32)[note.noteHTML length]);
//            if (result == noErr) {
//                note.encryptedNoteHTML = [NSData dataWithBytes:htmlBuffer length:[note.noteHTML length]];
//                note.noteHTML = kEncrypted_Placeholder_Notice;
//
//                return YES;
//            }else {
//                return NO;
//            }
//        }else {
//            return NO;
//        }
//    } else {
//        return NO;
//    }
//}
//
//- (BOOL)decryptNote:(Note *)note withPassword:(NSString*)password
//{
//    // Rich Text Modification:  Decrypt HTML and plain text.                    //leg20211129 - TopXNotes_Catalyst_2
//
//    // Decrypt note text with provided password.                                //leg20121204 - 1.3.0
//    OSErr result =  noErr;
//    const char *    utf8Password;
//    Str31           pPassword = "\p";
//    char *          textData;
//    char *          htmlData;
//    UInt32          key;
//
//    // Validate password
//    if ([self verifyPassword:note withPasword:password]) {
//        NSLog(@"correct password");
//    } else {
//        NSLog(@"wrong password");
//        [self alertOKWithTitle:@"Password Validation" message:@"Password incorrect!"];  //leg20210518 - TopXNotes2
//
//        return NO;
//    }
//
//    // Get decryption key.
//    utf8Password = [password UTF8String];
//    memcpy((void *)&pPassword[1], (const void *)&utf8Password[0], (unsigned long)strlen(utf8Password));
//    pPassword[0] = strlen(utf8Password);
//    key = MakeKeyFromPassword(pPassword);
//
//    // Decrypt the plain text.
//    textData = (char*) calloc ([note.encryptedNoteText length],sizeof(char));
//    [note.encryptedNoteText getBytes:textData length:[note.encryptedNoteText length]];
//
//    result = Decrypt(key, (void *)textData, (UInt32)[note.encryptedNoteText length]);
//    if (result != noErr)
//        return NO;
//
//    note.noteText = [[NSString alloc] initWithBytes:(const void *)textData length:(NSUInteger)[note.encryptedNoteText length] encoding:(NSStringEncoding)NSUTF8StringEncoding];
//    free(textData);
//
//    // Decrypt the plain text.
//    htmlData = (char*) calloc ([note.encryptedNoteHTML length],sizeof(char));
//    [note.encryptedNoteHTML getBytes:htmlData length:[note.encryptedNoteHTML length]];
//
//    result = Decrypt(key, (void *)htmlData, (UInt32)[note.encryptedNoteHTML length]);
//    if (result != noErr)
//        return NO;
//
//    note.noteHTML = [[NSString alloc] initWithBytes:(const void *)htmlData length:(NSUInteger)[note.encryptedNoteHTML length] encoding:(NSStringEncoding)NSUTF8StringEncoding];
//    free(htmlData);
//
//    // Fix Issue #003 - Decryption Fails.                                       //leg20211110 - TopXNotes_Catalyst_2
//    //  Replace decrypted note in model temporarily.
//    [model replaceNoteAtIndex:lockUnlockNoteIndex withNote:note];
//
//    return YES;
//}
//
//#pragma mark - PasswordViewControllerDelegate
//
//- (BOOL)didGetUnprotect: (NSString *)password
//{
//    // Use password to decrypt note and remove protection.                      //leg20121204 - 1.3.0
//    BOOL result = NO;
//    Note* note = [model getNoteForIndex: lockUnlockNoteIndex];
//
//    if ([self decryptNote:note withPassword:password]) {
//        UIImage *padLockImage = [UIImage imageNamed:@"unlock.png"];
//        [lockUnlockCellButton setBackgroundImage:padLockImage forState:UIControlStateNormal];
//        note.encryptedPassword = [NSData data];
//        note.decryptedNoteFlag = [NSNumber numberWithBool:NO];
//        note.protectedNoteFlag = [NSNumber numberWithBool:NO];
//        [model replaceNoteAtIndex:lockUnlockNoteIndex withNote:note];
//        result = YES;
//    };
//
//    return result;
//}
//
//- (BOOL)didGetDecrypt: (NSString *)password
//{
//    // Use password to temporarilly decrypt note so that it can be edited.      //leg20121204 - 1.3.0
//    BOOL result = NO;
//    Note* note = [model getNoteForIndex: lockUnlockNoteIndex];
//
//    if ([self decryptNote:note withPassword:password]) {
//        note.decryptedNoteFlag = [NSNumber numberWithBool:YES];
//        note.decryptedPassword = password;
//        [model replaceNoteAtIndex:lockUnlockNoteIndex withNote:note];
//
//        result = YES;
//        // This is now handled by segue @"Edit_Note_Segue".                     //leg20210422 - TopXNotes2
//        noteIndex = (int)lockUnlockNoteIndex;
//        [self performSegueWithIdentifier:@"Edit_Note_Segue" sender: self];
//};
//
//    return result;
//}
//
//- (BOOL)didSetProtect: (NSString *)password withHint:(NSString *)hint
//{
//    // Use password and hint to encrypt note make it protected.                 //leg20121204 - 1.3.0
//    BOOL result = NO;
//    Note* note = [model getNoteForIndex: lockUnlockNoteIndex];
//
//    if ([self encryptNote:note withPassword:password]) {
//        UIImage *padLockImage = [UIImage imageNamed:@"lock.png"];
//        [lockUnlockCellButton setBackgroundImage:padLockImage forState:UIControlStateNormal];
//        note.passwordHint = hint;
//        note.decryptedNoteFlag = [NSNumber numberWithBool:NO];
//        note.protectedNoteFlag = [NSNumber numberWithBool:YES];
//        [model replaceNoteAtIndex:lockUnlockNoteIndex withNote:note];
//        result = YES;
//    };
//
//    return result;
//}
//•••• removed - not needed for Email ••••

#pragma mark - Private Methods

// Sort type changed.                      //leg20121022 - 1.2.2

// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.

- (void)sortControlHit:(id)sender {
    
    UIBarButtonItem * sortButtonItem = sender;
    
    switch (sortButtonItem.tag) {
            
        case 1:     // sort field
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByTitleAscending;
//                    [sortButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
                    [sortButtonItem setTitle:@"Sort Title"];                    //leg20150924 - 1.5.0
                   break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByTitleDescending;
//                    [sortButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
                    [sortButtonItem setTitle:@"Sort Title"];                    //leg20150924 - 1.5.0
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByDateAscending;
//                    [sortButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
                    [sortButtonItem setTitle:@"Sort Date"];                     //leg20150924 - 1.5.0
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByDateDescending;
//                    [sortButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
                    [sortButtonItem setTitle:@"Sort Date"];                     //leg20150924 - 1.5.0
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByDateDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByDateAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByTitleDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByTitleAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    
    // Reading Defaults
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    // Update the Sort settings in user defaults
    [savedSettingsDictionary setObject:[NSNumber numberWithInt:(int)sortType] forKey:kSortType_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self reloadDisplay];
    
    // Save data as it is sorted.                                               //leg20220629 - TopXNotes_Catalyst_2
    [model saveData];
}

//•••• removed - not needed for Email ••••
//// Replace updateBadgeValue with TopXNotes 1.6.0 version.                       //leg20210713 - TopXNotes2
//// Update the tab bar item badge value.
//- (void)updateBadgeValue {
//    NSString *tabTitle = self.navigationItem.title;
//
//    // If we are in "TopXNotes" tab, set the badge.                             //leg20210708 - TopxNotes 1.6
//    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2
//        int unSyncedNotesCount = 0;
//        for (int i=0; i < [model numberOfNotes]; i++) {
//            Note *note = [model    getNoteForIndex:i];
//            if ([note.needsSyncFlag boolValue])                                 //leg20210708 - TopxNotes 1.6
//                unSyncedNotesCount++;
//        }
//
//        if (unSyncedNotesCount > 0)
////            self.tabBarController.tabBar.selectedItem.badgeValue = [NSString stringWithFormat:@"%d", unSyncedNotesCount];   //leg20210713 - TopXNotes2
//            self.tabBarController.tabBar.items[0].badgeValue = [NSString stringWithFormat:@"%d", unSyncedNotesCount];   //leg20210728 - TopXNotes2
//        else
////            self.tabBarController.tabBar.selectedItem.badgeValue = nil;         //leg20210713 - TopXNotes2
//            self.tabBarController.tabBar.items[0].badgeValue = nil;         //leg20210728 - TopXNotes2
//    }
//}
//•••• removed - not needed for Email ••••

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";
    }
    
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

    return appFile;
}


// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
    Note *note = [model getNoteForIndex:noteIndex];

    // Assemble the Email
    NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
    completeSubject = [completeSubject stringByAppendingString:@"\""];
    
    NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
    NSMutableString *body = [NSMutableString stringWithString:note.noteText];

    // encode the strings for email
    [subject encodeForEmail];

    [body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];    // Make sure CRs are recognized
    [body encodeForEmail];

    NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
    NSURL *encodedURL = [NSURL URLWithString:emailMsg];
         
    // Send the assembled message to Mail!
    [[UIApplication sharedApplication] openURL:encodedURL];
}


- (void)sendEmail
{

    // This can run on devices running iPhone OS 2.0 or later
    // The MFMailComposeViewController class is only available in iPhone OS 3.0 or later.
    // So, we must verify the existence of the above class and provide a workaround for devices running
    // earlier versions of the iPhone OS.
    // We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
    // We launch the Mail application on the device, otherwise.
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
}

-(void)displayComposerSheet
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
        
    Note *note = [model getNoteForIndex:noteIndex];

    // Assemble the Email - Don't set any recipients, let user do during composition
    NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
    completeSubject = [completeSubject stringByAppendingString:@"\""];
    
    NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
    
    // Fix Issue #035 - Emailing From Email Tab Produces Email Without //leg20220208 - TopXNotes_Catalyst_2
    //  Linefeeds. Change sending Body as plain text to sending Body as HTML.
//    NSMutableString *body = [NSMutableString stringWithString:note.noteText];
    NSMutableString *body = [NSMutableString stringWithString:note.noteHTML];

    // Modified body preparation to match procedure used in NoteViewController  //leg20150924 - 1.5.0
    //  -displayComposerSheet
    // Change carriage returns to line feeds so they willl be recognized.        //leg20100701 - 1.0.2
    [body replaceOccurrencesOfString:@"\r" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];
    [picker setSubject:subject];
//    [picker setMessageBody:body isHTML:NO];
    [picker setMessageBody:body isHTML:YES];                                    //leg20220208 - TopXNotes_Catalyst_2
    
    // Present the composer
    [[self navigationController] presentViewController:picker animated:YES completion:nil];    //leg20140311 - 1.3.1
    
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
#pragma unused (controller, error)

    NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
        
        [self alertEmailStatus:resultMessage];
}

// Get Top View Controller for Presenting Alerts.                               //leg20210519 - TopXNotes2

- (UIViewController *)topViewController{
  return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
  if (rootViewController.presentedViewController == nil) {
    return rootViewController;
  }

  if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
    UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
    UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
    return [self topViewController:lastViewController];
  }

  UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
  return [self topViewController:presentedViewController];
}

#pragma mark - UIAlertController

// Replace deprecated UIAlertView with UIAlertController.                       //leg20210420 - TopXNotes2
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
            // Do nothing––no action required!
        }];
        
        [alert addAction:defaultAction];

//    [self presentViewController:alert animated:YES completion:nil];
       [self.topViewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
    // open an alert with just an OK button
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
//                            delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//    [alert show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210429 - TopXNotes2
    [self alertOKWithTitle:@"Email Status" message:alertMessage];
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
    // open a alert with an OK and cancel button
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
                            delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}

- (void)alertSimpleAction:(NSString*)alertMessage
{
    // open an alert with just an OK button
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Validation" message: alertMessage
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

    // use "buttonIndex" to decide your action
    //
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        //NSLog(@"Cancel");
    } else {
        //NSLog(@"OK");
        [self sendEmail];
    }
}

@end

//@interface EmailTableTableViewController ()
//
//@end
//
//@implementation EmailTableTableViewController
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//
//    // Uncomment the following line to preserve selection between presentations.
//    // self.clearsSelectionOnViewWillAppear = NO;
//
//    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
//}
//
//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
//    return 0;
//}
//
///*
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
//
//    // Configure the cell...
//
//    return cell;
//}
//*/
//
///*
//// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the specified item to be editable.
//    return YES;
//}
//*/
//
///*
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }
//}
//*/
//
///*
//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
//}
//*/
//
///*
//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the item to be re-orderable.
//    return YES;
//}
//*/
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//@end
