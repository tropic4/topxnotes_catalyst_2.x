//
//  NoteSettingsInfoViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 5/19/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//


#import "NoteSettingsInfoViewController.h"
#import "Constants.h"                                                           //leg20220310 - TopXNotes_Catalyst_2

@implementation NoteSettingsInfoViewController
@synthesize model;
@synthesize heading;                                                            //leg20220310 - TopXNotes_Catalyst_2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
        //		self.title = @"About TopXNotes Touch";
        self.title = [NSString stringWithFormat:@"About %@",                    //leg20150924 - 1.5.0
                      [self infoValueForKey:@"CFBundleDisplayName"]];           //leg20150924 - 1.5.0
	}
	
	return self;
}

// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Automatically invoked after -loadView
// This is the preferred override point for doing additional setup after -initWithNibName:bundle:
//
- (void)viewDidLoad
{

    // Set title of view.                                                       //leg20210419 - TopXNotes2
    self.title = @"Encryption Preferences";
    
    [super viewDidLoad];

    self.view.backgroundColor = [Constants lightBlueColor];                     //leg20220310 - TopXNotes_Catalyst_2
}

- (void)viewWillAppear:(BOOL)animated {                                         //leg20140205 - 1.2.7
#pragma unused (animated)

    [super viewWillAppear:animated];

    // Fill in the info view text.                                              //leg20220310 - TopXNotes_Catalyst_2
    heading.text = NSLocalizedString(@"Note_Settings_Info_Heading", @"");
//    NSString * const formatStr = NSLocalizedString(@"Note_Settings_Info_Message", @"");
//    self.instructionsText.text = [NSString stringWithFormat: formatStr, kNumberOfAutoBackups];
    self.instructionsText.text = NSLocalizedString(@"Note_Settings_Info_Message", @"");
}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)
    
    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)

	// do something here as our view re-appears

    [super viewDidAppear:animated];
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
