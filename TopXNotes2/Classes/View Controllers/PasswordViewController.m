//
//  PasswordViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 11/15/12.
//
//

#import "PasswordViewController.h"
#import <QuartzCore/QuartzCore.h>
#include <CoreGraphics/CGBase.h>
#include <CoreFoundation/CFDictionary.h>

@interface PasswordViewController ()
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message;           //leg20210422 - TopXNotes2
- (void)alertOKAndCancelWithTitle:(NSString*)title message:(NSString*)message;  //leg20210422 - TopXNotes2
@end

@implementation PasswordViewController
@synthesize delegate = _delegate;
@synthesize passwordDialogType;                                                 //leg20210422 - TopXNotes2

// Determine whether or not a tall iPhone                                       //leg20150924 - 1.5.0
- (BOOL)isTall
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (MAX(screenSize.height, screenSize.width) >=568)
            return YES; // iPhone 5 or greater
    }

    return NO;
}

// This is now handled by segue @"Password_Dialog_Segue".                       //leg20210422 - TopXNotes2
//- (id)initWithNibName:(NSString *)nibNameOrNil
//                                  bundle:(NSBundle *)nibBundleOrNil
//                                  withPasswordDialogType:(NSInteger)type
//                                  andNoteTitle:(NSString*)title
//{
//    passwordDialogType = type;
//
//    // Trim whitespace from front and back of note title.
//    self.noteTitle = [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    // JHL- Toolbar not needed with new style
/*    if ([self isTall])                                                          //leg20150924 - 1.5.0
    {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        CGFloat longSize = MAX(screenSize.height, screenSize.width);

        self.toolBar.frame = CGRectOffset (self.toolBar.frame, 0, -(longSize-480.0) );
    }
*/
    
// JHL- Add conditional for iOS 13 only
  //  self.navigationController.overrideUserInterfaceStyle = UIUserInterfaceStyleDark;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target: self action:@selector(dismissCancelAction:)];
    // Fix Issue #014 - "Double Done Issue". Change title of "Done."             //leg20211213 - TopXNotes_Catalyst_2
    //  Give "Done" button different titles depending on action.
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target: self action:@selector(dismissDoneAction:)];
    
    
    // Set title of view based on action requested.                             //leg20210422 - TopXNotes2
    switch (passwordDialogType) {
        case RemovePassword:
            {
                self.navigationItem.title = @"Remove Password";
                
                // Fix Issue #014 - "Double Done Issue". Change title of "Done."//leg20211213 - TopXNotes_Catalyst_2
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]
                                                          initWithTitle:@"Remove"
                                                          style:UIBarButtonItemStyleDone
                                                          target: self action:@selector(dismissDoneAction:)];

            }
            break;
            
        case SetPassword:
            {
                self.navigationItem.title = @"Set Password";

                // Fix Issue #014 - "Double Done Issue". Change title of "Done."//leg20211213 - TopXNotes_Catalyst_2
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]
                                                          initWithTitle:@"Encrypt"
                                                          style:UIBarButtonItemStyleDone
                                                          target: self action:@selector(dismissDoneAction:)];
            }
            break;
            
       case GetPassword:
            {
                self.navigationItem.title = @"Get Password";

                // Fix Issue #014 - "Double Done Issue". Change title of "Done."//leg20211213 - TopXNotes_Catalyst_2
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]
                                                          initWithTitle:@"Decrypt"
                                                          style:UIBarButtonItemStyleDone
                                                          target: self action:@selector(dismissDoneAction:)];
            }
            break;
            
        default:
            break;
    }

    // Get frame of default password view
    CGRect frame = self.settingPasswordView.frame;

//    // Adjust frame of password view and title if iOS 7 or greater.             //leg20150402 - 1.5.0
//    if (IS_OS_7_OR_LATER) {
//        frame = CGRectOffset (frame, 0, 15 );
//        self.titleLabel.frame = CGRectOffset (self.titleLabel.frame, 0, 15 );
//    }
    
    switch (passwordDialogType) {
        case RemovePassword:
            {
                self.gettingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.gettingPasswordView];
                
                self.settingPasswordView.hidden = YES;
                self.gettingPasswordView.hidden = NO;
                self.hintPasswordLabel.text = self.passwordHint;
                self.titleLabel.text = @"Remove Protection";
                self.getPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to remove protection from note \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.gettingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.oldPasswordTextField becomeFirstResponder];
            }
            break;
            
        case SetPassword:
            {
                self.settingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.settingPasswordView];
                
                self.gettingPasswordView.hidden = YES;
                self.settingPasswordView.hidden = NO;
                self.titleLabel.text = @"Set Protection";
                self.setPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to protect note \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.settingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.createPasswordTextField becomeFirstResponder];
            }
            break;
            
       case GetPassword:
            {                                      
                self.gettingPasswordView.frame = frame;
                [self.view bringSubviewToFront:self.gettingPasswordView];

                self.settingPasswordView.hidden = YES;
                self.gettingPasswordView.hidden = NO;
                self.hintPasswordLabel.text = self.passwordHint;
                self.titleLabel.text = @"Get Password";
                self.getPasswordPromptLabel.text = [NSString stringWithFormat: @"Enter password to view this note: \"%@\"", self.noteTitle];

                CALayer *viewLayer = [self.gettingPasswordView layer];
                //viewLayer.borderColor = [[UIColor cyanColor] CGColor];
                viewLayer.borderWidth = 6.0f;
                
                viewLayer.masksToBounds = YES;
                viewLayer.cornerRadius = 7.0f;
                [self.oldPasswordTextField becomeFirstResponder];
            }
            break;
            
        default:
            break;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Actions
- (IBAction)dismissCancelAction:(id)sender
{
#pragma unused (sender)

 	[self.oldPasswordTextField resignFirstResponder];
	[self.createPasswordTextField resignFirstResponder];
	[self.confirmPasswordTextField resignFirstResponder];
	[self.hintPasswordTextField resignFirstResponder];
    
    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (IBAction)dismissDoneAction:(id)sender
{
#pragma unused (sender)
    
 	[self.oldPasswordTextField resignFirstResponder];
	[self.createPasswordTextField resignFirstResponder];
	[self.confirmPasswordTextField resignFirstResponder];
	[self.hintPasswordTextField resignFirstResponder];

    switch (passwordDialogType) {
        case RemovePassword:
            {
                if ([self.oldPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.oldPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else {                    
                    // Give user opportunity to cancel Remove Password.
                    [self alertProceedCancelAction: [NSString stringWithFormat:@"Tap \"Proceed\" to Unprotect note \"%@\"", self.noteTitle]];
                }
            }
            break;
            
        case SetPassword:
            {
                if ([self.createPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.createPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else if ([self.createPasswordTextField.text caseInsensitiveCompare:self.confirmPasswordTextField.text] == NSOrderedSame) {
                    if ([self.createPasswordTextField.text caseInsensitiveCompare:self.hintPasswordTextField.text] == NSOrderedSame) {
                        [self alertSimpleAction: @"Hint can not be same as Password!"];
                    } else if ([self.delegate didSetProtect:self.createPasswordTextField.text withHint:self.hintPasswordTextField.text]) {
                        [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
                    }
                } else {
                    [self alertSimpleAction: @"Password does not match Confirm Password!"];
                }
            }
            break;
            
        case GetPassword:
            {
                if ([self.oldPasswordTextField.text length] < 4) {
                    [self alertSimpleAction: @"Passwords must be at least 4 characters!"];
                } else if ([self.oldPasswordTextField.text length] > 31) {
                    [self alertSimpleAction: @"Passwords must be no more than 31 characters!"];
                } else {
                    if ([self.delegate didGetDecrypt:self.oldPasswordTextField.text]) {
                        [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
                    }
                }
            }
            break;
            
        default:
            break;
    }

    
}

#pragma mark - UIAlertController

- (void)alertProceedCancelAction:(NSString*)alertMessage
{
//	// Open an alert with an OK and cancel button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove Password" message: alertMessage
//                                                   delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Proceed", nil];
//	[alert show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210422 - TopXNotes2
    [self alertOKAndCancelWithTitle:@"Remove Password" message:alertMessage];
}

- (void)alertSimpleAction:(NSString*)alertMessage
{
//	// Open an alert with just an OK button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Validation" message: alertMessage
//                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//	[alert show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210422 - TopXNotes2
    [self alertOKWithTitle:@"Password Validation" message:alertMessage];
}

// Replace deprecated UIAlertView with UIAlertController.                       //leg20210422 - TopXNotes2
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
               handler:^(UIAlertAction * action) {
                // Do nothing––no action required!
            }];
            
            [alert addAction:defaultAction];

            [self presentViewController:alert animated:YES completion:nil];
}

- (void)alertOKAndCancelWithTitle:(NSString*)title message:(NSString*)message   //leg20210422 - TopXNotes2
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];
//                                    preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        if ([self.delegate didGetUnprotect:self.oldPasswordTextField.text]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {
         // Do nothing––action cancelled!
     }];

    [alert addAction:defaultAction];
    [alert addAction:cancelAction];

    [self presentViewController:alert animated:YES completion:nil];
}

// Replaced deprecated UIAlertView with UIAlertController.                      //leg20210422 - TopXNotes2
//#pragma mark UIAlertViewDelegate
//
//
//- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//#pragma unused (actionSheet)
//
//	// use "buttonIndex" to decide your action
//	//
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		// "Cancel" button
//        ;
//	} else {
//		// "Proceed" button
//        if ([self.delegate didGetUnprotect:self.oldPasswordTextField.text]) {
//            [self dismissViewControllerAnimated:YES completion:nil];            //leg20140205 - 1.2.7
//        }
//	}
//}

//
// Task of keeping view out from under keyboard is now handled by               //leg20210611 - TopXNotes2
//  IQKeyboardManager framework.
//
//#pragma mark - UITextFieldDelegate
//- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
//{
//#pragma unused (textField)
//
//    return YES;
//}
//
//- (void) textFieldDidBeginEditing:(UITextField *)textField
//{
//#pragma unused (textField)
//
//}
//
//- (void) textFieldDidEndEditing:(UITextField *)textField
//{
//#pragma unused (textField)
//
// 	[self.oldPasswordTextField resignFirstResponder];
//	[self.createPasswordTextField resignFirstResponder];
//	[self.confirmPasswordTextField resignFirstResponder];
//	[self.hintPasswordTextField resignFirstResponder];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//	[textField resignFirstResponder];
//
//    [self dismissDoneAction:nil];
//
//    return YES;
//}

@end
