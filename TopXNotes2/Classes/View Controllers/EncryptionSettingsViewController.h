//
//  EncryptionSettingsViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 2/4/13.
//
//

#import <UIKit/UIKit.h>

@class Model;

@interface EncryptionSettingsViewController : UIViewController {
        NSMutableDictionary *savedSettingsDictionary;
        
        IBOutlet Model			*model;
}

@property (nonatomic, strong) Model *model;
@property (strong, nonatomic) IBOutlet UILabel *encryptionStatusLabel;
@property (strong, nonatomic) IBOutlet UISwitch *encryptionStatusSwitch;

- (void)updateEncryptionStatus:(BOOL)status;
- (IBAction)encryptionEnableOrDisable:(id)sender;

@end

