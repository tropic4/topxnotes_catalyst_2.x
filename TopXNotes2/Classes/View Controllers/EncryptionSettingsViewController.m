//
//  EncryptionSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 2/4/13.
//
//

#import "EncryptionSettingsViewController.h"
#import "Constants.h"
#import "Model.h"
#import "Note.h"

@interface EncryptionSettingsViewController ()
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message;           //leg20210422 - TopXNotes2
@end

@implementation EncryptionSettingsViewController

@synthesize model;
@synthesize encryptionStatusLabel;
@synthesize encryptionStatusSwitch;

- (void)encryptionStatusAlert {
//	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Encryption" message:@"All notes must have Protection Removed before Encryption can be turned off!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alertView show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210422 - TopXNotes2
    [self alertOKWithTitle:@"Encryption" message:@"All notes must have Protection Removed before Encryption can be turned off!"];
}

#pragma mark - UIAlertController

// Replace deprecated UIAlertView with UIAlertController.                       //leg20210422 - TopXNotes2
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
               handler:^(UIAlertAction * action) {
                // Do nothing––no action required!
            }];
            
            [alert addAction:defaultAction];

            [self presentViewController:alert animated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		// this will appear as the title in the navigation bar
		self.title = @"Encryption Preferences";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    // Set title of view.                                                       //leg20210419 - TopXNotes2
    self.title = @"Encryption Preferences";
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Get default Encryption enable/disable setting.
    NSNumber *encryptionStatus = [savedSettingsDictionary objectForKey: kEncryptionStatus_Key];
    
    // Use setting if present, else set default.
    if (encryptionStatus) {
        [self updateEncryptionStatus:[encryptionStatus boolValue]];
    } else {
        encryptionStatus = [NSNumber numberWithBool:NO];
        [self updateEncryptionStatus:[encryptionStatus boolValue]];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Enable/Disable Encryption

- (void)updateEncryptionStatus:(BOOL)status {

    // Update Encryption status legend to reflect on/off switch.
    if (status) {
		[encryptionStatusLabel setText:@"Encryption Enabled"];
    } else {
		[encryptionStatusLabel setText:@"Encryption Disabled"];
    }

	encryptionStatusSwitch.on = status;

    // Save the setting to defaults.
    [savedSettingsDictionary setObject:[NSNumber numberWithBool:status] forKey:kEncryptionStatus_Key];
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    // Notify notepad that encryption status may have changed.                  //leg20210609 - TopXNotes2
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
}


- (IBAction)encryptionEnableOrDisable:(id)sender {
    if (model.hasEncryptedNotes) {
        [self encryptionStatusAlert];
        [self updateEncryptionStatus:YES];
    }else {
        [self updateEncryptionStatus:[sender isOn]];
    }
}

@end
