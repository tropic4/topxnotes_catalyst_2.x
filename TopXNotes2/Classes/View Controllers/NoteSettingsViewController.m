//
//  NoteSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
// Modified to configure Font Picker based on macro FONT_PICKER_HAS_SIZE_WHEEL  //leg20220309 - TopXNotes_Catalyst_2
//  FONT_PICKER_HAS_SIZE_WHEEL = 0 - Picker has only Font Name setting.
//  FONT_PICKER_HAS_SIZE_WHEEL = 1 - Picker has Font Name and Size settings.

#import "NoteSettingsViewController.h"
#import "NoteFontPickerController.h"
#import "Formatter.h"
#import "Constants.h"

@interface NoteSettingsViewController () <UIPopoverPresentationControllerDelegate>  //leg20220310 - TopXNotes_Catalyst_2
@end

@implementation NoteSettingsViewController

@synthesize pickerViewContainer;
@synthesize noteFontPickerController;
@synthesize noteFontPickerViewContainer;
@synthesize model;
@synthesize isSetDefaultFontMode;                                               //leg20211230 - TopXNotes_Catalyst_2
@synthesize modalButton;                                                        //leg20220310 - TopXNotes_Catalyst_2

NSComparisonResult familyNameSort(id string1, id string2, void *context)
{
#pragma unused (context)

	return [string1 localizedCaseInsensitiveCompare:string2];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"Note Preferences";
	}
	
	return self;

// Note that model is not yet valid here -- reference it from -viewDidAppear
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];

	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)  
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

	// Build a sorted array of all available Font Names For Family Names.
	NSMutableArray *allFontNamesForFamilys = [NSMutableArray array];
	NSMutableArray *familyFontNamesArray = (NSMutableArray*)[UIFont familyNames];
    
	NSEnumerator *enumerator = [familyFontNamesArray objectEnumerator];
	id anObject;
	 
	while (anObject = [enumerator nextObject]) {
		[allFontNamesForFamilys addObject:anObject];	// Add family name
		
		NSArray *fontNamesForFamily = [UIFont fontNamesForFamilyName:anObject];
		
		NSEnumerator *enumerator2 = [fontNamesForFamily objectEnumerator];
		id anObject2;
		 
		while (anObject2 = [enumerator2 nextObject]) {
			[allFontNamesForFamilys addObject:anObject2];	// Add font names of family
		}
	}
    

    // Reduce the number of Fonts in the font picker by including only Font     //leg20220210 - TopXNotes_Catalyst_2
    //  Family Names and not the variations like Regular, Bold, Light, etc.
//    noteFontPickerController.fontNamesArray = (NSMutableArray *)[allFontNamesForFamilys sortedArrayUsingFunction:familyNameSort context:NULL];
    noteFontPickerController.fontNamesArray = (NSMutableArray *)[familyFontNamesArray sortedArrayUsingFunction:familyNameSort context:NULL];

    // Create the master list of Font indexes to be used for Quick Find of Font.//leg20220218 - TopXNotes_Catalyst_2
    NSInteger fontCount = [noteFontPickerController.fontNamesArray count];
    NSMutableArray *fontIndexesArray = [[NSMutableArray alloc] init];

    // Order list so search finds from top of list.
    for (int index=(int)fontCount-1; index>=0; index--)
    {
        [fontIndexesArray addObject: [NSNumber numberWithInt:index]];
    }

    // Pass the list of font names to the picker so user can quick find fonts.
    noteFontPickerController.listContent = [[NSMutableArray alloc] initWithArray:fontIndexesArray];

    // Pass font setting mode.                                                  //leg20211230 - TopXNotes_Catalyst_2
    noteFontPickerController.isSetDefaultFontMode = isSetDefaultFontMode;

	// Build an array of selectable Font Sizes.
//    noteFontPickerController.fontSizesArray = [NSArray arrayWithObjects:@"8", @"10", @"12", @"14", @"16", @"18",@"20", @"22", @"24",@"26", @"28", @"30",nil];
    // Change Font size selection from selecting point sizes to the allowable   //leg20220928 - TopXNotes_Catalyst_2
    //  font parameter 1 - 7. Reconfigure font wheel display and font
    //  example display.
	noteFontPickerController.fontSizesArray = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6",@"7",nil];

	// Assemble the picker view
	[pickerViewContainer addSubview:noteFontPickerViewContainer];
	
    // Get Font data from saved settings
    NSInteger fontNameRow = 0;
    NSString * fontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
    if (fontName != nil) {
        fontNameRow = [noteFontPickerController.fontNamesArray indexOfObject:fontName];
        if (fontNameRow == NSNotFound)
            fontNameRow = 0;
    }
    
    NSInteger fontSizeRow = 0;
    NSString * fontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
    if (fontSize != nil) {
        fontSizeRow = [noteFontPickerController.fontSizesArray indexOfObject:fontSize];
        if (fontSizeRow == NSNotFound)
            fontSizeRow = 0;
    }
    
    // Get last chosen style Font defaults.                                     //leg20220111 - TopXNotes_Catalyst_2                                                               //leg20110416 - 1.0.4
	NSInteger lastFontNameRow = 0;
	NSString * lastFontName = [savedSettingsDictionary objectForKey: kFontNameLastChosen_Key];
	if (lastFontName != nil) {
        lastFontNameRow = [noteFontPickerController.fontNamesArray indexOfObject:lastFontName];
		if (lastFontNameRow == NSNotFound)
            lastFontNameRow = 0;
	}
	
	NSInteger lastFontSizeRow = 0;
	NSString * lastFontSize = [savedSettingsDictionary objectForKey: kFontSizeLastChosen_Key];
	if (lastFontSize != nil) {
        lastFontSizeRow = [noteFontPickerController.fontSizesArray indexOfObject:lastFontSize];
		if (lastFontSizeRow == NSNotFound)
            lastFontSizeRow = 0;
	}

    // Select the saved setting font and size in the picker depending on        //leg20220111 - TopXNotes_Catalyst_2
    //  whether or not picking the default font or picking font for a style.
    if (isSetDefaultFontMode) {
        // Set wheel(s) of picker to saved default.                             //leg20220309 - TopXNotes_Catalyst_2
//#if FONT_PICKER_HAS_SIZE_WHEEL
//        [noteFontPickerController.pickerView selectRow:fontNameRow inComponent:FONT_NAME_COMPONENT animated:NO];
//        [noteFontPickerController.pickerView selectRow:fontSizeRow inComponent:FONT_SIZE_COMPONENT animated:NO];
//#else
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        [noteFontPickerController.pickerView selectRow:fontNameRow inComponent:FONT_NAME_COMPONENT animated:NO];
//#endif

        // Update the label with the current font name.                         //leg20220309 - TopXNotes_Catalyst_2
//#if FONT_PICKER_HAS_SIZE_WHEEL
////        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@ pt", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT],
////                                                                                        [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontSizeRow forComponent:FONT_SIZE_COMPONENT]];
////        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT];
//        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT];
//
//        // Construct font example description.                                      //leg20220928 - TopXNotes_Catalyst_2
//        NSString *fontSizeDescription = [noteFontPickerController fontSizeDescription:[noteFontPickerController.pickerView selectedRowInComponent:FONT_SIZE_COMPONENT]];
//        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT],
//                                              fontSizeDescription];
//#else
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT]];
        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT];
//#endif
        
        noteFontPickerController.label.font = [UIFont fontWithName:font
                                                              size:16.0];
    } else {
        
#if FONT_PICKER_HAS_SIZE_WHEEL
        // Select rows of picker wheel(s).                                      //leg20220303 - TopXNotes_Catalyst_2
        [noteFontPickerController.pickerView selectRow:lastFontNameRow inComponent:FONT_NAME_COMPONENT animated:NO];
        [noteFontPickerController.pickerView selectRow:lastFontSizeRow inComponent:FONT_SIZE_COMPONENT animated:NO];
#else
        [noteFontPickerController.pickerView selectRow:lastFontNameRow inComponent:FONT_NAME_COMPONENT animated:NO];
#endif

        // Update the label with the current font name.                         //leg20220309 - TopXNotes_Catalyst_2
#if FONT_PICKER_HAS_SIZE_WHEEL
//        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@ pt", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontNameRow forComponent:FONT_NAME_COMPONENT],
//                                                                                        [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontSizeRow forComponent:FONT_SIZE_COMPONENT]];
//        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontNameRow forComponent:FONT_NAME_COMPONENT];
        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT];
        
        // Construct font example description.                                      //leg20220928 - TopXNotes_Catalyst_2
        NSString *fontSizeDescription = [noteFontPickerController fontSizeDescription:[noteFontPickerController.pickerView selectedRowInComponent:FONT_SIZE_COMPONENT]];
//        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT],
//                                              fontSizeDescription];
        // Set initial font description to last font chosen with font tool.     //leg20221005 - TopXNotes_Catalyst_2
        //  Fixes issue where font description of default font was shown
        //  initially instead of last font chosen by font tool.
        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontNameRow forComponent:FONT_NAME_COMPONENT],
                                              fontSizeDescription];
#else
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        noteFontPickerController.label.text = [NSString stringWithFormat:@"%@", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontNameRow forComponent:FONT_NAME_COMPONENT]];
        NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:lastFontNameRow forComponent:FONT_NAME_COMPONENT];
#endif
        noteFontPickerController.label.font = [UIFont fontWithName:font
                                                              size:16.0];
    }

    // Show the "Point" label if appropriate.                                   //leg20220309 - TopXNotes_Catalyst_2
//#if FONT_PICKER_HAS_SIZE_WHEEL
//    // Show the "Point" label appropriate to the version of iOS.                //leg20140205 - 1.2.7
//    if (IS_OS_7_OR_LATER) {
//        _iOS6PointLabel.hidden = YES;
//        _iOS7PointLabel.hidden = NO;
//    } else {
//        _iOS6PointLabel.hidden = NO;
//        _iOS7PointLabel.hidden = YES;
//    }
//#else
//    _iOS6PointLabel.hidden = YES;
//    _iOS7PointLabel.hidden = YES;
//#endif

    // Point labels now obsolete.                                               //leg20220928 - TopXNotes_Catalyst_2
    _iOS6PointLabel.hidden = YES;
    _iOS7PointLabel.hidden = YES;


    
	// Make sure the picker has access to the notepad
	noteFontPickerController.model = model;
    
    // Make the searchBar the first responder.                                  //leg20220218 - TopXNotes_Catalyst_2
    [noteFontPickerController.quickFind becomeFirstResponder];
}

- (void)viewDidLoad {
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	// make sure Restore button is positioned appropriate to device orientation
    [super viewDidLoad];

    // This will appear as the title in the navigation bar.                     //leg20210519 - TopXNotes2
    self.title = @"Note Preferences";

	
	// add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton]; //leg20220310 - TopXNotes_Catalyst_2
	self.navigationItem.rightBarButtonItem = modalButton;                       //leg20140216 - 1.2.7
    
    // Add a bprder to picker view.                                             //leg20220111 - TopXNotes_Catalyst_2
    CALayer *viewLayer = [self.pickerViewContainer layer];
    viewLayer.borderColor = [[UIColor grayColor] CGColor];
    viewLayer.borderWidth = 2.0f;
    viewLayer.masksToBounds = YES;
    viewLayer.cornerRadius = 5.0f;
}

// user clicked the "i" button, present the info view.
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)
    // Show the Info view in a popover.                                         //leg20220310 - TopXNotes_Catalyst_2
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"NoteSettingsInfoViewController"];

    // Present the controller.
    //  on iPad, this will be a Popover.
    //  on iPhone, this will be an action sheet.
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];

    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = modalButton;
    popController.delegate = self;
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	self.pickerViewContainer = nil;
	
	self.noteFontPickerViewContainer = nil;
	
	[super viewDidUnload];
}

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

# pragma mark - Popover Presentation Controller Delegate

// Popover delegate methods for Font settings popover.                          //leg20220310 - TopXNotes_Catalyst_2

// TODO: Determine what to replace deprecated methods with.

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // called when a Popover is dismissed
    NSLog(@"Popover was dismissed with external tap. Have a nice day!");
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // return YES if the Popover should be dismissed
    // return NO if the Popover should not be dismissed
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
    // called when the Popover changes positon
}

@end
