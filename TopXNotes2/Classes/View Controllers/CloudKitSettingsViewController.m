//
//  CloudKitSettingsViewController.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 9/6/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
// Derived from EncryptionSettingsViewController.h/m.                           //leg20220906 - TopXNotes_Catalyst_2

#import <CloudKit/CloudKit.h>
#import "CloudKitManager.h"
#import "CloudKitSettingsViewController.h"
#import "TopXNotesAppDelegate.h"
#import "Constants.h"
#import "Model.h"
#import "Note.h"

// Reachability
#import <SystemConfiguration/SystemConfiguration.h>
#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

@interface CloudKitSettingsViewController ()
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message;
@end

@implementation CloudKitSettingsViewController

@synthesize model;
@synthesize cloudKitStatusLabel;
@synthesize cloudKitStatusSwitch;

#pragma mark - UIAlertController

// Replace deprecated UIAlertView with UIAlertController.
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
               handler:^(UIAlertAction * action) {
                // Do nothing––no action required!
            }];
            
            [alert addAction:defaultAction];

            [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark Reachablity

- (BOOL)hasActiveWiFiConnection
{
// An enumeration that defines the return values of the network state
//  of the device.
//
typedef enum {
    NotReachable = 0,
    ReachableViaCarrierDataNetwork,
    ReachableViaWiFiNetwork
} NetworkStatus;

     SCNetworkReachabilityFlags     flags;
     SCNetworkReachabilityRef        reachabilityRef;
     BOOL                            gotFlags;
     
     reachabilityRef   = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault (), [@"www.google.com"UTF8String]);
     gotFlags          = SCNetworkReachabilityGetFlags(reachabilityRef, &flags);
     CFRelease(reachabilityRef);
     if (!gotFlags) {
        return NO;
    }
    
    if( flags & ReachableViaWiFiNetwork ) {
        return YES;
    }
    
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    // Set title of view.
    self.title = @"CloudKit Syncing Preferences";
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

    // Get current CloudKit Sync status.
    NSNumber *cloudKitStatus = model.cloudKitSync;
    
    // Use setting if present, else set default.
    if (cloudKitStatus) {
        [self updateCloudKitStatus:[cloudKitStatus boolValue]];
    } else {
        cloudKitStatus = [NSNumber numberWithBool:NO];
        [self updateCloudKitStatus:[cloudKitStatus boolValue]];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Enable/Disable CloudKit Syncing

- (void)updateCloudKitStatus:(BOOL)status {

    // Update CloudKit Syncing status legend to reflect on/off switch.
    if (status) {
        [cloudKitStatusLabel setText:@"CloudKit Syncing Enabled"];
    } else {
        [cloudKitStatusLabel setText:@"CloudKit Syncing Disabled"];
    }

    cloudKitStatusSwitch.on = status;

    // Save the setting to the model.
    model.cloudKitSync = [NSNumber numberWithBool:status];
    [model saveData];
}


- (IBAction)cloudKitEnableOrDisable:(id)sender {
   if (![self hasActiveWiFiConnection] && cloudKitStatusSwitch.on) {
       [self alertOKWithTitle:@"A WIFI connection is required to enable CloudKit Sync -- Turn WIFI On in Settings!"
                      message:@"Check your networking configuration!"];
       [cloudKitStatusLabel setText:@"CloudKit Syncing Disabled"];
       cloudKitStatusSwitch.on = NO;
       return;
   }
    
    if ([[NSFileManager defaultManager] ubiquityIdentityToken] != nil) {
//        iCloudAvailable = true;
    } else {
        [self alertOKWithTitle:@"An iCloud Container with iCloud Drive must be available to enable CloudKit Sync!"
                       message:@"Check your iCloud Settings!"];
        [cloudKitStatusLabel setText:@"CloudKit Syncing Disabled"];
        cloudKitStatusSwitch.on = NO;
        return;
    }
    
    // Determine which notepad will be the starting point for syncing.
    if ([sender isOn]) {
        
//        // Establish the Update Subscripton.
//        [CloudKitManager updateCloudSubscriptions];
        
        // Dialog to select base notepad.
        [self chooseInitialNotepadAlert:sender];
    }
    
    // Update the enable/disable switch state.
    [self updateCloudKitStatus:[sender isOn]];
}

// Select initial notepad to use as sync start alert.                           //leg20220917 - TopXNotes_Catalyst_2
-(void)chooseInitialNotepadAlert:(id)sender {
    __block BOOL result = true;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Select Start-up Option for CloudKit Syncing"
                                                                   message:@"Upload - Upload this device's Notepad to iCloud as the base Notepad.\n\n Replace - Replace this device's NotePad with base in iCloud, if it exists.\n\n On - Enable without uploading or replacing Notepad now."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
       handler:^(UIAlertAction * action) {
        
        // Update the enable/disable switch state.
        [self updateCloudKitStatus:![sender isOn]];
    }];

    UIAlertAction* onAction = [UIAlertAction actionWithTitle:@"On" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        
        // Establish the Update Subscripton.
        [CloudKitManager updateCloudSubscriptions];

        // Update the enable/disable switch state.
        [self updateCloudKitStatus:[sender isOn]];
    }];

    UIAlertAction *uploadAction = [UIAlertAction actionWithTitle:@"Upload"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {

                                                            // Establish the Update Subscripton.
                                                            [CloudKitManager updateCloudSubscriptions];
        
                                                            // Upload this device's Notepad to iCloud as base Notepad.
                                                            BOOL syncResult = [self->model syncCloudData:YES];
                                                        }];

     UIAlertAction* replaceAction = [UIAlertAction actionWithTitle:@"Replace" style:UIAlertActionStyleDefault
        handler:^(UIAlertAction * action) {

         // Establish the Update Subscripton.
         [CloudKitManager updateCloudSubscriptions];

         // Download iCloud Notepad if it exists.
         // Fetch the Notepad records and use the most recent one.
         [CloudKitManager fetchNotepadWithCompletionHandler:^(NSArray *results, NSError *error) {
             if (results.count > 0) {
                 if (error) {
                     if (error.code == 6) {
                         NSLog(@"Error 6 fetching iCloud notepad records - error=\"%@\"", [error localizedDescription]);
                     } else {
                         NSLog(@"Error fetching iCloud notepad records - error=\"%@\"", [error localizedDescription]);
                     }
                     result = false;
                } else {
                     NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:results.count];
                    for(CKRecord *record in results) {
                        [arr addObject:record];
                        NSLog(@"Notepad recordID: %@ creationDate: %@ modifiedDate: %@",record.recordID, record.creationDate, record.modificationDate);
                    }
                     
                     // Sort by created date
                     NSArray *sortedRecords = [arr sortedArrayUsingComparator:^NSComparisonResult(CKRecord *record1, CKRecord *record2){
                           return [record2.modificationDate compare:record1.modificationDate]; // sort most recent to top.
                     }];
                     NSLog(@"Sorted %lu Notepad records", (unsigned long)sortedRecords.count);

                     for(CKRecord *record in sortedRecords) {
                         NSLog(@"Sorted Notepad recordID: %@ creationDate: %@ modifiedDate: %@",record.recordID, record.creationDate, record.modificationDate);
                     }

                     // Replace the notepad with the most recent from CloudKit.
                     CKRecord *updateRecord = sortedRecords[0];
                    
                    TopXNotesAppDelegate *appDelegate =
                        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];

                    [appDelegate replaceNotepadFromCloudKitUpdate: updateRecord];

                    [[NSNotificationCenter defaultCenter] postNotificationName:kReset_Display_After_CloudKit_Update object:nil];
                 }
             }
         }];

     }];

    [alert addAction:uploadAction];
    [alert addAction:replaceAction];
    [alert addAction:onAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
