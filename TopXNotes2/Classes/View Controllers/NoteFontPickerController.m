//
//  NoteFontPickerController.m
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying Fonts and Font Sizes.
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
// Remove Font Size Wheel for setting notepad default font.                     //leg20221003 - TopXNotes_Catalyst_2
//
// Fix Issue #052 - "Font Size Control Missing"                                 //leg20220928 - TopXNotes_Catalyst_2
// Re-implemented Font Size Wheel in Font tool and Note Settings.
//  Allowable Font Size parameters (1-7)  correspond to fixed font sizes:
//  1 - xx-small
//  2 - x-small
//  3 - small
//  4 - medium
//  5 - large
//  6 - x-large
//  7 - xx-large
//
// Modified to configure Font Picker based on macro FONT_PICKER_HAS_SIZE_WHEEL  //leg20220309 - TopXNotes_Catalyst_2
//  FONT_PICKER_HAS_SIZE_WHEEL = 0 - Picker has only Font Name setting.
//  FONT_PICKER_HAS_SIZE_WHEEL = 1 - Picker has Font Name and Size settings.

#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "NoteFontPickerController.h"

@implementation NoteFontPickerController

@synthesize fontNamesArray, fontSizesArray;
@synthesize pickerView;
@synthesize label;
@synthesize model;
@synthesize saveFontButton;
@synthesize closeButton;                                                        //leg20211230 - TopXNotes_Catalyst_2
@synthesize isSetDefaultFontMode;                                               //leg20211230 - TopXNotes_Catalyst_2
@synthesize delegate;                                                           //leg20211230 - TopXNotes_Catalyst_2
@synthesize listContent;                                                        //leg20220218 - TopXNotes_Catalyst_2
@synthesize quickFind;                                                          //leg20220218 - TopXNotes_Catalyst_2

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

- (NSString*)pathToBackupData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kBackupModelFileName];

	return appFile;
}

// Provide font size description.                                               //leg20220928 - TopXNotes_Catalyst_2
- (NSString*)fontSizeDescription:(NSInteger)size {
        switch (size+1) {
            case 1:
                return @" 1:xx-small";
            break;
             
            case 2:
                return @" 2:x-small";
            break;
             break;
             
            case 3:
                return @" 3:small";
            break;
             break;
             
            case 4:
                return @" 4:medium";
            break;
             
            case 5:
                return @" 5:large";
            break;
             
            case 6:
                return @" 6:x-large";
            break;
             
            case 7:
                return @" 7:xx-large";
            break;
             
            default:
                return @" 4:medium";
             break;
        }
}

// Save picker font information.                                                //leg20220111 - TopXNotes_Catalyst_2
-(IBAction)saveFontSelection {
    // Reading Defaults to get current default font.
    NSUserDefaults *defaults;
    NSMutableDictionary *savedSettingsDictionary;
    defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    // Get Font data from picker.                                               //leg20220309 - TopXNotes_Catalyst_2
    NSInteger fontNameRow = 0;
    NSInteger fontSizeRow = 0;
#if FONT_PICKER_HAS_SIZE_WHEEL
    if (isSetDefaultFontMode) {
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        fontNameRow = [pickerView selectedRowInComponent:FONT_NAME_COMPONENT];
    } else {
        fontNameRow = [pickerView selectedRowInComponent:FONT_NAME_COMPONENT];
        fontSizeRow = [pickerView selectedRowInComponent:FONT_SIZE_COMPONENT];
    }
#else
    NSInteger fontNameRow = [pickerView selectedRowInComponent:FONT_NAME_COMPONENT];
#endif
    
    if (isSetDefaultFontMode) { // Set default font for all notes
        // Update the Font settings in user defaults                            //leg20220309 - TopXNotes_Catalyst_2
//#if FONT_PICKER_HAS_SIZE_WHEEL
//        [savedSettingsDictionary setObject:[fontNamesArray objectAtIndex:fontNameRow] forKey:kNoteFontName_Key];
//        [savedSettingsDictionary setObject:[fontSizesArray objectAtIndex:fontSizeRow] forKey:kNoteFontSize_Key];
//#else
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        [savedSettingsDictionary setObject:[fontNamesArray objectAtIndex:fontNameRow] forKey:kNoteFontName_Key];
//#endif
        
        // Save settings
        [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {    // Set font for a single style run.
        // Update the Font settings in user defaults                            //leg20220309 - TopXNotes_Catalyst_2
#if FONT_PICKER_HAS_SIZE_WHEEL
        [savedSettingsDictionary setObject:[fontNamesArray objectAtIndex:fontNameRow] forKey:kFontNameLastChosen_Key];
        [savedSettingsDictionary setObject:[fontSizesArray objectAtIndex:fontSizeRow] forKey:kFontSizeLastChosen_Key];
#else
        [savedSettingsDictionary setObject:[fontNamesArray objectAtIndex:fontNameRow] forKey:kFontNameLastChosen_Key];
#endif
        
        // Save settings
        [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // Pass info back to delegate.                                          //leg20220309 - TopXNotes_Catalyst_2
#if FONT_PICKER_HAS_SIZE_WHEEL
        [self.delegate setSelectedFontFamilyAndSize:[fontNamesArray objectAtIndex:fontNameRow]
                                               size:[[fontSizesArray objectAtIndex:fontSizeRow] intValue]];
#else
        [self.delegate setSelectedFontFamilyAndSize:[fontNamesArray objectAtIndex:fontNameRow]
                                               size:16];
#endif
        
        [self.delegate dismissPopOverViewController:(UIViewController*)self];
   }
}

- (IBAction)closePopover:(id)sender {
    [self.delegate dismissPopOverViewController:(UIViewController*)self];
}

#pragma mark  UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
#pragma unused (pickerView)
    
    // TODO: I put this here because I couldn't figure a better place.          //leg20211230 - TopXNotes_Catalyst_2
    //   Normally this  would be in -viewDidLoad or -viewWillAppear.
    self.closeButton.hidden = isSetDefaultFontMode;

#if FONT_PICKER_HAS_SIZE_WHEEL
    if (isSetDefaultFontMode) {
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        return 1;
    } else {
        return 2;
    }
#else
	return 1;
#endif
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
#pragma unused (pickerView)
    
	if (component == FONT_NAME_COMPONENT)
		return [fontNamesArray count];
	else
		return [fontSizesArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	if (component == FONT_NAME_COMPONENT)
		return [fontNamesArray objectAtIndex:row];
	else
		return [fontSizesArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
#pragma unused (pickerView, component)
    
	if (component == FONT_NAME_COMPONENT) {
		return FONT_NAME_COMPONENT_WIDTH;
	}
	else
		return FONT_SIZE_COMPONENT_WIDTH;
}

-(void)pickerView:(UIPickerView *)inPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
#pragma unused (row, component)
    
	// If the user chooses a new row, update the label accordingly.             //leg20220309 - TopXNotes_Catalyst_2
#if FONT_PICKER_HAS_SIZE_WHEEL
//    label.text = [NSString stringWithFormat:@"%@ - %@ pt", [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT],
//                                                           [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_SIZE_COMPONENT] forComponent:FONT_SIZE_COMPONENT]];
    // Construct font example description.                                      //leg20220928 - TopXNotes_Catalyst_2
    if (isSetDefaultFontMode) {
        // We don't need font size wheel for setting default font.              //leg20221003 - TopXNotes_Catalyst_2
        label.text = [NSString stringWithFormat:@"%@", [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT]];
    } else {
        NSString *fontSizeDescription = [self fontSizeDescription:[inPickerView selectedRowInComponent:FONT_SIZE_COMPONENT]];
        label.text = [NSString stringWithFormat:@"%@ - %@", [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT],
                                                            fontSizeDescription];
    }
#else
    label.text = [NSString stringWithFormat:@"%@", [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT]];
#endif
    
	label.font = [UIFont fontWithName:[self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT]
                                 size:16.0];                                    //leg20210519 - TopXNotes2
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
#pragma unused (searchBar)
    
    // only show the status bar's cancel button while in edit mode
    quickFind.showsCancelButton = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
#pragma unused (searchBar)
    
    quickFind.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
#pragma unused (searchBar)
    
    // Search the table content for font names that are prefixed with
    //  "searchText", ignoring case.
    //    If found move the picker wheel to that font.
    //
    NSString *fontFamilyName;
    NSNumber *val;

    for (val in listContent)
    {
        int index = [val intValue];
        
        fontFamilyName = [fontNamesArray objectAtIndex:index];
        if ([fontFamilyName.lowercaseString hasPrefix:searchText.lowercaseString]) {
            
            // Move the picker wheel to the font name.
            [pickerView selectRow:index
                  inComponent:0
                     animated:YES];
            
            // Update the font description.
            [self pickerView:pickerView didSelectRow:index inComponent:0];
        }
    }
}

// called when cancel button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [quickFind resignFirstResponder];
    quickFind.text = @"";
}

// called when Search (in our case "Done") button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [quickFind resignFirstResponder];
}

@end
