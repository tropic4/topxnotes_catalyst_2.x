//
//  NoteRichTextViewController.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 10/14/21.
//  Copyright © 2021 Tropical Software, Inc. All rights reserved.
//
//  Presents a Rich Text Editor View which outputs HTML.
//   Derived from: https://github.com/nnhubbard/ZSSRichTextEditor
//


#import "TopXNotesAppDelegate.h"
#import "NotePadViewController.h"
#import "BackupSettingsViewController.h"
#import "NoteRichTextViewController.h"
#import "PasswordViewController.h"
#import "Encrypt.h"
#import "Constants.h"
#import "Formatter.h"
#import "Model.h"
#import "Note.h"
#import "NSMutableString+EmailEncodingExtensions.h"
#import "HoverView.h"
#import "APActivityProvider.h"
#import "ZSSDemoPickerViewController.h"
#import "TopXNotes2-Swift.h"
#import "NSAttributedString+MMRTFWithImages.h"

NSString *Show_HoverView_RTE = @"SHOW";

//@interface NoteRichTextViewController ()
//
//@end
@interface NoteRichTextViewController () <UIPopoverPresentationControllerDelegate>  //leg20211230 - TopXNotes_Catalyst_2
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftButton;
@end


@implementation NoteRichTextViewController

@synthesize notePaperView, slider;
@synthesize fontSliderValue;
@synthesize model;
@synthesize noteFontName, noteFontSize;

@synthesize hoverView, emailButton;
@synthesize tapRecognizer, swipeLeftRecognizer, longPressRecognizer;
@synthesize toolsHUDButton;
@synthesize noteCreationDate, barButtonItem;
@synthesize searchFieldOutlet;                                                  //leg20220526 - TopXNotes_Catalyst_2
@synthesize noteIndex;
@synthesize displayTreeIndex;                                                   //leg20220527 - TopXNotes_Catalyst_2
@synthesize selectedGroupUUID;                                                  //leg20220526 - TopXNotes_Catalyst_2

// Remove obsolete code.                                                        //leg20220621 - TopXNotes_Catalyst_2
//- (void)updateUI
//{
//    self.slider.value = self.fontSliderValue;
////    noteText.font = [UIFont fontWithName:noteFontName size:[noteFontSize intValue]];
//
//    // Fix Issue #003 - Ruled notepaper lines do not vary with fontsize.
//    //  Comparison of systemVersion in Catalyst Mac returns the MacOS version
//    //  instead of the iOS version, so API check was invalid.
//    // Since target iOS is now always > 4.0, use lineHeight property.
//    //  lineHeight @Property available beginning in iOS 4.0.
//    //
//    // Get width to make note paper lines.
////    self.notePaperView.lineWidth = noteText.font.lineHeight;
//
//    [self.view setNeedsDisplay];
//    [self.notePaperView setNeedsDisplay];
//}
//
//- (void)setFontSliderValue:(int)newFontSliderValue
//{
//    if (newFontSliderValue < 0) newFontSliderValue = 0;
//    if (newFontSliderValue > 100) newFontSliderValue = 100;
//    fontSliderValue = newFontSliderValue;
//    NSMutableString *fontSizeString = (NSMutableString*)[NSString
//                                        stringWithFormat:@"%d",
//                                        (int)[self fontSizeForNotePaperView:nil]];
//    self.noteFontSize = fontSizeString;
//    [self updateUI];
//}
//
//- (IBAction)fontSliderValueChanged:(UISlider *)sender
//{
//    self.fontSliderValue = sender.value;
//}
//
////- (float)fontSizeForNotePaperView:(NotePaperView *)requestor
//- (float)fontSizeForNotePaperView:(id)requestor
//{
//#pragma unused (requestor)
//
//    float fontSize = 0;
//        if (self.fontSliderValue <= 10) {
//            fontSize = 8.0;
//        } else if (self.fontSliderValue <= 20) {
//            fontSize = 10.0;
//        } else if (self.fontSliderValue <= 30) {
//            fontSize = 12.0;
//        } else if (self.fontSliderValue <= 40) {
//            fontSize = 14.0;
//        } else if (self.fontSliderValue <= 50) {
//            fontSize = 16.0;
//        } else if (self.fontSliderValue <= 60) {
//            fontSize = 18.0;
//        } else if (self.fontSliderValue <= 70) {
//            fontSize = 20.0;
//        } else if (self.fontSliderValue <= 80) {
//            fontSize = 22.0;
//        } else if (self.fontSliderValue <= 90) {
//            fontSize = 24.0;
//        } else if (self.fontSliderValue <= 100) {
//            fontSize = 26.0;
//        }
//    return fontSize;
//}
//
//- (void)updateFontSizeSlider
//{
//    if ([noteFontSize intValue] == 8) {
//        self.fontSliderValue = 10;
//    } else if ([noteFontSize intValue] == 10) {
//        self.fontSliderValue = 20;
//    } else if ([noteFontSize intValue] == 12) {
//        self.fontSliderValue = 30;
//    } else if ([noteFontSize intValue] == 14) {
//        self.fontSliderValue = 40;
//    } else if ([noteFontSize intValue] == 16) {
//        self.fontSliderValue = 50;
//    } else if ([noteFontSize intValue] == 18) {
//        self.fontSliderValue = 60;
//    } else if ([noteFontSize intValue] == 20) {
//        self.fontSliderValue = 70;
//    } else if ([noteFontSize intValue] == 22) {
//        self.fontSliderValue = 80;
//    } else if ([noteFontSize intValue] == 24) {
//        self.fontSliderValue = 90;
//    } else if ([noteFontSize intValue] == 26) {
//        self.fontSliderValue = 100;
//    } else {
//        self.fontSliderValue = 100;
//    }
//}
//
//- (void)viewDidLoadNotePaperView
//{
//    [self updateFontSizeSlider];
//
//    self.notePaperView.delegate = self;
//    UIGestureRecognizer *pinchgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self.notePaperView action:@selector(pinch:)];
//    [self.notePaperView addGestureRecognizer:pinchgr];
//    [self updateUI];
//}

#pragma mark - Helpers

-(NSString*)getNextUntitledNoteName:(BOOL)shouldUpdate {                        //leg20220621 - TopXNotes_Catalyst_2
    // Reading Defaults to get next number.
    NSUserDefaults *defaults;
    NSMutableDictionary *savedSettingsDictionary;
    defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Get next untitled note number.
    NSInteger noteNumber = 0;
    NSNumber *nextNoteNumber = [savedSettingsDictionary objectForKey: kNextNoteNumber_Key];
    if (nextNoteNumber != nil)
        noteNumber = [nextNoteNumber integerValue];

    if (shouldUpdate) {
        nextNoteNumber = [NSNumber numberWithInteger:noteNumber+1];
        [savedSettingsDictionary setObject:nextNoteNumber forKey:kNextNoteNumber_Key];
        
        // Save settings
        [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    return [NSString stringWithFormat:@"untitled note %ld", (long)noteNumber];
}

// Create a note with title.                                                    //leg20220527 - TopXNotes_Catalyst_2
-(void)createNoteWithTitle:(NSString *)title withinGroup:(NSUUID*)groupUUID {   //leg20220919 - TopXNotes_Catalyst_2
    // macOS is server, and as such controls noteIDs assignment.  On iOS    //leg20211207 - TopXNotes_Catalyst_2
    //  noteID is 0 at note creation.
    #if TARGET_OS_UIKITFORMAC
        noteID = [model nextNoteID];
    #else
        noteID = [NSNumber numberWithUnsignedLong: 0];
    #endif

    // Change init to support Groups.                                       //leg20220519 - TopXNotes_Catalyst_2
    Note *note = [[Note alloc] initWithNoteTitle:title
                                            text:title
                                          withID:noteID
                                     andWithUUID:[Model genNoteUUID]
                                     withinGroup:groupUUID                      //leg20220919 - TopXNotes_Catalyst_2
                                          onDate:[NSDate date]];
    
    // Make html from text.                                                     //leg20220621 - TopXNotes_Catalyst_2
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:note.noteText];
    note.noteHTML = [Ashton encode:attributedString];

    // Save noteID for when added note is saved.
    noteID = note.noteID;
    
    // Mark note as eligible to be synced
    note.syncFlag = [NSNumber numberWithBool:YES];
    
    // Mark note needs to be synced
    note.needsSyncFlag = [NSNumber numberWithBool:YES];
    
    // Add note to front of list, so it will have an index of 0.
    [model addNote:note];
    noteIndex = 0;

    // Put note into its parent group.                                          //leg20220527 - TopXNotes_Catalyst_2
    [model moveNoteOrGroup:note.noteUUID
                 fromGroup:nil
                   toGroup:note.groupUUID];
    
    // Get updated note to add to display tree.
    note = [model getNoteForUUID:note.noteUUID];

    // Add note to display list.
    [model addDisplayNote:note];
}
#pragma mark - Actions

-(IBAction)toggleToolsHUD:(UIBarButtonItem *)sender {
    
    // Remove gesture recognizer while heads-up display is active
    [self.view removeGestureRecognizer:swipeLeftRecognizer];
    
    // Save the bar button so that it can be re-enabled once timer has expired.
    self.barButtonItem = sender;
    self.barButtonItem.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:Show_HoverView_RTE object:nil];
}

-(IBAction)toggleNotepaperBG:(UIBarButtonItem *)sender {
#pragma unused (sender)

    // Show/Hide notebook paper background.
    if (showNotepaper) {
        self.notePaperView.hidden = YES;
        showNotepaper = NO;
    } else {
        self.notePaperView.hidden = NO;
        showNotepaper = YES;
    }
    
    // Save show/hide notepaper background state
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Set current state of show/hide notepaper.
    [savedSettingsDictionary setObject:[NSNumber numberWithBool:showNotepaper] forKey:kShowNotepaper_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(IBAction)showBackupRestore:(UIBarButtonItem *)sender {
#pragma unused (sender)
    
    [self performSegueWithIdentifier:@"Show_Backup_Restore_Segue" sender: self];
}

// Temporary routine for checking out popover.
//-(IBAction)showFontPickerPopove:(UIBarButtonItem *)sender {
//#pragma unused (sender)
//
//    // grab the view controller we want to show         //leg20211230 - TopXNotes_Catalyst_2
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
////        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"Pop"];
////        UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"Note_Preferences_Segue"];
//    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"NoteSettingsViewController"];
//
//    // present the controller
//    // on iPad, this will be a Popover
//    // on iPhone, this will be an action sheet
//    controller.modalPresentationStyle = UIModalPresentationPopover;
//    [self presentViewController:controller animated:YES completion:nil];
//
//    // configure the Popover presentation controller
//    UIPopoverPresentationController *popController = [controller popoverPresentationController];
//    popController.permittedArrowDirections = UIPopoverArrowDirectionUp;
//    popController.delegate = self;
//
//    // in case we don't have a bar button as reference
//    popController.sourceView = self.view;
//    popController.sourceRect = CGRectMake(30, 50, 10, 10);
//}

-(IBAction)done {
    // If Emailing a note we must eat keyboard notifications so as to not
    //    crash when we come out of Mail Composer.
    if (emailInProgress) {
        return;
    }
    
    __block NSString* titleString;
    NSDate* date = [NSDate date];
    noteIsDirty = NO;                // Mark note no longer dirty.

    // If note was unchanged pop view controller, else update the model with changed note
    if ([doneButton.title isEqualToString:@"Edit"]) {

        self.navigationController.toolbarHidden=YES;

        // Enable Done button
        doneButton.enabled = YES;
        doneButton.style = UIBarButtonItemStyleDone;
        doneButton.title = @"Done";

    } else if ([doneButton.title isEqualToString:@"Save"]) {
        
        // MARK: practice exporting as html and rtf.                            //leg20211027 - TopXNotes_Catalyst_2
        // TODO: •••••Remove ••••• practice exporting as html and rtf.          //leg20211027 - TopXNotes_Catalyst_2
        // Issue #015 apparently caused by calling -exportHTML. See log at      //leg20211215 - TopXNotes_Catalyst_2
        //  tag //leg20211027.  Comment-out call for now.
//        [self exportHTML];
        
        // Get the HTML and decode it into an NSString.
        [self getHTML:^(NSString *theHTML, NSError * _Nullable error) {
            
            // Show the toolbar.
            self.navigationController.toolbarHidden=NO;

            // The ZSSRichTextEditor.m -getText function squnches all the text
            //  from the HTML together, messing up the title. The following
            //  code replaces calling -getText with code that preserves
            //  white space, carriage returns, etc.

            // Convert HTML string with UTF-8 encoding to NSAttributedString
            NSError *error2 = nil;
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                   initWithData: [theHTML dataUsingEncoding:NSUnicodeStringEncoding]
                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                   documentAttributes: nil
                                                   error: &error2 ];
            
            // If an error occurred creating the attributed string containing
            //  images we must use Ashton to decode the HTML into an attributed
            //  string.
            if ([error2 code] != 0) {
                NSLog(@"Error creating attributed string from HTML - error=\"%@\", code=%ld", [error2 localizedDescription], (long)[error2 code] );
                attributedString = [Ashton decode:theHTML defaultAttributes:[NSDictionary dictionary]];
            }

            // Fill in the Text extracted from the HTML.
            self->notePlainText = attributedString.string;
            
            // Remove any leading control characters from note text.            //leg20211117 - TopXNotes_Catalyst_2
            NSRange range = [self->notePlainText rangeOfCharacterFromSet:[NSCharacterSet alphanumericCharacterSet]];
            if (range.location != NSNotFound)
                self->notePlainText = [self->notePlainText substringFromIndex:range.location];
            
            // Remove any leading whitespace from note text.                    //leg20211116 - TopXNotes_Catalyst_2
            range = [self->notePlainText rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
            self->notePlainText = [self->notePlainText stringByReplacingCharactersInRange:range withString:@""];
            

            // Create note title from 1st kNoteTitleMaxLength characters of text
            if ([self->notePlainText length] < kNoteTitleMaxLength)
                titleString = self->notePlainText;
            else
                titleString = [self->notePlainText substringToIndex:(NSUInteger)kNoteTitleMaxLength];

            // Remove non-breaking spaces from note title.                      //leg20220118 - TopXNotes_Catalyst_2
            titleString = [titleString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];

            // Make title end at first newline character
            range = [titleString rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]];
            if (range.location != NSNotFound)
                titleString = [titleString substringToIndex:range.location];

            // Update the title in the navigation bar.                          //leg20220921 - TopXNotes_Catalyst_2
            self.title = titleString;

            // Use a new initWithTitle method which allows specification of the
            //  creation date.  This solves the problem of the creation date being
            //  updated every time a note is edited.
            
            // Fix Sync issue where newly added mobile note was lost on Sync
            //  and notes could be duplicated.  Caused by giving new notes a NoteID.
            //  NoteIDs should only be assigned by Mac TopXNotes3 Catalyst in order
            //  to keep NoteIDs unique.
            
            // Change init to support Groups.  Add to root group initially.     //leg20220519 - TopXNotes_Catalyst_2
            Note *note = [[Note alloc] initWithNoteTitle:titleString
                                                    text:self->notePlainText
                                                  withID:[NSNumber numberWithLong:0]
                                             andWithUUID:[Model genNoteUUID]
                                             withinGroup:[self->model getUUIDOfGroupAtIndex:0]
                                                  onDate:date
                                              createDate:self.noteCreationDate];

            // Fill in html from Rich Text Editor.
            note.noteHTML = theHTML;
            
            // Fill in data from note in model.
            note.createDate = [self->model getNoteForIndex:self->noteIndex].createDate;
            note.encryptedNoteText = [self->model getNoteForIndex:self->noteIndex].encryptedNoteText;
            note.encryptedNoteHTML = [self->model getNoteForIndex:self->noteIndex].encryptedNoteHTML;   //leg20211129 - TopXNotes_Catalyst_2
            note.encryptedPassword = [self->model getNoteForIndex:self->noteIndex].encryptedPassword;
            note.passwordHint = [self->model getNoteForIndex:self->noteIndex].passwordHint;
            note.decryptedPassword = [self->model getNoteForIndex:self->noteIndex].decryptedPassword;
            note.groupID = [self->model getNoteForIndex:self->noteIndex].groupID;
            note.groupUUID = [self->model getNoteForIndex:self->noteIndex].groupUUID;                   //leg20220527 - TopXNotes_Catalyst_2
            note.noteUUID = [self->model getNoteForIndex:self->noteIndex].noteUUID;                     //leg20220527 - TopXNotes_Catalyst_2
            
            // macOS is server, and as such controls noteIDs assignment.        //leg20211207 - TopXNotes_Catalyst_2
            #if TARGET_OS_UIKITFORMAC
                if ([[self->model getNoteForIndex:self->noteIndex].noteID unsignedLongValue] == 0)
                        note.noteID = [self->model nextNoteID];
                    else
                        note.noteID = [self->model getNoteForIndex:self->noteIndex].noteID;
            #else
                note.noteID = [self->model getNoteForIndex:self->noteIndex].noteID;
            #endif
            
            note.protectedNoteFlag = [self->model getNoteForIndex:self->noteIndex].protectedNoteFlag;
            note.decryptedNoteFlag = [self->model getNoteForIndex:self->noteIndex].decryptedNoteFlag;
            note.npSync = [self->model getNoteForIndex:self->noteIndex].npSync;
            note.nfChangedSync = [self->model getNoteForIndex:self->noteIndex].nfChangedSync;

            // Mark note as eligible to be synced
            note.syncFlag = [NSNumber numberWithBool:YES];

            // Mark note needs to be synced
            note.needsSyncFlag = [NSNumber numberWithBool:YES];
            
            // If the note is a protected note and is currently decrypted,      //leg20211130 - TopXNotes_Catalyst_2
            //  re-encrypt it.
            if ([note.decryptedNoteFlag boolValue] && [note.protectedNoteFlag boolValue])
                [self reEncryptNote:note withPassword:note.decryptedPassword];

            // Update the data model of notes
            [self->model replaceNoteAtIndex:self->noteIndex withNote:note];
            
            // Update the data model of notes
            [self->model replaceDisplayNoteAtIndex:self->displayTreeIndex withNote:note];
            
            // Update the note index since the note is now at the top of the list
            self->noteIndex = 0;
            
            // Update the table view display list.                              //leg20220527 - TopXNotes_Catalyst_2
            [self->model replaceDisplayNoteAtIndex:self->displayTreeIndex withNote:note];

            // Enable Done button
            self->doneButton.enabled = YES;
            self->doneButton.style = UIBarButtonItemStylePlain;
            self->doneButton.title = @"Edit";

            // Notify notepad that it has changed.
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
            
            // Sync notepad to CloudKit container.                              //leg20220906 - TopXNotes_Catalyst_2
            BOOL syncResult = [self->model syncCloudData:YES];                  //leg20220912 - TopXNotes_Catalyst_2

            // Notify notepad to make an auto backup.                           //leg20211206 - TopXNotes_Catalyst_2
            [[NSNotificationCenter defaultCenter] postNotificationName:kAutoBackups_Notification object:nil];
        }];
    } else {
        // "Done"  note unchanged
        self.navigationController.toolbarHidden=NO;
        
        // Enable Done button
        doneButton.enabled = YES;
        doneButton.style = UIBarButtonItemStylePlain;
        doneButton.title = @"Edit";
    }
}

// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Prepare to edit or create new note.
- (void)setUpNoteAction {
    // Insure Email In Progress flag is initially off.
    emailInProgress = NO;
    
    // Get font information.
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];

    // Get Font names and sizes
    noteFontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
    if (noteFontName == nil) {
        noteFontName = (NSMutableString*)kNoteTextFont;
        [savedSettingsDictionary setObject:noteFontName forKey:kNoteFontName_Key];
    }
    noteFontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
    if (noteFontSize == nil) {
        noteFontSize = (NSMutableString*)[NSString stringWithFormat:@"%d", kNoteTextFontSize];
        [savedSettingsDictionary setObject:noteFontSize forKey:kNoteFontSize_Key];
    }

    // Get last chosen style Font defaults.                                     //leg20220111 - TopXNotes_Catalyst_2                                                               //leg20110416 - 1.0.4
    NSString * lastFontName = [savedSettingsDictionary objectForKey: kFontNameLastChosen_Key];
    if (lastFontName == nil) {
        lastFontName = noteFontName;
        [savedSettingsDictionary setObject:lastFontName forKey:kFontNameLastChosen_Key];
    }
    NSString * lastFontSize = [savedSettingsDictionary objectForKey: kFontSizeLastChosen_Key];
    if (lastFontSize == nil) {
        lastFontSize = noteFontSize;
        [savedSettingsDictionary setObject:lastFontSize forKey:kFontSizeLastChosen_Key];
    }

    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// In response to a swipe gesture, show the HUD.
 - (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer {
    // Remove gesture recognizer while heads-up display is active
    [self.view removeGestureRecognizer:recognizer];
    [[NSNotificationCenter defaultCenter] postNotificationName:Show_HoverView_RTE object:nil];
}

// Pass information to view controller segued to.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show_Backup_Restore_Segue"] ) {
        // Pass model to EncryptionSettingsViewController view.
        BackupSettingsViewController *controller = (BackupSettingsViewController *)segue.destinationViewController;
        controller.model = self.model;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Make tab and nav bars darker color only in iOS target.                   //leg20211116 - TopXNotes_Catalyst_2
#if !TARGET_OS_UIKITFORMAC
    // Make the tab bar a darker color so selected tab stands-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.tabBarController.tabBar.barTintColor = [UIColor lightGrayColor];

    // Make the navigation bar a darker color so controls stand-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
#endif

    // Set navigation and tab appearance for macOS.                             //leg20220118 - TopXNotes_Catalyst_2
#if TARGET_OS_UIKITFORMAC
    UINavigationBarAppearance *barAppearance = [[UINavigationBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    UITabBarAppearance *tabBarAppearance = [[UITabBarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    barAppearance.backgroundColor = [UIColor lightGrayColor];
//    tabBarAppearance.backgroundColor = [UIColor lightGrayColor];
    self.navigationController.navigationBar.standardAppearance = barAppearance;
    self.navigationController.navigationBar.scrollEdgeAppearance = barAppearance;
//    self.tabBarController.tabBar.standardAppearance = tabBarAppearance;
#endif

    // If notepad is empty then create at least 1 note.
    if (model.numberOfNotes == 0) {
            
        // Create a new untitled note with the next number.                     //leg20220621 - TopXNotes_Catalyst_2
        NSString *newNoteName = [self getNextUntitledNoteName:YES];
        self.title = newNoteName;
        [self createNoteWithTitle:newNoteName                                   //leg20220919 - TopXNotes_Catalyst_2
                      withinGroup:[self->model getRootGroupUUID]];

        // First run "untitled note" created.  Notify notepad to make an auto   //leg20220222 - TopXNotes_Catalyst_2
        //  backup. Prevents crash if Backup/Restore picker called before the
        //  first back-up would normally be made.
        [[NSNotificationCenter defaultCenter] postNotificationName:kAutoBackups_Notification object:nil];

        // Notify notepad that it has changed.
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    }

    // MARK: Begin RTE code •••••••••••••••••••••••••••••••••••••••••••••••••
    
    //Set Custom CSS
    NSString *customCSS = @"";
    [self setCSS:customCSS];
        
    self.receiveEditorDidChangeEvents = YES;
    self.toolIsPushedView = NO;                                                 //leg20211130 - TopXNotes_Catalyst_2
    self.alwaysShowToolbar =YES;                                                //leg20211230 - TopXNotes_Catalyst_2
    
    // HTML Content to set in the editor.                                       //leg20211115 - TopXNotes_Catalyst_2
    //
    // For new note content is blank.
    NSString *html = @"";
    if (noteIndex != -1) {
        Note *theNote = [model getNoteForIndex:noteIndex];

        html = theNote.noteHTML;

        if ([theNote.noteHTML isEqualToString:@""]) {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:theNote.noteText];
            html = [Ashton encode:attributedString];
            noteHTML = html;
        } else {
            noteHTML = theNote.noteHTML;
        }
    }
  
    // Pretty up HTML.
    self.formatHTML = YES;
    
    // Set the base URL if you would like to use relative links, such as to images.
    self.baseURL = [NSURL URLWithString:@"http://www.zedsaid.com"];
    self.shouldShowKeyboard = NO;
    
    // Set the HTML contents of the editor
//    [self setPlaceholder:@"This is a placeholder that will show when there is no content(html)"];
    // Eliminate placeholder text
    [self setPlaceholder:[self getNextUntitledNoteName:NO]];                                                  //leg20220621 - TopXNotes_Catalyst_2
    
    [self setHTML:html];
    
// MARK: end RTE code ••••••••••••••••••••••••••••••••••••
    
    // Insure there is a back button on iPhones.
    self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
    self.navigationItem.leftItemsSupplementBackButton = YES;

    // Insure access to notepad for split view initial view.
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    self.model = appDelegate.model;

    // Make the navigation bar a darker color so controls stand-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];

    // Called by SplitView Delegate when Back button is hit.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitViewBackButtonHit)
                                                 name:kSplitView_Back_Button_Hit object:nil];

   // Prepare to edit or create new note.
    [self setUpNoteAction];

    // Since we can have a heads-up display we don't need the Email button on the text view.
    //emailButton.hidden = YES;

    // Create a swipe gesture recognizer to recognize left or right swipes.
    //  Keep a reference to the recognizer so that it can be added to and removed from the view.
    swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft+UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    swipeLeftRecognizer.delegate = self;

    // Since we now bring up the HUD on all devices we do not need the separate email button.
    emailButton.hidden = YES;

    // Either adding a new note or editing an existing note.
    if (noteIndex == -1) {

        // Create a new untitled note with the next number.                     //leg20220621 - TopXNotes_Catalyst_2
        NSString *newNoteName = [self getNextUntitledNoteName:YES];
        self.title = newNoteName;
        [self createNoteWithTitle:newNoteName                                   //leg20220919 - TopXNotes_Catalyst_2
                      withinGroup:selectedGroupUUID];

        // Notify notepad that it has changed.
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    } else {
        // Existing note.
        Note *note = [model getNoteForIndex:noteIndex];
        
        // Refresh from source each time.
        noteHTML = note.noteHTML;
        
        syncFlag = note.syncFlag;
        noteID = note.noteID;
        needsSyncFlag = note.needsSyncFlag;
        self.noteCreationDate = note.createDate;
        
        // this will appear as the title in the navigation bar
        self.title = note.title;

        // If note is protected, obscure note contents now that we have         //leg20211201 - TopXNotes_Catalyst_2
        //  retreived the decrypted data for the note view.
        if ([note.protectedNoteFlag boolValue]) {
            note.noteText = kEncrypted_Placeholder_Notice;
            note.noteHTML = kEncrypted_Placeholder_Notice;

            // Update the data model of notes
            [self->model replaceNoteAtIndex:self->noteIndex withNote:note];
        }
    }

// Remove obsolete code.                                                        //leg20220621 - TopXNotes_Catalyst_2
//    UIBarButtonItem * showHUDButtonItem = [[UIBarButtonItem alloc]
//                                           initWithImage:[UIImage
//                                           imageNamed:@"fonts.png"]
//                                           style:UIBarButtonItemStylePlain
//                                           target:self
//                                           action:@selector(toggleToolsHUD:)];
//
//    UIBarButtonItem * toggleNotepaperBGButtonItem = [[UIBarButtonItem alloc]
//                                                initWithImage:[UIImage
//                                                imageNamed:@"notepaperBG.png"]
//                                                style:UIBarButtonItemStylePlain
//                                                target:self
//                                                action:@selector(toggleNotepaperBG:)];

    // Build a toolbar.
    UIBarButtonItem * bkupRestoreButtonItem = [[UIBarButtonItem alloc]
                                                 initWithImage:[UIImage
                                                 imageNamed:@"data_backup-32"]
                                                 style:UIBarButtonItemStylePlain
                                                 target:self
                                                 action:@selector(showBackupRestore:)];

    UIBarButtonItem * actionButtonItem = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                            target:self
                                            action:@selector(emailNote)];

    // Spacer button
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    self.navigationController.toolbarHidden=NO;
    self.navigationController.toolbar.barStyle = UIBarStyleDefault;             //leg20211208 - TopXNotes_Catalyst_2
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.navigationController.toolbar.barTintColor = [UIColor lightGrayColor];  //leg20211208 - TopXNotes_Catalyst_2
    self.navigationController.toolbar.translucent = YES;
//    self.toolbarItems = [NSArray arrayWithObjects:toggleNotepaperBGButtonItem, flexiableItem, showHUDButtonItem, bkupRestoreButtonItem, flexiableItem, actionButtonItem, nil];
    // Eliminate obsolete tools from toolbar.                                   //leg20220621 - TopXNotes_Catalyst_2
    self.toolbarItems = [NSArray arrayWithObjects:flexiableItem, bkupRestoreButtonItem, flexiableItem, actionButtonItem, nil];
 
    // Set toolbar appearance for macOS.                                        //leg20220118 - TopXNotes_Catalyst_2
#if TARGET_OS_UIKITFORMAC
    UIToolbarAppearance *toolBarAppearance = [[UIToolbarAppearance alloc] initWithIdiom:UIUserInterfaceIdiomPad];
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    toolBarAppearance.backgroundColor = [UIColor lightGrayColor];
    self.navigationController.toolbar.standardAppearance = toolBarAppearance;
    self.navigationController.toolbar.compactAppearance = toolBarAppearance;

// NOOP this call when compiling on Catalina and Xcode 12.4 because             //leg20220118 - TopXNotes_Catalyst_2
//  macCatalyst 15.0 SDK not available.
#if BUILDING_ON_MONTEREY
    if (@available(macCatalyst 15.0, *)) {
        self.navigationController.toolbar.scrollEdgeAppearance = toolBarAppearance;
    } else {
        // Fallback on earlier versions
    }
#endif
    
#endif

// Remove obsolete code.                                                        //leg20220621 - TopXNotes_Catalyst_2
//    // Disable these buttons since they make no sense in Rich Text Editor.
//    toggleNotepaperBGButtonItem.enabled = NO;
//    showHUDButtonItem.enabled = NO;

}

// MARK: Begin RTE functions ••••••••••••••••••••••••••••••••••••

- (void)showInsertURLAlternatePicker {
    
    [self dismissAlertView];
    
    ZSSDemoPickerViewController *picker = [[ZSSDemoPickerViewController alloc] init];
    picker.demoView = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:picker];
    nav.navigationBar.translucent = NO;
    [self presentViewController:nav animated:YES completion:nil];
    
}


- (void)showInsertImageAlternatePicker {
    
    [self dismissAlertView];
    
    ZSSDemoPickerViewController *picker = [[ZSSDemoPickerViewController alloc] init];
    picker.demoView = self;
    picker.isInsertImagePicker = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:picker];
    nav.navigationBar.translucent = NO;
    [self presentViewController:nav animated:YES completion:nil];
    
}


- (void)exportHTML {
    [self getHTML:^(NSString *result, NSError * _Nullable error) {
        
        NSLog(@"-exportHTML, after -getHTML result=%@", result);

        // MARK: Experimental code to save notes to .html and .rtf files.       //leg20211027 - TopXNotes_Catalyst_2
        Note *theNote = [self->model getNoteForIndex:self->noteIndex];
        
        theNote.noteHTML = result;
        NSError *error2;
        
        // Export note as HTML.
        NSString *htmlStringToWrite = result;
        NSString *htmlFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"myFile.html"];
        [htmlStringToWrite writeToFile:htmlFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error2];
        if ([error2 code] != 0) {
            NSLog(@"Error saving note as HTML file - error=\"%@\"", [error2 localizedDescription]);
        }

        // Export note as RTF.

        // Convert HTML string with UTF-8 encoding to NSAttributedString
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                               initWithData: [htmlStringToWrite dataUsingEncoding:NSUnicodeStringEncoding]
                                               options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                               documentAttributes: nil
                                               error: &error2 ];
        // If an error occurred creating the attributed string containing
        //  images we must use Ashton to decode the HTML into an attributed
        //  string.
        if ([error2 code] != 0) {
            NSLog(@"- (void)exportHTML - Error creating attributed string from HTML - error=\"%@\", code=%ld", [error2 localizedDescription], (long)[error2 code] );
            attributedString = [Ashton decode:htmlStringToWrite defaultAttributes:[NSDictionary dictionary]];
        }

        NSString *rtfEncodedString = [attributedString encodeRTFWithImages];
        NSString *rtfFilePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:@"myFile.rtf"];
        [rtfEncodedString writeToFile:rtfFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error2];
        if ([error2 code] != 0) {
            NSLog(@"- (void)exportHTML - Error saving note as RTF file - error=\"%@\", code=%ld", [error2 localizedDescription], (long)[error2 code] );
        }

//        // Make sure Mac notepad gets saved.
//        [self->model saveData];

    }];

}

- (void)editorDidChangeWithText:(NSString *)text andHTML:(NSString *)html {
    
//    NSLog(@"Text Has Changed: %@", text);
//
//    NSLog(@"HTML Has Changed: %@", html);
    
    // Since the text changed, enable the Save button.
    if (!noteIsDirty) {
        doneButton.title = @"Save";
        doneButton.style = UIBarButtonItemStyleDone;
        doneButton.enabled = YES;
        noteIsDirty = YES;
    }
}

// Callback routine if editor scrolled.                                         //leg20211213 - TopXNotes_Catalyst_2
- (void)editorDidScrollWithPosition:(NSInteger)position
{
    NSLog(@"Did scroll with position: %ld", (long)position);
}

- (void)hashtagRecognizedWithWord:(NSString *)word {
    
    NSLog(@"Hashtag has been recognized: %@", word);
    
}

- (void)mentionRecognizedWithWord:(NSString *)word {
    
    NSLog(@"Mention has been recognized: %@", word);
    
}

//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}


// MARK: End RTE functions ••••••••••••••••••••••••••••••••••••

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

    // Default to tools that don't trigger -viewDidDisappear.                   //leg20211130 - TopXNotes_Catalyst_2
    self.toolIsPushedView = NO;
    
    // Make tab and nav bars darker color only in iOS target.                   //leg20211116 - TopXNotes_Catalyst_2
#if !TARGET_OS_UIKITFORMAC
    // Make the tab bar a darker color so selected tab stands-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.tabBarController.tabBar.barTintColor = [UIColor lightGrayColor];

    // Make the navigation bar a darker color so controls stand-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
#endif

    // add our custom button as the nav bar's custom right view
    doneButton = [[UIBarButtonItem alloc]
                   initWithTitle:@"Edit"
                   style:UIBarButtonItemStyleBordered
                   target:self
                   action:@selector(action:)];
    
    // Enable "Done" button.
    doneButton.enabled = YES;
    doneButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = doneButton;
    
    dateLabel.text = dateString;

    // Get text characteristics from defaults dictionary.
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil)
        savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
    else
        savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    noteFontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
    noteFontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];

    // Get last chosen style Font defaults.                                     //leg20220111 - TopXNotes_Catalyst_2                                                               //leg20110416 - 1.0.4
    NSString * lastFontName = [savedSettingsDictionary objectForKey: kFontNameLastChosen_Key];
    if (lastFontName == nil) {
        lastFontName = noteFontName;
        [savedSettingsDictionary setObject:lastFontName forKey:kFontNameLastChosen_Key];
    }
    NSString * lastFontSize = [savedSettingsDictionary objectForKey: kFontSizeLastChosen_Key];
    if (lastFontSize == nil) {
        lastFontSize = noteFontSize;
        [savedSettingsDictionary setObject:lastFontSize forKey:kFontSizeLastChosen_Key];
    }

    // Get show/hide notepaper state.
    showNotepaper = [[savedSettingsDictionary objectForKey:kShowNotepaper_Key] boolValue];

    // Restore show/hide notebook paper background state.
    if (showNotepaper) {
        self.notePaperView.hidden = NO;
    } else {
        self.notePaperView.hidden = YES;
    }
    
    
// Remove obsolete code.                                                        //leg20220621 - TopXNotes_Catalyst_2
////    // Set text attributes.
////    noteText.textColor = [UIColor blackColor];
////    noteText.font = [UIFont fontWithName:noteFontName size:[noteFontSize intValue]];
//    [self updateFontSizeSlider];
//    [self updateUI];

    // If notepad is empty then create at least 1 note.
    if (model.numberOfNotes == 0) {
        
        // Create a new untitled note with the next number.                     //leg20220621 - TopXNotes_Catalyst_2
        NSString *newNoteName = [self getNextUntitledNoteName:YES];
        self.title = newNoteName;
        [self createNoteWithTitle:newNoteName                                   //leg20220919 - TopXNotes_Catalyst_2
                      withinGroup:[self->model getRootGroupUUID]];

        // Notify notepad that it has changed.
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    }
    
    // Either adding a new note or editing an existing note.
    if (noteIndex == -1) {  // Add new note.
        noteIndex = 0;      // Reset noteIndex to first note in list.
        
        // macOS is server, and as such controls noteIDs assignment.  On iOS    //leg20211207 - TopXNotes_Catalyst_2
        //  noteID is 0 at note creation.
        #if TARGET_OS_UIKITFORMAC
            noteID = [model nextNoteID];
        #else
            noteID = [NSNumber numberWithUnsignedLong: 0];
        #endif

        // Change init to support Groups.                                       //leg20220519 - TopXNotes_Catalyst_2
        Note *note = [[Note alloc] initWithNoteTitle:@""
                                                text:@""
                                              withID:noteID
                                         andWithUUID:[Model genNoteUUID]
                                         withinGroup:selectedGroupUUID
                                              onDate:[NSDate date]];
        
        // Save noteID for when added note is saved.
        noteID = note.noteID;
        
        // Mark note as eligible to be synced
        note.syncFlag = [NSNumber numberWithBool:YES];
        
        // Mark note needs to be synced
        note.needsSyncFlag = [NSNumber numberWithBool:YES];

        // Add note to front of list, so it will have an index of 0.
        [model addNote:note];
        noteIndex = 0;

        // Put note into its parent group.                                      //leg20220527 - TopXNotes_Catalyst_2
        [model moveNoteOrGroup:note.noteUUID
                     fromGroup:nil
                       toGroup:note.groupUUID];
        
        // Get updated note to add to display tree.                             //leg20220527 - TopXNotes_Catalyst_2
        note = [model getNoteForUUID:note.noteUUID];

        // Add note to display list.                                            //leg20220526 - TopXNotes_Catalyst_2
        [model addDisplayNote:note];

        // Enable Done button
        doneButton.enabled = YES;
        doneButton.style = UIBarButtonItemStyleDone;
        doneButton.title = @"Done";
    } else {    // Edit existing note.
        Note *note = [model getNoteForIndex:noteIndex];
        if ([note.noteHTML isEqualToString:@""]) {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:note.noteText];
            note.noteHTML = [Ashton encode:attributedString];
        }
        
        noteHTML = note.noteHTML;
        
        // Disable Edit button if note is encrypted.
        if ([note.protectedNoteFlag boolValue])
            doneButton.enabled = NO;
        
        // Disable Edit button if this is a currently decrypted note that
        //  the viewing session was interrupted by a switch to another tab,
        //  and has now been returned to.
        if (note.decryptedNoteFlag && [note.noteText isEqualToString:kEncrypted_Placeholder_Notice]) {
            doneButton.enabled = NO;
        }
    }
    
    noteIsDirty = NO;
}

// Split View Back Button Hit -- Make sure a dirty note gets updated.
- (void)splitViewBackButtonHit {
    // Make sure note gets updated if portrait mode Back button hit.
    if (noteIsDirty)
        [self viewWillDisappear:true];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    // Make sure note gets updated if user switches to another view as long as  //leg20211130 - TopXNotes_Catalyst_2
    //  -viewDidDisappear wasn't triggered by a pushed tool view such as the
    //  Font or Color pickers.
    if (noteIsDirty && !self.toolIsPushedView)
        [self done];
}

- (void)killTimer
{
    // reset the HUD timer
    [myTimer invalidate];
    myTimer = nil;

    self.barButtonItem.enabled = YES;
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
     // calculate whether touchPoint was within hoverview
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint touchPoint = [sender locationInView:self.view];
        if (!CGRectContainsPoint (hoverView.frame, touchPoint)) {
        
            // Touch point was in the text view, a signal to hide HUD.
            [self killTimer];
            [self showHoverView:NO];
        }
    }
}

- (void)showHoverView:(BOOL)show
{
    // Stop any HUD timer that might be counting down.

    // fade animate the view out of view by affecting its alpha
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.40];

    if (show)
    {
        
        // Create a tap gesture recognizer to recognize single taps.
        //  Keep a reference to the recognizer so that it can be added to
        //  and removed from the view.
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        tapRecognizer.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:tapRecognizer];
        tapRecognizer.delegate = self;

        self.barButtonItem.enabled = NO;
        // as we start the fade effect, start the timeout timer for automatically hiding HoverView
        self.hoverView.alpha = 1.0;
        myTimer = [NSTimer timerWithTimeInterval:15.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:myTimer forMode:NSDefaultRunLoopMode];
    }
    else
    {
        // Remove gesture recognizer while heads-up display is inactive.
        [self.view removeGestureRecognizer:tapRecognizer];

        self.hoverView.alpha = 0.0;
        self.barButtonItem.enabled = YES;
    }
    
    [UIView commitAnimations];
}

- (void)timerFired:(NSTimer *)timer
{
#pragma unused (timer)

    // time has passed, hide the HoverView
    [self showHoverView: NO];

    // Restore gesture recognizer
    [self.view addGestureRecognizer:swipeLeftRecognizer];
}

- (void)showViewNotif:(NSNotification *)aNotification
{
#pragma unused (aNotification)
    [self showHoverView:(self.hoverView.alpha != 1.0)]; // Show if not visible.
}


- (void)action:(id)sender
{
#pragma unused (sender)

    // the custom right view button was clicked, handle it here
    //
    // Make the keyboard go away
//    [noteText resignFirstResponder];
    
    //•leg 06/09/09 - scrolling experiment - scroll down to 50th character
    //[noteText scrollRangeToVisible:NSMakeRange(50, 1)];

    [self done];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//#pragma unused (interfaceOrientation)
//
//    return YES;
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
    NSLog(@"NoteRichTextViewController didReceiveMemoryWarning");
}


- (void)dealloc {
    // no longer interested in Show_HoverView_RTE notifs
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Show_HoverView_RTE object:nil];
    
}

// Get a temporary files directory.                                             //leg20220215 - TopXNotes_Catalyst_2
- (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix
{
    NSString *  result;
     
    result = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", prefix]];
    assert(result != nil);

    if (![[NSFileManager defaultManager] fileExistsAtPath:result])
    {
        NSError *err = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:result
                                       withIntermediateDirectories:YES attributes:nil error:&err])
        {
            NSLog(@"Activity share: Error creating temporary share directory: %@", err);
        }
    }

    return result;
}

#pragma mark Emailing

// Respond to Email UIButton
//
// This is now the call to show the Activity Viewer which includes Email.
//
- (void)emailNote
{
    // Get the note to share.
    Note *note = [model getNoteForIndex:noteIndex];

    // Get the path to a temporary files directory
    NSString * tempPath = [self pathForTemporaryFileWithPrefix:@"tempShares"];
        
    // Save note as HTML in temporary file.                                     //leg20220215 - TopXNotes_Catalyst_2
    NSError *error;
    NSString *htmlStringToWrite = note.noteHTML;
    NSString *noteFileName = [NSString stringWithFormat:@"%@.html", note.title];
    NSString *htmlFilePath = [tempPath stringByAppendingPathComponent:noteFileName];
    [htmlStringToWrite writeToFile:htmlFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if ([error code] != 0) {
        NSLog(@"Error saving note as HTML file - error=\"%@\"", [error localizedDescription]);
    }

    // Make URL to share the note as an HTML file.                              //leg20220215 - TopXNotes_Catalyst_2
    NSURL *URL = [NSURL fileURLWithPath:htmlFilePath];

    APActivityProvider *activityProvider = [[APActivityProvider alloc] initWithPlaceholderItem:@""];
    
    // !!!: The plain text could just as easily be gotten from -editorDidChangeWithTextAndHTML - consider doing so.
    NSAttributedString *decodedString = [Ashton decode:note.noteHTML defaultAttributes:[NSDictionary dictionary]];
    
    // Remove non-breaking spaces from text.                                    //leg20220210 - TopXNotes_Catalyst_2
    NSString* plainText = [decodedString.string stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];

#if TARGET_OS_UIKITFORMAC
    NSString *textDataToShare = plainText;
    activityProvider.textToShare = plainText;
    activityProvider.htmlToShare = [NSString stringWithFormat:@"%@%@%@", @"<html><body>", note.noteHTML, @"</body></html>"];
    NSArray *items = @[activityProvider, URL];
#else
    NSString *textToShare = plainText;
    activityProvider.textToShare = plainText;
    activityProvider.htmlToShare = [NSString stringWithFormat:@"%@%@%@", @"<html><body>", note.noteHTML, @"</body></html>"];
//    UISimpleTextPrintFormatter *textDataToShare = [[UISimpleTextPrintFormatter alloc] initWithText:textToShare];
    // Use the WKWebKit print formatter.                                        //leg20220302 - TopXNotes_Catalyst_2
    UIViewPrintFormatter *textDataToShare = [self printEditorView];
    NSArray *items = @[activityProvider, URL, [NSString stringWithFormat:@"%@%@%@", @"<html><body>", note.noteHTML, @"</body></html>"], textDataToShare];
#endif

    APActivityIcon *ca = [[APActivityIcon alloc] init];
    NSArray *acts = @[ca];
    
    UIActivityViewController *avc = [[UIActivityViewController alloc]
                                              initWithActivityItems:items
                                              applicationActivities:acts];
//                                                  applicationActivities:nil];   // eliminate Maps       //leg20220210 - TopXNotes_Catalyst_2
    avc.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
                                 @"com.apple.reminders.sharingextension",       //leg20220210 - TopXNotes_Catalyst_2
                                 @"com.tropic4.mapsApp",                        //leg20220210 - TopXNotes_Catalyst_2
                                 UIActivityTypePostToTwitter,
                                 UIActivityTypePostToFacebook,
//                                     UIActivityTypePrint,
//                                     UIActivityTypeMessage,
                                 UIActivityTypePostToWeibo,
                                 UIActivityTypeCopyToPasteboard,
                                 UIActivityTypeMarkupAsPDF,
                                 UIActivityTypeAddToReadingList,
                                 UIActivityTypePostToFlickr,
                                 UIActivityTypePostToVimeo,
                                 UIActivityTypePostToTencentWeibo,
                                 UIActivityTypeAirDrop,
                                 UIActivityTypeOpenInIBooks,
                                 nil];
    
    // Prevent crash on iPads
    if ( [avc respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8 or greater;

        // Use actionButtonItem of toolbarItems as bar button to anchor
        //  activities popover view.
        avc.popoverPresentationController.canOverlapSourceViewRect = YES;
#if TARGET_OS_UIKITFORMAC
        // For macOS show popover in middle of view.                            //leg20220413 - TopXNotes_Catalyst_2
        avc.popoverPresentationController.sourceRect = CGRectMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame), 0, 0);
        avc.popoverPresentationController.sourceView = self.view;
#else
//        avc.popoverPresentationController.barButtonItem = self.toolbarItems[5];
        
        // Fixed crashing bug when share tool selected.  Caused by elimination  //leg20220912 - TopXNotes_Catalyst_2
        //  of unused tools from toolbarItems - index to share tool changed
        //  from 5 to 3.
        avc.popoverPresentationController.barButtonItem = self.toolbarItems[3];
#endif
        
        // TODO: Background color doesn't happen in Simulator - try real device.
        avc.popoverPresentationController.backgroundColor = [UIColor redColor];
    }
    
    [self presentViewController:avc animated:YES completion:nil];
    
    // Completion handler processes result of selected activity.
    // Replaced deprecated "CompletionHandler".
    avc.completionWithItemsHandler = ^(NSString *act,
                                              BOOL done,
                                              NSArray *returnedItems,
                                              NSError *error)
    {
         NSString *ServiceMsg = @"Sent!";
         if ( [act isEqualToString:UIActivityTypeMail] )           done = NO;   //leg20220210 - TopXNotes_Catalyst_2
         if ( [act isEqualToString:@"com.tropic4.mapsApp"] )       done = NO;   //leg20220210 - TopXNotes_Catalyst_2
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"Posted on twitter!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"Posted on facebook!";
         if ( [act isEqualToString:UIActivityTypePrint] )          ServiceMsg = @"Printed!";
         if ( [act isEqualToString:UIActivityTypeMessage] )        ServiceMsg = @"SMS sent!";
         if ( [act isEqualToString:UIActivityTypePostToWeibo] )    ServiceMsg = @"Posted on Weibo!";
         if ( [act isEqualToString:UIActivityTypeCopyToPasteboard] ) ServiceMsg = @"Copied to Pasteboard!";
         if ( [act isEqualToString:UIActivityTypeMarkupAsPDF] ) ServiceMsg = @"Completed Markup!";
        
         if ( done )
         {
             // Reporting service message after activity.
             //  permission to proceed with Restore action.
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:ServiceMsg
                                            message:@""
                                            preferredStyle:UIAlertControllerStyleAlert];

             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                handler:^(UIAlertAction * action) {
                 // Do nothing––no action required.!

             }];
             
             [alert addAction:defaultAction];

             [self presentViewController:alert animated:YES completion:nil];
         }
     };
}

// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
    Note *note = [model getNoteForIndex:noteIndex];

    // Assemble the Email
    //    NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];
    NSString *subjectPrefix = [NSString stringWithFormat:@"Re: %@ %@ --> \"",
                               [self infoValueForKey:@"CFBundleDisplayName"],
                               versionInfo];
    NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
    completeSubject = [completeSubject stringByAppendingString:@"\""];
    
    NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
    NSMutableString *body = [NSMutableString stringWithString:note.noteText];

    // encode the strings for email
    [subject encodeForEmail];

    [body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];    // Make sure CRs are recognized
    [body encodeForEmail];

    NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
    NSURL *encodedURL = [NSURL URLWithString:emailMsg];
         
    // Send the assembled message to Mail!
    [[UIApplication sharedApplication] openURL:encodedURL];
}


- (void)sendEmail
{

    // This can run on devices running iPhone OS 2.0 or later
    // The MFMailComposeViewController class is only available in iPhone OS 3.0 or later.
    // So, we must verify the existence of the above class and provide a workaround for devices running
    // earlier versions of the iPhone OS.
    // We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
    // We launch the Mail application on the device, otherwise.
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            [self displayComposerSheet];
        }
        else
        {
            [self launchMailAppOnDevice];
        }
    }
    else
    {
        [self launchMailAppOnDevice];
    }
}

-(void)displayComposerSheet
{
    // Insure Email In Progress flag is turned ON.
    emailInProgress = YES;
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
       
    Note *note = [model getNoteForIndex:noteIndex];

    // Assemble the Email - Don't set any recipients, let user do during composition
    //    NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];
    NSString *subjectPrefix = [NSString stringWithFormat:@"Re: %@ %@ --> \"",
                               [self infoValueForKey:@"CFBundleDisplayName"],
                               versionInfo];
    NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
    completeSubject = [completeSubject stringByAppendingString:@"\""];
    
    NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
    NSMutableString *body = [NSMutableString stringWithString:note.noteText];
    
    // Change carriage returns to line feeds so they willl be recognized.
    [body replaceOccurrencesOfString:@"\r" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];
    [picker setSubject:subject];
    [picker setMessageBody:body isHTML:NO];
    
    // Present the composer
    [self presentViewController:picker animated:YES completion:nil];
    
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
#pragma unused (controller, error)

    emailInProgress = NO;
    NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }
    
        [self dismissViewControllerAnimated:YES completion:nil];

        [self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
    // open an alert with just an OK button
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
                            delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
    // open a alert with an OK and cancel button
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
                            delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // use "buttonIndex" to decide your action
    //
    // the user clicked one of the OK/Cancel buttons
    
    if (alertView == passwordAlertView) {
        if (buttonIndex == 0)
        {
            // Cancel pressed, no password entered.
            oldPasswordString = nil;
        } else {
            // OK pressed, field contains password.
            oldPasswordString = [alertView textFieldAtIndex:0].text;
        }
        
    } else {
        if (buttonIndex == 0)
        {
            //NSLog(@"Cancel");
        } else {
            //NSLog(@"OK");
            [self sendEmail];
        }
    }
}

#pragma mark UIActionSheet

- (void)dialogOKCancelAction
{
    // open a dialog with an OK and cancel button
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
                                    delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        //NSLog(@"Proceed");
        [self sendEmail];
    }
    else
    {
        //NSLog(@"Cancel");
    }
    
    //[myTableView deselectRowAtIndexPath:[myTableView indexPathForSelectedRow] animated:NO];
}

#pragma mark - Encryption

- (BOOL)reEncryptNote:(Note *)note withPassword:(NSString*)password
{
    // Rich Text Modification:  Encrypt HTML and plain text.                    //leg20211129 - TopXNotes_Catalyst_2

    // Re-encrypt note text with provided password.
    OSErr result =  noErr;
    const char *    utf8Password;
    const char *    textBuffer;
    const char *    htmlBuffer;
    Str31           pPassword = "\p";
    
    // Make decryption key.
    utf8Password = [password UTF8String];
    
    memcpy((void *)&pPassword[1], (const void *)&utf8Password[0], (unsigned long)strlen(utf8Password));
    
    pPassword[0] = strlen(utf8Password);
    UInt32 key = MakeKeyFromPassword(pPassword);
    
    // Encrypt plain text.
    textBuffer = [note.noteText UTF8String];

    result = Encrypt(key, (void *)textBuffer, (int)[note.noteText length]);
    if (result != noErr)
        return NO;
    
    note.encryptedNoteText = [NSData dataWithBytes:textBuffer length:[note.noteText length]];
    note.noteText = kEncrypted_Placeholder_Notice;

    // Encrypt HTML.
    htmlBuffer = [note.noteHTML UTF8String];

    result = Encrypt(key, (void *)htmlBuffer, (int)[note.noteHTML length]);
    if (result != noErr)
        return NO;
    
    note.encryptedNoteHTML = [NSData dataWithBytes:htmlBuffer length:[note.noteHTML length]];
    note.noteHTML = kEncrypted_Placeholder_Notice;

    note.decryptedNoteFlag = [NSNumber numberWithBool:NO];
    note.decryptedPassword = @"rutabaga";
    
    note.protectedNoteFlag = [NSNumber numberWithBool:YES];
    
    return YES;
}

# pragma mark - Popover Presentation Controller Delegate

// Popover delegate methods for Font settings popover.                          //leg20211230 - TopXNotes_Catalyst_2

// TODO: Determine what to replace deprecated methods with.

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // called when a Popover is dismissed
    NSLog(@"Popover was dismissed with external tap. Have a nice day!");
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // return YES if the Popover should be dismissed
    // return NO if the Popover should not be dismissed
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
    // called when the Popover changes positon
}


@end
