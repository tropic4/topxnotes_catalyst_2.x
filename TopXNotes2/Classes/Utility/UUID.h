//
//  UUID.h
//  TopXNotes
//
//  Created by Lewis E. Garrett on 3/27/12.
//  Copyright (c) 2012 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>                                                         //leg20210413 - TopXNotes2

@interface NSString (UUID)

+ (NSString *)uuid;

@end
