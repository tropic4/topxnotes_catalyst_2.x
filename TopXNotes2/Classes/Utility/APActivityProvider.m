//
//  APActivityProvider.m
//  TestUIActivityViewController
//
//  Created by Lewis Garrett on 2/18/13.
//  Copyright (c) 2013 Lewis Garrett. All rights reserved.
//

#import "APActivityProvider.h"

@implementation APActivityProvider

@synthesize textToShare;
@synthesize htmlToShare;                                                        //leg20220210 - TopXNotes_Catalyst_2

- (id) activityViewController:(UIActivityViewController *)activityViewController
          itemForActivityType:(NSString *)activityType
{
#pragma unused (activityViewController)
    
    if ( [activityType isEqualToString:UIActivityTypePostToTwitter] ) {
        if ([self.textToShare length] > 140) {
            return [self.textToShare substringToIndex:139];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypePostToFacebook] ) {
        if ([self.textToShare length] > 63206) {
            return [self.textToShare substringToIndex:63205];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypeMessage] ) {
        if ([self.textToShare length] > 500) {
             return [self.textToShare substringToIndex:499];
        } else {
            return self.textToShare;
        }
    } else if ( [activityType isEqualToString:UIActivityTypeMail] ) {
        
    // macOS Mail requires sharing html so that message is HTML format.         //leg20220215 - TopXNotes_Catalyst_2
#if TARGET_OS_UIKITFORMAC
        return self.htmlToShare;
#else
        return self.textToShare;
#endif
    } else if( [activityType isEqualToString:@"com.tropic4.mapsApp"] ) {
        return @"OpenMyapp custom text";
    } else {
        return self.textToShare;
    }
}
- (id) activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
#pragma unused (activityViewController)
    
    return @"";
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController //leg20150924 - 1.5.0
              subjectForActivityType:(NSString *)activityType
{
#pragma unused (activityViewController)

    // !!!: This doesn't do anything for UIActivityTypeMessage                  //leg20220210 - TopXNotes_Catalyst_2
    if ([activityType isEqualToString:UIActivityTypeMail] || [activityType isEqualToString:UIActivityTypeMessage])
    {
        return @"Re: TopXNotes Note";
    }
    
    return nil;
}

@end

@implementation APActivityIcon
- (NSString *)activityType {
    return @"com.tropic4.mapsApp";
}

- (NSString *)activityTitle {
    return @"Open Maps";
}

//- (UIImage *) activityImage { return [UIImage imageNamed:@"lines.png"]; }
- (UIImage *) activityImage {
    return [UIImage imageNamed:@"103-map.png"];
}

- (BOOL) canPerformWithActivityItems:(NSArray *)activityItems
{
#pragma unused (activityItems)
    
    return YES;
}

- (void) prepareWithActivityItems:(NSArray *)activityItems
{
#pragma unused (activityItems)
    
}

- (UIViewController *) activityViewController {
    return nil;
}

- (void) performActivity {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"maps://"]];
}

@end
