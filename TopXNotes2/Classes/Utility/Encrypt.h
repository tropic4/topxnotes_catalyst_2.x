
#include <MacTypes.h>

OSErr Encrypt( UInt32 inSeed, void *ioData, UInt32 inDataSize );
OSErr Decrypt( UInt32 inSeed, void *ioData, UInt32 inDataSize );
UInt32 MakeKeyFromPassword( Str31 inPassword);
