//
//  NoteListCell.m
//  NotesTopX
//
//  Created by Lewis Garrett on 5/2/09.
//  Copyright 2009 Iota. All rights reserved.
//
// Modified to allow 1 or 2 line displays based on TopXNotes2_Prefix.pch        //leg20210506 - TopXNotes2
//  global variables SYNC_DEBUG and NOTE_LIST_CELL_DISPLAY_FORMAT.
//


#import "NoteListCell.h"
#import "Constants.h"

@implementation NoteListCell

@synthesize dataDictionary;
@synthesize noteTitleLabel;
@synthesize dateLabel;
@synthesize infoLabel;
@synthesize padLockButton;
@synthesize encryptionStatus;

#define LEFT_COLUMN_OFFSET		10
#define LEFT_COLUMN_WIDTH		165
		
#define UPPER_ROW_TOP			0

#define CELL_HEIGHT				50


- (id)initWithReuseIdentifier:(NSString *)identifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
//    // You can do this here specifically or at the table level
//    //  for all cells.
//    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    switch (NOTE_LIST_CELL_DISPLAY_FORMAT) {
        // You can do this here specifically or at the table level
        //  for all cells.
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
      case OneLine:
        {
            //
            // 1 line display with Note title on the left and the Note
            //  date on the right
            //
            if (self)
            {
                // Create label views to contain the various pieces of text that make up the cell.
                // Add these as subviews.
                noteTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero]; // layoutSubViews will decide the final frame
                noteTitleLabel.backgroundColor = [UIColor clearColor];
                noteTitleLabel.opaque = NO;
                noteTitleLabel.textColor = [UIColor blackColor];
                noteTitleLabel.highlightedTextColor = [UIColor whiteColor];
        //        noteTitleLabel.font = [UIFont systemFontOfSize:16];
                noteTitleLabel.font = [UIFont boldSystemFontOfSize:16];
                [self.contentView addSubview:noteTitleLabel];
                
                // Add a padlock button for protecting/unprotecting a note.
                self.padLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [self.padLockButton addTarget:self action:@selector(padlockTapped:forEvent:) forControlEvents:UIControlEventTouchUpInside];
                [self.contentView addSubview:self.padLockButton];
                
                dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                dateLabel.backgroundColor = [UIColor clearColor];
                dateLabel.opaque = NO;
                dateLabel.textColor = [UIColor blueColor];
                dateLabel.highlightedTextColor = [UIColor whiteColor];
                dateLabel.font = [UIFont systemFontOfSize:11];
                [self.contentView addSubview:dateLabel];
            }
        }
        break;

        case TwoLine:
        {
            // You can do this here specifically or at the table level
            //  for all cells.
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            //
               // 2 line display, same as 1 line display except date area divided
               //  vertically with date in upper half and info in lower half.
               //
               if (self)
               {
                    // Create label views to contain the various pieces of text that make up the cell.
                   // Add these as subviews.
                   noteTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                   noteTitleLabel.backgroundColor = [UIColor clearColor];
                   noteTitleLabel.opaque = NO;
                   noteTitleLabel.textColor = [UIColor blackColor];
                   noteTitleLabel.highlightedTextColor = [UIColor whiteColor];
                   noteTitleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];

                   [self.contentView addSubview:noteTitleLabel];

                   // Add a padlock button for protecting/unprotecting a note.
                   self.padLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
                   [self.padLockButton addTarget:self action:@selector(padlockTapped:forEvent:) forControlEvents:UIControlEventTouchUpInside];
                   [self.contentView addSubview:self.padLockButton];
                   
                   dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                   dateLabel.backgroundColor = [UIColor clearColor];
                   dateLabel.opaque = NO;
                   dateLabel.textColor = [UIColor blueColor];
                   dateLabel.highlightedTextColor = [UIColor whiteColor];
                   dateLabel.font = [UIFont systemFontOfSize:11];
                   [self.contentView addSubview:dateLabel];

                   infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                   infoLabel.backgroundColor = [UIColor clearColor];
                   infoLabel.opaque = NO;
                   infoLabel.textColor = [UIColor redColor];
                   infoLabel.highlightedTextColor = [UIColor whiteColor];
                   infoLabel.font = [UIFont systemFontOfSize:9];
                   [self.contentView addSubview:infoLabel];
               }
           }
           break;

        case TwoLineWithPreview:
        {
            //
            // 2 line display with preview, top line: bolded title
            //  bottom line: modification date - note preview beyond title.
            //
            // Created new note list cell display format similar to Apple       //leg20210513 - TopXNotes2
            //  Notes app. Implemented Auto Layout Constraints programmatically.
            //
            if (self)
            {
                // Add a padlock button for protecting/unprotecting a note.
                self.padLockButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [self.padLockButton addTarget:self action:@selector(padlockTapped:forEvent:) forControlEvents:UIControlEventTouchUpInside];
                [self.contentView addSubview:self.padLockButton];

                // Create label views to contain the various pieces of text that make up the cell.
                // Add these as subviews.
                noteTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                noteTitleLabel.backgroundColor = [UIColor clearColor];
                noteTitleLabel.opaque = NO;
                noteTitleLabel.textColor = [UIColor blackColor];
                noteTitleLabel.highlightedTextColor = [UIColor whiteColor];
//                noteTitleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
                noteTitleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];   //leg20210528 - TopXNotes2
                noteTitleLabel.adjustsFontForContentSizeCategory = YES;                             //leg20210528 - TopXNotes2
                noteTitleLabel.translatesAutoresizingMaskIntoConstraints = false;                   //leg20210528 - TopXNotes2
                noteTitleLabel.insetsLayoutMarginsFromSafeArea = true;
                
                [self.contentView addSubview:noteTitleLabel];

                // noteTitleLabel Constraints.
                [noteTitleLabel.leftAnchor constraintEqualToAnchor:self.padLockButton.rightAnchor constant:5].active = YES;
                [noteTitleLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-40.0].active = YES;
//                [noteTitleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:4].active = YES;
                [noteTitleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:10].active = YES; //leg20210524 - TopXNotes2

                dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                dateLabel.backgroundColor = [UIColor clearColor];
                dateLabel.opaque = NO;
                dateLabel.textColor = [UIColor blueColor];
                dateLabel.highlightedTextColor = [UIColor whiteColor];
//                dateLabel.font = [UIFont systemFontOfSize:14];
                dateLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];    //leg20210528 - TopXNotes2
                dateLabel.adjustsFontForContentSizeCategory = YES;                              //leg20210528 - TopXNotes2
                dateLabel.translatesAutoresizingMaskIntoConstraints = false;                    //leg20210528 - TopXNotes2
                [self.contentView addSubview:dateLabel];
                
                
                // dateLabel Constraints.
                [dateLabel.leftAnchor constraintEqualToAnchor:self.padLockButton.rightAnchor constant:5].active = YES;
//                [dateLabel.rightAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:LEFT_COLUMN_WIDTH-50].active = YES;
                [dateLabel.rightAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:175.0].active = YES;    //leg20210524 - TopXNotes2
//                [dateLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:26.0].active = YES;
                [dateLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:30.0].active = YES;    //leg20210524 - TopXNotes2

                infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];    // layoutSubViews will decide the final frame
                infoLabel.backgroundColor = [UIColor clearColor];
                infoLabel.opaque = NO;
                infoLabel.textColor = [UIColor blueColor];
                infoLabel.highlightedTextColor = [UIColor whiteColor];
//                infoLabel.font = [UIFont systemFontOfSize:14];
                infoLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];    //leg20210528 - TopXNotes2
                infoLabel.adjustsFontForContentSizeCategory = YES;                              //leg20210528 - TopXNotes2
                infoLabel.translatesAutoresizingMaskIntoConstraints = false;                    //leg20210528 - TopXNotes2
                [self.contentView addSubview:infoLabel];
                
                // infoLabel Constraints.
                [infoLabel.leftAnchor constraintEqualToAnchor:dateLabel.rightAnchor constant:5].active = YES;
                [infoLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-36.0].active = YES;
//                [infoLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:26.0].active = YES;
                [infoLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:30.0].active = YES;    //leg20210524 - TopXNotes2
            }
        }
        break;


      default:
        break;
    }
    
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    CGRect frame;
    
    switch (NOTE_LIST_CELL_DISPLAY_FORMAT) {
      case OneLine:
        {
            // Layout:  1-line, padlock button - note title - date modified - accessory
            //  indicator.
            if ([encryptionStatus boolValue]) {
                frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP+12, 20, 20);
                self.padLockButton.frame = frame;
                
                frame = CGRectMake(contentRect.origin.x + 40, UPPER_ROW_TOP, 145, CELL_HEIGHT);
                noteTitleLabel.frame = frame;
                
                // Vary content frame depending on sync debug mode.
                if (!SYNC_DEBUG) {
                    frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
                } else {
                    frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
                }
                dateLabel.frame = frame;
            } else {
                frame = CGRectZero;
                self.padLockButton.frame = frame;

                frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
                noteTitleLabel.frame = frame;
                
                // Vary content frame depending on sync debug mode.
                if (!SYNC_DEBUG) {
                    frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
                } else {
                    frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
                }
                dateLabel.frame = frame;
            }
        }
        break;

        case TwoLine:
        {
           // Layout:  2-line, padlock button - note title - date modified
           //  - creation date and info - accessory indicator.
           if ([encryptionStatus boolValue]) {
               frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP+12, 20, 20);
               self.padLockButton.frame = frame;
               
               frame = CGRectMake(contentRect.origin.x + 40, UPPER_ROW_TOP, 145, CELL_HEIGHT);
               noteTitleLabel.frame = frame;
               
               // Vary content frame depending on sync debug mode.
               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               } else {
                   frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
               }
               dateLabel.frame = frame;

               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 176.0 + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
               } else {
                   frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, 1600, CELL_HEIGHT/2);
               }
               infoLabel.frame = frame;
           } else {
               frame = CGRectZero;
               self.padLockButton.frame = frame;

               frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               noteTitleLabel.frame = frame;
               
               noteTitleLabel.translatesAutoresizingMaskIntoConstraints = false;
                                  
               // Vary content frame depending on sync debug mode.
               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               } else {
                   frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
               }
               dateLabel.frame = frame;

               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 171.0 + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
               } else {
                   frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, 1600, CELL_HEIGHT/2);
               }
               infoLabel.frame = frame;
           }
       }
       break;
            
       case TwoLineWithPreview:
        {
           // Layout:  2-line, padlock button - note title - date modified
           //  - preview
           if ([encryptionStatus boolValue]) {
               frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP+12, 20, 20);
               self.padLockButton.frame = frame;
               
               frame = CGRectMake(contentRect.origin.x + 40, UPPER_ROW_TOP, 145, CELL_HEIGHT);
               noteTitleLabel.frame = frame;
               
               // Vary content frame depending on sync debug mode.
               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               } else {
                   frame = CGRectMake(contentRect.origin.x + 185.0, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
               }
               dateLabel.frame = frame;

               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 176.0 + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
               } else {
                   frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, 1600, CELL_HEIGHT/2);
               }
               infoLabel.frame = frame;
           } else {
               frame = CGRectZero;
               self.padLockButton.frame = frame;

               frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               noteTitleLabel.frame = frame;
               
               noteTitleLabel.translatesAutoresizingMaskIntoConstraints = false;
                                  
               // Vary content frame depending on sync debug mode.
               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, LEFT_COLUMN_WIDTH, CELL_HEIGHT);
               } else {
                   frame = CGRectMake(contentRect.origin.x + 170.0 + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP, 1600, CELL_HEIGHT);
               }
               dateLabel.frame = frame;

               if (!SYNC_DEBUG) {
                   frame = CGRectMake(contentRect.origin.x + 171.0 + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, LEFT_COLUMN_WIDTH, CELL_HEIGHT/2);
               } else {
                   frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, CELL_HEIGHT/2, 1600, CELL_HEIGHT/2);
               }
               infoLabel.frame = frame;
           }
       }
          break;

      default:
        break;
    }
}

-(void) padlockTapped:(id) sender forEvent:(UIEvent *)event
{
#pragma unused (event)

    [self.delegate padlockTouchedAt:self withButton:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];

	// When the selected state changes, set the highlighted state of the
    //  labels accordingly
	noteTitleLabel.highlighted = selected;
}

- (void)setDataDictionary:(NSDictionary *)newDictionary
{
	if (dataDictionary == newDictionary)
	{
		return;
	}
	dataDictionary = newDictionary;
	
	// update value in subviews
	noteTitleLabel.text = [dataDictionary objectForKey:kTitleKey];
	dateLabel.text = [dataDictionary objectForKey:kExplainKey];
}

// Adjust frame of contentView based on indentation properties.                 //leg20220503 - TopXNotes_Catalyst_2
-(void)setFrame:(CGRect)frame {
    float inset = self.indentationLevel * self.indentationWidth;

    frame.origin.x += inset;
    frame.size.width -= inset;

    [ super setFrame:frame ];
}

@end
