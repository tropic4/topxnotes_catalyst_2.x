//
//  GroupListCell.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 5/10/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//

#import "GroupListCell.h"
#import "Constants.h"

@implementation GroupListCell

@synthesize dataDictionary;
@synthesize groupTitleLabel;
@synthesize folderImageView;                                                    //leg20220824 - TopXNotes_Catalyst_2
@synthesize disclosureButton;
@synthesize disclosureStatus;

#define LEFT_COLUMN_OFFSET       10
#define LEFT_COLUMN_WIDTH        165
        
#define UPPER_ROW_TOP            0

#define CELL_HEIGHT              50

- (id)initWithReuseIdentifier:(NSString *)identifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
//    // You can do this here specifically or at the table level
//    //  for all cells.
//    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    //
    // 1 line display with Note title on the left and the Note
    //  date on the right
    //
    if (self)
    {
        // Add a disclosure button for opening/closing Group.
        self.disclosureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.disclosureButton addTarget:self action:@selector(disclosureTapped:forEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.disclosureButton];

        // Create label views to contain the various pieces of text that make up the cell.
        // Add these as subviews.
//        groupTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero]; // layoutSubViews will decide the final frame
        groupTitleLabel = [[UITextField alloc] initWithFrame:CGRectZero]; // layoutSubViews will decide the final frame //leg20220923 - TopXNotes_Catalyst_2
        groupTitleLabel.backgroundColor = [UIColor clearColor];
        groupTitleLabel.opaque = NO;
        groupTitleLabel.tag = 200;  // entry in cell prototype not there at run.//leg20220923 - TopXNotes_Catalyst_2
        groupTitleLabel.textColor = [UIColor blackColor];
//        groupTitleLabel.highlightedTextColor = [UIColor whiteColor];          //leg20220923 - TopXNotes_Catalyst_2
        groupTitleLabel.font = [UIFont boldSystemFontOfSize:16];
        groupTitleLabel.adjustsFontForContentSizeCategory = YES;                             //leg20210528 - TopXNotes2
        groupTitleLabel.translatesAutoresizingMaskIntoConstraints = false;                   //leg20210528 - TopXNotes2
        groupTitleLabel.insetsLayoutMarginsFromSafeArea = true;
        
        // Capture when editing of title begins.                                   //leg20220923 - TopXNotes_Catalyst_2
        [groupTitleLabel addTarget:self action:@selector(titleBeginEdit:forEvent:) forControlEvents:UIControlEventEditingDidBegin];

        [self.contentView addSubview:groupTitleLabel];

        // groupTitleLabel Constraints.
//        [groupTitleLabel.leftAnchor constraintEqualToAnchor:self.disclosureButton.rightAnchor constant:5].active = YES;
        [groupTitleLabel.leftAnchor constraintEqualToAnchor:self.disclosureButton.rightAnchor constant:5+35].active = YES;  //leg20220824 - TopXNotes_Catalyst_2
        [groupTitleLabel.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor constant:-40.0].active = YES;
        [groupTitleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:10].active = YES;

        // Add redundant folder icon to Group cell.                             //leg20220824 - TopXNotes_Catalyst_2
        folderImageView = [[UIImageView alloc] initWithFrame:CGRectZero]; // layoutSubViews will decide the final frame
        [folderImageView setImage:[UIImage imageNamed:@"folder.png"]];
        [self.contentView addSubview:folderImageView];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect contentRect = [self.contentView bounds];
    CGRect frame;
    
    // Layout:  1-line, disclosure button - group title
    //  indicator.
    frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP+12, 20, 20);
    self.disclosureButton.frame = frame;
    
    // Add redundant folder icon to Group cell.                                 //leg20220824 - TopXNotes_Catalyst_2
    frame = CGRectMake(contentRect.origin.x + LEFT_COLUMN_OFFSET +30, UPPER_ROW_TOP+12, 20, 20);
    self.folderImageView.frame = frame;
    
//    frame = CGRectMake(contentRect.origin.x + 40, UPPER_ROW_TOP, 145, CELL_HEIGHT);
    frame = CGRectMake(contentRect.origin.x + 40 +25, UPPER_ROW_TOP, 145, CELL_HEIGHT); //leg20220824 - TopXNotes_Catalyst_2
    groupTitleLabel.frame = frame;
}

// Capture cell of Group being edited.                                          //leg20220923 - TopXNotes_Catalyst_2
-(void) titleBeginEdit:(id) sender forEvent:(UIEvent *)event
{
#pragma unused (event)

    [self.delegate titleEditingAt:self withTextField:sender];
}

-(void) disclosureTapped:(id) sender forEvent:(UIEvent *)event
{
#pragma unused (event)

    [self.delegate disclosureTouchedAt:self withButton:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // When the selected state changes, set the highlighted state of the
    //  labels accordingly
    groupTitleLabel.highlighted = selected;
}

- (void)setDataDictionary:(NSDictionary *)newDictionary
{
    if (dataDictionary == newDictionary)
    {
        return;
    }
    dataDictionary = newDictionary;

    // update value in subviews
    groupTitleLabel.text = [dataDictionary objectForKey:kTitleKey];
}

// Adjust frame of contentView based on indentation properties.
-(void)setFrame:(CGRect)frame {
    float inset = self.indentationLevel * self.indentationWidth;

    frame.origin.x += inset;
    frame.size.width -= inset;

    [ super setFrame:frame ];
}

@end

