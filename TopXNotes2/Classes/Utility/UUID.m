//
//  UUID.m
//  TopXNotes
//
//  Created by Lewis E. Garrett on 3/27/12.
//  Copyright (c) 2012 Iota. All rights reserved.
//

#import "UUID.h"

@implementation NSString (UUID)


+ (NSString *)uuid
{
    NSString *uuidString = nil;
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    if (uuid) {
#if __has_feature(objc_arc)
        uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
#else
        uuidString = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
#endif
        CFRelease(uuid);
    }
#if __has_feature(objc_arc)
    return uuidString;
#else
    return [uuidString autorelease];
#endif
}

@end