//
//  NoteListCell.h
//  NotesTopX
//
//  Created by Lewis Garrett on 5/2/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NoteListCell;

// Delegate for communicating cell taps.                                        //leg20121204 - 1.3.0
@protocol NoteListCellDelegate <NSObject>
- (void)padlockTouchedAt: (NoteListCell*)cell withButton:(UIButton*)button;
@end

@interface NoteListCell : UITableViewCell
{
	NSDictionary	*dataDictionary;
	UILabel			*noteTitleLabel;
	UILabel			*dateLabel;
	UILabel			*infoLabel;
    UIButton        *padLockButton;                                             //leg20121204 - 1.3.0
}

@property (nonatomic, strong) id<NoteListCellDelegate> delegate;                //leg20121204 - 1.3.0
@property (nonatomic, strong) NSDictionary *dataDictionary;
@property (nonatomic, strong) UILabel *noteTitleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) UIButton *padLockButton;                          //leg20121204 - 1.3.0
@property (nonatomic, strong) NSNumber *encryptionStatus;                       //leg20130205 - 1.3.0

- (id)initWithReuseIdentifier:(NSString *)identifier;                           //leg20140205 - 1.2.7
-(void) padlockTapped:(id) sender forEvent:(UIEvent *)event;                    //leg20121204 - 1.3.0

@end
