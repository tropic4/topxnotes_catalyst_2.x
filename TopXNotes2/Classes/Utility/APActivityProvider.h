//
//  APActivityProvider.h
//  TestUIActivityViewController
//
//  Created by Lewis Garrett on 2/18/13.
//  Copyright (c) 2013 Lewis Garrett. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol APActivityProviderDelegate <NSObject>
//- (void)passText:(NSString *)text;
//@end

@interface APActivityProvider : UIActivityItemProvider <UIActivityItemSource> {
    
}

@property (nonatomic, strong) NSString * textToShare;
@property (nonatomic, strong) NSString * htmlToShare;                           //leg20220210 - TopXNotes_Catalyst_2
//@property (nonatomic, weak) id <APActivityProviderDelegate> delegate;
@end

@interface APActivityIcon : UIActivity
@end
