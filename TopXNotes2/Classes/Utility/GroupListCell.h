//
//  GroupListCell.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 5/10/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GroupListCell;

// Delegate for communicating cell info.
@protocol GroupListCellDelegate <NSObject>
- (void)disclosureTouchedAt: (GroupListCell*)cell withButton:(UIButton*)button;
- (void)titleEditingAt: (GroupListCell*)cell withTextField:(UITextField*)textField;  //leg20220923 - TopXNotes_Catalyst_2
@end

@interface GroupListCell : UITableViewCell
{
    NSDictionary    *dataDictionary;
//    UILabel         *groupTitleLabel;
    UITextField     *groupTitleLabel;                                           //leg20220923 - TopXNotes_Catalyst_2
    UIImageView     *folderImageView;                                           //leg20220824 - TopXNotes_Catalyst_2
    UIButton        *disclosureButton;
}

@property (nonatomic, strong) id<GroupListCellDelegate> delegate;
@property (nonatomic, strong) NSDictionary *dataDictionary;
//@property (nonatomic, strong) UILabel *groupTitleLabel;
@property (nonatomic, strong) UITextField *groupTitleLabel;                     //leg20220923 - TopXNotes_Catalyst_2
@property (nonatomic, strong) UIImageView *folderImageView;                     //leg20220824 - TopXNotes_Catalyst_2
@property (nonatomic, strong) UIButton *disclosureButton;
@property (nonatomic, strong) NSNumber *disclosureStatus;

- (id)initWithReuseIdentifier:(NSString *)identifier;
-(void) disclosureTapped:(id) sender forEvent:(UIEvent *)event;
-(void) titleBeginEdit:(id) sender forEvent:(UIEvent *)event;                   //leg20220923 - TopXNotes_Catalyst_2

@end

NS_ASSUME_NONNULL_END
