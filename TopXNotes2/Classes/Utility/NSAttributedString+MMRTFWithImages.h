//
//  NSAttributedString+MMRTFWithImages.h
//  RichTextEditor
//
//  Created by Lewis Garrett on 9/30/21.
//  Copyright © 2021 Aryan Ghassemi. All rights reserved.
//
// Category derived from post by LaborEtArs: https://stackoverflow.com/a/29181130/1911037
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 NSAttributedString (MMRTFWithImages)

 */
@interface NSAttributedString (MMRTFWithImages)

- (NSString *)encodeRTFWithImages;

@end

NS_ASSUME_NONNULL_END
