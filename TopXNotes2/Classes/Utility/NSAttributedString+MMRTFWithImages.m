//
//  NSAttributedString+MMRTFWithImages.m
//  RichTextEditor
//
//  Created by Lewis Garrett on 9/30/21.
//  Copyright © 2021 Aryan Ghassemi. All rights reserved.
//
//
// Category derived from post by LaborEtArs: https://stackoverflow.com/a/29181130/1911037
//

#import "NSAttributedString+MMRTFWithImages.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 NSAttributedString (MMRTFWithImages)

 */
@implementation NSAttributedString (MMRTFWithImages)

/*
 encodeRTFWithImages

 */
- (NSString *)encodeRTFWithImages {

    NSMutableAttributedString*  stringToEncode = [[NSMutableAttributedString alloc] initWithAttributedString:self];
    NSRange                     strRange = NSMakeRange(0, stringToEncode.length);

    //
    // Prepare the attributed string by removing the text attachments (images) and replacing them by
    // references to the images dictionary
    NSMutableDictionary*        attachmentDictionary = [NSMutableDictionary dictionary];
    while (strRange.length) {
        // Get the next text attachment
        NSRange effectiveRange;
        NSTextAttachment* textAttachment = [stringToEncode attribute:NSAttachmentAttributeName
                                                             atIndex:strRange.location
                                                      effectiveRange:&effectiveRange];

        strRange = NSMakeRange(NSMaxRange(effectiveRange), NSMaxRange(strRange) - NSMaxRange(effectiveRange));

        if (textAttachment) {
            // Text attachment found -> store image to image dictionary and remove the attachment
            NSFileWrapper*  fileWrapper = [textAttachment fileWrapper];

            UIImage*    image = [[UIImage alloc] initWithData:[fileWrapper regularFileContents]];
            // Kepp image size
            UIImage*    scaledImage = [self imageFromImage:image
                                               withSize:textAttachment.bounds.size];
            NSString*   imageKey = [NSString stringWithFormat:@"_MM_Encoded_Image#%zi_", [scaledImage hash]];
            [attachmentDictionary setObject:scaledImage
                                     forKey:imageKey];

            [stringToEncode removeAttribute:NSAttachmentAttributeName
                                      range:effectiveRange];
            [stringToEncode replaceCharactersInRange:effectiveRange
                                          withString:imageKey];
            strRange.length += [imageKey length] - 1;
        } // if
    } // while

    //
    // Create the RTF stream; without images but including our references
    NSData*             rtfData = [stringToEncode dataFromRange:NSMakeRange(0, stringToEncode.length)
                                    documentAttributes:@{
                                                         NSDocumentTypeDocumentAttribute:   NSRTFTextDocumentType
                                                         }
                                                 error:NULL];
    NSMutableString*    rtfString = [[NSMutableString alloc] initWithData:rtfData
                                                              encoding:NSASCIIStringEncoding];

    //
    // Replace the image references with hex encoded image data
    for (id key in attachmentDictionary) {
        NSRange     keyRange = [rtfString rangeOfString:(NSString*)key];
        if (NSNotFound != keyRange.location) {
            // Reference found -> replace with hex coded image data
            UIImage*    image = [attachmentDictionary objectForKey:key];
            NSData*     pngData = UIImagePNGRepresentation(image);

            NSString*   hexCodedString = [self hexadecimalRepresentation:pngData];
            NSString*   encodedImage = [NSString stringWithFormat:@"{\\*\\shppict {\\pict \\pngblip %@}}", hexCodedString];

            [rtfString replaceCharactersInRange:keyRange withString:encodedImage];
        }
    }
    return rtfString;
}

/*
 imageFromImage:withSize:

 Scales the input image to pSize
 */
- (UIImage *)imageFromImage:(UIImage *)pImage
                   withSize:(CGSize)pSize {

    UIGraphicsBeginImageContextWithOptions(pSize, NO, 0.0);
    [pImage drawInRect:CGRectMake(0, 0, pSize.width, pSize.height)];

    UIImage*    resultImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return resultImage;
}

/*
 hexadecimalRepresentation:

 Returns a hex codes string for all bytes in a NSData object
 */
- (NSString *) hexadecimalRepresentation:(NSData *)pData {

    static const char*  hexDigits = "0123456789ABCDEF";

    NSString*   result = nil;

    size_t      length = pData.length;
    if (length) {

        NSMutableData*  tempData = [NSMutableData dataWithLength:(length << 1)];    // double length
        if (tempData) {
            const unsigned char*    src = [pData bytes];
            unsigned char*          dst = [tempData mutableBytes];

            if ((src) &&
                (dst)) {
                // encode nibbles
                while (length--) {
                    *dst++ = hexDigits[(*src >> 4) & 0x0F];
                    *dst++ = hexDigits[(*src++ & 0x0F)];
                } // while

                result = [[NSString alloc] initWithData:tempData
                                               encoding:NSASCIIStringEncoding];
            } // if
        } // if
    } // if
    return result;
}

@end


