//
//  CloudKitManager.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 8/30/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
//  Derived from Yalantis CloudKit-demo Created by Maksim Usenko on 3/16/15.    //leg20220830 - TopXNotes_Catalyst_2
//

#import <Foundation/Foundation.h>

typedef void(^CloudKitCompletionHandler)(NSArray *results, NSError *error);

@interface CloudKitManager : NSObject

+ (void)fetchNotepadWithCompletionHandler:(CloudKitCompletionHandler)handler;

+ (void)fetchNotepadRecordWithId:(NSString *)recordId completionHandler:(CloudKitCompletionHandler)handler;

+ (void)createRecord:(NSString *)path
   completionHandler:(CloudKitCompletionHandler)handler;

+ (void)updateRecordFileWithId:(NSString *)recordId
                          path:(NSString *)path
             completionHandler:(CloudKitCompletionHandler)handler;

+ (void)removeRecordWithId:(NSString *)recordId
         completionHandler:(CloudKitCompletionHandler)handler;

+ (void) updateCloudSubscriptions;

@end
