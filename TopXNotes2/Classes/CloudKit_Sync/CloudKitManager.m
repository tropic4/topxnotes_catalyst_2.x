//
//  CloudKitManager.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 8/30/22.
//  Copyright © 2022 Tropical Software, Inc. All rights reserved.
//
//  Derived from Yalantis CloudKit-demo Created by Maksim Usenko on 3/16/15.    //leg20220830 - TopXNotes_Catalyst_2
//

#import "CloudKitManager.h"
#import <CloudKit/CloudKit.h>
#import <UIKit/UIKit.h>
#import "Notepad.h"
#import "Model.h"

NSString * const kNotepadRecord = @"Notepad";

@implementation CloudKitManager

+ (CKDatabase *)privateCloudDatabase {
    CKContainer *container = [CKContainer containerWithIdentifier:@"iCloud.com.tropic4.topxnotes-cloudkit-sync"];
    CKDatabase *iCloudDB = [container privateCloudDatabase];
    return iCloudDB;
}

// Retrieve existing records
+ (void)fetchNotepadWithCompletionHandler:(CloudKitCompletionHandler)handler {

    NSPredicate *predicate = [NSPredicate predicateWithValue:YES];
    CKQuery *query = [[CKQuery alloc] initWithRecordType:kNotepadRecord predicate:predicate];

    [[self privateCloudDatabase] performQuery:query
                                inZoneWithID:nil
                           completionHandler:^(NSArray *results, NSError *error) {

                               if (!handler) return;

                               dispatch_async(dispatch_get_main_queue(), ^{
//                                   handler ([self mapCities:results], error);
                                   handler (results, error);
                               });
    }];
}


// Fetch Notepad record with recordId
+ (void)fetchNotepadRecordWithId:(NSString *)recordId completionHandler:(CloudKitCompletionHandler)handler {
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    [[self privateCloudDatabase] fetchRecordWithID:recordID completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!handler) return;
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler (nil, error);
            });
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler (@[record], error);
        });
        
    }];
}

// add a new record
//+ (void)createRecord:(NSDictionary *)recordDic completionHandler:(CloudKitCompletionHandler)handler {
+ (void)createRecord:(NSString *)path completionHandler:(CloudKitCompletionHandler)handler {
    
    CKRecord *record = [[CKRecord alloc] initWithRecordType:kNotepadRecord];

    // Set notepad file CKAsset.
//    TopXNotesAppDelegate *appDelegate =
//        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSURL *assetFileUrl = [NSURL fileURLWithPath:[appDelegate.model pathToData]];
    NSURL *assetFileUrl = [NSURL fileURLWithPath:path];
    if(assetFileUrl) {
        CKAsset *asset = [[CKAsset alloc] initWithFileURL:assetFileUrl];
        record[@"File"] = asset;
    }

    // Set notepad version.
//    record[@"Version"] = [appDelegate.model notePadVersion];
    record[@"Version"] = [NSNumber numberWithInt:7];    // This field not used.

    // Save the record.
    [[self privateCloudDatabase] saveRecord:record completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!handler) return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler (@[record], error);
        });
    }];
}

// updating the record by recordId
+ (void)updateRecordFileWithId:(NSString *)recordId path:(NSString *)path completionHandler:(CloudKitCompletionHandler)handler {
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    [[self privateCloudDatabase] fetchRecordWithID:recordID completionHandler:^(CKRecord *record, NSError *error) {
        
        if (!handler) return;
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler (nil, error);
            });
            return;
        }
        
        NSURL *assetFileUrl = [NSURL fileURLWithPath:path];
        if(assetFileUrl) {
            CKAsset *asset = [[CKAsset alloc] initWithFileURL:assetFileUrl];
            record[@"File"] = asset;
        }
        
        record[@"Version"] = [NSNumber numberWithInt:7];    // This field not really used.

        [[self privateCloudDatabase] saveRecord:record completionHandler:^(CKRecord *record, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler (@[record], error);
            });
        }];
    }];
}

// remove the record
+ (void)removeRecordWithId:(NSString *)recordId completionHandler:(CloudKitCompletionHandler)handler {
    
    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:recordId];
    [[self privateCloudDatabase] deleteRecordWithID:recordID completionHandler:^(CKRecordID *recordID, NSError *error) {
        
        if (!handler) return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            handler (nil, error);
        });
    }];
}

+ (void) updateCloudSubscriptions {
    NSPredicate *allPredicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    CKSubscription *updateSubscription = [[CKQuerySubscription alloc]
                                               initWithRecordType:@"Notepad" predicate:allPredicate options:
//        (CKQuerySubscriptionOptionsFiresOnRecordCreation | CKQuerySubscriptionOptionsFiresOnRecordUpdate)];
                                               (CKQuerySubscriptionOptionsFiresOnRecordUpdate)];
    CKNotificationInfo *updateNotificationInfo = [CKNotificationInfo new];
    updateNotificationInfo.shouldBadge = NO;
    updateNotificationInfo.shouldSendContentAvailable = YES;
    updateSubscription.notificationInfo = updateNotificationInfo;
//    updateSubscription.notificationInfo.desiredKeys = [NSArray arrayWithObjects:@"File", @"Version", nil];
    updateSubscription.notificationInfo.desiredKeys = [NSArray arrayWithObjects:@"Version", nil];

    [[self privateCloudDatabase] saveSubscription:updateSubscription
                                 completionHandler:^(CKSubscription *theSubscription, NSError *error) {
        if (error){ //error handling
          NSLog(@"Error saving CloudKit subscription - error=\"%@\"", [error localizedDescription]);
        }
        NSLog(@"Notepad Subscription created");
    }];
}
 
@end
