//
//  TxnMergeNotepad.m
//  TopXNotes
//
//  Created by Dirk Jan Maasen on 4/4/13.
//  Copyright 2013 Tropical Software. All rights reserved.
//
// Rewritten by Lewis E. Garrett beginning 1/9/2020                             //leg20200109 - Catalyst
//  to adapt for use by TopXNotes Catalyst.
//

#import "TxnMergeNotepad.h"
#import "Constants.h"                                                           //leg20200219 - Catalyst
#import "SyncConstants.h"                                                       //leg20200219 - Catalyst
#import "Note.h"

// Structures

typedef struct {
	NoteIDT			noteID;
	UInt16			pageNumber;
	NSDate			*createTimestamp;                                           //leg20200219 - Catalyst
} NewNoteData;

//INTERFACES:

@interface MergeNotepad ()
- (void) getTopXNotesToSync;
- (OSErr) getMacSyncNotesInfo;
	
- (UInt8) isNewNoteOnMobile:(Note*)inNote;
- (UInt8) isNewNoteOnMac:(Note*)inNote;                                         //leg20200219 - Catalyst
- (void) syncMobileNote2Mac:(Note*)inNote;
- (NoteIDT) addMobileNote2Mac:(Note*)inNote;

@end

static int sortNoteDataByDate(id newNoteData1, id newNoteData2, void* ignore);

@implementation MergeNotepad

@synthesize modelMobile;  //leg20200309 - Catalyst

// ---------------------------------------------------------------------------
// Number of Mac Notes
// ---------------------------------------------------------------------------
//
- (int) numMacNotes {
	return	numMacNotes;
}

// ---------------------------------------------------------------------------
// Number of Mobile Notes
// ---------------------------------------------------------------------------
//
- (int) numMobileNotes {
	return	numMobileNotes;
}

// ---------------------------------------------------------------------------
// Initialize MergeNotepad
// ---------------------------------------------------------------------------
//
- (id) init: (NSString*) deviceName macModel: (Model *) model {                 //leg20200311 - Catalyst
	if (self = [super init]) {
        modelMac = model;
		noteTitles = [NSMutableArray array];
        noteTexts = [NSMutableArray array];
		noteHTMLs = [NSMutableArray array];                                     //leg20211202 - TopXNotes_Catalyst_2
		noteIDs = [NSMutableArray array];
		noteCreateDates = [NSMutableArray array];
		noteModifyDates = [NSMutableArray array];
		newNoteList = [NSMutableArray array];

// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//        // Determine the proper value for nextNoteID.                           //leg20200324 - Catalyst
//        NSNumber *maxNoteID = [self updateMacNextNoteIDWithMaxNextNoteID];
//        NoteIDT nextID = [[model getNextNoteID] unsignedLongValue];
//        NoteIDT maxID = [maxNoteID unsignedLongValue];
//        if (nextID < maxID) {
//            [model setNextNoteID:maxNoteID];
//        }

        // Initialize device sync info dictionary.                              //leg20200226 - Catalyst
        deviceSyncInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"", @"siDeviceUDID",
                                                                            @"", @"siDeviceName",
                                                                            [NSDate dateWithTimeIntervalSinceReferenceDate:0], @"siSyncDate",
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//                                                                            [NSNumber numberWithUnsignedLong: noteID_None + 1], @"siNextNoteID",     //leg20200310 - Catalyst
//                                                                            [NSNumber numberWithUnsignedLong: groupID_Root + 1], @"siNextGroupID",   //leg20200310 - Catalyst
//                                                                            [NSNumber numberWithUnsignedLong: catID_None + 1], @"siNextCategoryID",  //leg20200310 - Catalyst
                                                                            [NSNumber numberWithInt:1], @"siLastFileVersion",
                                                                            nil];
		[self getTopXNotesToSync];

        modelMobile = [[Model alloc] init: kModelFileNameMobile];
		numMobileNotes = (int)[modelMobile numberOfNotes];
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//        nextNewNoteID = [[modelMobile syncNextNewNoteID] unsignedLongValue];             //leg20200310 - Catalyst
//
//		// If mobile file has not been synced, then last sync timestamp is meaningless
//		// Set last sync timestamp to current time to disable note duplication
//        if (nextNewNoteID == 0) {
        if ([[modelMobile deviceLastSyncDate] compare: [NSDate dateWithTimeIntervalSinceReferenceDate:0]] == NSOrderedSame) {
			iosLastSyncDate = [NSDate date];                                    //leg20200219 - Catalyst
		}
		else {
			iosLastSyncDate = [modelMobile deviceLastSyncDate];                 //leg20200306 - Catalyst
		}

		// Get the unique device ID of where the note pad came from
		deviceUDID = [modelMobile deviceUDID];                                  //leg20200306 - Catalyst
    
        if (!deviceUDID || [deviceUDID isEqualToString: @""]) {                 //leg20200311 - Catalyst
			deviceUDID = @"";
            [deviceSyncInfo setObject:@"Unknown-UDID" forKey: @"siDeviceUDID"];
		}
		else {
            if ([deviceUDID length] != 0) {                                     //leg20200219 - Catalyst
                [deviceSyncInfo setObject: deviceUDID forKey: @"siDeviceUDID"];
            } else {
                [deviceSyncInfo setObject:@"Unknown Device" forKey: @"siDeviceUDID"];
            }

            [deviceSyncInfo setObject: @"" forKey: @"siDeviceName"];            //leg20200219 - Catalyst
            if ([deviceName length] != 0) {
                [deviceSyncInfo setObject: deviceName forKey: @"siDeviceName"];
			}
#pragma mark TODO Figure out why it matters if this is a new file.  Also, this test isn't reliable for determining new file.
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//            bool newFile = numMobileNotes == 1 && nextNewNoteID == 0;           //leg20200219 - Catalyst
			bool newFile = numMobileNotes == 1;                                 //leg20200605 - Catalyst
            [self getSyncInfoForNotePadWithUDID: newFile];                      //leg20200319 - Catalyst
		}
	}

	DEBUG_LOG(@"Data file from %@ loaded.", deviceName);
	return self;
}

// ---------------------------------------------------------------------------
// -dealloc
// ---------------------------------------------------------------------------
//
- (void) dealloc {                                                              //leg20200219 - Catalyst
}

// ---------------------------------------------------------------------------
// Get Mac notes to sync into arrays.
// ---------------------------------------------------------------------------
// Convert GetTopXNotesToSync() WASTe to Model/Note.                            //leg20200219 - Catalyst
//
- (void) getTopXNotesToSync {
	SInt16			i, numNotes;

	[noteIDs removeAllObjects];
	[noteCreateDates removeAllObjects];
	numMacNotes = 0;
	numNotes = [modelMac numberOfNotes];                                        //leg20200309 - Catalyst
    
	for (i = 0; i < numNotes; ++i) {
		Note *noteDef = [modelMac getNoteForIndex: i];                          //leg20200311 - Catalyst

        // Add it to list if it's "Sync" category and is not password protected.
        //  With TopXNotes Catalyst Categories no longer exist, so all
        //  Mac notes are considered as in the "Sync" category.
        if ([self isMobileNote: noteDef.noteID]) {    // hard-coded YES for now!!!!     //leg20200311 - Catalyst
			[noteIDs addObject: noteDef.noteID];
			[noteCreateDates addObject: noteDef.createDate];
			numMacNotes++;
		}
	}
}

// ---------------------------------------------------------------------------
// Convert GetMacSyncNotesInfo() WASTe to Model/Note.                           //leg20200207 - Catalyst
// ---------------------------------------------------------------------------
//
- (OSErr) getMacSyncNotesInfo {
	int						i;
	OSErr 					err = noErr;
	
	[noteTitles removeAllObjects];
    [noteTexts removeAllObjects];
	[noteHTMLs removeAllObjects];                                               //leg20211202 - TopXNotes_Catalyst_2
	[noteModifyDates removeAllObjects];

	for (i = 0; i < numMacNotes && err == noErr; ++i) {
        NoteIDT noteID = (NoteIDT)[[noteIDs objectAtIndex:i] unsignedLongValue];
        Note *noteDef = [modelMac getNoteForNoteID: [NSNumber numberWithUnsignedLong: noteID]];  //leg20200316 - Catalyst

        if (noteDef != nil) {
            [noteTitles addObject:noteDef.title];                               //leg20200327 - Catalyst
            [noteTexts addObject:noteDef.noteText];                             //leg20200327 - Catalyst
            [noteHTMLs addObject:noteDef.noteHTML];                             //leg20211202 - TopXNotes_Catalyst_2
            [noteModifyDates addObject: noteDef.date];
        } else {
            err = -1;
            break;
        }
	}
	return err;
}

#pragma mark TODO Determine why -doesSyncInfoMatch always returns true.

// ---------------------------------------------------------------------------
// -doesSyncInfoMatch
// ---------------------------------------------------------------------------
//
// Convert doesSyncInfoMatch() WASTe to Model/Note.                             //leg20200219 - Catalyst
//
- (Boolean) doesSyncInfoMatch {
    
    // A dateWithTimeIntervalSinceReferenceDate:0 indicates "no date". Compare  //leg20200226 - Catalyst
    //  siSyncDate to last sync date or to the reference date.    
    NSDate *siSyncDate = [deviceSyncInfo objectForKey:@"siSyncDate"];
    if (([iosLastSyncDate compare: siSyncDate] != NSOrderedSame)||
            ([siSyncDate compare: [NSDate dateWithTimeIntervalSinceReferenceDate:0]] == NSOrderedSame)) {
        DEBUG_LOG(@"Sync mismatch!! iOS timestamp = %@; Mac timestamp = %@",
                    iosLastSyncDate, siSyncDate);
	}
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//	else {
//        NoteIDT siNextNoteID = [[deviceSyncInfo objectForKey:@"siNextNoteID"] unsignedLongValue];
//		NoteIDT iOSNextNoteID = [[modelMobile syncNextNewNoteID] unsignedLongValue];    //leg20200306 - Catalyst
//		if (iOSNextNoteID != siNextNoteID) {
//            DEBUG_LOG(@"Sync mismatch!! iOS next noteID = %X; Mac next noteID = %X",
//                      (unsigned int)iOSNextNoteID, (unsigned int)siNextNoteID);
//		}
//	}
	return	YES;
}

// ---------------------------------------------------------------------------
// -clearAllSyncInfo
// ---------------------------------------------------------------------------
//
// Convert clearAllSyncInfo() WASTe to Model/Note.                              //leg20200219 - Catalyst
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//
- (void) clearAllSyncInfo
{
    // Clear sync status info for on-Mac notes    
    NSString *deviceUDID = [deviceSyncInfo objectForKey: @"siDeviceUDID"];
    
	for (int i = 0; i < numMacNotes; ++i) {
		NoteIDT noteID = (NoteIDT)[[noteIDs objectAtIndex:i] unsignedLongValue];

        SInt16 udidSlot = [self isNoteSyncedWithUDID: [NSNumber numberWithUnsignedLong: noteID] inUDID: deviceUDID];    //leg20200331 - Catalyst
		if (udidSlot != -1) {
            Note *noteDef = [modelMac getNoteForNoteID: [NSNumber numberWithUnsignedLong: noteID]];  //leg20200331 - Catalyst
            [self clearNoteSyncInfo: noteDef for: deviceUDID];                  //leg20200331 - Catalyst
		}
    }
	// Empty the sync date slot so it can be reused.
    [self clearSyncDataSlotForNotePadWithUDID: [deviceSyncInfo objectForKey:@"siDeviceUDID"]];

	// Clear the sync date in the Iphone notepad.
//    [modelMobile setDeviceLastSyncDate: 0];                                     //leg20200306 - Catalyst
	[modelMobile setDeviceLastSyncDate: [NSDate dateWithTimeIntervalSinceReferenceDate:0]]; //leg20200605 - Catalyst

//	NSNumber *syncNextNewNoteID = [NSNumber numberWithUnsignedLong: noteID_None + 1];
//	[modelMobile setSyncNextNewNoteID:syncNextNewNoteID];                       //leg20200306 - Catalyst

    // Zero note IDs in mobile notepad to make them all new
    for (int idx = 0; idx < numMobileNotes; idx++) {
        Note *note = [modelMobile getNoteForIndex:idx];                         //leg20200306 - Catalyst
        [note setNoteID:0];
//        note.syncFlag = [NSNumber numberWithBool:NO];                           //leg20200528 - Catalyst
//        note.needsSyncFlag = [NSNumber numberWithBool:NO];
        note.syncFlag = [NSNumber numberWithBool:YES];                          //leg20211206 - TopXNotes_Catalyst_2
        note.needsSyncFlag = [NSNumber numberWithBool:YES];                     //leg20211206 - TopXNotes_Catalyst_2

        // ???: missing replaceNoteAtIndex -- must be a mistake!                //leg20211203 - TopXNotes_Catalyst_2
        [modelMobile replaceNoteAtIndex:idx withNote:note];                     
    }
    
    // Reset noteIDs to beginning.                                              //leg20211203 - TopXNotes_Catalyst_2
    [modelMac setNextNoteID:[NSNumber numberWithUnsignedLong: noteID_None+1]];
    
	for (int idx = 0; idx < numMacNotes; idx++) {
		Note *note = [modelMac getNoteForIndex:idx];
		note.noteID = [modelMac nextNoteID];
        note.syncFlag = [NSNumber numberWithBool:YES];                          //leg20211206 - TopXNotes_Catalyst_2
        note.needsSyncFlag = [NSNumber numberWithBool:YES];                     //leg20211206 - TopXNotes_Catalyst_2
        [modelMac replaceNoteAtIndex:idx withNote:note];
	}
}

// ---------------------------------------------------------------------------
// -isNewNoteOnMobile
// ---------------------------------------------------------------------------
//
- (UInt8) isNewNoteOnMobile:(Note*)inNote
{
	return ([inNote.noteID unsignedLongValue] == 0) && ([inNote.protectedNoteFlag
				boolValue] == false);
}

// ---------------------------------------------------------------------------
// -isNewNoteOnMac
// ---------------------------------------------------------------------------
//
// Convert isNewNoteOnMac() WASTe to Model/Note.                                //leg20200219 - Catalyst
// ---------------------------------------------------------------------------
//
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
- (UInt8) isNewNoteOnMac:(Note *)noteDef {
	UInt8		result = 0;
	
    // Determine if note will be new on Mac.                                    //leg20200617 - Catalyst
    //  Return b'00000001' if synced with UDID.                                 //leg20200617 - Catalyst
    if ([self isNoteSyncedWithUDID: noteDef.noteID inUDID: [deviceSyncInfo objectForKey: @"siDeviceUDID"]] == -1) {
		result |= 1;
	}
		
//    #pragma mark TODO Determine what is being determined in this evaluation
//
//    if (nextNewNoteID >= noteID_None + 2 && (NoteIDT)[noteDef.noteID unsignedLongValue] >= nextNewNoteID) {  //leg20200316 - Catalyst
//        result |= 2;
//        if ([noteDef.createDate compare:lastSyncDate] != NSOrderedAscending) {  //leg20200312 - Catalyst
//            result |= 4;
//        }
//    }
    
    //  Return b'00000010' if NoteID >= nextNoteID.                             //leg20200617 - Catalyst
    if ([modelMac.getNextNoteID unsignedLongValue] >= noteID_None + 2 &&
            (NoteIDT)[noteDef.noteID unsignedLongValue] >= [modelMac.getNextNoteID unsignedLongValue]) {
		result |= 2;
        
        //  Return b'00000100' if note create date > last sync date.            //leg20200617 - Catalyst
		if ([noteDef.createDate compare:lastSyncDate] != NSOrderedAscending) {  //leg20200312 - Catalyst
			result |= 4;
		}
	}

    //  Return b'00000000' if not new note on Mac.                              //leg20200617 - Catalyst
	return result;
}

// ---------------------------------------------------------------------------
// -syncMobileNote2Mac
// ---------------------------------------------------------------------------
//
// Convert SyncMobileNote2Mac() WASTe to Model/Note.                            //leg20200219 - Catalyst
// ---------------------------------------------------------------------------
//
// Modify to handle noteIDs as NSNumbers.                                       //leg20200312 - Catalyst
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//
- (void) syncMobileNote2Mac:(Note*)inNote
{
    // Get the note definition of mobile note
    Note *noteDef = [modelMac getNoteForNoteID: inNote.noteID];                 //leg20200309 - Catalyst

	// Insure that note gets saved
    noteDef.nfChangedSync = [NSNumber numberWithBool: YES];                     //leg20200219 - Catalyst
	inNote.nfChangedSync = [NSNumber numberWithBool: YES];                      //leg20200605 - Catalyst
    
    // Insure that note no longer needs syncing.                                //leg20220817 - TopXNotes_Catalyst_2
    noteDef.needsSyncFlag = [NSNumber numberWithBool:NO];
    inNote.needsSyncFlag = [NSNumber numberWithBool:NO];
    
    // Replace Mac note with Mobile note in modelMac.                           //leg20200327 - Catalyst
    //
    // I'm not sure if it is necessary to go through all of this just to        //leg20200605 - Catalyst
    //  replace the note in modelMac with the mobile note, but I'll
    //  leave it like this for now.
    NSUInteger index = NSNotFound;
    index = [modelMac getIndexOfNoteWithNoteID: noteDef.noteID];
    if (index != NSNotFound) {
        // Fix syncing of changed mobile notes by preserving the sync status.   //leg20200622 - Catalyst
        inNote.npSync = noteDef.npSync;
        [modelMac replaceNoteAtIndex:index withNote:inNote];
    }
}

// ---------------------------------------------------------------------------
// -addMobileNote2Mac
// ---------------------------------------------------------------------------
//
// Convert AddMobileNote2Mac() WASTe to Model/Note.                             //leg20200219 - Catalyst
//
// Modify to handle noteIDs as NSNumbers.                                       //leg20200312 - Catalyst
//
- (NoteIDT) addMobileNote2Mac:(Note*)inNote
{
	NSNumber *noteID = inNote.noteID;

    // Get ID for new mobile note to be added to mac notes.
    if ([noteID longValue] == 0) {
        noteID = modelMac.nextNoteID;
        inNote.noteID = noteID;
    }

    // Modify Sync with TopXNotes iOS 1.6.0 so that added mobile notes are      //leg20220811 - TopXNotes_Catalyst_2
    //  placed in the Root Group.
    
    // Change init to support Groups.  Add to root group initially.             //leg20220811 - TopXNotes_Catalyst_2
    Note *noteDef = [[Note alloc] initWithNoteTitle:inNote.title
                                            text:inNote.noteText
                                          withID:noteID
                                     andWithUUID:[Model genNoteUUID]
                                     withinGroup:[modelMac getRootGroupUUID]
                                          onDate:inNote.date
                                      createDate:inNote.createDate];
    
    noteDef.noteHTML = inNote.noteHTML;                                         //leg20211202 - TopXNotes_Catalyst_2
    
    // Set nfChangedSync here instead of calling -syncMobileNote2Mac.           //leg20200312 - Catalyst
    noteDef.nfChangedSync = [NSNumber numberWithBool: YES];
    
    // Add Note to Mac Model.
    [modelMac addNote: noteDef];                                                //leg20200309 - Catalyst
    
    // Move Group to root-group.                                                //leg20220811 - TopXNotes_Catalyst_2
    [modelMac moveNoteOrGroup:noteDef.noteUUID
                   fromGroup:nil
                     toGroup:[modelMac getRootGroupUUID]];

    // Get updated note to add to display tree.                                 //leg20220811 - TopXNotes_Catalyst_2
    Note* addedNote = [modelMac getNoteForUUID:noteDef.noteUUID];

    // Add note to display list.                                                //leg20220811 - TopXNotes_Catalyst_2
    [modelMac addDisplayNote:addedNote];

	[self syncMobileNote2Mac:addedNote];                                        //leg20200528 - Catalyst

    // Keep track of noteID, page number (obsolete) and createDate.             //leg20220811 - TopXNotes_Catalyst_2
    NewNoteData noteData = {(NoteIDT)[addedNote.noteID unsignedLongValue], 0, addedNote.createDate};
	[newNoteList addObject: [NSValue value:&noteData withObjCType:@encode(NewNoteData)]];
    
	return (NoteIDT)[noteID unsignedLongValue];  // Return doesn't appear to be used!!   //leg20200312 - Catalyst
}

// ---------------------------------------------------------------------------
// -mergeMobile
// ---------------------------------------------------------------------------
//
// Convert mergeMobile() WASTe to Model/Note.                                   //leg20200219 - Catalyst
//
- (void) mergeMobile: (Boolean) misMatchedSync
		  noteCounts: (SyncNoteCounts*)syncNoteCounts
{
	int				iMac, iMobile, numNotes;
	Boolean			found;
	NewNoteData		noteData;
	Note *			note;

	DEBUG_LOG(@"iOS file start.");
    syncNoteCounts->ncDeleted_iOS = 0;                                          //leg20200219 - Catalyst
    syncNoteCounts->ncUpdated_iOS = 0;
    syncNoteCounts->ncAdded_iOS = 0;
    syncNoteCounts->ncRestored_iOS = 0;
    syncNoteCounts->ncDeleted_Mac = 0;
    syncNoteCounts->ncUpdated_Mac = 0;
    syncNoteCounts->ncAdded_Mac = 0;
    syncNoteCounts->ncRestored_Mac = 0;
    syncNoteCounts->ncDuplicated = 0;

    // MARK: Step 1.  Between the mobile and Mac notepads, determine the Note ID and last sync time for use in merging process

// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
////    if ([[deviceSyncInfo objectForKey:@"siNextNoteID"] unsignedLongValue] < nextNewNoteID) {
//    NoteIDT a = [[deviceSyncInfo objectForKey:@"siNextNoteID"] unsignedLongValue];
//    NoteIDT b = nextNewNoteID;
//    if (a < b) {
//		nextNewNoteID = [[deviceSyncInfo objectForKey:@"siNextNoteID"] unsignedLongValue];
//	}
//    nextNewNoteID = [modelMac.getNextNoteID unsignedLongValue];                 //leg20200605 - Catalyst
    
	lastSyncDate = iosLastSyncDate;
    
    if ([[deviceSyncInfo objectForKey:@"siSyncDate"] compare:lastSyncDate] == NSOrderedAscending) { //leg20200312 - Catalyst
        lastSyncDate = [deviceSyncInfo objectForKey:@"siSyncDate"];
    }

    // MARK: Step 2.  For all the Mobile notes:

    // Step 2.	For all the Mobile notes:
	//		A.  If match found on Mac side, see if Mac needs updating.
	//		B.  If new, add it to the new notes list AND the Mac notepad
	//		C.  Skip it if deletes are enabled
	//          otherwise add it back to the Mac notepad
	//
	for (iMobile = 0; iMobile < numMobileNotes; iMobile++) {
		note = [modelMobile getNoteForIndex:iMobile];                           //leg20200306 - Catalyst
		if ([note.protectedNoteFlag boolValue]) {
			break;
		}
        
        // If mobile note doesn't have a title, give it one.
        note.title = [note.title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([note.title isEqualToString: @""]) {
            note.title = @"untitled";
            note.noteText = @"untitled";
        }

        // Look for note in Mac set                                             //leg20200219 - Catalyst
		NoteIDT noteID = (NoteIDT)[note.noteID unsignedLongValue];
		for (iMac = 0, numNotes = (int)[noteIDs count], found = NO; iMac < numNotes; iMac++) {
			if ((found = [[noteIDs objectAtIndex:iMac] isEqualToNumber:note.noteID] &&
                 ([self isNoteSyncedWithUDID: [NSNumber numberWithUnsignedLong: noteID] inUDID: [deviceSyncInfo objectForKey: @"siDeviceUDID"]] != -1))) {
				break;
			}
		}
		if (found) {
			// Found note - check for content update                            //leg20200219 - Catalyst
            Note *noteDef = [modelMac getNoteForNoteID: [NSNumber numberWithUnsignedLong: noteID]];  //leg20200327 - Catalyst
			NSDate *modifyTime = note.date;                                     //leg20200226 - Catalyst
            
            // Use the modelMac's UUID info in case modelMobile version is      //leg20220812 - TopXNotes_Catalyst_2
            //  less than kNotePadVersion7 and doesn't have UUID info.
            note.noteUUID = noteDef.noteUUID;
            note.groupUUID = noteDef.groupUUID;
            note.depth = noteDef.depth;

            // See if change in both notes was after last sync date.
            if (([noteDef.date compare:[deviceSyncInfo objectForKey:@"siSyncDate"]] == NSOrderedDescending) &&  //leg20200312 - Catalyst
                ([modifyTime compare:iosLastSyncDate] == NSOrderedDescending)) {    //leg20200327 - Catalyst
                                
                // Changed the way an existing synced note which has been       //leg20220817 - TopXNotes_Catalyst_2
                //  changed on both mac and mobile sides is handled:
                //  Now the side with the most recent modified date is synced
                //  to the other side. Previously, the note versions were
                //  duplicated on both sides resulting in 2 copies of same
                //  titled note on each side.
                if ([noteDef.date compare:modifyTime] == NSOrderedDescending) {
                    // Mac note is newer, nothing to do
                    noteDef.nfChangedSync = [NSNumber numberWithBool: YES];
                    syncNoteCounts->ncUpdated_iOS++;
                }
                else if ([noteDef.date compare:modifyTime] == NSOrderedAscending) {
                    // Mobile note is newer; update Mac copy.
                    //  Comparing noteDef.date to modifyTime, modifyTime is
                    //  newer date: ascending
                    [self syncMobileNote2Mac:note];
                    syncNoteCounts->ncUpdated_Mac++;
                }
            }
            else if ([noteDef.date compare:modifyTime] == NSOrderedDescending) { //leg20200312 - Catalyst
				// Mac note is newer, nothing to do
				noteDef.nfChangedSync = [NSNumber numberWithBool: YES];
				syncNoteCounts->ncUpdated_iOS++;
			}
            else if ([noteDef.date compare:modifyTime] == NSOrderedAscending) { //leg20200312 - Catalyst
                // Mobile note is newer; update Mac copy.
                //  Comparing noteDef.date to modifyTime, modifyTime is
                //  newer date: ascending
				[self syncMobileNote2Mac:note];
				syncNoteCounts->ncUpdated_Mac++;
            } else {
                // Note unchanged on mobile or mac.
            }
            
			// Done with this note, delete Mac info
			[noteIDs removeObjectAtIndex:iMac];
			[noteCreateDates removeObjectAtIndex:iMac];
			numNotes--;
		}  // found
		else if ([self isNewNoteOnMobile:note]) {
			// New mobile note; Create a new Mac note.
			[self addMobileNote2Mac:note];
			syncNoteCounts->ncAdded_Mac++;
		}
		else {
            // Note not found, possible delete candidate
            // MARK: IsSyncDeleteNotes() is hard-coded to always return true!
            BOOL IsSyncDeleteNotes = true;
//            if (!cTopXNotesDocument->IsSyncDeleteNotes() || misMatchedSync) {     the old carbon code!!!
//            if (YES || misMatchedSync) {                                        //leg20200226 - Catalyst
            // Prevent "code will not be executed" on "else" condition, which   //leg20211208 - TopXNotes_Catalyst_2
            //  allows ncDeleted_iOS to be incremented.
			if (!IsSyncDeleteNotes || misMatchedSync) {                         //leg20211208 - TopXNotes_Catalyst_2
				// Deletes disabled or mismatch on; keep note
                if ([modelMac getNoteForNoteID: [NSNumber numberWithUnsignedLong:noteID]]) {   //leg20200327 - Catalyst
					[self addMobileNote2Mac:note];
					syncNoteCounts->ncRestored_Mac++;
				}
			}
			else {
				syncNoteCounts->ncDeleted_iOS++;
			}
		}//  not found
	} // for iMobile

// MARK: Step 3.    For all remaining Mac notes in the Mobile category that didn't have matches on the Mobile side:

	// Step 3.	For all remaining Mac notes in the Mobile category that didn't
	//			have matches on the Mobile side:
	//		A:	If note is new based on Note ID, add it to the new notes list
	//		B:	If Note is old, it should be deleted if allowed.

	for (iMac = 0; iMac < (int)[noteIDs count]; iMac++) {                       //leg20200219 - Catalyst
		NoteIDT noteID = (NoteIDT)[[noteIDs objectAtIndex:iMac] unsignedLongValue];
        Note *noteDef = [modelMac getNoteForNoteID: [NSNumber numberWithUnsignedLong:noteID]];   //leg20200312 - Catalyst

		UInt8 newNote = [self isNewNoteOnMac:noteDef];
		if (newNote) {
			// New Mac note
			noteData.noteID = (NoteIDT)[noteDef.noteID unsignedLongValue];
			noteData.pageNumber = 0;
			noteData.createTimestamp = noteDef.createDate;
			[newNoteList addObject: [NSValue value:&noteData withObjCType:@encode
				(NewNoteData)]];
            noteDef.nfChangedSync = [NSNumber numberWithBool:YES];
			syncNoteCounts->ncAdded_iOS++;
		}
		else {
			// Delete note
            // MARK: IsSyncDeleteNotes() is hard-coded to always return true!
            if (YES && !misMatchedSync) {
                NSUInteger index = NSNotFound;
                index = [modelMac getIndexOfNoteWithNoteID: [NSNumber numberWithUnsignedLong:noteID]];   //leg20200312 - Catalyst

                if (index != NSNotFound)
                    [modelMac removeNoteAtIndex: index];                     //leg20200312 - Catalyst

                syncNoteCounts->ncDeleted_Mac++;
			}
			else {
				syncNoteCounts->ncRestored_iOS++;
			}
		}
	} //for iMac
	
	DEBUG_LOG(@"iOS file merge completed");
}


// ---------------------------------------------------------------------------
// -sortNoteDataByDate
// ---------------------------------------------------------------------------
//
static int sortNoteDataByDate(id newNoteData1, id newNoteData2, void* ignore) {
#pragma unused (ignore)
	NewNoteData		noteData1, noteData2;

	[(NSValue*)newNoteData1 getValue:&noteData1];
	[(NSValue*)newNoteData2 getValue:&noteData2];

	if (noteData1.createTimestamp == noteData2.createTimestamp) return NSOrderedSame;
	if (noteData1.createTimestamp <  noteData2.createTimestamp) return NSOrderedAscending;
	return NSOrderedDescending;
}


// ---------------------------------------------------------------------------
// - writeMobileFile
// ---------------------------------------------------------------------------
//
// Convert writeMobileFile() WASTe to Model/Note.                               //leg20200219 - Catalyst
//
//- (NSDictionary *) writeMobileFile                                              //leg20200316 - Catalyst
- (NSDictionary *) writeMobileFile: (Boolean) isClearSync                       //leg20200528 - Catalyst
{
#pragma mark SYNC SPECIFICATION - 5. Build notepad for return to iDevice.

	//
	// Now, build the Model for return to the iPhone
	//
    // Fix Issue #6 - "Clear Sync Status" not working                           //leg20200528 - Catalyst
    //
    // If clearSync no need to process notepad further.                         //leg20200528 - Catalyst
    if (!isClearSync) {
        // Now that Mac is all synced up, get subset of the notes that's needs to go back to the iPhone
        [self getTopXNotesToSync];
        OSErr err = [self getMacSyncNotesInfo];
        if (err != noErr) return false;

        // Now, empty out the model so that it can be reused to send notes back to the iPhone
        [modelMobile removeAllUnprotectedNotes];                                //leg20200306 - Catalyst
        int numNotes = (int)[noteIDs count];
        for (int i = 0; i < numNotes; ++i) {
            Note *note = [[Note alloc] initWithNoteTitle: [noteTitles objectAtIndex:i]    //leg20200310 - Catalyst
                                                    text: [noteTexts objectAtIndex:i]
                                                  withID: [noteIDs objectAtIndex:i]
                                                  onDate: [noteModifyDates objectAtIndex:i]
                                              createDate: [noteCreateDates objectAtIndex:i]];
            
            note.noteHTML = [noteHTMLs objectAtIndex:i];                        //leg20211202 - TopXNotes_Catalyst_2
            note.syncFlag = [NSNumber numberWithBool:NO];    // turn sync flag off to show note has been synced
            note.needsSyncFlag = [NSNumber numberWithBool:NO];

            // Add note to front of list, so it will have an index of 0.
            [modelMobile addNote:note];                                         //leg20200306 - Catalyst
        }

        // Save the last synchronized date for iPhone/iPad.                     //leg20200219 - Catalyst
        // Use current date and time for date last synchronized.
        NSDate *syncDate = [NSDate date];
        NSDate *syncDateTime = syncDate;                                        //leg20200226 - Catalyst
        [deviceSyncInfo setObject: syncDateTime forKey:@"siSyncDate"];

        // Set the date that the iPhone notepad was last synchronized in the    //leg20200219 - Catalyst
        //  Iphone notepad.
        [modelMobile setDeviceLastSyncDate:syncDate];                           //leg20200306 - Catalyst
        
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//        // Make sure mobile notepad's nextNoteID matches the mac notepad's.     //leg20200325 - Catalyst
//        NSNumber *syncNextNewNoteID = [modelMac getNextNoteID];
//        [modelMobile setSyncNextNewNoteID:syncNextNewNoteID];                   //leg20200306 - Catalyst
    }

	// Save the model to its file storage.
	[modelMobile saveData];                                                     //leg20200309 - Catalyst

	// Does the model contain any notes?
	Boolean HaveNotesToSend	= ([modelMobile numberOfNotes] > 0);                //leg20200306 - Catalyst
    
    // Create dictionary of return values.                                      //leg20200316 - Catalyst
    NSDictionary *writeMobileFileInfoDict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithBool: HaveNotesToSend], @"writeMobileFile_HaveNotesToSend",
                          deviceSyncInfo, @"writeMobileFile_deviceSyncInfo", nil];
    
	DEBUG_LOG(@"New data file for %@ created.", [deviceSyncInfo objectForKey: @"siDeviceName"]);
    
	return writeMobileFileInfoDict;                                             //leg20200316 - Catalyst
}

#pragma mark - Helper Methods

// ---------------------------------------------------------------------------
//    clearNoteSyncInfo
// ---------------------------------------------------------------------------
//
// Convert ClearNoteSyncInfo() to ObjC.                                         //leg20200207 - Catalyst
//
- (void) clearNoteSyncInfo: (Note *)ioNoteDef for: (NSString *)deviceUDID {     //leg20200331 - Catalyst
    for (int i=0; i<kMaxSyncDevices; i++) {

        if ([[[ioNoteDef.npSync objectAtIndex:i] objectForKey:@"nsDeviceUDID"] compare: deviceUDID] == NSOrderedSame) {
            // Insure that npSync and its elements are mutable so that we
            //  can update just the udidSlot.
            NSMutableDictionary *npSyncDictionary = [NSMutableDictionary dictionaryWithDictionary: ioNoteDef.npSync[i]];
            [npSyncDictionary setObject:@"" forKey:@"nsDeviceUDID"];
            [npSyncDictionary setObject:[NSDate dateWithTimeIntervalSinceReferenceDate:0] forKey:@"siSyncDate"];
            NSMutableArray *npSyncArray = [NSMutableArray arrayWithArray:ioNoteDef.npSync];
            [npSyncArray replaceObjectAtIndex:i withObject: npSyncDictionary];
            ioNoteDef.npSync = npSyncArray;
        }
    }
    ioNoteDef.nfChangedSync = [NSNumber numberWithBool:NO];
    ioNoteDef.syncFlag = [NSNumber numberWithBool:NO];
    
    NSUInteger idx = [modelMac getIndexOfNoteWithNoteID:ioNoteDef.noteID];
//    if (index >= 0) {
    // ???: index defined here by strings.h -- must be a mistake!               //leg20211203 - TopXNotes_Catalyst_2
    if (idx >= 0) {
        [modelMac replaceNoteAtIndex:idx withNote:ioNoteDef];
    }
}

// ---------------------------------------------------------------------------
// -isNoteSyncedWithUDID
// ---------------------------------------------------------------------------
//
// Convert IsNoteSyncedWithUDID() to ObjC.                                      //leg20200207 - Catalyst
//
- (SInt16) isNoteSyncedWithUDID: (NSNumber *)inNoteID inUDID:(NSString *)UDID {
    Note *noteDef = [modelMac getNoteForNoteID: inNoteID];                      //leg20200316- Catalyst
    SInt16 result = -1;        // -1 - not synced with UDID

    if (noteDef != nil) {
        for (int i=0; i<kMaxSyncDevices; i++) {

            if ([[[noteDef.npSync objectAtIndex:i] objectForKey:@"nsDeviceUDID"] compare: UDID] == NSOrderedSame) {
                result = i;
                return result;
            }
        }
    }

    return result;
}

// ---------------------------------------------------------------------------
// -isMobileNote - Does the unprotected Mac note exist?
// ---------------------------------------------------------------------------
//
// Convert IsMobileNote() to ObjC.                                              //leg20200216 - Catalyst
//
- (BOOL) isMobileNote:(NSNumber*)inNoteID {

    Note *note = [modelMac getNoteForNoteID: inNoteID];
    
    if (note != nil) {
        if ([note.protectedNoteFlag boolValue]) {  //leg20200311 - Catalyst
            return false;
        }
    } else {
        return false;
    }

    return true;     // hard-code for now.
}

// ---------------------------------------------------------------------------
//        isNotePadSyncedWithUDID
// ---------------------------------------------------------------------------
//
// Convert IsNotePadSyncedWithUDID() to ObjC.                                   //leg20200207 - Catalyst
//
- (SInt16) isNotePadSyncedWithUDID:(NSString *)inUDID
{
    SInt16 result = -99;            // 0 to 9 exisitng slot in use
                                    // -10 to -1 not found; empty slot to use
                                    // -99 not found, no empty slot
    
    NSArray *deviceSyncInfoTable = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];
    for (int i=0; i<kMaxSyncDevices; i++) {

        if ([[[deviceSyncInfoTable objectAtIndex:i] objectForKey: @"siDeviceUDID"] isEqualToString: inUDID]) {   //leg20200317 - Catalyst
            result = i;
            return result;
        }

        if (result == -99 && [[[deviceSyncInfoTable objectAtIndex:i] objectForKey: @"siDeviceUDID"] length] == 0) {
            result = -(i + 1);
        }
    }
    
    return result;
}

// ---------------------------------------------------------------------------------
//        • getSyncInfoForNotePadWithUDID
// ---------------------------------------------------------------------------------
//
// Convert getSyncInfoForNotePadWithUDID() to ObjC.                             //leg20200207 - Catalyst
//
- (BOOL) getSyncInfoForNotePadWithUDID:(BOOL)inMapUDID {                        //leg20200319 - Catalyst

    // Get saved deviceSyncTable and insure it is mutable so that we            //leg20200318 - Catalyst
    //  can update the slot.
    NSArray *deviceSyncInfoTableFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];
    NSMutableArray *deviceSyncInfoTable = [NSMutableArray array];
    [deviceSyncInfoTable addObjectsFromArray:deviceSyncInfoTableFromDefaults];
    
    // Get a copy of the device sync info.
    NSMutableDictionary *ioSyncInfo = [NSMutableDictionary dictionaryWithDictionary: deviceSyncInfo];   //leg20200319 - Catalyst

    // Note:  I don't understand the reason for this routine, or what it        //leg20200319 - Catalyst
    //  is actually doing.  Also, inMapUDID is only TRUE if notepad contains
    //  only 1 note and nextNoteID == 0.
//    if (inMapUDID) {
//        NSString    *previousUDID;
//
//        for (int i=0; i<kMaxSyncDevices; i++) {
//            // Compare device info table slot device name to ioSyncInfo
//            //  device name.
//            if ([[[deviceSyncInfoTable objectAtIndex:i] objectForKey:@"siDeviceName"] compare:[ioSyncInfo objectForKey: @"siDeviceName"]] == NSOrderedSame) {
//
//                // Save device name from device info table into previousUDID.
//                previousUDID = [[deviceSyncInfoTable objectAtIndex:i] objectForKey:@"siDeviceName"];
//
//                // Save ioSyncInfo device name into device info table device name.
//                [[deviceSyncInfoTable objectAtIndex:i] setObject:[ioSyncInfo objectForKey: @"siDeviceUDID"] forKey:@"siDeviceUDID"];
//
//                for (int p=1, pLast=[modelMac numberOfNotes]; p<=pLast; p++) {  //leg20200316 - Catalyst
//                    Note *noteDef = [modelMac getNoteForIndex: p];
//
//                    if ([self isMobileNote: noteDef.noteID]) {    // hard-coded YES for now!!!!     //leg20200311 - Catalyst
//                        for (int x=0; x<kMaxSyncDevices; x++) {
//
//                            // noteDef.npSync is an array of dictionary objects
//                            //  containing 2 keys: nsDeviceUDID and siSyncDate,
//                            if ([[[noteDef.npSync objectAtIndex:x] objectForKey:@"nsDeviceUDID"] compare: previousUDID] == NSOrderedSame) {
//                                [[noteDef.npSync objectAtIndex:x] setObject:[ioSyncInfo objectForKey: @"siDeviceUDID"] forKey:@"nsDeviceUDID"];
//                                break;
//                            }
//                        }
//                    }
//                }
//                break;
//            }
//        }
//    }
    
    // Set default device sync info.
    [ioSyncInfo setObject: @"???" forKey:@"siDeviceName"];
    [ioSyncInfo setObject: [NSDate dateWithTimeIntervalSinceReferenceDate:0] forKey:@"siSyncDate"];
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//    [ioSyncInfo setObject: [NSNumber numberWithUnsignedLong: noteID_None + 1] forKey:@"siNextNoteID"];
//    [ioSyncInfo setObject: [NSNumber numberWithUnsignedLong: groupID_Root + 1] forKey:@"siNextGroupID"];
//    [ioSyncInfo setObject: [NSNumber numberWithUnsignedLong: catID_None + 1] forKey:@"siNextCategoryID"];
    [ioSyncInfo setObject: [NSNumber numberWithInt: 0] forKey:@"siLastFileVersion"];

    SInt16 udidSlot = [self isNotePadSyncedWithUDID: [ioSyncInfo objectForKey: @"siDeviceUDID"]];

    if (udidSlot >= 0) {
        // Fill the device sync info with data from the found device            //leg20200319 - Catalyst
        //  sync info table slot.
        deviceSyncInfo = [NSMutableDictionary dictionaryWithDictionary: [deviceSyncInfoTable objectAtIndex: udidSlot]];

        return TRUE;
    }

    // Fill the device sync info with default data.
    deviceSyncInfo = [NSMutableDictionary dictionaryWithDictionary: ioSyncInfo];    //leg20200319 - Catalyst
    
    return FALSE;
}

// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//// ---------------------------------------------------------------------------------
////		• updateMacNextIDWithMaxNextID
//// ---------------------------------------------------------------------------------
////
//// Added.                                                                       //leg20200324 - Catalyst
////
//- (NSNumber*) updateMacNextNoteIDWithMaxNextNoteID {
//
//    // Iterate the device sync info table entries to determine the maximum
//    //  next NoteID set for all sync slots.
//
//    // Get saved device Sync Info Table.
//    NSArray *deviceSyncInfoTable = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];
//
//    unsigned long maxNoteID = noteID_None;
//
//    for (int i=0; i<kMaxSyncDevices; i++) {
//        if (maxNoteID < [[[deviceSyncInfoTable objectAtIndex:i] objectForKey:@"siNextNoteID"] unsignedLongValue]) {
//            maxNoteID = [[[deviceSyncInfoTable objectAtIndex:i] objectForKey:@"siNextNoteID"] unsignedLongValue];
//        }
//    }
//
//    return [NSNumber numberWithUnsignedLong:maxNoteID];
//}

// ---------------------------------------------------------------------------------
//		• clearSyncDataSlotForNotePadWithUDID
// ---------------------------------------------------------------------------------
// Convert clearSyncDataSlotForNotePadWithUDID() to ObjC.                       //leg20200207 - Catalyst
//
- (SInt16) clearSyncDataSlotForNotePadWithUDID:(NSString *)inUDID {
    SInt16 udidSlot = [self isNotePadSyncedWithUDID: inUDID];

	if (udidSlot >= 0) {
		// Clear the UDID of the synced note pad.
        NSMutableArray *deviceSyncTableFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceSyncInfoTable"];  //leg20200331 - Catalyst
        NSMutableArray *deviceSyncTable = [NSMutableArray arrayWithArray:deviceSyncTableFromDefaults];                              //leg20200331 - Catalyst
        NSMutableDictionary *deviceSyncInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"", @"siDeviceUDID",
                                                                            @"", @"siDeviceName",
                                                                            [NSDate dateWithTimeIntervalSinceReferenceDate:0], @"siSyncDate",   //leg20200217 - Catalyst
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//                                                                            [NSNumber numberWithUnsignedLong: noteID_None + 1], @"siNextNoteID",
//                                                                            [NSNumber numberWithUnsignedLong: groupID_Root + 1], @"siNextGroupID",
//                                                                            [NSNumber numberWithUnsignedLong: catID_None + 1], @"siNextCategoryID",
                                                                            [NSNumber numberWithInt:1], @"siLastFileVersion",
                                                                            nil];
        [deviceSyncTable replaceObjectAtIndex: udidSlot withObject: deviceSyncInfo];

        [[NSUserDefaults standardUserDefaults] setObject:deviceSyncTable forKey:@"DeviceSyncInfoTable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
	}

	return udidSlot;

}

// ---------------------------------------------------------------------------
// -defaultSyncArray
// ---------------------------------------------------------------------------
//
// Create default sync info array.                                              //leg20200219 - Catalyst
//
- (NSArray *) defaultSyncArray {

    NSDictionary *syncDictionarys[kMaxSyncDevices];
    NSDictionary *noteSyncInfoDict = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"", @"nsDeviceUDID",
                          [NSDate dateWithTimeIntervalSinceReferenceDate:0], @"siSyncDate", nil];

    for (int i=0; i<kMaxSyncDevices; i++) {
        syncDictionarys[i] = noteSyncInfoDict;
    }
    return [NSArray arrayWithObjects:syncDictionarys count:kMaxSyncDevices];
}


@end
