//
//  SyncViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 5/16/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
//  Sync Server view peforms synchronization of notepad with iDevice.
//
// Rewritten by Lewis E. Garrett beginning 1/9/2020                             //leg20200109 - Catalyst
//  to adapt for use by TopXNotes Catalyst.
//

#import "TopXNotesAppDelegate.h"
#import "SyncNotePad.h"
#import "NotePadViewController.h"
#import "SyncViewController.h"
#import "Model.h"
#import "Note.h"
#import "Constants.h"                                                           //leg20200219 - Catalyst
#import "SyncConstants.h"                                                       //leg20200219 - Catalyst
#import "Formatter.h"                                                           //leg20200207 - Catalyst
#import "TopXNotes2-Swift.h"

@interface SyncViewController () <UIPopoverPresentationControllerDelegate>      //leg20220311 - TopXNotes_Catalyst_2
@end

//CLASS IMPLEMENTATIONS:
@implementation SyncViewController

@synthesize model;
@synthesize notePadViewController;
@synthesize modalButton;                                                        //leg20220311 - TopXNotes_Catalyst_2


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // this will appear as the title in the navigation bar
        self.title = @"NoteSync Server";
    }
    return self;
}

- (IBAction)showEditDevicesTable:(id)sender {
        // Modified so that a Swift view controller can be loaded from .xib and //leg20200521 - experiment
        // presented by Objective-C code.  Used medium.com article as guide:
        // https://medium.com/ios-os-x-development/swift-and-objective-c-interoperability-2add8e6d6887#a8a9
        //
        
        // SyncDevicesTableViewController view
        SyncDevicesTableViewController *syncDevicesTableViewController = [[SyncDevicesTableViewController alloc] initWithNibName:@"SyncDevicesTableViewController" bundle:nil];
        [self.navigationController pushViewController:syncDevicesTableViewController animated:YES];

        
    }

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get view controller for Info button.
	modalViewController = [[SyncInfoViewController alloc] initWithNibName:@"SyncInfoViewController" bundle:nil];
	modalViewController.model = self.model;
	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	// Add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
    modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton]; //leg20220311 - TopXNotes_Catalyst_2
	self.navigationItem.rightBarButtonItem = modalButton;
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];
//    self.view.backgroundColor = UIColor.blackColor;
    // Insure Clear Sync is off.                                                //leg20200331 - Catalyst
    [clearSyncStatusSwitch setOn:NO];
    [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];

    [self initSyncTable];
}

// user clicked the "i" button, present the info view.
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)
    // Show the Info view in a popover.                                         //leg20220311 - TopXNotes_Catalyst_2
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"SyncInfoViewController"];

    // Present the controller.
    //  on iPad, this will be a Popover.
    //  on iPhone, this will be an action sheet.
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];

    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popController.barButtonItem = modalButton;
    popController.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)

    [super viewWillDisappear:animated];

}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    // Return YES for supported orientations
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

# pragma mark - Popover Presentation Controller Delegate

// Popover delegate methods for Font settings popover.                          //leg20220311 - TopXNotes_Catalyst_2

// TODO: Determine what to replace deprecated methods with.

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // called when a Popover is dismissed
    NSLog(@"Popover was dismissed with external tap. Have a nice day!");
}

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    
    // return YES if the Popover should be dismissed
    // return NO if the Popover should not be dismissed
    return YES;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView *__autoreleasing  _Nonnull *)view {
    
    // called when the Popover changes positon
}

// Initialize TopXNotes Sync Services NSNetServiceBrowser.                      //leg20200206 - Catalyst
-(void) initSyncTable {
    // Turn off Clear Sync Status Information flag
    clearSyncStatusInformation = NO;

    // Create the service browser
    browser = [[NSNetServiceBrowser alloc] init];
    services = [[NSMutableArray alloc] init];
    iPhoneNames = [[NSMutableArray alloc] init];
    udids = [[NSMutableArray alloc] init];
    appNames = [[NSMutableArray alloc] init];
    [browser setDelegate:self];
    
    // Stop using product code ID to identify potential TopXNnotes
    //  touch sync clients beginning with Apple Mac App Store
    //  version.  Product still has to be maintained until versions
    //  of TopXNotes Mac and TopXNotes touch are coordinated.
    //  Construct the service name from service name and TopXNnotes
    //  product key
    //
#define SERVICE_NAME    @"TopXNotes Sync"
#define PRODUCT_CODE    @"TXNT2-TEKAK-2B6SA-GC5SI"
    productCodeIdentifier = [NSString stringWithFormat:@"%@ %@", SERVICE_NAME, PRODUCT_CODE];

    currentSelectRow = -1;

    // Specify our net service type.
    #define  kSyncServiceIdentifier @"topx-syncsrv"
    #define VERSION_NOTE_SYNC           2
    NSString * servicesOfType = [NSString stringWithFormat:@"_%@%d._tcp.", kSyncServiceIdentifier, VERSION_NOTE_SYNC];
 
    // Using CocoaAsyncSocket TCP
    
    // Start browsing for bonjour services
    netServiceBrowser = [[NSNetServiceBrowser alloc] init];
    
    [netServiceBrowser setDelegate:self];
    [netServiceBrowser searchForServicesOfType:servicesOfType inDomain:@"local."];

    // Configure Sync button.
    [syncButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [syncButton setEnabled:NO];
    [syncButton setHidden:NO];
    
    // Remove setting controls' color to Tropical Green.                        //leg20211210 - TopXNotes_Catalyst_2
//    UIColor * tropicalGreenUIColor = [Constants controlsColor];

    // Add frame to button.                                                     //leg20200514 - Catalyst
    CALayer *viewLayer = [syncButton layer];
//    viewLayer.borderColor = [tropicalGreenUIColor CGColor];
    viewLayer.borderWidth = 1.0f;
    viewLayer.masksToBounds = YES;
    viewLayer.cornerRadius = 3.0f;
    
//    clearSyncStatusSwitch.onTintColor = tropicalGreenUIColor;
}

#pragma mark NSNetServiceBrowser delegates

// Reconfigure this member to be more efficient.
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
#pragma unused (aNetServiceBrowser, moreComing)
    DEBUG_LOG(@"Did find service, _cmd: %s", sel_getName(_cmd));
    
    // Show TopXNotes Sync Services.
    iPhoneName = (NSMutableString *)[[aNetService name] substringFromIndex:[productCodeIdentifier length]];
    NSString *fullName = (NSMutableString *)[[aNetService name] substringFromIndex:0];
    #pragma unused (fullName)

    if ([services indexOfObject:aNetService] != NSNotFound) {
    
        // Net Service already in arrays, so we must reorder its position.
        [iPhoneNames removeObjectAtIndex:[services indexOfObject:aNetService]];
        [udids removeObjectAtIndex:[services indexOfObject:aNetService]];
        [appNames removeObjectAtIndex:[services indexOfObject:aNetService]];
        [services removeObject:aNetService];
    }
    
    // Add the Net Service info to the arrays.
    [iPhoneNames addObject:iPhoneName];
    [services addObject:aNetService];
    [udids addObject:@""];      // Just a placeholder for now.
    [appNames addObject:@""];   // Just a placeholder for now.
    
    // [aNetService startMonitoring] will trigger -didUpdateTXTRecordData
    //  which will get the UDID of the iDevice with the TopXNotes Sync Service.
    aNetService.delegate = self;
    [aNetService startMonitoring];
    
    // No need to update display if more services are coming.
    if (!moreComing) {
        // Comment-out resolveWithTimeout here since that shouldn't be done
        //  until a Sync is undertaken. This may have been the source of the
        //  mDNSResponder DNSServiceResolve "active for over two minutes."
        //  errors. See our DTS incident 270310974, follow-up 5/30/13 DTS Email.
        //[aNetService resolveWithTimeout:5.0];
        [self.tableView reloadData];
    }
}

// Extract net service information.
- (void)netService:(NSNetService *)sender didUpdateTXTRecordData:(NSData *)data
{
#pragma unused (sender)
    
    NSDictionary *txtDict;
    NSData *udidData;
    NSString *udid;
    NSData *appNameData;
    NSString *appName;
    
    if (data !=nil) {
        txtDict = [NSNetService dictionaryFromTXTRecordData:data];
        
        udidData = [txtDict objectForKey:@"udid"];
        udid = [[NSString alloc]
                initWithData:udidData
                encoding:NSUTF8StringEncoding];
                
        appNameData = [txtDict objectForKey:@"apnm"];
        
        // If there is no appNameData the appName should be TopXNotesFree
        //  because the appName key was inadvertantly left out of the
        //  TXTRecordData for version 1.5.0 of TopXNotes Free, and will be
        //  corrected in a future release.
        if (appNameData)
            appName = [[NSString alloc]
                    initWithData:appNameData
                    encoding:NSUTF8StringEncoding];
        else
            appName = @"TopXNotesFree";
       
        if ([services indexOfObject:sender] != NSNotFound) {
            [udids replaceObjectAtIndex:[services indexOfObject:sender] withObject:udid];
            [appNames replaceObjectAtIndex:[services indexOfObject:sender] withObject:appName];
            [self.tableView reloadData];
        }
    }
    else
        return;
    
    [sender stopMonitoring];
    
    DEBUG_LOG(@"The TXTRecordData dictionary contents is: %@, _cmd: %s", txtDict, sel_getName(_cmd));
    DEBUG_LOG(@"The UDID is: %@, _cmd: %s", udid, sel_getName(_cmd));
    DEBUG_LOG(@"The AppName is: %@, _cmd: %s", appName, sel_getName(_cmd));
}

// Convert -netServiceDidResolveAddress.                                        //leg20200207 - Catalyst
- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
    // Using CocoaAsyncSocket TCP
    DEBUG_LOG(@"Did resolve service: %@", [sender addresses]);

    // Convert UDID refrences to NSStrings.                                     //leg20200207 - Catalyst
    // Check to see if UDID table is full and cancel Sync if it is.
    NSMutableString* deviceUDID = [udids objectAtIndex:[services indexOfObject:sender]];

    if ([self isNotePadSyncedWithUDID: deviceUDID] == -99) {

        DEBUG_LOG(@"No room in UDID table for UDID: %@, _cmd: %s", deviceUDID, sel_getName(_cmd));

        // Stop the service.
        [sender stop];

        // Stop and hide the progress wheel
        [progressIndicator stopAnimating];
        [progressIndicator setHidden:YES];
        [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];            //leg20200212 - Catalyst

        // Display alert "No Slots Available…".
        [self syncErrorAlert: NSLocalizedString(@"No Slots Available Message Title", @"") message: NSLocalizedString(@"No Slots Available Message", @"")];

        NSUInteger ndx = -1;
        ndx = [services indexOfObject:sender];
        if (ndx != NSNotFound) {
            [iPhoneNames removeObjectAtIndex:ndx];
            [udids removeObjectAtIndex:ndx];
            [services removeObject:sender];
        }

        serverService = nil;

        // Update display.
        [self.tableView reloadData];

        [self endSync:kSYNC_ERROR_005 isCancel:YES];

        return;
    }

    if (serverAddresses == nil)
    {
        serverAddresses = [[sender addresses] mutableCopy];
    }

    if (asyncSocket == nil)
    {
        asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        [self connectToNextAddress];
    }

    // Prepare to send my begin sync command with an added sub value to be used for
    //  determining the version of NoteSync and/or error conditions.
    totalBytesRead = 0;
    struct Sync_Record syncRecord;
    syncRecord.command_ID = CFSwapInt32HostToBig(kSTART_SYNCV1); // Use version 1 for 1.7.3.
    syncRecord.secondary.code_Value.code = CFSwapInt16HostToBig(kNOTE_SYNC_VER_CURRENT);
    NSData *syncData = [NSData dataWithBytes:&syncRecord length:sizeof(syncRecord)];

    // Write the begin sync command to iOS (signal to return iOS notepad to MacOS.
    DEBUG_LOG(@"writeData, write the begin sync command to iOS, tag=%d, _cmd: %s",
                SYNC_WRITE_BEGIN_SEND_TAG, sel_getName(_cmd));
    [asyncSocket writeData:syncData withTimeout:READ_TIMEOUT tag:SYNC_WRITE_BEGIN_SEND_TAG];

}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
{
#pragma unused (sender, errorDict)
    
    DEBUG_LOG(@"Did not resolve service, _cmd: %s", sel_getName(_cmd));
    
//    #define    kSYNC_ERROR_003         0x00000003      // Mac did not resolve service
//    [self endSync:kSYNC_ERROR_003 isCancel:YES];

}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
#pragma unused (aNetServiceBrowser, moreComing)
    DEBUG_LOG(@"Did remove service, _cmd: %s", sel_getName(_cmd));
    
    NSUInteger ndx = -1;
    ndx = [services indexOfObject:aNetService];
    if (ndx != NSNotFound) {
        [iPhoneNames removeObjectAtIndex:ndx];
        [udids removeObjectAtIndex:ndx];
        [appNames removeObjectAtIndex:ndx];
        [services removeObject:aNetService];
    }
    
    // No need to update display if more services are coming.
    if (!moreComing) {
        [self.tableView reloadData];
    }
}
// Add Sync Services table handling to SyncViewController.                      //leg20200205 - Catalyst
#pragma mark - UITableViewDelegate - Table Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

//    return 5;
    return [services count];
}

// Customize the appearance of table view cells.                                //leg20200205 - Catalyst
// Refactor MainWindowController.m's NSTableView -objectValueForTableColumn()   //leg20200207 - Catalyst
//  method to make UITableView -cellForRowAtIndexPath() method.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    static NSString *CellIdentifier = @"Sync_Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    NSString *syncClientName;
    
    // Stop using product code ID to identify potential TopXNnotes
    //  touch sync clients beginning with Apple Mac App Store
    //  version.  Product still has to be maintained until versions
    //  of TopXNotes Mac and TopXNotes touch are coordinated.  No
    //  longer display product code in Sync Services table view.
    //
    // Look for a product code embedded in the services name (pre TopXNotes
    //  touch 1.0.3.)  If so then get the iPhone name from immediately after.
    //
    int serviceNameLength = [[[services objectAtIndex:indexPath.row] name] length];
    NSString *dash1 = [[[[services objectAtIndex:indexPath.row] name] substringFromIndex:20]substringToIndex:1];
    NSString *dash2 = [[[[services objectAtIndex:indexPath.row] name] substringFromIndex:26]substringToIndex:1];
    NSString *dash3 = [[[[services objectAtIndex:indexPath.row] name] substringFromIndex:32]substringToIndex:1];
                                                                                                                
    if (serviceNameLength >= 38 &&
        [dash1 isEqualToString:@"-"] &&
        [dash2 isEqualToString:@"-"] &&
        [dash3 isEqualToString:@"-"]) {
            
            NSString            *timestampStr;
            NSString            *deviceUDIDStr;

            NSMutableString* deviceDesc = [[NSMutableString alloc] initWithString:
                [[[services objectAtIndex:indexPath.row] name] substringFromIndex:
                [productCodeIdentifier length]]];
            if ([deviceDesc length]) {

                NSMutableString* deviceUDID = [udids objectAtIndex:indexPath.row];

                if ([deviceUDID length]) {
                    deviceUDIDStr = deviceUDID;
                    timestampStr = [self getSyncDeviceLastTimestamp: deviceUDIDStr];    // Get formatted date string.
                    if (timestampStr != nil) {
                        NSString* timestamp = timestampStr;
                        [deviceDesc appendString:@": last synced on "];
                        [deviceDesc appendString:timestamp];
                    }
                }
            }
        
            syncClientName = [NSString stringWithFormat:@"%@ Sync Service - %@", [appNames objectAtIndex:indexPath.row], deviceDesc];
    } else {
        // There wasn't a product code embedded in the services name so
        // TopXNotes touch must be version 1.0.3 or greater.  Get the
        // iPhone name from just after SERVICE_NAME.
        //
        syncClientName = [NSString stringWithFormat:@"%@ - %@", SERVICE_NAME,
                [[[services objectAtIndex:indexPath.row] name] substringFromIndex:15]];
    }

    cell.textLabel.text = syncClientName;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

// Refactor MainWindowController.m's NSTableView -tableViewSelectionDidChange() //leg20200207 - Catalyst
//  method to make UITableView -didSelectRowAtIndexPath() method.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    NSInteger selectRow = indexPath.row;

    // Reconfigure display based on selection and deselection of Service table. //leg20200519 - Catalyst
    if (currentSelectRow == selectRow) {
         // it was already selected
         currentSelectRow = -1;
         selectRow = -1;
         [tableView deselectRowAtIndexPath:indexPath animated:YES];
         [numSyncedIPhoneToMacLabel setText: @""];
         [numSyncedMacToIPhoneLabel setText: @""];
     } else {
         // wasn't yet selected, so let's remember it
         currentSelectRow = selectRow;
         [numSyncedIPhoneToMacLabel setText:( selectRow >= 0 ?
                                                 [NSString stringWithFormat: NSLocalizedString(@"Ready To Sync Message", @""),
                                                 [iPhoneNames objectAtIndex: selectRow]] : @"")];
         [numSyncedMacToIPhoneLabel setText: @""];
     }

    // Enable/Disable Sync button depending on whether or not a service is selected
    if (selectRow >= 0)
        [syncButton setEnabled:YES];
    else
        [syncButton setEnabled:NO];

    if (selectRow != currentSelectRow) {
        currentSelectRow = selectRow;
    }
}

#pragma mark - Helper Methods

// Show sync error alert messages.                                              //leg20200212 - Catalyst
- (void)syncErrorAlert: (NSString *) title message:(NSString *)alertMessage {
    
    // Convert deprecated UIAlertView to UIAlertController.                     //leg20200518 - Catalyst
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                   message:alertMessage
                                   preferredStyle:UIAlertControllerStyleAlert];
     
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"") style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        #pragma unused (action)
    }];
     
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

// ---------------------------------------------------------------------------
//        isNotePadSyncedWithUDID
// ---------------------------------------------------------------------------
// Convert IsNotePadSyncedWithUDID() to ObjC.                                   //leg20200207 - Catalyst
//
- (SInt16)isNotePadSyncedWithUDID:(NSString *)inUDID
{
    SInt16 result = -99;            // 0 to 9 exisitng slot in use
                                    // -10 to -1 not found; empty slot to use
                                    // -99 not found, no empty slot
    
    NSArray *deviceSyncInfoTable = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];
    for (int i=0; i<kMaxSyncDevices; i++) {

        if ([[[deviceSyncInfoTable objectAtIndex:i] objectForKey: @"siDeviceUDID"] isEqualToString: inUDID]) {   //leg20200317 - Catalyst
            result = i;
            return result;
        }

        if (result == -99 && [[[deviceSyncInfoTable objectAtIndex:i] objectForKey: @"siDeviceUDID"] length] == 0) {
            result = -(i + 1);
        }
        
    }
    
    return result;
}

// ---------------------------------------------------------------------------
//        getSyncDeviceLastTimestamp
// ---------------------------------------------------------------------------
// Convert GetSyncDeviceLastTimestamp() to ObjC.                                //leg20200207 - Catalyst
//
- (NSString *)getSyncDeviceLastTimestamp:(NSString *)inDeviceUDID
{
    SInt16 udidSlot = [self isNotePadSyncedWithUDID: inDeviceUDID];

    NSArray *deviceSyncInfoTable = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];

    if (udidSlot >= 0) {
        return [NSString longDate: [[deviceSyncInfoTable objectAtIndex:udidSlot] objectForKey: @"siSyncDate"]];
    }

    return nil;
}

// ---------------------------------------------------------------------------
//        getSyncDeviceLastTimestamp
// ---------------------------------------------------------------------------
// Convert TimestampString() to ObjC.                                           //leg20200207 - Catalyst
//
- (NSString *)timestampString:(NSDate *)timeStamp
{
    NSString* dateString = [NSString longDate:timeStamp];

    return dateString;
}

// Convert InitializeSyncData() to UIKit.                                       //leg20200211 - Catalyst
//
// Removes any previous mobile model file.
//
- (void) InitializeSyncData {
    pathToMobileNotepad = [Model pathToData: kModelFileNameMobile];             //leg20200309 - Catalyst

    // Erase any previous notepad model file, ignore any errors
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToMobileNotepad]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:pathToMobileNotepad error:&error];
    }
}


// ---------------------------------------------------------------------------
//        setSyncDataAllChangedNotes
// ---------------------------------------------------------------------------
// Convert setSyncDataAllChangedNotes() to ObjC.                                //leg20200212 - Catalyst
// Finish refactoring to ObjC. Beware Mutability of NSDictionarys, NSArrays.    //leg20200217 - Catalyst
//
// Set Sync Data for Mac notes.

- (UInt32) setSyncDataAllChangedNotes: (NSString *) inUDID timeStamp:(NSDate *)inTimeStamp {
    UInt32		numNotesFound = 0;
	UInt32		noteCount = self.model.numberOfNotes;

    Note *note;

	for (UInt32 i = 0; i < noteCount; ++i) {
        note = [model getNoteForIndex:i];
// Fix referencing nfChangedSync value incorrectly.                             //leg20200602 - Catalyst
//        if (note.nfChangedSync) {
		if ([note.nfChangedSync boolValue]) {
			int		freeSlot = -1, udidSlot = 0;
			for (; udidSlot<kMaxSyncDevices; udidSlot++) {
                if ([[[note.npSync objectAtIndex:udidSlot] objectForKey:@"nsDeviceUDID"] length] == 0) {
					if (freeSlot == -1) {
						freeSlot = udidSlot;
					}
				}
				else {
                    if ([[[note.npSync objectAtIndex:udidSlot] objectForKey:@"nsDeviceUDID"] compare:inUDID] == NSOrderedSame) {
						break;
					}
				}
			}
			if (udidSlot == kMaxSyncDevices && freeSlot != -1) {
				// Entry not found, use empty slot
				udidSlot = freeSlot;
			}
			
			if (udidSlot < kMaxSyncDevices) {
                // Insure that npSync and its elements are mutable so that we   //leg20200326 - Catalyst
                //  can update just the udidSlot.
                NSMutableDictionary *npSyncDictionary = [NSMutableDictionary dictionaryWithDictionary: note.npSync[udidSlot]];
                [npSyncDictionary setObject:inUDID forKey:@"nsDeviceUDID"];
                [npSyncDictionary setObject:inTimeStamp forKey:@"siSyncDate"];
                NSMutableArray *npSyncArray = [NSMutableArray arrayWithArray:note.npSync];
                [npSyncArray replaceObjectAtIndex:udidSlot withObject: npSyncDictionary];
                note.npSync = npSyncArray;
                note.nfChangedSync = [NSNumber numberWithBool:NO];
				note.needsSyncFlag = [NSNumber numberWithBool:NO];              //leg20200317 - Catalyst
                [model replaceNoteAtIndex:i withNote:note];                     //leg20200326 - Catalyst
				numNotesFound++;
			}
		}
	}
    
	return numNotesFound;
}

// ---------------------------------------------------------------------------------
//        • setSyncDataForNotePadWithUDID
// ---------------------------------------------------------------------------------
// Convert SetSyncDataForNotePadWithUDID() to ObjC.                             //leg20200217 - Catalyst
//
//  DeviceSyncInfo is now stored in an NSArray of NSDictionarys.
//
- (UInt32) setSyncDataForNotePadWithUDID: (NSMutableDictionary *) ioSyncInfo {  //leg20200325 - Catalyst
    
    SInt16 udidSlot = [self isNotePadSyncedWithUDID: [ioSyncInfo objectForKey:@"siDeviceUDID"]];

    if (udidSlot == -99) {
        [self syncErrorAlert: NSLocalizedString(@"No Slots Available Message Title", @"") message: NSLocalizedString(@"No Slots Available Message", @"")];
        return -99;
    }

    if (udidSlot < 0) {
        udidSlot = (-udidSlot) - 1;
    }
        
    // Update values for slot.                                                  //leg20200325 - Catalyst
    if (udidSlot >= 0) {
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//        [ioSyncInfo setObject: [model getNextNoteID] forKey: @"siNextNoteID"];
        [ioSyncInfo setObject: [model notePadVersion] forKey: @"siLastFileVersion"];

        // Get saved deviceSyncTable and make it muteable so that we            //leg20200318 - Catalyst
        //  can update the slot.
        NSArray *deviceSyncInfoTableFromDefaults = [[NSUserDefaults standardUserDefaults] objectForKey: @"DeviceSyncInfoTable"];
        NSMutableArray *deviceSyncInfoTable = [NSMutableArray array];
        [deviceSyncInfoTable addObjectsFromArray:deviceSyncInfoTableFromDefaults];

        // Update the device sync info slot.
        [deviceSyncInfoTable replaceObjectAtIndex:udidSlot withObject:ioSyncInfo];  //leg20200325 - Catalyst
        [[NSUserDefaults standardUserDefaults] setObject:deviceSyncInfoTable forKey:@"DeviceSyncInfoTable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return udidSlot;
    
}

#pragma mark - Action Methods

// Convert syncAction() to UIKit.                                               //leg20200211 - Catalyst

// Perform a sync operation with the TopXNotes/iPhone previously selected
-(IBAction) syncAction:(id) sender {
#pragma unused (sender)
    
    // Using CocoaAsyncSocket TCP
    DEBUG_LOG(@"\r\r\r\r\r\rUsing CocoaAsyncSocket TCP: %s", sel_getName(_cmd));
        
    // Prepare TopXNotes note data for sync and load sync notes table view
    [self InitializeSyncData];

    // Get the NetService
    NSIndexPath *tableSelection = [self.tableView indexPathForSelectedRow];
    service = [services objectAtIndex: tableSelection.row];
    iPhoneName = [iPhoneNames objectAtIndex: tableSelection.row];
    
    [numSyncedMacToIPhoneLabel setText: @""];
    [numSyncedIPhoneToMacLabel setText: @""];

    [progressIndicator setHidden:NO];
    [progressIndicator startAnimating];

    if (serverService == nil)
    {
        serverService = service;
        
        [serverService setDelegate:self];
        [serverService resolveWithTimeout:5.0];
    }
}

// Refactor to UIKit.                                                           //leg20200217 - Catalyst
-(IBAction) clearSyncStatusCheckboxAction:(id) sender {
#pragma unused (sender)

    if (clearSyncStatusSwitch.isOn) {
        clearSyncStatusInformation = YES;
        [syncButton setTitle:NSLocalizedString(@"Clear Sync Info", @"") forState:UIControlStateNormal];
        
        // Issue warning about Clear Sync.                                      //leg20200331 - Catalyst
        [self syncErrorAlert: NSLocalizedString(@"Clear Sync Warning Title", @"")
                     message: NSLocalizedString(@"Clear Sync Warning Message", @"")];
    } else {
        clearSyncStatusInformation = NO;
        [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark SYNC Networking TCP - CocoaAsyncSocket
#pragma mark -

#pragma mark CocoaAsyncSocket connection

// CocoaAsyncSocket connection code.                                            //leg20200212 - Catalyst
- (void)connectToNextAddress
{
    BOOL done = NO;
    
    while (!done && ([serverAddresses count] > 0))
    {
        NSData *addr;
        
        // Note: The serverAddresses array probably contains both IPv4
        //  and IPv6 addresses. If your server is also using
        //  GCDAsyncSocket then you don't have to worry about it since
        //  the socket automatically handles both protocols for you
        //  transparently.
        //
        if (YES) // Iterate forwards
        {
            addr = [serverAddresses objectAtIndex:0];
            [serverAddresses removeObjectAtIndex:0];
        }
        else // Iterate backwards
        {
            addr = [serverAddresses lastObject];
            [serverAddresses removeLastObject];
        }
        
        DEBUG_LOG(@"Attempting connection to %@", addr);
        
        NSError *err = nil;
        if ([asyncSocket connectToAddress:addr error:&err])
        {
            done = YES;
        }
        else
        {
            DEBUG_LOG(@"Unable to connect: %@", err);
        }
        
    }
    
    if (!done)
    {
        DEBUG_LOG(@"Unable to connect to any resolved address");
    }
}

#pragma mark CocoaAsyncSocket delegates

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
#pragma unused (sock)
    DEBUG_LOG(@"Socket:DidConnectToHost: %@ Port: %hu", host, port);
    
    connected = YES;
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)socket withError:(NSError *)error
{
    NSLog(@"Socket Did Disconnect with Error %@ with User Info %@.", error, [error userInfo]);

    [socket setDelegate:nil];
    //    [self setSocket:nil];
    asyncSocket = nil;
    serverService = nil;
    serverAddresses = nil;
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    
    DEBUG_LOG(@"Did Write Data, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
    
    // If write of "begin send" record to iOS.
    if (tag == SYNC_WRITE_BEGIN_SEND_TAG) {

        // Read message from iOS concerning start of sync.
        [sock readDataToLength:SYNC_INFO_RECORD_LENGTH withTimeout:READ_TIMEOUT tag:SYNC_READ_BEGIN_INFO_TAG];
        
        DEBUG_LOG(@"readDataToLength - read begin sync info record, tag=%d, _cmd: %s",
                    SYNC_READ_BEGIN_INFO_TAG, sel_getName(_cmd));
        
    } else if (tag == SYNC_WRITE_FILE_INFO_TAG) {
        DEBUG_LOG(@"Did write sync file info from MacOS, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));

        // If there is at least 1 note, send the combined notepad model back to the iPhone
        if (self->HaveNotesToSend) {
            lengthOfFileToSend = [notepadFileToSend length];
            DEBUG_LOG(@"NotePad to send back to iPhone is %d bytes, _cmd: %s",
                      lengthOfFileToSend, sel_getName(_cmd));
            
            DEBUG_LOG(@"writeData the entire file from MacOS, length=%d, tag=%d, _cmd: %s",
                      lengthOfFileToSend, SYNC_WRITE_FILE_ENTIRE_TAG, sel_getName(_cmd));
            [asyncSocket writeData:notepadFileToSend withTimeout:READ_TIMEOUT tag:SYNC_WRITE_FILE_ENTIRE_TAG];
            
            NSLog(@"Sync Complete! _cmd: %s", sel_getName(_cmd));
        }
    } else if (tag == SYNC_WRITE_FILE_BUFFER_TAG) {
        DEBUG_LOG(@"Did write sync file buffer from MacOS, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
    } else if (tag == SYNC_WRITE_FILE_ENTIRE_TAG) {
        DEBUG_LOG(@"Did write entire file from MacOS, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
        
        // Save the same last synchronized date for Mac
        if (iPhoneName) {
            NSString   *deviceNameStr;

            if ([iPhoneName length] != 0) {
                deviceNameStr = iPhoneName;
            } else {
                deviceNameStr = @"Unknown Device";
            }
            
            // Save device name.
            if ([[deviceSyncInfo objectForKey: @"siDeviceName"] length] == 3 && [[deviceSyncInfo objectForKey: @"siDeviceName"] compare:@"???"]  == NSOrderedSame) {
                [deviceSyncInfo setObject:deviceNameStr forKey: @"siDeviceName"];
            }
        }

        [self setSyncDataAllChangedNotes: [deviceSyncInfo objectForKey: @"siDeviceUDID"] timeStamp: [deviceSyncInfo objectForKey: @"siSyncDate"]];

        // Update Device Sync Info Table slot if not clearing sync info.        //leg20200331 - Catalyst
        if (!clearSyncStatusInformation) {
            [self setSyncDataForNotePadWithUDID: deviceSyncInfo];
        } else {
            // Insure clear sync info switch is turned off.                     //leg20200331 - Catalyst
            [clearSyncStatusSwitch setOn:NO];
        }

        // Make sure Mac notepad gets saved.
        [model saveData];

        
        // Display sync results and end.
        [self endSync:syncErrorCode isCancel:canceledSync];

        // It is now safe to disconnect.
        [sock disconnectAfterReadingAndWriting];
    } else {
        DEBUG_LOG(@"Unknown tag, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
    }
}

// Refactor to UIKit.                                                           //leg20200217 - Catalyst
- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag
                                                elapsed:(NSTimeInterval)elapsed
                                                bytesDone:(NSUInteger)length
{
#pragma unused (sock, length)
    DEBUG_LOG(@"Write timeout, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
    
    if (elapsed <= READ_TIMEOUT)
    {
        DEBUG_LOG(@"Write timeout extended, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
        return READ_TIMEOUT_EXTENSION;
    } else {

        [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:@"Sending information to iPhone or iPad was interrupted while Syncing! Restart TopXNotes Mobile before retrying!"]]; //leg20220817 - TopXNotes_Catalyst_2
        DEBUG_LOG(@"numSyncedMacToIPhoneLabel=%@, _cmd: %s",
                  numSyncedMacToIPhoneLabel.text, sel_getName(_cmd));
        [numSyncedIPhoneToMacLabel setText: [NSString stringWithFormat:@"Sync operation cancelled!"]];
        DEBUG_LOG(@"numSyncedIPhoneToMacLabel=%@, _cmd: %s",
                    numSyncedIPhoneToMacLabel.text, sel_getName(_cmd));

        // Stop and hide the progress wheel
        [progressIndicator stopAnimating];
        [progressIndicator setHidden:YES];
        [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];

        // Erase the temporary notepad model file, ignore any errors
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathToMobileNotepad]) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:pathToMobileNotepad error:&error];
        }

        // Disconnect the socket.
        [asyncSocket disconnect];

        return 0.0;
    }
}

// Refactor to UIKit.                                                           //leg20200217 - Catalyst
- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
                                                elapsed:(NSTimeInterval)elapsed
                                                bytesDone:(NSUInteger)length
{
#pragma unused (sock)
    DEBUG_LOG(@"Read timeout,bytesDone=%lu tag=%ld, cmd=%s", (unsigned long)length, tag, sel_getName(_cmd));
    
    if (elapsed <= READ_TIMEOUT)
    {
        DEBUG_LOG(@"Write timeout extended, tag=%ld, cmd=%s", tag, sel_getName(_cmd));
        return READ_TIMEOUT_EXTENSION;
    } else {

        [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:@"Reading information from iPhone or iPad was interrupted while Syncing! Restart TopXNotes Mobile before retrying!"]];   //leg20220817 - TopXNotes_Catalyst_2
        DEBUG_LOG(@"numSyncedMacToIPhoneLabel=%@, _cmd: %s",
                    numSyncedMacToIPhoneLabel.text, sel_getName(_cmd));
        [numSyncedIPhoneToMacLabel setText: [NSString stringWithFormat:@"Sync operation cancelled!"]];
        DEBUG_LOG(@"numSyncedIPhoneToMacLabel=%@, _cmd: %s",
                    numSyncedIPhoneToMacLabel.text, sel_getName(_cmd));

        // Stop and hide the progress wheel.
        [progressIndicator stopAnimating];
        [progressIndicator setHidden:YES];
        [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];

        // Erase the temporary notepad model file, ignore any errors
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathToMobileNotepad]) {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath:pathToMobileNotepad error:&error];
        }

        // Disconnect the socket.
        [asyncSocket disconnect];

        return 0.0;
    }
    
    return 0.0;
}

- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag
{
#pragma unused (sock)
    DEBUG_LOG(@"Did read partial data, read %lu bytes from iOS, tag=%lu, _cmd: %s",
                (unsigned long)partialLength, tag, sel_getName(_cmd));
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
#pragma unused (sock)
    // This method is executed on the socketQueue (not the main thread)
    DEBUG_LOG(@"\n\nDid read data, read %lu bytes from iOS, tag=%lu, _cmd: %s",
                (unsigned long)[data length], tag, sel_getName(_cmd));
    
    dispatch_async(dispatch_get_main_queue(), ^{
        @autoreleasepool {

            self->totalBytesRead += [data length];
            DEBUG_LOG(@"Total # of bytes of file read so far=%d,_cmd: %s",
                      self->totalBytesRead, sel_getName(_cmd));

            // Handle the readData
            switch (tag) {

                case SYNC_READ_BEGIN_INFO_TAG:
                {
                    union BufferRedefined buffer;

                    DEBUG_LOG(@"Did read SYNC_READ_BEGIN_INFO_TAG, read %lu bytes from Mac, tag=%lu, _cmd: %s",
                              (unsigned long)[data length], tag, sel_getName(_cmd));

                    // Get data read from the iOS we're syncing with and transfer into a byte buffer
                    [data getBytes:&buffer length:[data length]];

                    if (CFSwapInt16BigToHost(buffer.syncRecord.secondary.code_Value.code) == kSYNC_NOERROR) {
                        // Received signal from iOS to proceed with reading notepad from iOS.

                        // Prepare file read buffer
                        if (!self->receivedData)
                            self->receivedData = [[NSMutableData alloc] initWithCapacity:4096];
                        self->totalBytesRead = 0;

                        // Read the notepad from iOS until EOF is reached.
                        [sock readDataToData:[Constants FileDataEOF] withTimeout:READ_TIMEOUT tag:SYNC_READ_FILE_EOF_TAG];
                    } else {
                        // Sync can not proceed because there is a NoteSync version
                        //  mismatch between iOS and Mac.

                        // Display sync results and end.
                        [self endSync:CFSwapInt16BigToHost(buffer.syncRecord.secondary.code_Value.code) isCancel:YES];
                    }
                }
                    break;

                case SYNC_READ_FILE_EOF_TAG:
                {
                    // We read a notepad file from iOS until end-of-file.
                    NSRange range;
                    range.location = 0;
                    range.length = [data length] - [[Constants FileDataEOF] length];

                    // Strip EOF marker from data, leaving just the notepad file.
                    [self->receivedData appendData:[data subdataWithRange:range]];

                    // We read the entire notepad file from iOS.
                    DEBUG_LOG(@"We read the entire notepad file from iOS, tag=%ld, length=%lu, _cmd: %s",
                              tag, (unsigned long)[self->receivedData length], sel_getName(_cmd));

                    // Perform the synchronization process.
                    [self processSyncData:self->receivedData];

                    self->receivedData = nil;
                }
                    break;

                default:
                    DEBUG_LOG(@"Unknown tag, tag=%ld, _cmd: %s", tag, sel_getName(_cmd));
                    break;
            }
        } // end of: @autoreleasepool
    } // end of: dispatch_async(dispatch_get_main_queue(), ^ Block
    ); // end of: dispatch_async(dispatch_get_main_queue()
}

// Refactor to UIKit.                                                           //leg20200217 - Catalyst
// Implement refactored TxnMergeNotepad.                                        //leg20200219 - Catalyst
//
// Receive notepad from iOS, synchronize with MacOS notepad, return notepad
//  to iOS.  Using AsyncSocket.  data is the notepad coming from iOS.
- (void) processSyncData:(NSData*)data
{
    canceledSync = NO;
    Boolean         misMatchedSync = NO;
    syncErrorCode = 0;
    union BufferRedefined readBuffer;
    HaveNotesToSend = NO;
    numMacNotes = 0;
    numMobileNotes = 0;

    DEBUG_LOG(@"Begin processing notepad file from iOS. _cmd: %s", sel_getName(_cmd));
    
    // Clear any previous display of sync results
    [numSyncedMacToIPhoneLabel setText: @""];
    [numSyncedIPhoneToMacLabel setText: @""];

    // Log the path to notepad file for debugging purposes
    const char *adrsofstring = [pathToMobileNotepad cStringUsingEncoding:NSASCIIStringEncoding];
    DEBUG_LOG(@"Path to notepad file: \"%s\".", adrsofstring);

    // Get the data coming from the iPhone into a buffer if is a possible
    //  sync info record with an error code.
    if ([data length] <= sizeof(struct Sync_Record)) {

        // Get data read from the iOS device we're syncing with and transfer into a byte buffer
        [data getBytes:&readBuffer length:[data length]];

        // If we didn't get a file back from the iPhone, then it must be some kind of error.
        if (kSYNC_INFO == CFSwapInt32BigToHost(readBuffer.syncRecord.command_ID))
            syncErrorCode = CFSwapInt16BigToHost(readBuffer.syncRecord.secondary.code_Value.code);
        canceledSync = YES;
    } else {
        // The received notepad is contained in -processSyncData parameter:  data.
        DEBUG_LOG(@"Received notepad from TopXNotes_iPhone of %lu bytes, _cmd: %s",
                  (unsigned long)[data length], sel_getName(_cmd));
    }

    // Proceed with processing notepad if no error code returned from iOS device.
    if (!canceledSync) {
        // Write the incoming notepad file model to a file.
        if ([data writeToFile:pathToMobileNotepad atomically:YES]) {
            DEBUG_LOG(@"Model file created successfully with %lu bytes, _cmd: %s", (unsigned long)[data length], sel_getName(_cmd));
            NSLog(@"Sync Exchange of notepad data completed successfully.");
        }

        // Initialize sync processor.                                           //leg20200311 - Catalyst
        mergeNotepad = [[MergeNotepad alloc] init: iPhoneName
                                         macModel: model];
        numMacNotes = [mergeNotepad numMacNotes];
        numMobileNotes = [mergeNotepad numMobileNotes];                                    

        // Check to see if iPhone/iPad notepad version has sync date within model.
//        if ([[[mergeNotepad modelMobile] notePadVersion] intValue] < kNotePadVersion2) {    //leg20200309 - Catalyst
        
        // The variable "notePadVersion" is always the current version since    //leg20220817 - TopXNotes_Catalyst_2
        //  it is set once the notepad is loaded.  A new variable added to
        //  model, "notePadVersionAtOpen" is now used for checking version
        //  level of model data.
        int iOSnotePadVersion = [[[mergeNotepad modelMobile] notePadVersionAtOpen] intValue];
        
        if (iOSnotePadVersion < kNotePadVersion2) {
            // Notepad does not have a sync date so we can't sync with it.
            [self syncErrorAlert: NSLocalizedString(@"TopXNotes Requires Update Title", @"") message: NSLocalizedString(@"TopXNotes Requires Update Message", @"")];
            canceledSync = YES;
            clearSyncStatusInformation = NO;
            [self endSync:999 isCancel:YES];
            return;
        } else if (iOSnotePadVersion >= kNotePadVersion7) {

            // TODO: Notepad has Group information so we can't sync with it yet.
            //  TopxNotes Select Sync routines will need to be re-designed
            //  in order to accommodate Groups
            canceledSync = YES;
            clearSyncStatusInformation = NO;
            [self endSync:kSYNC_ERROR_006 isCancel:YES];
            return;
        } else {
            if (!clearSyncStatusInformation) {
                if (![mergeNotepad doesSyncInfoMatch]) {

                    // Synchronize info don't match.  Warn the user.
                    if (NO) { //Disable above "mis-matched sync message."
                        canceledSync = YES;     // Cancel was pressed, so cancel sync.
                        misMatchedSync = NO;
                        clearSyncStatusInformation = NO;
                    } else {
                        // The Mac and iDevice notepads were not previously in sync with one another,
                        //  so we need to clear sync status info before proceeding with sync.
                        misMatchedSync = YES;
                        [mergeNotepad clearAllSyncInfo];
                        NSLog(@"Device mismatch -> Sync Information CLEARED.");

                        // Insure clear sync status flag is off so that normal syncing can proceed.
                        clearSyncStatusInformation = NO;
                        clearSyncStatusSwitch.on = NO;
                    }
                }
            }
        } // else
    } //if ((!cancelSync

    // If the clear sync status information flag is on we will not actually sync notes.  Instead,
    //    we will clear the sync information from all Mac and iPhone sync notes
    if (clearSyncStatusInformation) {
        [mergeNotepad clearAllSyncInfo];
    }
    else {
        if (!canceledSync) {
            [mergeNotepad mergeMobile: misMatchedSync
                           noteCounts: &syncNoteCounts];
        }
    }

#pragma mark SYNC SPECIFICATION - 5. Build notepad for return to iDevice.

//    NSDictionary *writeMobileFileInfoDict = [mergeNotepad writeMobileFile];     //leg20200316 - Catalyst
    NSDictionary *writeMobileFileInfoDict = [mergeNotepad writeMobileFile: clearSyncStatusInformation];     //leg20200528 - Catalyst
    
    // Get return values from dictionary.                                       //leg20200316 - Catalyst
    HaveNotesToSend = [[writeMobileFileInfoDict objectForKey:@"writeMobileFile_HaveNotesToSend"] boolValue];
    deviceSyncInfo = [writeMobileFileInfoDict objectForKey:@"writeMobileFile_deviceSyncInfo"];

#pragma mark SYNC SPECIFICATION - 6. Return notepad to iDevice.

    // Load our notepad model into an NSData object for sending to the iPhone
    lengthOfFileToSend = 0;
    notepadFileToSend = [NSData dataWithContentsOfFile:pathToMobileNotepad];

    union    BufferRedefined buffer;
    buffer.syncRecord.command_ID    = CFSwapInt32HostToBig(kSYNC_INFO);
//    UInt32 notePadLength;
    // Return the size of the model; if no notes in model, say it's zero.
    if (HaveNotesToSend) {
//        notePadLength = [notepadFileToSend length];
        buffer.syncRecord.secondary.notepad_Size = CFSwapInt32HostToBig([notepadFileToSend length]);

    } else
        buffer.syncRecord.secondary.notepad_Size = 0;

    NSData *syncData = [NSData dataWithBytes:&buffer length:sizeof(buffer.syncRecord)];

    // Write the sync info record, telling iOS how big a file to expect.
    //  Completion processing continues in -didWriteDataWithTag.
    DEBUG_LOG(@"writeData sync file info from MacOS, tag=%d, _cmd: %s",
                SYNC_WRITE_FILE_INFO_TAG, sel_getName(_cmd));
    [asyncSocket writeData:syncData withTimeout:READ_TIMEOUT tag:SYNC_WRITE_FILE_INFO_TAG];
}

// Refactor to UIKit.                                                           //leg20200217 - Catalyst
// Display Sync results and end connection.
- (void) endSync:(NSInteger)withError isCancel:(BOOL)isCancel
{
#pragma mark SYNC SPECIFICATION - 7. Display Sync results.
    // Display sync results

    // Stop and hide the progress wheel
    [progressIndicator stopAnimating];
    [progressIndicator setHidden:YES];
    [syncButton setTitle:NSLocalizedString(@"Sync", @"") forState:UIControlStateNormal];
    [syncButton setEnabled:NO];
    currentSelectRow = -1;

    // Get the Mac's name from UIDevice.                                        //leg20200318 - Catalyst
    UIDevice *device = [UIDevice currentDevice];
    NSString *machineName = [device name];
    if(!machineName)
        machineName = @"Mac Computer";

    [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"Read Info Interrupted Message", @"")]];
    DEBUG_LOG(@"numSyncedMacToIPhoneLabel=%@, _cmd: %s",
                numSyncedMacToIPhoneLabel.text, sel_getName(_cmd));
    [numSyncedIPhoneToMacLabel setText: [NSString stringWithFormat:NSLocalizedString(@"Sync Op Cancelled Message", @"")]];
    DEBUG_LOG(@"numSyncedIPhoneToMacLabel=%@, _cmd: %s",
                numSyncedIPhoneToMacLabel.text, sel_getName(_cmd));
    if (isCancel) { // Sync error
        if (kSYNC_ERROR_001 == withError) {
            // Sync cancelled because TopXNotes iPhone must first be upgraded to a newer version!
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_001 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else if (kSYNC_ERROR_002 == withError) {
            // Sync cancelled because TopXNotes Mac must first be upgraded to a newer version!
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_002 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else if (kSYNC_ERROR_003 == withError) {
            // Sync cancelled because TopXNotes NoteSync service was not found!
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_003 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else if (kSYNC_ERROR_004 == withError) {
            // Clear Sync Status Information cancelled at user request - before any information was cleared!
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_004 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else if (kSYNC_ERROR_005 == withError) {
            // Sync cancelled because the maximum number of synced iPhones/iPads (10) has been reached!
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_005 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else if (kSYNC_ERROR_006 == withError) {
            // TODO: Notepad has Group information so we can't sync with it yet.//leg20220817 - TopXNotes_Catalyst_2
            // Sync cancelled because TopXNotes Select can't sync with notepads containing Groups information.
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_006 Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        } else {
            // Sync cancelled!  NoteSync version may be incompatible. Make sure  TopXNotes Mac and iPhone are the most current versions.
            [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"kSYNC_ERROR_default Message", @"")]];
            DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        }
    } else if (clearSyncStatusInformation) { // Clear Sync
        clearSyncStatusInformation = NO;
        // %d note(s) Sync status information cleared from %@.
        [numSyncedMacToIPhoneLabel setText: [NSString stringWithFormat:NSLocalizedString(@"Cleared Results Message", @""), numMobileNotes, iPhoneName ]];
        DEBUG_LOG(@"%@", numSyncedMacToIPhoneLabel.text);
        // %d note(s) Sync status information cleared from %@.
        [numSyncedIPhoneToMacLabel setText: [NSString stringWithFormat:NSLocalizedString(@"Cleared Results Message", @""), numMacNotes, machineName]];
        DEBUG_LOG(@"%@", numSyncedIPhoneToMacLabel.text);
    } else { // Normal Sync
        #pragma mark TODO Why IsSyncDeleteNotes() is hard-coded to always return true!
//        if (gTopXDocument->IsSyncDeleteNotes()) {
        if (YES) {
            // For %@: %d note(s) deleted and %d note(s) added and %d note(s) synced from %@.
            NSString * const formatStrIOS = NSLocalizedString(@"iOS Results Message 1", @"");
            [numSyncedMacToIPhoneLabel setText: [NSString
                                                            stringWithFormat: formatStrIOS, iPhoneName,
                                                            syncNoteCounts.ncDeleted_iOS, syncNoteCounts.ncAdded_iOS,
                                                            syncNoteCounts.ncUpdated_iOS, machineName]];
            DEBUG_LOG(@"numSyncedMacToIPhoneLabel=%@", numSyncedMacToIPhoneLabel.text);
            // For %@: %d note(s) deleted, %d note(s) duplicated and %d note(s) added and %d note(s) synced from %@.
            NSString * const formatStrMac = NSLocalizedString(@"Mac Results Message 1", @"");
            [numSyncedIPhoneToMacLabel setText: [NSString
                                                            stringWithFormat: formatStrMac, machineName,
                                                            syncNoteCounts.ncDeleted_Mac, syncNoteCounts.ncDuplicated,
                                                            syncNoteCounts.ncAdded_Mac, syncNoteCounts.ncUpdated_Mac,
                                                            iPhoneName]];
            DEBUG_LOG(@"numSyncedIPhoneToMacLabel=%@", numSyncedIPhoneToMacLabel.text);
        }
        else { //Note delete disabled, so notes may restore
            // "For %@: %d note(s) restored, %d note(s) added and %d note(s) synced from %@."
            NSString * const formatStrIOS = NSLocalizedString(@"iOS Results Message 2", @"");
            [numSyncedMacToIPhoneLabel setText: [NSString
                                                            stringWithFormat: formatStrIOS, iPhoneName,
                                                            syncNoteCounts.ncRestored_iOS, syncNoteCounts.ncAdded_iOS,
                                                            syncNoteCounts.ncUpdated_iOS, machineName]];
            DEBUG_LOG(@"numSyncedMacToIPhoneLabel=%@", numSyncedMacToIPhoneLabel.text);
            // "For %@: %d note(s) duplicated and  %d note(s) restored, %d note(s) added and %d note(s) synced from %@."
            NSString * const formatStrMac = NSLocalizedString(@"Mac Results Message 2", @"");
            [numSyncedIPhoneToMacLabel setText: [NSString
                                                            stringWithFormat: formatStrMac, machineName,
                                                            syncNoteCounts.ncDuplicated, syncNoteCounts.ncRestored_Mac,
                                                            syncNoteCounts.ncAdded_Mac, syncNoteCounts.ncUpdated_Mac,
                                                            iPhoneName]];
            DEBUG_LOG(@"numSyncedIPhoneToMacLabel=%@", numSyncedIPhoneToMacLabel.text);
        }
    }

    // Update the sync timestamp
    [self.tableView reloadData];    // Reload services table.

//    // Erase the temporary notepad model file, ignore any errors
//    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToMobileNotepad]) {
//        NSError *error;
//        [[NSFileManager defaultManager] removeItemAtPath:pathToMobileNotepad error:&error];
//    }

    // Disconnect the socket.
    [asyncSocket disconnect];
}

@end
