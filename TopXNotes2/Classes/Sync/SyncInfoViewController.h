
#import <UIKit/UIKit.h>
#import "Model.h"

@class Model;

@interface SyncInfoViewController : UIViewController
{
    IBOutlet UILabel *heading;                                                  //leg20220311 - TopXNotes_Catalyst_2
	IBOutlet UILabel *appName;
	IBOutlet UILabel *copyright;
	IBOutlet Model	 *model;
}

@property (nonatomic, strong) Model *model;
@property (strong, nonatomic) IBOutlet UITextView *instructionsText;            //leg20220311 - TopXNotes_Catalyst_2
@property (strong, nonatomic) IBOutlet UILabel *heading;                        //leg20220311 - TopXNotes_Catalyst_2

- (IBAction)dismissAction:(id)sender;

@end
