//
//  TxnMergeNotepad.h
//  TopXNotes
//
//  Created by Dirk Jan Maasen on 4/4/13.
//  Copyright 2013 Tropical Software. All rights reserved.
//
//
// Rewritten by Lewis E. Garrett beginning 1/9/2020                             //leg20200109 - Catalyst
//  to adapt for use by TopXNotes Catalyst.
//

#import "Model.h"
#import "Note.h"

typedef struct {
	int				ncDeleted_iOS;
	int				ncUpdated_iOS;
	int				ncAdded_iOS;
	int				ncRestored_iOS;
	int				ncDeleted_Mac;
	int				ncUpdated_Mac;
	int				ncAdded_Mac;
	int				ncRestored_Mac;
	int				ncDuplicated;
} SyncNoteCounts;

@class MergeNotepad;

@interface MergeNotepad : NSObject {
	NSMutableArray			*noteTitles;
    NSMutableArray          *noteTexts;
	NSMutableArray			*noteHTMLs;                                         //leg20211202 - TopXNotes_Catalyst_2
	NSMutableArray			*noteIDs;
	NSMutableArray			*noteCreateDates;
	NSMutableArray			*noteModifyDates;
	NSMutableArray			*newNoteList;

	int						numMacNotes;
	int						numMobileNotes;

    Model                   *modelMac;                                          //leg20200311 - Catalyst
    Model					*modelMobile;                                       //leg20200305 - Catalyst
    
	NSString				*deviceUDID;

// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//	UInt32					nextNewNoteID;
    NSDate                  *lastSyncDate;                                      //leg20200219 - Catalyst
	NSDate					*iosLastSyncDate;                                   //leg20200219 - Catalyst
	NSMutableDictionary     *deviceSyncInfo;                                    //leg20200219 - Catalyst
}
@property (nonatomic, strong) Model* modelMobile;                               //leg20200309 - Catalyst

// read accessors
- (int) numMacNotes;
- (int) numMobileNotes;

- (id) init: (NSString*) deviceName macModel: (Model *) model;                  //leg20200311 - Catalyst
    
- (Boolean) doesSyncInfoMatch;

- (void) clearAllSyncInfo;

- (void) mergeMobile: (Boolean) misMatchedSync
		  noteCounts: (SyncNoteCounts*)syncNoteCounts;

//- (NSDictionary *) writeMobileFile;                                                //leg20200316 - Catalyst
- (NSDictionary *) writeMobileFile: (Boolean) isClearSync;	                    //leg20200528 - Catalyst

@end
