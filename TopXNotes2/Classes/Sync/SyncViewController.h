//
//  SyncViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 5/16/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
// Rewritten by Lewis E. Garrett beginning 1/9/2020                             //leg20200109 - Catalyst
//  to adapt for use by TopXNotes Catalyst.
//

#import <UIKit/UIKit.h>
#import "NotePadViewController.h"
#import "SyncInfoViewController.h"
#import "NotePaperView.h"                                                       //leg20140205 - 1.2.7
#import "Model.h"
#import "GCDAsyncSocket.h"                                                      //leg20200212 - Catalyst
#import "TxnMergeNotepad.h"                                                     //leg20200219 - Catalyst

// Adapt Note Sync Info structure                                               //leg20200212 - Catalyst
typedef    struct {
    NSString              *nsDeviceUDID;
    NSDate                *nsDeviceDate;
} NoteSyncInfo;                                                                            

@class Note;
@class Model;

// Add Sync Services table handling to SyncViewController.                      //leg20200205 - Catalyst
// Add NSNetServiceBrowserDelegate and NSNetServiceDelegate delegate support.   //leg20200206 - Catalyst
@interface SyncViewController : UIViewController <UITableViewDataSource,        //leg20200205 - Catalyst
                                                    UITableViewDelegate,        //leg20200205 - Catalyst
                                                    NSNetServiceDelegate,       //leg20200206 - Catalyst
                                                    NSNetServiceBrowserDelegate>//leg20200206 - Catalyst
{

//	IBOutlet UILabel					*productCodeLabel;	
    IBOutlet UILabel					*longerStatusLabel;
    IBOutlet UILabel					*shortStatusLabel;
    IBOutlet UILabel					*numberOfNotesSyncedLabel;
    IBOutlet UISwitch					*synchSwitch;
	IBOutlet Model						*__weak model;
    IBOutlet UIActivityIndicatorView    *progressIndicator;

    IBOutlet UIButton                   *syncButton;                            //leg20200207 - Catalyst
    IBOutlet UILabel                    *numSyncedIPhoneToMacLabel;             //leg20200207 - Catalyst
    IBOutlet UILabel                    *numSyncedMacToIPhoneLabel;             //leg20200207 - Catalyst
    IBOutlet UISwitch                   *clearSyncStatusSwitch;                 //leg20200211 - Catalyst
    
    UIBarButtonItem *modalButton;                                               //leg20220311 - TopXNotes_Catalyst_2

    NotePadViewController		*__weak notePadViewController;
	
	BOOL syncEnabled;                                                           //leg20110502 - 1.0.4
	NSMutableDictionary *savedSettingsDictionary;                               //leg20110502 - 1.0.4
	SyncInfoViewController	*modalViewController;
    
    // Net Service browser delegate support.                                    //leg20200206 - Catalyst
    NSNetServiceBrowser   *browser;
    NSNetService          *service;
    NSMutableArray        *services;
    NSMutableArray        *iPhoneNames;
    NSMutableArray        *udids;
    NSMutableArray        *appNames;
    NSInteger             currentSelectRow;

    NSString              *pathToMobileNotepad;                                 //leg20200309 - Catalyst
    NSString              *productCodeIdentifier;
    NSMutableString       *iPhoneName;

	int						totalBytesRead;
	MergeNotepad			*mergeNotepad;                                      //leg20200219 - Catalyst
	
	BOOL					clearSyncStatusInformation;
	
    // GCDAsyncSocket                                                           //leg20130620 - 1.7.7
	BOOL                connected;
	NSNetServiceBrowser *netServiceBrowser;
	NSNetService        *serverService;
	NSMutableArray      *serverAddresses;
	NSMutableData       *receivedData;

    // Sync control variables.                                                  //leg20200212 - Catalyst
    GCDAsyncSocket      *asyncSocket;                                           

    int numMacNotes;
    int numMobileNotes;

    UInt32  syncErrorCode;
    Boolean canceledSync;
    int     lengthOfFileToSend;
    NSData  * notepadFileToSend;
    Boolean HaveNotesToSend;

    NSMutableDictionary *deviceSyncInfo;
    SyncNoteCounts    syncNoteCounts;                                           //leg20200217 - Catalyst
}

@property (nonatomic, strong) IBOutlet UIBarButtonItem *modalButton;            //leg20220311 - TopXNotes_Catalyst_2
@property (nonatomic, weak) Model* model;
@property (nonatomic, weak) NotePadViewController* notePadViewController;
@property (nonatomic, weak) IBOutlet UITableView *tableView;                    //leg20200205 - Catalyst

//-(IBAction) syncEnableOrDisable:(id)sender;

@end
