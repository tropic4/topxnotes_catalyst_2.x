//
//  SyncViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 5/16/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NotePadViewController.h"
#import "SyncInfoViewController.h"
#import "NotePaperView.h"                                                       //leg20140205 - 1.2.7
#import "Model.h"

@class Note;
@class Model;

@interface SyncSettingsViewController : UIViewController <UIActionSheetDelegate>
{

	IBOutlet UILabel					*productCodeLabel;	
    IBOutlet UILabel					*longerStatusLabel;
    IBOutlet UILabel					*shortStatusLabel;
    IBOutlet UILabel					*numberOfNotesSyncedLabel;
    IBOutlet UISwitch					*synchSwitch;
	IBOutlet Model						*__weak model;
	
	NotePadViewController		*__weak notePadViewController;
	
	BOOL syncEnabled;                                                           //leg20110502 - 1.0.4
	NSMutableDictionary *savedSettingsDictionary;                               //leg20110502 - 1.0.4
	SyncInfoViewController	*modalViewController;
}

@property (strong, nonatomic) IBOutlet NotePaperView *notePaperView;            //leg20140205 - 1.2.7
@property (nonatomic, strong) IBOutlet UILabel* numberOfNotesSyncedLabel;
@property (nonatomic, weak) Model* model;
@property (nonatomic, weak) NotePadViewController* notePadViewController;

-(IBAction) syncEnableOrDisable:(id)sender;

@end
