//
//  SyncNotePad.h
//  TopXNotes
//
//  Created by Lewis Garrett on 5/23/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotePadViewController.h"
#import "SyncInfoViewController.h"
#import "Model.h"


@class Note;
@class Model;
@class GCDAsyncSocket;                                                          

@interface SyncNotePad : NSObject <UIActionSheetDelegate, NSNetServiceDelegate> {

	IBOutlet UIActivityIndicatorView	*progressIndicator;
	IBOutlet Model						*__weak model;
	
//	NotePadViewController		*__weak notePadViewController;                  //leg20210713 - TopXNotes2

	// Networking
	int						totalBytesRead;
	int						numberOfBytesExpected;
	NSMutableData			*receivedData;

#if DEBUG
    NSUInteger  blockNumber;
#endif

	BOOL isRunning;

	GCDAsyncSocket *listenSocket;    
    NSNetService * netService;
	NSMutableArray *connectedSockets;                                           
    NSUInteger  nextExpectedReadDataType;

    NSFileHandle * listeningSocket;

	NSString* productCodePart1Text;
	NSString* productCodePart2Text;
	NSString* productCodePart3Text;
	NSString* productCodePart4Text;
	NSString* productCodeOwnerNameText;
	NSMutableString* completeProductCodeText;
	
	NSMutableDictionary *savedSettingsDictionary;		
	SyncInfoViewController	*modalViewController;
}

@property (nonatomic, strong) IBOutlet UILabel* numberOfNotesSyncedLabel;
@property (nonatomic, weak) Model* model;
//@property (nonatomic, weak) NotePadViewController* notePadViewController;     //leg20210713 - TopXNotes2

-(void)toggleSyncing:(BOOL)syncON;

@end
