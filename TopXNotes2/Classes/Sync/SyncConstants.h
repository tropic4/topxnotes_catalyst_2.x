
//
//  SyncConstants.h
//  TopXNotes_touch
//
//  Created by Lewis Garrett on 02/19/2020.
//  Copyright (c) 2020 Tropical Software, Inc. All rights reserved.
//
//leg20200219 - Catalyst
//

#pragma mark -
#pragma mark Constants
#pragma mark -


#define VERSION_NOTE_SYNC           2

// Sync command ID definitions
#define	kSTART_SYNC             0x47471002                                      
#define	kSTART_SYNCV1           0x47471001                                      
#define	kSYNC_INFO              0x47471009

// Sync version definitions
#define	kNOTE_SYNC_VER_CURRENT	kNOTE_SYNC_VER_002
#define	kNOTE_SYNC_VER_000      0x00000000
#define	kNOTE_SYNC_VER_001      0x00000001
#define	kNOTE_SYNC_VER_002      0x00000002
                                      
// CocoaAsyncSocket defines
#define READ_TIMEOUT 15.0
#define READ_TIMEOUT_EXTENSION 10.0

#define SYNC_INFO_RECORD_LENGTH 8

#define SYNC_READ_BEGIN_INFO_TAG 101
#define SYNC_READ_FILE_INFO_TAG 102
#define SYNC_READ_FILE_EOF_TAG 309

#define SYNC_WRITE_FILE_BUFFER_TAG 201

#define SYNC_WRITE_BEGIN_SEND_TAG 301
#define SYNC_WRITE_FILE_INFO_TAG 302
#define SYNC_WRITE_FILE_ENTIRE_TAG 303

#define SERVICE_NAME    @"TopXNotes Sync"

// Sync error codes
#define    kSYNC_NOERROR           0x00000000
#define    kSYNC_ERROR_001         0x00000001      // Mac NoteSync version > iOS
#define    kSYNC_ERROR_002         0x00000002      // Mac NoteSync version < iOS
#define    kSYNC_ERROR_003         0x00000003      // Mac did not resolve service
#define    kSYNC_ERROR_004         0x00000004      // Clear sync cancelled
#define    kSYNC_ERROR_005         0x00000005      // Can't sync - UDID slots full.
#define    kSYNC_ERROR_006         0x00000006      // Can't sync Groups yet.    //leg20220817 - TopXNotes_Catalyst_2

// Sync constants
typedef    UInt32            NoteIDT;
typedef    UInt32            CategoryIDT;
typedef    UInt32            GroupIDT;

#define kMaxSyncDevices 10

#define groupID_Root    0x00000000
#define noteID_None     0x80000000
#define catID_None      0x00000000

//#define MODERN_API_APP 1
#define SYNC_DEBUG 1

// Macros
#define DEBUG_LOG(fmt, ...) if (SYNC_DEBUG) NSLog(fmt, ##__VA_ARGS__)
