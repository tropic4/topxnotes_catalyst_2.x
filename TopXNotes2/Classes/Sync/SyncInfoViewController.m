
#import "SyncInfoViewController.h"
#import "Constants.h"                                                           //leg20220311 - TopXNotes_Catalyst_2

@implementation SyncInfoViewController
@synthesize model;
@synthesize instructionsText;
@synthesize heading;                                                            //leg20220311 - TopXNotes_Catalyst_2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
        self.title = [NSString stringWithFormat:@"About %@",                    //leg20150924 - 1.5.0
                      [self infoValueForKey:@"CFBundleDisplayName"]];           //leg20150924 - 1.5.0
	}
	
	return self;
}


// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Automatically invoked after -loadView
// This is the preferred override point for doing additional setup after -initWithNibName:bundle:
//
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [Constants lightBlueColor];                     //leg20220311 - TopXNotes_Catalyst_2
}

- (void)viewWillAppear:(BOOL)animated {                                         //leg20140205 - 1.2.7
#pragma unused (animated)

    [super viewWillAppear:animated];

    // Fill in the info view text.                                              //leg20220311 - TopXNotes_Catalyst_2
    heading.text = NSLocalizedString(@"Sync_Server_Info_Heading", @"");
    self.instructionsText.text = NSLocalizedString(@"Sync_Server_Info_Message", @"");}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)

    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)
	// do something here as our view re-appears

    [super viewDidAppear:animated];
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
