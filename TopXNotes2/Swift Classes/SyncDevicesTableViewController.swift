//
//  SyncDevicesTableViewController.swift
//  TopXNotes
//
//  Created by Lewis Garrett on 5/21/20.
//
// Fix Issue #002 - Implement "Edit Synced Devices Table" view.                 //leg20200525 - Catalyst

import UIKit

@objc class SyncDevicesTableViewController: UITableViewController {
    // Device Sync Info Table.
    let deviceSyncInfoTable = NSMutableArray()

    // MARK: - Actions
    
    @IBAction func freeDeviceTapped(_ sender: AnyObject) {
        self.showWarning(for: sender.tag)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem

//        navigationController?.navigationBar.prefersLargeTitles = true
//        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
        title = "Sync Devices Table"
        
        // Register the table view cell.
        tableView.register(UINib(nibName: "SyncDevicesCell", bundle: nil), forCellReuseIdentifier: "Sync_Devices_Cell")

        // Get user defaults dictionary.
        let userDefaults = UserDefaults.standard
        
        // Get device sync info table array from user defaults.
        let deviceSyncInfoTableFromDefaults = userDefaults.object(forKey: "DeviceSyncInfoTable") as! NSArray?

        // Add the array of device sync info dictionarys to mutable array.
        self.deviceSyncInfoTable.addObjects(from: deviceSyncInfoTableFromDefaults as! [NSDictionary])

        // Debugging code loads table with test data.
        if false {
            var row = 0
            for deviceSyncInfo in deviceSyncInfoTable {
                print(deviceSyncInfo)
                let dictionary: NSMutableDictionary = [
                    "siDeviceUDID" : NSUUID().uuidString,
                    "siDeviceName" : randomPhoneName(),
                    "siSyncDate" : NSDate(timeIntervalSinceReferenceDate: TimeInterval(Int.random(in: 1 ... 10000))),
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//                    "siNextNoteID" : UInt32(noteID_None + 1),
//                    "siNextGroupID" : UInt32(groupID_Root + 1),
//                    "siNextCategoryID" : UInt32(catID_None + 1),
                    "siLastFileVersion" : Int(1)
                ]
                    
                print(dictionary)
                self.deviceSyncInfoTable.replaceObject(at: row, with: dictionary)

                row += 1
            }
        }
    }

    private func randomPhoneName() -> String {
      let randomNumber = Int.random(in: 1 ... 100)
      return "iPhone Number \(randomNumber)"
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Sync Device Slots (Device Name, Device ID, Last Sync Date)"
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.text? = headerView.textLabel?.text?.capitalized ?? ""
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceSyncInfoTable.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Sync_Devices_Cell", for: indexPath)
        
        // Get the device sync info table for this row.
        let deviceSyncInfo = deviceSyncInfoTable[indexPath.row] as! NSDictionary

        // Configure the cell...
        configureButton(for: cell, and: indexPath.row, with: deviceSyncInfo)
        configureText(for: cell, with: deviceSyncInfo)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

// MARK: - Helper Methods

    func configureText(for cell: UITableViewCell, with info: NSDictionary) {
      let deviceName = info.object(forKey: "siDeviceName") as! String
      let deviceUDID = info.object(forKey: "siDeviceUDID") as! String
      let deviceSyncDate = info.object(forKey: "siSyncDate") as! NSDate
      if let label = cell.viewWithTag(102) as? UILabel {
        label.text = deviceName
      }
        
      if let label = cell.viewWithTag(103) as? UILabel {
        label.text = deviceUDID
      }
        
      if let label = cell.viewWithTag(104) as? UILabel {
        if (deviceSyncDate == NSDate(timeIntervalSinceReferenceDate: 0)) {
            label.text = "Unused Sync Slot"
        } else {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
            label.text = formatter.string(from: (deviceSyncDate as NSDate) as Date)
        }
      }
    }
    
    func configureButton(for cell: UITableViewCell, and row: Int, with info: NSDictionary) {
        if let button = cell.viewWithTag(101) as? UIButton {
            button.tag = row + 500
            if (info.object(forKey: "siSyncDate") as! NSDate == NSDate(timeIntervalSinceReferenceDate: 0)) {
                button.isHidden = true
            }
        } else if let button = cell.viewWithTag(row + 500) as? UIButton {
            if (info.object(forKey: "siSyncDate") as! NSDate == NSDate(timeIntervalSinceReferenceDate: 0)) {
                button.isHidden = true
            }
        }
    }

    func clearSyncSlot(for row: Int) {
        let dictionary: NSMutableDictionary = [
            "siDeviceUDID" : "",
            "siDeviceName" : "",
            "siSyncDate" : NSDate(timeIntervalSinceReferenceDate: 0),
// Remove reliance on device based nextNoteID.                                  //leg20200605 - Catalyst
//            "siNextNoteID" : UInt32(noteID_None + 1),
//            "siNextGroupID" : UInt32(groupID_Root + 1),
//            "siNextCategoryID" : UInt32(catID_None + 1),
            "siLastFileVersion" : Int(1)
        ]
        
        // Update the device sync info table slot with the changed dictionary.
        self.deviceSyncInfoTable.replaceObject(at: row, with: dictionary)

        // Update the standard user defaults with the updated device sync info table.
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.deviceSyncInfoTable, forKey: "DeviceSyncInfoTable")
        _ = userDefaults.synchronize()
        
        // Update the UI.
        tableView.reloadData()

        print("Sync Slot row=\(row) was reset to defaults!")
    }
    
    func showWarning(for row: Int) {
        let actualRow = row - 500
        let deviceSyncInfo = deviceSyncInfoTable[actualRow] as! NSDictionary
        let deviceName = deviceSyncInfo.object(forKey: "siDeviceName") as! String
        
        let alertController = UIAlertController(title: "This action is NOT undoable!",
                                                message: "Are you sure that you want to quit syncing with the device named \"\(deviceName)\"?", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            // Free the sync slot by setting it to default data.
            self.clearSyncSlot(for: actualRow)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            // Do nothing.
        }))
        
        // Show the warning message
        self.present(alertController, animated: true)
    }
    

}
