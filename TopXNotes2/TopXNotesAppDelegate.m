//
//  TopXNotesAppDelegate.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/5/09.
//  Copyright Iota 2009. All rights reserved.
//

#if TARGET_OS_MACCATALYST
#import <AppKit/AppKit.h>                                                       //leg20220413 - TopXNotes_Catalyst_2
#endif

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <CloudKit/CloudKit.h>                                                   //leg20220829 - TopXNotes_Catalyst_2
#import "CloudKitManager.h"                                                     //leg20220829 - TopXNotes_Catalyst_2

#import "TopXNotesAppDelegate.h"
#import "SearchViewController.h"
#import "NoteRichTextViewController.h"                                          //leg20211115 - TopXNotes_Catalyst_2
#import "NotePadViewController.h"					                            //leg20110613 - 1.1.0
#import "SyncNotePad.h"
#import "Model.h"
#import "UUID.h"
#import "Constants.h"

#import "SyncConstants.h"                                                       //leg20210916 - TopXNotes_Catalyst_2
#import "SyncViewController.h"                                                  //leg20210920 - TopXNotes_Catalyst_2

#import "DDLog.h"                                                               //leg20130514 - 1.2.6
#import "DDTTYLogger.h"                                                         //leg20130514 - 1.2.6
#import "DDASLLogger.h"                                                         //leg20130514 - 1.2.6

#import "APLCloudManager.h"                                                     //leg20221010 - TopXNotes_Catalyst_2

#import "TopXNotes2-Swift.h"


// Log levels: off, error, warn, info, verbose
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#if TARGET_OS_MACCATALYST
@interface TopXNotesAppDelegate () <UISplitViewControllerDelegate, NSToolbarDelegate>   //leg20220413 - TopXNotes_Catalyst_2
#else
@interface TopXNotesAppDelegate () <UISplitViewControllerDelegate>
#endif

@end


@implementation TopXNotesAppDelegate
@synthesize notePadViewController;                                              //leg20210527 - TopXNotes2
@synthesize noteRichTextViewController;                                         //leg20220413 - TopXNotes_Catalyst_2
@synthesize window;                                                             //leg20210430 - TopXNotes2
@synthesize tabBarController;
@synthesize model;
@synthesize syncNotePad;
@synthesize encryptionStatus;                                                   //leg20130205 - 1.3.0

// Fetch objects from our bundle based on keys in our Info.plist.               //leg20211202 - TopXNotes_Catalyst_2
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

#pragma mark - CloudKit Sync

// Remote Notifications.                                                        //leg20220901 - TopXNotes_Catalyst_2
- (CKNotificationInfo *)notificationInfo {
    CKNotificationInfo *note = [[CKNotificationInfo alloc] init];
    note.alertBody = @"Notepad changed notification!";
    note.shouldBadge = YES;
    note.shouldSendContentAvailable = YES;
    return note;
}

// Replace notepad using "File" CKAsset from the record.                        //leg20220906 - TopXNotes_Catalyst_2
- (BOOL)replaceNotepadFromCloudKitUpdate:(CKRecord *)record {
        
        // Write the CKAsset "File" to a file path
        CKAsset *notepadFile = [record objectForKey:@"File"];
        NSURL *notepadURL = notepadFile.fileURL;
        NSString *notepadPath = notepadURL.path;

        // Prepare to retreive backup file descriptions
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *fileError = nil;
        NSString *pathToiCloudUpdatedFile = notepadPath;
        NSString *pathToModelDataFile = [self.model pathToData];
        NSString *pathToBackupModelDataFile = [Model pathToData:@"Backup_Notepad_b4_iCloud_Update"];;

        // Make a backup copy of the notepad
        [fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
        [fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&fileError];

        Boolean FILE_DOES_NOT_EXIST = ![fileManager fileExistsAtPath:pathToModelDataFile];
        Boolean FILE_WAS_REMOVED = [fileManager removeItemAtPath:pathToModelDataFile error:&fileError];
        Boolean ERROR_WAS_ZERO  = [fileError code] == 0;

        // Delete the old Model file and if no errors replace it with the received Model file.
        if ((FILE_DOES_NOT_EXIST || FILE_WAS_REMOVED) && ERROR_WAS_ZERO) {
            
            // Replace the model (current notepad) with the backup notepad
            Boolean FILE_WAS_COPIED  = [fileManager copyItemAtPath:pathToiCloudUpdatedFile toPath:pathToModelDataFile error:&fileError];
            
            // If file copied ok and made the current notepad, report success
            if (FILE_WAS_COPIED && [self.model loadData]) {
                NSLog(@"iCloud update of notepad restored successfully.");
            } else {
                NSLog(@"Error copying iCloud file to model - error=\"%@\"", [fileError localizedDescription]);
                return false;
            }
        } else {
            NSLog(@"Error replacing notepad file with iCloud update - error=\"%@\"", [fileError localizedDescription]);
            return false;
        }

        // Notify notepad that it has changed.
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotification_Refresh_NoteList object:nil];
    
        return true;
}

// Convert notification to CKQueryNotification and process it.                  //leg20220906 - TopXNotes_Catalyst_2
- (void)processCloudKitNotification:(NSDictionary *)notificationInfo {
    __block CKRecord *notepadRecord;
    
    CKQueryNotification *note = [CKQueryNotification notificationFromRemoteNotificationDictionary:notificationInfo];
    if(!note)
        return;
    if (note.queryNotificationReason == CKQueryNotificationReasonRecordUpdated) {
        NSLog(@"CloudKit Update Record Notification Received");
        
        // Fetch the updated record and use it to replace the notepad.
        [CloudKitManager fetchNotepadRecordWithId:note.recordID.recordName
                                      completionHandler:^(NSArray *results, NSError *error) {

                  if (error) {
                      NSLog(@"Error fetching iCloud Notepad with Record Id\"%@\"! - error=\"%@\"", note.recordID.recordName, [error localizedDescription]);
                  } else {
                      NSLog(@"Successfully fetched iCloud Notepad with Record Id\"%@\"!", note.recordID.recordName);
                      notepadRecord = results[0];
                      [self replaceNotepadFromCloudKitUpdate: notepadRecord];

                      [[NSNotificationCenter defaultCenter] postNotificationName:kReset_Display_After_CloudKit_Update object:nil];
                 }
        }];

    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken - token=\"%@\"", deviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Error didFailToRegisterForRemoteNotificationsWithError - error=\"%@\"", [error localizedDescription]);
}

// Apple Sample CloudPhotos additions to app delegate.                          //leg20221010 - TopXNotes_Catalyst_2
- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // for the sake of UIStateRestoration (we restore early in the launch process)
    // and
    // APLMainTableViewController may be loaded before we have a chance to setup our CloudManager object so create it earlier here
    //
    // create our cloud manager object responsible for all CloudKit operations
    _cloudManager = [[APLCloudManager alloc] init];
    
    // listen for user login token changes so we can refresh and reflect our UI based on user login
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(iCloudAccountAvailabilityChanged:)
                                                 name:NSUbiquityIdentityDidChangeNotification
                                               object:nil];
    return YES;
}

#pragma mark - Application Delegate

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
#pragma unused (application)
    // Override point for customization after application launch.

    // Register for CloudKit subscription notifications.                        //leg20220901 - TopXNotes_Catalyst_2
    [application registerForRemoteNotifications];

    // if are are logged in, subscribe to changes to our record type (this      //leg20221010 - TopXNotes_Catalyst_2
    //  will allow for push notifications to other devices)
    [CloudManager accountAvailable:^(BOOL available) {
        [CloudManager subscribe];
    }];
    
#if TARGET_OS_UIKITFORMAC
    // For macOS change the name of the window to macOS executable name.        //leg20220223 - TopXNotes_Catalyst_2
    NSArray         *windows = [[UIApplication sharedApplication]windows];
    UIWindowScene   *scene;                                                     //leg20220413 - TopXNotes_Catalyst_2
    for (UIWindow   *window in windows) {
        window.windowScene.title = kAppNameMacOS;
        scene = window.windowScene;                                             //leg20220413 - TopXNotes_Catalyst_2
            break;
    }
    
    // Create a toolbar in titlebar.                                            //leg20220413 - TopXNotes_Catalyst_2
    NSToolbarItemIdentifier ToolbarItemIdentifier  = @"Toolbar Identifier";
    UITitlebar *titlebar = scene.titlebar;
    NSToolbar *toolbar = [[NSToolbar alloc] initWithIdentifier:ToolbarItemIdentifier];
    titlebar.toolbar = toolbar;
    toolbar.delegate = self;

    // Set titlebar style if available so toolbar looks same as on Catalina.    //leg20220415 - TopXNotes_Catalyst_2
#if BUILDING_ON_MONTEREY
    if (@available(macCatalyst 14.0, *)) {
        titlebar.toolbarStyle = UITitlebarToolbarStyleExpanded;
    } else {
        // Fallback on earlier versions
    }
#endif

    // Set up toolbar properties: Allow customization, give a default display   //leg20220413 - TopXNotes_Catalyst_2
    //  mode, and remember state in user defaults
    [toolbar setAllowsUserCustomization: YES];
    [toolbar setAutosavesConfiguration: YES];
    [toolbar setDisplayMode: NSToolbarDisplayModeIconAndLabel];
    
#endif
    
    
    //    // App Group experimental code.                                             //leg20210708 - TopxNotes2
//    NSURL *groupURL = [[NSFileManager defaultManager]
//        containerURLForSecurityApplicationGroupIdentifier:
//            @"group.com.tropic4.topxnotes"];
//
//    NSUserDefaults *myDefaults = [[NSUserDefaults alloc]
//        initWithSuiteName:@"group.com.tropic4.topxnotes"];
//    if ([myDefaults objectForKey:@"bar"] == nil) {
//        [myDefaults setObject:@"foo" forKey:@"bar"];
//        [myDefaults synchronize];
//    }

    // Create App Group sub directory if it doesn't already exist.              //leg20211202 - TopXNotes_Catalyst_2
    NSURL *appGroupURL = [[NSFileManager defaultManager]
        containerURLForSecurityApplicationGroupIdentifier:
            @"group.com.tropic4.topxnotes"];
    NSURL *appGroupSubDirectoryURL = [appGroupURL URLByAppendingPathComponent:[self infoValueForKey:(NSString *)kCFBundleNameKey]];
    NSError *error = nil;

    if ([[NSFileManager defaultManager] createDirectoryAtURL:appGroupSubDirectoryURL
                                 withIntermediateDirectories:NO
                                                  attributes:nil
                                                       error:&error]) {
        NSLog(@"Created App Group sub directory \"%@\".", [self infoValueForKey:(NSString *)kCFBundleNameKey]);

    }
    
    if ([error code] != 0) {
        if ([error code] != NSFileWriteFileExistsError)
            NSLog(@"Error creating App Group sub directory=%ld", (long)[error code]);
    }
    
    // Set Appearance Style for App.                                            //leg20210528 - TopXNotes2
    if (@available(iOS 13.0, *)) {
        window.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    // Configure SplitView as first tab.                                         //leg20210526 - TopXNotes2
    self.tabBarController = (UITabBarController *) self.window.rootViewController;
    UISplitViewController *splitViewController = (UISplitViewController *)nil;
    
    for(UIViewController *viewController in self.tabBarController.viewControllers){
        if([viewController.title isEqualToString:@"Notes"]){                    //leg20210713 - TopXNotes2
            splitViewController = (UISplitViewController *) viewController;
        }
    }
    
#if TARGET_OS_UIKITFORMAC
    // Setting macOS Master view to be 35% of width.                            //leg20220411 - TopXNotes_Catalyst_2
    splitViewController.maximumPrimaryColumnWidth = splitViewController.view.bounds.size.width;
    splitViewController.preferredPrimaryColumnWidthFraction = 0.35;
#else
    // Setting iOS Master view to be 50% of width of current orientation.       //leg20220411 - TopXNotes_Catalyst_2
    CGFloat minimumWidth = MIN(CGRectGetWidth(splitViewController.view.bounds),CGRectGetHeight(splitViewController.view.bounds));
    splitViewController.minimumPrimaryColumnWidth = minimumWidth / 2;
    splitViewController.maximumPrimaryColumnWidth = minimumWidth;
#endif
    
    UINavigationController *navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1];
    navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
    splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAutomatic;   //leg20220121 - TopXNotes_Catalyst_2
    splitViewController.delegate = self;

    // Make tab and nav bars darker color only in iOS target.                   //leg20211116 - TopXNotes_Catalyst_2
#if !TARGET_OS_UIKITFORMAC
    // Make the tab bar a darker color so selected tab stands-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    self.tabBarController.tabBar.barTintColor = [UIColor lightGrayColor];

    // Make the navigation bar a darker color so controls stand-out.
    // Remove backgrounds from bars to improve contrast.                        //leg20220705 - TopXNotes_Catalyst_2
//    navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];
#endif

    // Watch for memory warning notifications.                                  /leg20131204 - 1.2.6
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logMemoryWarningNotification:)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

    // Initialize the notepad model.                                            //leg20210413 - TopXNotes2
    model = [[Model alloc] init];

    // Set model for NoteRichTextViewController to prevent placeholder being    //leg20211115 - TopXNotes_Catalyst_2
    //  displayed at initialization.
    for(UINavigationController *navController in splitViewController.viewControllers){   //leg20211115 - TopXNotes_Catalyst_2
        if ([navController.topViewController isKindOfClass:[NoteRichTextViewController class]]) {
            noteRichTextViewController = (NoteRichTextViewController *)navController.topViewController; //leg20220413 - TopXNotes_Catalyst_2
            noteRichTextViewController.model = model;
            break;
        }
    }

    for(UINavigationController *navController in splitViewController.viewControllers){   //leg20211115 - TopXNotes_Catalyst_2
        if ([navController.topViewController isKindOfClass:[NotePadViewController class]]) {
            notePadViewController = (NotePadViewController *)navController.topViewController;
            notePadViewController.model = model;
            break;
        }
    }

    // Watch for "make an auto backup" notification.                            //leg20211206 - TopXNotes_Catalyst_2
    [[NSNotificationCenter defaultCenter] addObserver:notePadViewController selector:@selector(makeAutoBackup) name:kAutoBackups_Notification object:nil];    // Initialize the notepad model.                                            //leg20210413 - TopXNotes2

    // Configure logging framework                                              //leg20130514 - 1.2.6
	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	[DDLog addLogger:[DDASLLogger sharedInstance]];
    DDLogInfo(@"Logging started…");
    
// Implement Device Sync Info Table from TopXNotes_Catalyst_3 codebase.         //leg20210916 - TopXNotes_Catalyst_2
// Begin modification.                                                          //leg20200128 - Catalyst
    
// Implement TopXNotes Mac's DeviceSyncInfo structure with a NSDictionary.      //leg20200128 - Catalyst
    //typedef struct {
    //	StrUDID			siDeviceUDID;
    //	Str63			siDeviceName;
    //	time_t			siSyncDate;
    //	NoteIDT			siNextNoteID;
    //	GroupIDT		siNextGroupID;
    //	CategoryIDT		siNextCategoryID;
    //	UInt16			siLastFileVersion;
    //} DeviceSyncInfo;
        
    #pragma mark TODO Remove hard-coded erase standardUserDefaults each time.
    // *** DEBUGGING CODE ***
    if (NO) {
//    if (YES) {
        // Unnecessary to remove objects since resetStandardUserDefaults sets   //leg20211206 - TopXNotes_Catalyst_2
        //  standardUserDefaults to nil.
        // Note that must reboot to really get rid of standardUserDefaults in
        //  order to re-init.
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DeviceSyncInfoTable"];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstRunFlag"];
        [NSUserDefaults resetStandardUserDefaults];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
// FIXME: standardDefaults are a mess - do we need appDefs, etc? Reconfigure!   //leg20220901 - TopXNotes_Catalyst_2
// FIXME: need to initialize all defaults if firstRunFlag is nil or false;      //leg20220901 - TopXNotes_Catalyst_2
    
    Boolean firstRunFlag = [[[NSUserDefaults standardUserDefaults] objectForKey: @"FirstRunFlag"] boolValue];
    
    if (!firstRunFlag) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"FirstRunFlag"];
//        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"kiCloud_Enable_Key"];    //leg20220905 - TopXNotes_Catalyst_2
        
        // Initialize device sync info with default values - kMaxSyncDevices slots.
        NSMutableDictionary *deviceSyncInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"", @"siDeviceUDID",
                                                                            @"", @"siDeviceName",
                                                                            [NSDate dateWithTimeIntervalSinceReferenceDate:0], @"siSyncDate",   //leg20200310 - Catalyst
                                                                             [NSNumber numberWithInt:1], @"siLastFileVersion",
                                                                            nil];
        
        // Build table with kMaxSyncDevices slots and save it                   //leg20200317 - Catalyst
        //  in standardUserDefaults.
        NSMutableDictionary *deviceSyncInfoDictionarys[kMaxSyncDevices];        //leg20200319 - Catalyst

        for (int i=0; i<kMaxSyncDevices; i++) {
            deviceSyncInfoDictionarys[i] = deviceSyncInfo;
        }
        NSMutableArray *deviceSyncTable = [NSMutableArray arrayWithObjects:deviceSyncInfoDictionarys count:kMaxSyncDevices];

        [[NSUserDefaults standardUserDefaults] setObject:deviceSyncTable forKey:@"DeviceSyncInfoTable"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        // Fix Issue #061: "Auto backups quit working after starting with new   //leg20221004 - TopXNotes_Catalyst_2
        //  TopXNotes2 Container" - Initialize Auto Backup control variables at
        //  "first run" time.
        NSUserDefaults *defaults;
        defaults = [NSUserDefaults standardUserDefaults];
        
        NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
        if (dict != nil)
            savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
        else
            savedSettingsDictionary = [NSMutableDictionary dictionary];
        
        // Initialize auto backup control variables.                            //leg20221004 - TopXNotes_Catalyst_2
        [savedSettingsDictionary setObject:[NSNumber numberWithInt:1] forKey:kNextAutoBackupNumber_Key];
        [savedSettingsDictionary setObject:[NSMutableArray array] forKey:kAutoBackupsArray_Key];

        // Initialize next untitled note number.                                //leg20221005 - TopXNotes_Catalyst_2
        [savedSettingsDictionary setObject:[NSNumber numberWithInteger:0] forKey:kNextNoteNumber_Key];
            
        // Save settings
        [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // End modification.                                                        //leg20200128 - Catalyst
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *appDefs = [NSMutableDictionary dictionary];

    // Put defaults in the dictionary of application defaults
    [appDefs setObject:[NSMutableDictionary dictionary] forKey: kRestoreDataDictionaryKey];
				
    // Register the dictionary of defaults
    [defaults registerDefaults: appDefs];
	
    // Reading Defaults
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil) {
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	} else {
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    }
    
    // If dictionary has no entries, it is the first run of the App and we will //leg20140113 - 1.2.6
    //  generate a unique identifier as a replacement for the deprecated
    //  device UDID that Apple no longer allows us to use. We use an NSString
    //  category to create the UUID (Universally Unique Identifiers) only the
    //  first time the app is launched, and store it in user defaults. That way,
    //  our UUID will automatically be backed up and restored to a new device.

// FIXME: dict has one key here and so deviceUDID never gets set. When changed? //leg20220901 - TopXNotes_Catalyst_2
//    if (![dict count]) {
    if ([dict count] < 2) {
        // First run, generate a unique identifier and save it in the model.
        if ([defaults objectForKey:kUUID_GENERATED_KEY] == nil) {
            [model setDeviceUDID:[NSString uuid]];
            [model setRealDeviceUDID:@""];
            [defaults setObject:[model deviceUDID] forKey:kUUID_GENERATED_KEY];
        }
    } else {
        // Notepad already exists, so we use the old unique identifier
        //  from the iDevice, make sure it gets stored in defaults.  The old
        //  "real" device identifier should be available in realDeviceUDID, also.
        if ([defaults objectForKey:kUUID_GENERATED_KEY] == nil) {
            [defaults setObject:[model deviceUDID] forKey:kUUID_GENERATED_KEY];
        }
    }
    
//	// Get current state of show/hide notepaper.                                //leg20140222 - 1.2.7
//    NSNumber *showNotepaperNumber;
//    if (!(showNotepaperNumber = [savedSettingsDictionary objectForKey:kShowNotepaper_Key])) {
//        // Set default show/hide notepaper default state, NO=Hide.
//        showNotepaperNumber = [NSNumber numberWithBool:NO];
//    }
//
//    [savedSettingsDictionary setObject:showNotepaperNumber forKey:kShowNotepaper_Key];
    
    // Save settings
    [defaults setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [defaults synchronize];
    
// Remove setting controls' color to Tropical Green.                            //leg20211210 - TopXNotes_Catalyst_2
//    // Set the default color of most controls.                                  //leg20211208 - TopXNotes_Catalyst_2
//#if !TARGET_OS_UIKITFORMAC
//   self.window.tintColor = [Constants controlsColor];
//#endif
//

    // Get Sync settings.
    syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];

    // Configure syncing.
    firstBecameActive = YES;
    syncNotePad = [[SyncNotePad alloc] init];
    syncNotePad.model = model;

    if (syncEnabled)
        [syncNotePad toggleSyncing:YES];
    else
        [syncNotePad toggleSyncing:NO];


    // If running on Mac target change Sync tab so that Sync server view is     //leg20210920 - TopXNotes_Catalyst_2
    //  presented from .xib instead of Sync settings view segue.
#if TARGET_OS_UIKITFORMAC
    SyncViewController* syncVC = [[SyncViewController alloc] initWithNibName:@"SyncView" bundle:nil];
    UITabBarItem * syncItem = [[UITabBarItem alloc] initWithTitle:@"Sync"
                                                         image:[UIImage imageNamed:@"sync.png"]
                                                 selectedImage:[UIImage imageNamed:@"sync.png"]];
    syncVC.model = self.model;
    syncVC.tabBarItem  = syncItem;
    NSMutableArray* tabVCs = (NSMutableArray*)self.tabBarController.viewControllers;
    UINavigationController *myNavigationController;
    myNavigationController = [[UINavigationController alloc] initWithRootViewController:syncVC];
    
    // Replace Sync tab view controller with Sync server view controller.
    NSUInteger index = 0;
    Boolean foundTab = false;
    for(UIViewController *viewController in tabVCs){
        if([viewController.tabBarItem.title isEqualToString:@"Sync"]){
            foundTab = true;
            break;
        }
        index++;
    }
    
    if (foundTab) {
        tabVCs[index] = myNavigationController;
        [self.tabBarController setViewControllers:tabVCs];
    }
#endif


}

- (void)logMemoryWarningNotification:(NSNotification *)notif                    //leg20131204 - 1.2.6
{
#pragma unused (notif)

    NSLog(@"Memory warning notification received");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
#pragma unused (application)

	syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];   //leg20120315 - 1.2.0

	// Disable Syncing while in the background.									//leg20110523 - 1.0.4
	[syncNotePad toggleSyncing:NO];
	firstBecameActive = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
#pragma unused (application)

    syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];   //leg20120315 - 1.2.0

	// Re-enable syncing if it was enabled when we went into the background.	//leg20110523 - 1.0.4
	if (syncEnabled && !firstBecameActive) {
		[syncNotePad toggleSyncing:YES];
	}
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application         //leg20131204 - 1.2.6
{
#pragma unused (application)

    NSLog(@"applicationDidReceiveMemoryWarning");
}

- (void)dealloc {

	// unregister for this notification                                         /leg20131204 - 1.2.6
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidReceiveMemoryWarningNotification object:nil];

}

#pragma mark - NSNotifications

// iCloudAccountAvailabilityChanged notification.                               //leg20221010 - TopXNotes_Catalyst_2
- (void)iCloudAccountAvailabilityChanged:(NSNotification *)notif
{
    // the user signs out of iCloud (such as by turning off Documents & Data in Settings),
    // or
    // has signed back in:
    // so we need to refresh our UI, this will update our UI to reflect user login
    //
    
// TODO: need to do something with iCloudAccountAvailabilityChanged notification  //leg20221010 - TopXNotes_Catalyst_2
//    [CloudManager updateUserLogin:^() {
//
//        // always tell our main table (visible or not) to update based on account changes
//        APLMainTableViewController *mainTableViewController = [self mainViewController];
//        [mainTableViewController iCloudAccountAvailabilityChanged];
//
//        // is the current visible view controller our detail vc?  If so, just ask for it to update
//        UINavigationController *rootVC = (UINavigationController *)self.window.rootViewController;
//        UIViewController *currentViewController = rootVC.visibleViewController;
//        if ([currentViewController isKindOfClass:[APLDetailTableViewController class]])
//        {
//            // notify the detail view controller to update based on account changes
//            [(APLDetailTableViewController *)currentViewController iCloudAccountAvailabilityChanged];
//        }
//    }];
}


#pragma mark - Push Notifications

// Process the CloudKit notification.                                           //leg20221010 - TopXNotes_Catalyst_2
- (void)handlePush:(NSDictionary *)userInfo
{
    [CloudManager markNotificationsAsAlreadyRead];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        //NSLog(@"incoming push notification: app is active");
    }
    else if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
        //NSLog(@"incoming push notification: app is the background");
    }
    
    CKNotification *cloudKitNotification = [CKNotification notificationFromRemoteNotificationDictionary:userInfo];
    CKNotificationType notifType = [cloudKitNotification notificationType];
    
    // for debugging:
    //CKNotificationID *notificationID = [cloudKitNotification notificationID];
    //NSString *containerIdentifier = [cloudKitNotification containerIdentifier];
    //NSLog(@"CloudPhotos: Push notification received: %@", [cloudKitNotification alertBody]);
    //NSLog(@"notifType = %ld, notifID = %@, containerID = %@", (long)notifType, notificationID, containerIdentifier);
    
    if (notifType == CKNotificationTypeQuery)
    {
        // a notification generated based on the conditions set forth in a subscription object (NSPredicate, in our case a "true" predicate)
        CKQueryNotification *queryNotification = (CKQueryNotification *)[CKNotification notificationFromRemoteNotificationDictionary:userInfo];
        
        BOOL iCloudAvailable = false;
        BOOL iCloudSync = false;

        if ([[NSFileManager defaultManager] ubiquityIdentityToken] != nil) {
            iCloudAvailable = true;
        }

        iCloudSync = [model.cloudKitSync boolValue];

        // find out which notepad was modified
        CKRecordID *recordID = [queryNotification recordID];

        // we are only interested in the kNotepadVersionNumber attribute
        //  (because we set the CKSubscription's CKNotificationInfo 'desiredKeys' when we subscribed earlier)
        
        // here we can examine the title of the photo record without a query
        CKQueryNotificationReason reason = [queryNotification queryNotificationReason];
        
// TODO: Decide whether this message is really necessary here.
//        NSDictionary *recordFields = [queryNotification recordFields];
//        NSNumber *notepadVersion = recordFields[[APLCloudManager NotepadVersionAttribute]];
//        NSString *baseMessage;
//        NSString *finalMessage;
//        switch (reason)
//        {
//            case CKQueryNotificationReasonRecordCreated:
//                baseMessage = @"Version %d Notepad Record added.";
//                break;
//
//            case CKQueryNotificationReasonRecordUpdated:
//                baseMessage = @"Version %d Notepad Record updated.";
//                break;
//
//            case CKQueryNotificationReasonRecordDeleted:
//                baseMessage = @"Version %d Notepad Record deleted.";
//                break;
//        }
//        if (baseMessage != nil)
//        {
//            finalMessage = [NSString stringWithFormat:baseMessage, [notepadVersion intValue]];
//            NSLog(@"%@", finalMessage);
//        }
        
        // Use iCloud to Sync notepad if available and if iCloud Sync enabled.
        if (iCloudAvailable && iCloudSync) {
            // Process the record to update this device's notepad.
            if (reason == CKQueryNotificationReasonRecordUpdated) {
                // Fetch the updated record and use it to replace the notepad.
                [CloudManager fetchRecordWithID:recordID completionHandler:^(CKRecord *foundRecord, NSError *error) {
                    if (foundRecord != nil)
                    {
                        [self replaceNotepadFromCloudKitUpdate: foundRecord];

                        [[NSNotificationCenter defaultCenter] postNotificationName:kReset_Display_After_CloudKit_Update object:nil];
                    }
                }];
            }
        } else {
            NSLog(@"CloudKit update not processed because CloudKit Sync is turned off or iCloud not available!");
        }
    }
}

// Receive the CloudKit notification.                                           //leg20221010 - TopXNotes_Catalyst_2
//
// didReceiveRemoteNotification:
//
// This is called when:
// 1) the app is actively running (no push alert will appear)
// or
// 2) the app is in the background (push banner will appear, user taps the banner to open our app)
//
// If this app was not already running, then "didFinishLaunchingWithOptions" is called and passed info about the notification.
// So didFinishLaunchingWithOptions needs to handle the incoming notification by itself.
//
// To receive background notifications, this has to be turned on in Xcode:
//      Capabilities -> Background Modes: Remote Notifications
//          where this is added to the Info.plist as a result:
//                  <key>UIBackgroundModes</key>
//                  <array>
//                  <string>remote-notification</string>
//                  </array>
//
// Note:
// To test of the app was actually relaunched due to the push receive,
// use Instrument's Activity Monitor to test if it was truly launched (by checking the "running processes" for that device).
//
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler
{
    // note: userInfo must have this:
    //      "content-available" = 1;
    // also: In Info.plist:
    //          <key>UIBackgroundModes</key>
    //          <array>
    //          <string>remote-notification</string>
    //          </array>
    //
    // NOTE:
    // The content-available property with a value of 1 lets the remote notification act as a “silent” notification.
    // When a silent notification arrives, iOS wakes up your app in the background so that you can get new data from
    // your server or do background information processing. Users aren’t told about the new or changed information
    // that results from a silent notification, but they can find out about it the next time they open your app.
    //
    // NOTE:
    // If the notification has 1) alert, 2) badge, or 3) soundKey, then CloudKit uses priority 10, otherwise is uses 5.
    //
    [self handlePush:userInfo];
    
    // this must be called at the end
    completionHandler(UIBackgroundFetchResultNewData);
}

// Apple Sample CloudPhotos additions to app delegate.  --replaced--            //leg20221010 - TopXNotes_Catalyst_2
//// Receive CloudKit notification.                                               //leg20220906 - TopXNotes_Catalyst_2
//- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//      fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//
//    CKNotification *notice = [CKNotification notificationFromRemoteNotificationDictionary:userInfo];
//    if (notice.notificationType == CKNotificationTypeQuery) {
//        BOOL iCloudAvailable = false;
//        BOOL iCloudSync = false;
//
//        if ([[NSFileManager defaultManager] ubiquityIdentityToken] != nil) {
//            iCloudAvailable = true;
//        }
//
////        iCloudSync = [[[NSUserDefaults standardUserDefaults] objectForKey: kiCloud_Enable_Key] boolValue];
//        iCloudSync = [model.cloudKitSync boolValue];
//
//        // Use iCloud to Sync notepad if available and if iCloud Sync enabled.
//        if (iCloudAvailable && iCloudSync) {
//            [self processCloudKitNotification:userInfo];
//        }
//    }
//
//    completionHandler(UIBackgroundFetchResultNoData);
//}

// FIXME: Research UIStateRestoration to find out if we need this.              //leg20221014 - TopXNotes_Catalyst_2
//#pragma mark - UIStateRestoration
//
//// shouldSaveApplicationState                                                   //leg20221010 - TopXNotes_Catalyst_2
//- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder
//{
//    return YES;
//}
//
//// shouldRestoreApplicationState                                                //leg20221010 - TopXNotes_Catalyst_2
//- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder
//{
//    return YES;
//}

#pragma mark - Split view Delegate

// - collapseSecondaryViewController                                            //leg20210526 - TopXNotes2
- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] &&
//        [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[NoteViewController class]] &&
        [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[NoteRichTextViewController class]] &&
//        ([(NoteViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
//        ([model numberOfNotes] == 0)) {
//        (false)) {
//        (YES)) {
        (NO)) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }
}

//// Called when the view is shown again in the split view, invalidating the button and popover controller.   //leg20210611 - TopXNotes2
//- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
//    // Notify NoteView that view has changed.
//    if (svc.displayModeButtonItem == barButtonItem)
//        [[NSNotificationCenter defaultCenter] postNotificationName:kSplitView_Back_Button_Hit object:nil];
//}//API_DEPRECATED("Use splitViewController:willChangeToDisplayMode: and displayModeButtonItem instead", ios(2.0, 8.0)) API_UNAVAILABLE(tvos);

// This method replaces deprecated -willShowViewController.
// This method allows a client to update any bar button items etc.
- (void)splitViewController:(UISplitViewController *)svc willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode {
    // Notify NoteView that view has changed.
    if (displayMode == UISplitViewControllerDisplayModeOneOverSecondary)        // Xcode 12.4
//    if (displayMode == UISplitViewControllerDisplayModePrimaryOverlay)          // Xcode 11.7
        [[NSNotificationCenter defaultCenter] postNotificationName:kSplitView_Back_Button_Hit object:nil];
}

// Delegate methods for toolbar in titlebar.                                    //leg20220413 - TopXNotes_Catalyst_2
#pragma mark - NSToolbarDelegate

#if TARGET_OS_MACCATALYST

static NSToolbarItemIdentifier AddNoteToolbarItemIdentifier  = @"AddNote";      //leg20220615 - TopXNotes_Catalyst_2
static NSToolbarItemIdentifier AddGroupToolbarItemIdentifier  = @"AddGroup";    //leg20220615 - TopXNotes_Catalyst_2
static NSToolbarItemIdentifier DeleteItemToolbarItemIdentifier  = @"DeleteNoteOrGroup"; //leg20220614 - TopXNotes_Catalyst_2
static NSToolbarItemIdentifier SearchToolbarItemIdentifier  = @"SearchNote";
static NSToolbarItemIdentifier ShareItemToolbarItemIdentifier  = @"ShareNote";
static NSToolbarItemIdentifier PrintNoteToolbarItemIdentifier  = @"PrintNote";

- (void) search {
    int x =47;
}

- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar itemForItemIdentifier:(NSToolbarItemIdentifier)itemIdent willBeInsertedIntoToolbar:(BOOL)willBeInserted {
    // Required delegate method:  Given an item identifier, this method returns an item
    // The toolbar will use this method to obtain toolbar items that can be displayed in the customization sheet, or in the toolbar itself
    NSToolbarItem *toolbarItem = [[NSToolbarItem alloc] initWithItemIdentifier: itemIdent];

    NSToolbarItem *item = nil;

    
    if ([itemIdent isEqual:AddNoteToolbarItemIdentifier]) {
        // Set the text label to be displayed in the toolbar and customization palette
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                       target:notePadViewController
                                          action:@selector(newNote)];

        item = [NSToolbarItem itemWithItemIdentifier:AddNoteToolbarItemIdentifier
                                       barButtonItem:barButtonItem];
        item.bordered = true;
        item.toolTip = @"Add Note";
        item.label = @"Add Note";
        item.paletteLabel = @"Add Note";

        toolbarItem = item;
        
    } else if ([itemIdent isEqual:AddGroupToolbarItemIdentifier]) {             //leg20220615 - TopXNotes_Catalyst_2
        // Set the text label to be displayed in the toolbar and customization palette
//        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
//                                       initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                       target:notePadViewController
//                                          action:@selector(newGroup)];
        // Set copied SFSymbol plus_folder.png for Add Group tool.              //leg20220824 - TopXNotes_Catalyst_2
        UIImage *addGroupImage = [UIImage imageNamed:@"plus_folder.png"];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                          initWithImage:addGroupImage
                                                  style:UIBarButtonItemStylePlain
                                                 target:notePadViewController
                                                 action:@selector(newGroup)];

        item = [NSToolbarItem itemWithItemIdentifier:AddGroupToolbarItemIdentifier
                                       barButtonItem:barButtonItem];
        item.bordered = true;
        item.toolTip = @"Add Group";
        item.label = @"Add Group";
        item.paletteLabel = @"Add Group";

        toolbarItem = item;
        
     } else if ([itemIdent isEqual:DeleteItemToolbarItemIdentifier]) {
        // Set the text label to be displayed in the toolbar and customization palette
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                        target:notePadViewController
                                           action:@selector(deleteNoteOrGroup:)];   //leg20220614 - TopXNotes_Catalyst_2
         
         item = [NSToolbarItem itemWithItemIdentifier:DeleteItemToolbarItemIdentifier
                                        barButtonItem:barButtonItem];
         item.bordered = true;
         item.toolTip = @"Delete Note or Group";                                //leg20220614 - TopXNotes_Catalyst_2
         item.label = @"Delete Note or Group";
         item.paletteLabel = @"Delete Note or Group";

         toolbarItem = item;

     } else if ([itemIdent isEqual:ShareItemToolbarItemIdentifier]) {
        // Set the text label to be displayed in the toolbar and customization palette
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                        target:noteRichTextViewController
                                        action:@selector(emailNote)];
         
         item = [NSToolbarItem itemWithItemIdentifier:ShareItemToolbarItemIdentifier
                                        barButtonItem:barButtonItem];
         item.bordered = true;
         item.toolTip = @"Share Note";
         item.label = @"Share";
         item.paletteLabel = @"Share";

         toolbarItem = item;

     } else if ([itemIdent isEqual:SearchToolbarItemIdentifier]) {
        // Set the text label to be displayed in the toolbar and customization palette
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
//                                        target:noteRichTextViewController
//                                        action:@selector(search)];
                                        target:self
                                        action:@selector(search)];

         item = [NSToolbarItem itemWithItemIdentifier:SearchToolbarItemIdentifier
                                        barButtonItem:barButtonItem];
         item.bordered = true;
         item.toolTip = @"Search Note";
         item.label = @"Search";
         item.paletteLabel = @"Search";

         toolbarItem = item;

//        // Use a custom view, an NSSearchField, in the toolbar item
//        [toolbarItem setView: noteRichTextViewController.searchFieldOutlet];
//
//        [toolbarItem setMinSize:NSMakeSize(30, NSHeight([noteRichTextViewController.searchFieldOutlet frame]))];
//        [toolbarItem setMaxSize:NSMakeSize(400,NSHeight([noteRichTextViewController.searchFieldOutlet frame]))];


     } else if ([itemIdent isEqual:PrintNoteToolbarItemIdentifier]) {           //leg20220615 - TopXNotes_Catalyst_2
        // Set the text label to be displayed in the toolbar and customization palette
         UIImage *printerImage = [UIImage imageNamed:@"printer.png"];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]
                                           initWithImage:printerImage
                                                   style:UIBarButtonItemStylePlain
                                                  target:notePadViewController
                                                  action:@selector(printNote:)];
         
         item = [NSToolbarItem itemWithItemIdentifier:PrintNoteToolbarItemIdentifier
                                        barButtonItem:barButtonItem];
         item.bordered = true;
         item.toolTip = @"Print Note";
         item.label = @"Print Note";
         item.paletteLabel = @"Print Note";

         toolbarItem = item;
     } else {
        // itemIdent refered to a toolbar item that is not provide or supported by us or cocoa
        // Returning nil will inform the toolbar this kind of item is not supported
        toolbarItem = nil;
    }
    return toolbarItem;
}

- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar {
    /* Required method.  Returns the ordered list of items to be shown in the
     toolbar by default. If during initialization, no overriding values are
     found in the user defaults, or if the user chooses to revert to the
     default items this set will be used. */

    return [NSArray arrayWithObjects:
            AddNoteToolbarItemIdentifier, NSToolbarSpaceItemIdentifier,
            AddGroupToolbarItemIdentifier, NSToolbarSpaceItemIdentifier,        //leg20220615 - TopXNotes_Catalyst_2
            DeleteItemToolbarItemIdentifier, NSToolbarFlexibleSpaceItemIdentifier,
            ShareItemToolbarItemIdentifier,
            nil];
}

- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar {
    /* Required method.  Returns the list of all allowed items by identifier.
     By default, the toolbar does not assume any items are allowed, even the
     separator.  So, every allowed item must be explicitly listed.  The set of
     allowed items is used to construct the customization palette.  The order
     of items does not necessarily guarantee the order of appearance in the
     palette.  At minimum, you should return the default item list.*/

    return [NSArray arrayWithObjects:
            AddNoteToolbarItemIdentifier,
            AddGroupToolbarItemIdentifier,                                      //leg20220615 - TopXNotes_Catalyst_2
            ShareItemToolbarItemIdentifier,
            SearchToolbarItemIdentifier,
            NSToolbarFlexibleSpaceItemIdentifier,
            NSToolbarSpaceItemIdentifier,
//            NSToolbarShowFontsItemIdentifier, NSToolbarShowColorsItemIdentifier, NSToolbarPrintItemIdentifier,
// Remove NSToolbar default items NSToolbarShowFontsItemIdentifier              //leg20220614 - TopXNotes_Catalyst_2
//  and NSToolbarShowColorsItemIdentifier until they can be hooked-up properly.
//            NSToolbarPrintItemIdentifier,
            PrintNoteToolbarItemIdentifier,                                     //leg20220615 - TopXNotes_Catalyst_2
            nil];
}

// Enable/Disable toolbar items.
- (BOOL)validateToolbarItem:(NSToolbarItem *)toolbarItem {
    /* NSToolbarItemValidation extends the standard validation idea by
     introducing this new method which is sent to validators for each visible
     standard NSToolbarItem with a valid target/action pair.  Note: This
     message is sent from NSToolbarItem's validate method, howevever validate
     will not send this message for items that have custom views. */
    BOOL enable = NO;
    if ([[toolbarItem itemIdentifier] isEqual:AddNoteToolbarItemIdentifier]) {
        enable = YES;
    } else if ([[toolbarItem itemIdentifier] isEqual:AddGroupToolbarItemIdentifier]) {  //leg20220615 - TopXNotes_Catalyst_2
        enable = YES;
    } else if ([[toolbarItem itemIdentifier] isEqual:PrintNoteToolbarItemIdentifier]) {  //leg20220615 - TopXNotes_Catalyst_2
        enable = YES;
    } else if ([[toolbarItem itemIdentifier] isEqual:DeleteItemToolbarItemIdentifier]) {
        enable = ([notePadViewController.tableView indexPathForSelectedRow] != nil);
    } else if ([[toolbarItem itemIdentifier] isEqual:ShareItemToolbarItemIdentifier]) {
//        enable = ([[_transactionController selectedObjects] count] > 0);
        enable = YES;
    } else if ([[toolbarItem itemIdentifier] isEqual:SearchToolbarItemIdentifier]) {
//        enable = YES;
        enable = NO;
    }
    return enable;
}




#endif
@end

