//
//  SceneDelegate.h
//  TopXNotes2
//
//  Created by Lewis Garrett on 4/13/21.
//  Copyright © 2021 Tropical Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

