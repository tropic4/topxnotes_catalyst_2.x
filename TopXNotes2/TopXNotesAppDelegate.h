//
//  TopXNotesAppDelegate.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/5/09.
//  Copyright Iota 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class SyncNotePad;
@class NotePadViewController;                                                   //leg20210527 - TopXNotes2
@class NoteRichTextViewController;                                              //leg20220413 - TopXNotes_Catalyst_2
@class CKRecord;                                                                //leg20220912 - TopXNotes_Catalyst_2

// Apple Sample CloudPhotos additions to app delegate.  --begin--               //leg20221010 - TopXNotes_Catalyst_2
@class APLCloudManager;

// handy macro to access APLAppDelegate's APLCloudManager object
#define CloudManager [(TopXNotesAppDelegate *)[[UIApplication sharedApplication] delegate] cloudManager]

//@interface APLAppDelegate : NSObject <UIApplicationDelegate>
//
//@property (nonatomic, strong) APLCloudManager *cloudManager;
//
//@end
// Apple Sample CloudPhotos additions to app delegate.  --end--                 //leg20221010 - TopXNotes_Catalyst_2


// Conditionalize interface.                                                    //leg20200113 - Catalyst
#if !TARGET_OS_UIKITFORMAC
    @interface TopXNotesAppDelegate : NSObject
#else
    @interface TopXNotesAppDelegate : UIResponder
#endif
                                                <UIApplicationDelegate,
                                                UITabBarControllerDelegate,
                                                UIActionSheetDelegate, NSNetServiceDelegate>    //leg20110523 - 1.0.4
{
	IBOutlet Model *model;														//leg20110523 - 1.0.4
	
	NSMutableDictionary *savedSettingsDictionary;								//leg20110523 - 1.0.4
    
    // Made variables public so can be accessed by SceneDelegate.               //leg20210503 - TopXNotes2
@public
    BOOL syncEnabled;                                                           //leg20110523 - 1.0.4
    BOOL firstBecameActive;                                                     //leg20110523 - 1.0.4
    SyncNotePad *syncNotePad;                                                   //leg20110523 - 1.0.4
}

@property (nonatomic, strong) IBOutlet SyncNotePad *syncNotePad;                //leg20110523 - 1.0.4
@property (nonatomic, strong) IBOutlet Model *model;                            //leg20110523 - 1.0.4

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, strong) IBOutlet NotePadViewController *notePadViewController;    //leg20210527 - TopXNotes2
@property (nonatomic, strong) IBOutlet NoteRichTextViewController *noteRichTextViewController;  //leg20220413 - TopXNotes_Catalyst_2
@property (nonatomic, strong) NSNumber *encryptionStatus;                       //leg20130205 - 1.3.0

// Apple Sample CloudPhotos additions to app delegate.                          //leg20221010 - TopXNotes_Catalyst_2
@property (nonatomic, strong) APLCloudManager *cloudManager;                    //leg20221010 - TopXNotes_Catalyst_2

- (BOOL)replaceNotepadFromCloudKitUpdate:(CKRecord *)record;                    //leg20220912 - TopXNotes_Catalyst_2

@end

