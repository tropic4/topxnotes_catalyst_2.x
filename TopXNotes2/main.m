//
//  main.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 4/13/21.
//  Copyright © 2021 Tropical Software, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TopXNotesAppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([TopXNotesAppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
