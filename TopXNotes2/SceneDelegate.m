//
//  SceneDelegate.m
//  TopXNotes2
//
//  Created by Lewis Garrett on 4/13/21.
//  Copyright © 2021 Tropical Software, Inc. All rights reserved.
//

#import "SceneDelegate.h"
#import "Constants.h"                                                           //leg20210430 - TopXNotes2
#import "TopXNotesAppDelegate.h"                                                //leg20210503 - TopXNotes2
#import "NotePadViewController.h"                                               //leg20210503 - TopXNotes2
#import "SyncNotePad.h"                                                         //leg20210503 - TopXNotes2

@interface SceneDelegate ()

@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions {
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

    // Prior to iOS 13 the window property was available in the app delegate.   //leg20210503 - TopXNotes2
    //  The window property is needed to get access to the tabBarController's
    //  view controllers.  If less than iOS 13 the following code is performed
    //  in TopXNotesAppDelegate.m -applicationDidFinishLaunching.
    if (IS_OS_13_OR_LATER) {
        // Set the default color of most controls.                              //leg20210430 - TopXNotes2
        self.window.tintColor = [Constants controlsColor];

        // Get addressablity back to notepad view so that we can refresh        //leg20210413 - TopXNotes2
        //  it when we need to.
        TopXNotesAppDelegate *appDelegate = (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
        NSArray *vcs = self.window.rootViewController.childViewControllers;
        UINavigationController *notesNavigationVC  = [vcs objectAtIndex:0];
        NotePadViewController *notePadViewController = (NotePadViewController *)notesNavigationVC.topViewController;

        // Configure syncing.
        appDelegate->firstBecameActive = YES;
        appDelegate.syncNotePad = [[SyncNotePad alloc] init];
        appDelegate.syncNotePad.model = appDelegate.model;
        appDelegate.syncNotePad.notePadViewController = notePadViewController;  //leg20210504 - TopXNotes2

        if (appDelegate->syncEnabled)
            [appDelegate.syncNotePad toggleSyncing:YES];
        else
            [appDelegate.syncNotePad toggleSyncing:NO];
    }
}


- (void)sceneDidDisconnect:(UIScene *)scene {
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene {
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene {
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene {
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


@end
